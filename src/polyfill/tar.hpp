#ifndef POLYFILL_TAR_HPP_
#define POLYFILL_TAR_HPP_

#include <string>

namespace tarball {
	extern bool create( const std::string &outPath, const std::string &dir );
	extern bool extract( const std::string &tarPath, const std::string &outDir );

#if defined(__linux__) || defined(__unix__)
	constexpr bool toolInstalled() { return true; }
#else
	extern bool toolInstalled();
#endif
}

#endif /* POLYFILL_TAR_HPP_ */
