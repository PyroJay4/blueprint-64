#include "src/polyfill/exec.hpp"

#ifdef _WIN32

#include "src/polyfill/window-slit.hpp"

bool Command::execSync( const std::string &cmd ) {
	return Windows::execSync( cmd );
}

bool Command::execSync(
	const std::string &cmd,
	const std::string &workingDirectory,
	std::string &output
) {
	return Windows::execSync( cmd, workingDirectory, output );
}

bool Command::execDetached( const std::string &cmd ) {
	return Windows::execDetached( cmd );
}

#else

#include <cstdlib>
#include <cstdio>
#include "src/types.hpp"
#include "src/core/util.hpp"

bool Command::execSync( const string &cmd ) {
	return std::system( cmd.c_str() ) == 0;
}

bool Command::execSync(
	const std::string &cmd,
	const std::string &workingDirectory,
	std::string &output
) {
	const std::string fullCmd = "cd "s + quoteAndEscape( workingDirectory ) + "; " + cmd;
	output = "";

	FILE *proc = popen( fullCmd.c_str(), "r" );
	if( proc == nullptr ) return false;

	char buffer[1024];
	while( fgets( buffer, 1024, proc ) != nullptr ) {
		output += buffer;
	}

	return pclose( proc ) == 0;
}

bool Command::execDetached( const string &cmd ) {
	const string fullCmd = "setsid "s + cmd + " &";
	return std::system( fullCmd.c_str() ) == 0;
}

#endif
