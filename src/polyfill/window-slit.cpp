#include "src/polyfill/window-slit.hpp"

#ifdef _WIN32
#include <exception>
#include <windows.h>
#include <knownfolders.h>
#include <shlobj.h>
#include <winnt.h>
#include <string>
#include <locale>
#include <codecvt>
#include "src/polyfill/filesystem.hpp"

static inline std::string toUtf8( const wchar_t *wtsr ) {
	std::wstring_convert<std::codecvt_utf8_utf16<wchar_t>,wchar_t> converter;
	return converter.to_bytes( std::wstring( wtsr ) );
}

static std::string _getHomeDirectory() {
	wchar_t *buffer;
	if( SHGetKnownFolderPath(
		FOLDERID_Profile,
		KF_FLAG_NO_ALIAS | KF_FLAG_DEFAULT_PATH | KF_FLAG_NOT_PARENT_RELATIVE,
		nullptr,
		&buffer
	) == S_OK ) {
		const std::string result = toUtf8( buffer );
		CoTaskMemFree( buffer );
		return result;
	}

	if( buffer != nullptr ) {
		CoTaskMemFree( buffer );
	}

	throw std::runtime_error( "Failed to get home directory location from Windows." );
}

static std::string getDataDirectory(
	REFKNOWNFOLDERID folderId,
	const char *defaultRelativePath
) {
	wchar_t *buffer;
	if( SHGetKnownFolderPath(
		folderId,
		KF_FLAG_CREATE | KF_FLAG_NO_ALIAS | KF_FLAG_DONT_VERIFY | KF_FLAG_DEFAULT_PATH | KF_FLAG_NOT_PARENT_RELATIVE,
		nullptr,
		&buffer
	) == S_OK ) {
		const std::string result = toUtf8( buffer );
		CoTaskMemFree( buffer );
		return result;
	}

	if( buffer != nullptr ) {
		CoTaskMemFree( buffer );
	}

	return (std::filesystem::path( _getHomeDirectory() ) / defaultRelativePath).string();
}

std::string Windows::getKnownFolderPath( Windows::KnownFolder folder ) {
	switch( folder ) {
		case Windows::KnownFolder::Home:
			return _getHomeDirectory();
		case Windows::KnownFolder::LocalAppData:
			return getDataDirectory( FOLDERID_LocalAppData, "AppData\\Local" );
		case Windows::KnownFolder::RoamingAppData:
			return getDataDirectory( FOLDERID_RoamingAppData, "AppData\\Roaming" );
		case Windows::KnownFolder::Temp:
#ifdef _WIN64
			return std::filesystem::temp_directory_path().string();
#else
			return (std::filesystem::path( getDataDirectory( FOLDERID_LocalAppData, "AppData\\Local" ) ) / "Temp").string();
#endif
		default:
			throw std::domain_error( "Invalid folder" );
	}
}

static inline void clearFileAttributes( const std::string &path ) {
	SetFileAttributesA( path.c_str(), FILE_ATTRIBUTE_NORMAL );
}

void Windows::clearFileAttributesRecursive( const std::string &path ) {
	namespace fs = std::filesystem;

	const fs::path rootPath = fs::path( path );
	if( !fs::exists( rootPath ) ) return;

	clearFileAttributes( path );
	if( fs::status( path ).type() != fs::file_type::directory ) {
		return;
	}

	for( const auto &f : fs::recursive_directory_iterator( path ) ) {
		clearFileAttributes( f.path().string() );
	}
}

std::string Windows::getProgramPath() {
	wchar_t buffer[MAX_PATH];
	if( GetModuleFileName( nullptr, buffer, MAX_PATH - 1 ) <= 0 ) {
		throw std::runtime_error( "Failed to get program path." );
	}
	return toUtf8( buffer );
}

bool Windows::execSync( const std::string &cmd ) {
	_STARTUPINFOA si;
	PROCESS_INFORMATION pi;

	ZeroMemory( &si, sizeof(si) );
	ZeroMemory( &pi, sizeof(pi) );
	si.cb = sizeof(si);

	char lpCommandLine[cmd.length() + 1];
	cmd.copy( lpCommandLine, cmd.length() );
	lpCommandLine[cmd.length()] = '\0';

	if( !CreateProcessA(
		nullptr,
		lpCommandLine,
		nullptr,
		nullptr,
		true,
		CREATE_NO_WINDOW,
		nullptr,
		nullptr,
		&si,
		&pi
	)) return false;

	DWORD exitCode;
	WaitForSingleObject( pi.hProcess, INFINITE );
	bool success = GetExitCodeProcess( pi.hProcess, &exitCode ) && (exitCode == 0);
	CloseHandle( pi.hProcess );
	CloseHandle( pi.hThread );
	return success;
}

bool Windows::execSync( const std::string &cmd, const std::string &workingDirectory, std::string &output ) {
	_STARTUPINFOA si;
	PROCESS_INFORMATION pi;

	HANDLE stdOutRead = nullptr;
	HANDLE stdOutWrite = nullptr;

	SECURITY_ATTRIBUTES security;
	security.nLength = sizeof( SECURITY_ATTRIBUTES );
	security.bInheritHandle = true;
	security.lpSecurityDescriptor = nullptr;

	CreatePipe( &stdOutRead, &stdOutWrite, &security, 1024 );
	SetHandleInformation( stdOutRead, HANDLE_FLAG_INHERIT, 0 );

	ZeroMemory( &si, sizeof(si) );
	ZeroMemory( &pi, sizeof(pi) );
	si.cb = sizeof(si);
	si.hStdOutput = stdOutWrite;
	si.dwFlags |= STARTF_USESTDHANDLES;

	char lpCommandLine[cmd.length() + 1];
	cmd.copy( lpCommandLine, cmd.length() );
	lpCommandLine[cmd.length()] = '\0';

	char lpCurrentDirectory[workingDirectory.length() + 1];
	workingDirectory.copy( lpCurrentDirectory, workingDirectory.length() );
	lpCurrentDirectory[workingDirectory.length()] = '\0';

	output = "";
	if( !CreateProcessA(
		nullptr,
		lpCommandLine,
		nullptr,
		nullptr,
		true,
		CREATE_NO_WINDOW,
		nullptr,
		lpCurrentDirectory,
		&si,
		&pi
	)) return false;

	CloseHandle( stdOutWrite );

	DWORD bytesRead = 0;
	char buffer[1025];

	while( ReadFile( stdOutRead, buffer, 1024, &bytesRead, nullptr ) && bytesRead > 0 ) {
		buffer[bytesRead] = '\0';
		output += buffer;
	}

	DWORD exitCode;
	CloseHandle( stdOutRead );
	bool success = GetExitCodeProcess( pi.hProcess, &exitCode ) && (exitCode == 0);
	CloseHandle( pi.hProcess );
	CloseHandle( pi.hThread );

	return success;
}

bool Windows::execDetached( const std::string &cmd ) {
	static _STARTUPINFOA si;
	static PROCESS_INFORMATION pi;

	ZeroMemory( &si, sizeof(si) );
	ZeroMemory( &pi, sizeof(pi) );
	si.cb = sizeof(si);

	char lpCommandLine[cmd.length() + 1];
	cmd.copy( lpCommandLine, cmd.length() );
	lpCommandLine[cmd.length()] = '\0';

	return CreateProcessA(
		nullptr,
		lpCommandLine,
		nullptr,
		nullptr,
		true,
		DETACHED_PROCESS,
		nullptr,
		nullptr,
		&si,
		&pi
	);
}

bool Windows::isAlreadyRunning() {
	CreateMutexA( nullptr, true, "Global_blueprint64" );
	return GetLastError() == ERROR_ALREADY_EXISTS;
}

#endif
