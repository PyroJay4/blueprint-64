#ifndef SRC_POLYFILL_RANDOM_HPP_
#define SRC_POLYFILL_RANDOM_HPP_

#include <cstddef>

namespace Pseudorandom {

	extern void getBytes( unsigned char* dest, size_t num );

	inline unsigned char getByte() {
		unsigned char b;
		getBytes( &b, 1 );
		return b;
	}

	inline unsigned int getUInt() {
		unsigned int i;
		getBytes( (unsigned char*)&i, sizeof(unsigned int) );
		return i;
	}

}

#endif /* SRC_POLYFILL_RANDOM_HPP_ */
