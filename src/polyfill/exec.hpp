#ifndef SRC_POLYFILL_EXEC_HPP_
#define SRC_POLYFILL_EXEC_HPP_

#include <string>

namespace Command {

	extern bool execSync( const std::string &cmd );
	extern bool execSync( const std::string &cmd, const std::string &workingDirectory, std::string &output );
	extern bool execDetached( const std::string &cmd );

}



#endif /* SRC_POLYFILL_EXEC_HPP_ */
