#include "src/polyfill/appdata.hpp"

#ifdef _WIN32

#include "src/polyfill/window-slit.hpp"
#include "src/polyfill/filesystem.hpp"

namespace fs = std::filesystem;

inline static fs::path getLocalAppDataPath() {
	fs::path dataPath( Windows::getKnownFolderPath( Windows::KnownFolder::LocalAppData ) );
	fs::create_directories( dataPath );
	return dataPath;
}

inline static const fs::path &baseDir() {
	static const fs::path dir = getLocalAppDataPath() / "bowsers-blueprints";
	return dir;
}

const std::string AppData::configDir() {
	return (baseDir() / "config").string();
}

const std::string AppData::cacheDir() {
	return (baseDir() / "cache").string();
}

const std::string AppData::dataDir() {
	return (baseDir() / "data").string();
}

#else

#include <cstdlib>

using namespace std;

inline static string xdg( const char *varName, const string &defaultRelative ) {
	char *xdgVal = getenv( varName );
	if( xdgVal != nullptr && xdgVal[0] != '\0' ) {
		return string( xdgVal );
	}
	return string( getenv( "HOME" ) ) + "/" + defaultRelative;
}

const string AppData::configDir() {
	static const string dir = xdg( "XDG_CONFIG_HOME", ".config" ) + "/bowsers-blueprints";
	return dir;
}

const string AppData::cacheDir() {
	static const string dir = xdg( "XDG_CACHE_HOME", ".cache" ) + "/bowsers-blueprints";
	return dir;
}

const string AppData::dataDir() {
	static const string dir = xdg( "XDG_DATA_HOME", ".local/share" ) + "/bowsers-blueprints";
	return dir;
}

#endif
