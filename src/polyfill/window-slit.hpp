#ifndef SRC_POLYFILL_WINDOW_SLIT_HPP_
#define SRC_POLYFILL_WINDOW_SLIT_HPP_

#ifdef _WIN32

#include <string>

/* The Windows API includes a bunch of dumb poorly named macros that cause collisions with
 * other code. To avoid this, I only include the Windows API header in one file, then expose
 * only the methods I need through the header.
 *
 * Windows is dumb.
 */
namespace Windows {

	enum class KnownFolder : unsigned char {
		LocalAppData,
		RoamingAppData,
		Home,
		Temp
	};

	extern std::string getKnownFolderPath( KnownFolder folder );
	extern std::string getProgramPath();

	extern void clearFileAttributesRecursive( const std::string &path );

	extern bool execSync( const std::string &cmd );
	extern bool execSync( const std::string &cmd, const std::string &workingDirectory, std::string &output );
	extern bool execDetached( const std::string &cmd );

	extern bool isAlreadyRunning();

}

#endif

#endif /* SRC_POLYFILL_WINDOW_SLIT_HPP_ */
