#ifndef POLYFILL_APPDATA_HPP_
#define POLYFILL_APPDATA_HPP_

#include <string>

namespace AppData {
	const std::string configDir();
	const std::string cacheDir();
	const std::string dataDir();
}


#endif /* POLYFILL_APPDATA_HPP_ */
