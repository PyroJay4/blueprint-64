#ifndef POLYFILL_FILESYSTEM_HPP_
#define POLYFILL_FILESYSTEM_HPP_

#include <string>
#if __GNUC__ >= 8
#include <filesystem>
#else
#include <experimental/filesystem>
namespace std::filesystem {
	using namespace std::experimental::filesystem;
}
#endif

extern std::string createTemporaryDirectory();
extern std::filesystem::path currentProgramPath();

#ifdef _WIN32
	extern std::filesystem::path getHomeDirectory();
	extern void clearReadOnlyFlagOnWindows( const std::string &path );
	inline void clearReadOnlyFlagOnWindows( const std::filesystem::path &path ) {
		clearReadOnlyFlagOnWindows( path.string() );
	}
#else
	inline std::filesystem::path getHomeDirectory() { return std::filesystem::path( "~" ); }
	inline void clearReadOnlyFlagOnWindows( [[maybe_unused]] const std::string &path ) {}
	inline void clearReadOnlyFlagOnWindows( [[maybe_unused]] const std::filesystem::path &path ) {}
#endif

namespace std::filesystem {

	// More dumb Windows workarounds

	inline void forceDelete( const path &filename ) {
		clearReadOnlyFlagOnWindows( filename );
		remove( filename );
	}

	inline void forceDeleteRecursive( const path &filename ) {
		clearReadOnlyFlagOnWindows( filename );
		remove_all( filename );
	}

	inline bool tryDelete( const path &filename ) {
		clearReadOnlyFlagOnWindows( filename );
		std::error_code error;
		remove( filename, error );
		return !error;
	}

	inline bool tryDeleteRecursive( const path &filename ) {
		clearReadOnlyFlagOnWindows( filename );
		std::error_code error;
		remove_all( filename, error );
		return !error;
	}

#ifdef _WIN32
	extern bool tryCopyFileOverwrite( const path &source, const path &dest );
#else
	inline bool tryCopyFileOverwrite( const path &source, const path &dest ) {
		std::error_code error;
		return copy_file( source, dest, copy_options::overwrite_existing, error ) && !error;
	}
#endif

}

#endif /* POLYFILL_FILESYSTEM_HPP_ */
