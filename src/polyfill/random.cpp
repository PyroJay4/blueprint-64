#include "src/polyfill/random.hpp"

#ifdef _WIN32

	#include <random>
	#include <chrono>
	#include <cstring>
	#include "src/types.hpp"

	typedef std::uint_fast64_t fast64;

	void Pseudorandom::getBytes( unsigned char* dest, size_t num ) {
		static std::discard_block_engine<std::ranlux48_base, 389, 11> randomizer( (fast64)std::chrono::system_clock::now().time_since_epoch().count() );
		static unsigned char buffer[6];
		static unsigned char bufferHead = 6;

		size_t i = 0;
		while( num >= i + 6 ) {
			const fast64 f = randomizer();
			const uint a = f & 0xFFFFFFFFu;
			const ushort b = (f >> 32) & 0xFFFF;
			std::memcpy( &dest[i], &a, 4 );
			std::memcpy( &dest[i+4], &b, 2 );
			i += 6;
		}

		while( bufferHead < 6 && i < num ) {
			dest[i++] = buffer[bufferHead++];
		}

		if( bufferHead >= 6 ) {
			const fast64 f = randomizer();
			const uint a = f & 0xFFFFFFFFu;
			const ushort b = (f >> 32) & 0xFFFF;
			std::memcpy( buffer, &a, 4 );
			std::memcpy( &buffer[4], &b, 2 );
			bufferHead = 0;
		}

		while( i < num ) {
			dest[i++] = buffer[bufferHead++];
		}
	}

#else

	#include <fstream>

	void Pseudorandom::getBytes( unsigned char* dest, size_t num ) {
		static std::ifstream randomStream( "/dev/urandom" );
		randomStream.read( (char*)dest, num );
	}

#endif
