#include "src/polyfill/tar.hpp"

#include <cstdlib>
#include "src/core/util.hpp"

#if defined(__linux__) || defined(__unix__)
	using std::string;

	bool tarball::create( const string &outPath, const string &dir ) {
		const string cmd = "tar -c -z -f " + quoteAndEscape( outPath ) + " -C " + quoteAndEscape( dir ) + " .";
		return std::system( cmd.c_str() ) == 0;
	}

	bool tarball::extract( const string &tarPath, const string &outDir ) {
		const string cmd = "tar -x -z -f " + quoteAndEscape( tarPath ) + " -C " + quoteAndEscape( outDir );
		return std::system( cmd.c_str() ) == 0;
	}

#else
	#include "src/core/storage.hpp"
	#include "src/polyfill/filesystem.hpp"
	#include "src/polyfill/window-slit.hpp"

	namespace fs = std::filesystem;

	static inline string get7zCmd() {
		const string &path = AppPreferences::current().sevenZipPath;
		if( path.empty() ) return "7z";
		return quoteAndEscape( path );
	}

	bool tarball::create( const string &outPath, const string &dir ) {
		const string zipExe = get7zCmd();

		const string tempDir = createTemporaryDirectory();
		const string tempTar = (fs::path( tempDir ) / "temp.tar").string();

		if( !Windows::execSync( zipExe + " a " + quoteAndEscape( tempTar ) + " " + quoteAndEscape( (fs::path( dir ) / "*").string() ) + " -ttar -r" ) ) {
			fs::tryDeleteRecursive( fs::path( tempDir ) );
			return false;
		}

		const bool success = fs::tryDelete( fs::path( outPath ) ) && Windows::execSync( zipExe + " a " + quoteAndEscape( outPath ) + " " + quoteAndEscape( tempTar ) + " -tgzip" );
		fs::tryDeleteRecursive( fs::path( tempDir ) );
		return success;
	}

	bool tarball::extract( const string &tarPath, const string &outDir ) {
		const string unzipExe = get7zCmd();

		const string tempDir = createTemporaryDirectory();
		if( !Windows::execSync( unzipExe + " x " + quoteAndEscape( tarPath ) + " -tgzip -aoa -y -o" + quoteAndEscape( tempDir ) ) ) {
			fs::tryDeleteRecursive( fs::path( tempDir ) );
			return false;
		}

		// Get the only file in the directory
		clearReadOnlyFlagOnWindows( tempDir );
		for( const auto &tarFile : fs::directory_iterator( fs::path( tempDir ) ) ) {
			if( !Windows::execSync( unzipExe + " x " + quoteAndEscape( tarFile.path().string() ) + " -ttar -aoa -y -o" + quoteAndEscape( outDir ) ) ) {
				fs::tryDeleteRecursive( fs::path( tempDir ) );
				return false;
			}

			fs::tryDeleteRecursive( fs::path( tempDir ) );
			return true;
		}

		// No tarchive was extracted
		fs::tryDeleteRecursive( fs::path( tempDir ) );
		return false;
	}

	bool tarball::toolInstalled() {
		const string _7z = AppPreferences::current().sevenZipPath;
		if( _7z.empty() ) {
	#ifdef _WIN32
			return std::system( "where 7z" ) == 0;
	#else
			return std::system( "which 7z" ) == 0;
	#endif
		} else {
			return fs::exists( fs::path( _7z ) );
		}
	}
#endif
