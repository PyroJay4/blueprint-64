#include "src/ui/util.hpp"

#include <QFile>
#ifdef _WIN32
#include "src/polyfill/filesystem.hpp"
#endif

using namespace std;

static constexpr size_t BUFFER_SIZE = 1024;

class QFileStreamBuffer : public streambuf {

	private:
	QFile m_file;
	char m_buffer[BUFFER_SIZE];

	public:
	QFileStreamBuffer( const char *resourcePath ) :
		streambuf(),
		m_file( QFile( resourcePath ) )
	{
		m_file.open( QIODevice::ReadOnly );
	}

	~QFileStreamBuffer() {
		m_file.close();
	}

	protected:
	int underflow() override {
		if( m_file.atEnd() ) {
			return char_traits<char>::eof();
		}

		qint64 bytesRead = m_file.read( m_buffer, BUFFER_SIZE );
		if( bytesRead < 0 ) {
			return char_traits<char>::eof();
		}

		setg( m_buffer, m_buffer, &m_buffer[bytesRead] );
		return m_buffer[0];
	}

	pos_type seekpos( pos_type pos, [[maybe_unused]] ios_base::openmode which ) override {
		m_file.seek( pos );
		underflow();
		return m_file.pos();
	}

	pos_type seekoff( off_type off, ios_base::seekdir dir, ios_base::openmode which ) override {
		qint64 seekTo = 0;
		switch( dir ) {
			case ios_base::beg: seekTo = off; break;
			case ios_base::end: seekTo = m_file.size() + off; break;
			case ios_base::cur: seekTo = m_file.pos() + off; break;
			default: return -1;
		}

		if( seekTo < 0 || seekTo > m_file.size() ) {
			return -1;
		}

		return seekpos( seekTo, which );
	}

};

QResourceStream::QResourceStream( const char *resourcePath )
	: std::istream( new QFileStreamBuffer( resourcePath ) ) {}

void UiUtil::clearLayout( QLayout *layout ) {
	while( QLayoutItem *item = layout->takeAt( 0 ) ) {
		UiUtil::freeLayoutItem( item );
	}
}

void UiUtil::deleteItemAt( QLayout *layout, int index ) {
	UiUtil::freeLayoutItem( layout->takeAt( index ) );
}

void UiUtil::deleteItemAt( QGridLayout *layout, int row, int col ) {
	QLayoutItem *item = layout->itemAtPosition( row, col );
	layout->removeItem( item );
	UiUtil::freeLayoutItem( item );
}

void UiUtil::freeLayoutItem( QLayoutItem *item ) {
	if( item->layout() ) {
		UiUtil::freeLayoutItem( item->layout() );
	}

	if( item->widget() ) {
		item->widget()->deleteLater();
	}

	delete item;
}

#ifdef _WIN32
QString UiUtil::toNativePath( const QString &posixPath ) {
	if( posixPath.isNull() || posixPath.isEmpty() ) return posixPath;
	return QString( std::filesystem::path( posixPath.toStdString() ).make_preferred().string().c_str() );
}
#endif
