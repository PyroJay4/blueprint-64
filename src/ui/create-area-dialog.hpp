#ifndef SRC_UI_CREATE_AREA_DIALOG_HPP_
#define SRC_UI_CREATE_AREA_DIALOG_HPP_

#include <QDialog>
#include "src/types.hpp"
#include "src/core/enums.hpp"

namespace Ui {
	class CreateAreaDialog;
}

class CreateAreaDialog : public QDialog {
	Q_OBJECT

	private:
	Ui::CreateAreaDialog *m_ui;

	public:
	explicit CreateAreaDialog( QWidget *parent = nullptr );
	~CreateAreaDialog();

	void setLevel( LevelId levelId );
	ubyte areaIndex() const;

};

#endif /* SRC_UI_CREATE_AREA_DIALOG_HPP_ */
