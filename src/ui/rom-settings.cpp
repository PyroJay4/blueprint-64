#include "src/ui/rom-settings.hpp"
#include "ui_rom-settings.h"

#include <QFileDialog>
#include <QToolButton>
#include <QMessageBox>
#include <cstdio>
#include "src/core/blueprint.hpp"
#include "src/core/enums.hpp"
#include "src/core/ending.hpp"
#include "src/polyfill/filesystem.hpp"
#include "src/ui/util.hpp"

RomSettingsWidget::RomSettingsWidget( QWidget *parent ) :
	QWidget( parent ),
	m_ui( new Ui::RomSettingsWidget ),
	m_ignoreUpdates( true )
{
	m_ui->setupUi( this );
	FORWARD_SIGNAL( m_ui->editTweaksButton, this, clicked(), editTweaks() );
	FORWARD_SIGNAL( m_ui->editDialogButton, this, clicked(), editDialog() );
	FORWARD_SIGNAL( m_ui->editMusicButton, this, clicked(), editMusic() );
	FORWARD_SIGNAL( m_ui->editBackgroundsButton, this, clicked(), editBackgrounds() );
	FORWARD_SIGNAL( m_ui->editModulesButton, this, clicked(), editModules() );
	FORWARD_SIGNAL( m_ui->editActionsButton, this, clicked(), editMarioActions() );
	for( LevelId levelId : Enum::values<LevelId>() ) {
		m_ui->pauseWarpLevelSelect->addItem( Enum::toString( levelId ), (uint)levelId );
		if( levelId != LevelId::EndScreen ) {
			m_ui->startingLevelSelect->addItem( Enum::toString( levelId ), (uint)levelId );
		}
	}
	// Enable the clear button on the ROM select even though its otherwise read-only
	QToolButton *unlinkRomButton = m_ui->targetRomField->findChild<QToolButton*>();
	if( unlinkRomButton != nullptr ) {
		unlinkRomButton->setEnabled( true );
		QObject::connect( unlinkRomButton, SIGNAL(clicked(bool)), this, SLOT(updateData()) );
		FORWARD_SIGNAL( unlinkRomButton, this, clicked(), romTargetChanged() );
	}
	m_ignoreUpdates = false;

	connect( &m_createLevelDialog, SIGNAL(levelCreated(LevelId)), this, SLOT(levelAdded(LevelId)) );
	connect( m_ui->importedLevelsList, &SimpleListWidget::itemMove, this, [=](){
		m_moveLevelDialog.open( (LevelId)m_ui->importedLevelsList->currentData<uint>() );
	});
	connect( &m_moveLevelDialog, SIGNAL(accepted()), this, SLOT(refreshLevelList()) );
}

RomSettingsWidget::~RomSettingsWidget() {
	delete m_ui;
}

static void addLevelToList( QListWidget *list, LevelId levelId ) {
	assert( Blueprint::current()->hasLevel( levelId ) );
	char hexId[6];
	std::sprintf( hexId, "0x%02X ", (ubyte)levelId );
	hexId[5] = '\0';
	const string idAndName = string() + hexId + Blueprint::current()->levels().find( levelId )->second.name;
	list->addItem( idAndName.c_str() );
	list->item( list->count() - 1 )->setData( Qt::UserRole, QVariant::fromValue<uint>( (uint)levelId ) );
}

void RomSettingsWidget::refreshLevelList() {
	m_ui->importedLevelsList->clear();
	for( const auto &i : Blueprint::current()->levels() ) {
		addLevelToList( m_ui->importedLevelsList, i.first );
	}
	m_ui->importedLevelsList->clearSelection();
}

void RomSettingsWidget::blueprintChanged() {
	m_ignoreUpdates = true;
	const Blueprint *blueprint = Blueprint::current();
	m_ui->targetRomField->setText( blueprint->isCollab() ? blueprint->targetRomName().c_str() : blueprint->targetRomPath().string().c_str() );

	m_ui->browseButton->setVisible( !blueprint->isCollab() );
	m_ui->targetRomField->setReadOnly( !blueprint->isCollab() );
	m_ui->modeSwitchButton->setText( blueprint->isCollab() ? "Switch to Single File Mode" : "Switch to Collaboration Mode" );
	m_ui->modeSwitchButton->setIcon( QIcon::fromTheme( blueprint->isCollab() ? "user-identities" : "system-users" ) );

	const RomSpecs &romSpecs = blueprint->romSpecs();
	m_ui->romNameField->setText( romSpecs.internalName.c_str() );
	m_ui->levelBoundariesSelect->setCurrentIndex( (int)romSpecs.levelBoundaries );
	m_ui->startingLevelSelect->setCurrentText( Enum::toString( romSpecs.startingLevel ) );
	m_ui->pauseWarpLevelSelect->setCurrentText( Enum::toString( romSpecs.pauseWarpLevel ));
	m_ui->pauseWarpAreaSelect->setValue( (int)romSpecs.pauseWarpArea );
	m_ui->pauseWarpIdSelect->setValue( (int)romSpecs.pauseWarpId );
	m_ui->disableChecksumCheckbox->setChecked( !romSpecs.enableChecksum );
	m_ui->newLevelButton->setEnabled( blueprint->levels().size() < 30 );
	refreshLevelList();
	m_ui->editLevelButton->setEnabled( false );
	m_ui->deleteLevelButton->setEnabled( false );

	m_ignoreUpdates = false;
}

void RomSettingsWidget::updateData() {
	if( m_ignoreUpdates ) return;
	RomSpecs &rom = Blueprint::currentEditable()->romSpecs();
	rom.internalName = m_ui->romNameField->text().toStdString();
	rom.levelBoundaries = (LevelBoundarySize)m_ui->levelBoundariesSelect->currentIndex();
	rom.startingLevel = (LevelId)m_ui->startingLevelSelect->currentData().toUInt();
	rom.pauseWarpLevel = (LevelId)m_ui->pauseWarpLevelSelect->currentData().toUInt();
	rom.pauseWarpArea = (ubyte)m_ui->pauseWarpAreaSelect->value();
	rom.pauseWarpId = (ubyte)m_ui->pauseWarpIdSelect->value();
	rom.enableChecksum = !m_ui->disableChecksumCheckbox->isChecked();
	Blueprint::currentEditable()->setTargetRom( std::filesystem::path( m_ui->targetRomField->text().toStdString() ) );
}

void RomSettingsWidget::browseForRom() {
	QString targetRom = UiUtil::toNativePath( QFileDialog::getSaveFileName( this, "Select a ROM", QString(), "N64 ROMs (*.z64);;All Files (*)" ) );
	if( targetRom.isNull() || targetRom.isEmpty() ) return;
	if( !targetRom.endsWith( ".z64" ) ) {
		targetRom += ".z64";
	}

	m_ui->targetRomField->setText( targetRom );
	Blueprint::currentEditable()->setTargetRom( std::filesystem::path( targetRom.toStdString() ) );
	emit romTargetChanged();
}

void RomSettingsWidget::addLevel() {
	m_createLevelDialog.show();
}

void RomSettingsWidget::editSelectedLevel() {
	emit editLevel( (LevelId)m_ui->importedLevelsList->currentItem()->data( Qt::UserRole ).toUInt() );
}

void RomSettingsWidget::deleteSelectedLevel() {
	LevelId level = (LevelId)m_ui->importedLevelsList->currentItem()->data( Qt::UserRole ).toUInt();
	assert( Blueprint::current()->hasLevel( level ) );
	if( QMessageBox::question( this, "Delete Level?", "Are you sure you want to delete this level? This operation cannot be undone." ) == QMessageBox::Yes ) {
		Blueprint::currentEditable()->levels().erase( level );
		delete m_ui->importedLevelsList->takeItem( m_ui->importedLevelsList->currentRow() );
		m_ui->importedLevelsList->clearSelection();
		m_ui->editLevelButton->setEnabled( false );
		m_ui->deleteLevelButton->setEnabled( false );
		std::filesystem::forceDeleteRecursive( Blueprint::current()->getLevelPath( level ) );
		for( ubyte i = 0; i < 8; i++ ) {
			Blueprint::currentEditable()->areas().erase( AREA_ID( level, i ) );
			std::filesystem::forceDelete( Blueprint::current()->getAreaActorsPath( level, i ) );
		}
	}
}

void RomSettingsWidget::levelSelected() {
	m_ui->editLevelButton->setEnabled( true );
	m_ui->deleteLevelButton->setEnabled( true );
}

void RomSettingsWidget::levelAdded( LevelId levelId ) {
	addLevelToList( m_ui->importedLevelsList, levelId );
	emit editLevel( levelId );
}

void RomSettingsWidget::editEndScreen() {
	if( EndScreen::hasCustomImage() ) {
		m_endScreenDialog.open();
		return;
	}

	QString imgFilePath = UiUtil::toNativePath( QFileDialog::getOpenFileName( this, "Select an Image (320x240)", QString(), "Images (*.png *.bmp *.gif *.jpg *.jpeg *.xpm *.xbm *.pbm *.pgm *.ppm)" ) );
	if( imgFilePath.isNull() ) {
		return;
	}
	//TODO: choose resize method
	if( EndScreen::saveImage( imgFilePath.toStdString(), EndScreen::ResizeMethod::Stretch ) ) {
		QMessageBox::information( this, "End Screen Imported", "End Screen was imported successfully.", QMessageBox::Ok );
	} else {
		QMessageBox::critical( this, "Importer Machine Broke", "Failed to import end screen. Is it a valid image format?", QMessageBox::Ok );
	}

}

static const char *SWITCH_TO_COLLAB_MESSAGE = ""
	"In collaboration mode, instead of saving your blueprint to a single "
	"compressed file, the components of your blueprint will be saved to a "
	"folder, making it more suitable for use with version control such as Git.";

static const char *SWITCH_TO_NORMAL_MESSAGE = ""
	"Your blueprint will be switched to saving to a single file. Your "
	"collaboration folder will not be deleted, but it will no longer be "
	"updated.";

void RomSettingsWidget::switchMode() {
	if( QMessageBox::information(
		this,
		"Switch Mode?",
		Blueprint::current()->isCollab() ? SWITCH_TO_NORMAL_MESSAGE : SWITCH_TO_COLLAB_MESSAGE,
		QMessageBox::Ok | QMessageBox::Cancel
	) != QMessageBox::Ok ) return;

	QString savePath;
	if( Blueprint::current()->isCollab() ) {
		savePath = QFileDialog::getSaveFileName(
			this,
			"Save As",
			QString(),
			"Blueprint (.bpp)"
		);
		if( !savePath.isNull() && !savePath.endsWith( ".bbp" ) ) {
			savePath += ".bbp";
		}
	} else {
		savePath = QFileDialog::getExistingDirectory(
			this,
			"Select Collab Directory",
			QString(),
			QFileDialog::ShowDirsOnly
		);
	}

	if( savePath.isNull() ) return;
	savePath = UiUtil::toNativePath( savePath );

	try {
		Blueprint::currentEditable()->switchMode( savePath.toStdString() );
	} catch( ... ) {
		QMessageBox::critical( this, "File Error", "Failed to create files. Do you have write permissions?" );
	}
	blueprintChanged();
}

void RomSettingsWidget::romPathChanged() {
	if( !Blueprint::current()->isCollab() ) return;
	//TODO: restrict more characters on Windows
	const QString newName = m_ui->targetRomField->text().replace( "/", "" );
	m_ui->targetRomField->setText( newName );
	Blueprint::currentEditable()->setTargetRom( std::filesystem::path( newName.toStdString() ) );
}
