#include "src/ui/preferences.hpp"
#include "ui_preferences.h"

#include <QStyleFactory>

PreferencesDialog::PreferencesDialog( QWidget *parent ) :
	QDialog( parent ),
	m_ui( new Ui::PreferencesDialog )
{
	m_ui->setupUi( this );
}

PreferencesDialog::~PreferencesDialog() {
	delete m_ui;
}

void PreferencesDialog::showEvent( QShowEvent *event ) {
	m_ui->themeSelect->clear();
	m_ui->themeSelect->addItem( "System Default" );
	m_ui->themeSelect->addItems( QStyleFactory::keys() );
	m_ui->themeSelect->setCurrentIndex( 0 );

	const AppPreferences &data = AppPreferences::current();
	m_ui->openOnStartCheckbox->setChecked( data.openOnStart );
	m_ui->nativeMenuCheckbox->setChecked( data.nativeMenuBar );
	m_ui->importScaleSpinner->setValue( data.defaultScale );
	m_ui->retroarchTab->load();
	m_ui->mupenTab->load();
	m_ui->otherTab->load();
	m_ui->emulatorSelect->setCurrentIndex( (int)data.emuConfig.preferredEmulator );
	m_ui->themeSelect->setCurrentText( data.theme.c_str() );

	QWidget::showEvent( event );
}

void PreferencesDialog::save() {
	AppPreferences &data = AppPreferences::current();
	data.openOnStart = m_ui->openOnStartCheckbox->isChecked();
	data.nativeMenuBar = m_ui->nativeMenuCheckbox->isChecked();
	data.defaultScale = m_ui->importScaleSpinner->value();
	data.theme = m_ui->themeSelect->currentIndex() > 0 ? m_ui->themeSelect->currentText().toStdString() : ""s;
	data.emuConfig.preferredEmulator = (Emulator::Emulator)m_ui->emulatorSelect->currentIndex();
	m_ui->retroarchTab->save();
	m_ui->mupenTab->save();
	m_ui->otherTab->save();
	data.save();
	accept();
}
