#ifndef SRC_UI_DRAWING_LAYER_HELP_DIALOG_HPP_
#define SRC_UI_DRAWING_LAYER_HELP_DIALOG_HPP_

#include <QDialog>

namespace Ui {
	class DrawingLayerHelpDialog;
}

class DrawingLayerHelpDialog : public QDialog {
	Q_OBJECT

	private:
	Ui::DrawingLayerHelpDialog *m_ui;

	public:
	explicit DrawingLayerHelpDialog( QWidget *parent = nullptr );
	~DrawingLayerHelpDialog();

};



#endif /* SRC_UI_DRAWING_LAYER_HELP_DIALOG_HPP_ */
