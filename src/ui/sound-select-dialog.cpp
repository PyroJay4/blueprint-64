#include "src/ui/sound-select-dialog.hpp"
#include "ui_sound-select-dialog.h"

#include <cstdio>
#include <string>
#include <regex>
#include "src/core/sound.hpp"

static const std::regex SoundRegex(
	"(?:0x)([0-9a-fA-F]{1,8})",
	std::regex_constants::ECMAScript | std::regex_constants::optimize
);

inline static short reduceSoundId( uint sound ) {
	return (short)( ( (sound >> 20) & 0x0F00 ) | ( (sound >> 16) & 0xFF ) );
}

SoundSelectDialog::SoundSelectDialog( QWidget *parent ) :
	QDialog( parent ),
	m_ui( new Ui::SoundSelectDialog ),
	m_sound( 0 ),
	m_inEventHandler( true )
{
	m_ui->setupUi( this );
	m_ui->soundSelect->addItem( "No Sound", QVariant::fromValue<uint>( 0 ) );
	int i = 1;
	for( const auto &soundInfo : getAllNamedSounds() ) {
		m_ui->soundSelect->addItem( soundInfo.second, QVariant::fromValue<uint>( soundInfo.first ) );
		m_indexLookup[reduceSoundId(soundInfo.first)] = i++;
	}
	m_ui->soundSelect->addItem( "(Unknown)", QVariant::fromValue<uint>( 0xFFFFFFFFu ) );
	m_ui->soundSelect->setCurrentIndex( 0 );
	m_ui->manualSoundInput->setText( "0x00000000" );
	m_inEventHandler = false;
}

SoundSelectDialog::~SoundSelectDialog() {
	delete m_ui;
}

static const QString toHexString( uint sound ) {
	char hexString[11];
	std::sprintf( hexString, "0x%08X", sound );
	return QString( hexString );
}

void SoundSelectDialog::updateSelectBox() {
	if( m_sound == 0 ) {
		m_ui->soundSelect->setCurrentIndex( 0 );
		return;
	}
	auto i = m_indexLookup.find( reduceSoundId( m_sound ) );
	if( i == m_indexLookup.end() ) {
		m_ui->soundSelect->setCurrentIndex( m_ui->soundSelect->count() - 1 );
	} else {
		m_ui->soundSelect->setCurrentIndex( i->second );
	}
}

void SoundSelectDialog::setSound( uint sound ) {
	m_inEventHandler = true;
	m_sound = sound;
	formatSoundId();
	updateSelectBox();
	m_inEventHandler = false;
}

void SoundSelectDialog::soundSelected() {
	if( !m_inEventHandler ) {
		m_inEventHandler = true;
		setSoundUsingComboBox();
		m_inEventHandler = false;
	}
}

void SoundSelectDialog::soundIdChanged() {
	if( !m_inEventHandler ) {
		m_inEventHandler = true;
		setSoundUsingManualInput();
		m_inEventHandler = false;
	}
}

void SoundSelectDialog::formatSoundId() {
	m_inEventHandler = true;
	m_ui->manualSoundInput->setText( toHexString( m_sound ) );
	m_inEventHandler = false;
}

void SoundSelectDialog::setSoundUsingComboBox() {
	if( m_ui->soundSelect->currentData().toUInt() != 0xFFFFFFFFu ) {
		m_sound = m_ui->soundSelect->currentData().toUInt();
		formatSoundId();
	}
}

void SoundSelectDialog::setSoundUsingManualInput() {
	std::smatch matches;
	const string hexString = m_ui->manualSoundInput->text().toStdString();
	if( std::regex_search( hexString, matches, SoundRegex ) && matches.size() == 2 ) {
		m_sound = (uint)std::stoul( matches[1], nullptr, 16 );
		updateSelectBox();
	}
}
