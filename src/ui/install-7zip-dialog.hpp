#ifndef SRC_UI_INSTALL_7ZIP_DIALOG_HPP_
#define SRC_UI_INSTALL_7ZIP_DIALOG_HPP_

#include <QDialog>
#include <string>

namespace Ui {
	class Windows7zipDialog;
}

class Windows7zipDialog : public QDialog {
	Q_OBJECT

	private:
	Ui::Windows7zipDialog *m_ui;

	public:
	explicit Windows7zipDialog( QWidget *parent = nullptr );
	~Windows7zipDialog();

	std::string getExePath() const;

};

#endif /* SRC_UI_INSTALL_7ZIP_DIALOG_HPP_ */
