#ifndef UI_START_WINDOW_HPP_
#define UI_START_WINDOW_HPP_

#include <QMainWindow>
#include "src/ui/create-dialog.hpp"

namespace Ui {
	class StartWindow;
}

class StartWindow : public QMainWindow {
	Q_OBJECT

	private:
	Ui::StartWindow *m_ui;
	CreateDialog m_createDialog;

	signals:
	void projectCreated( CreateProjectOptions options );
	void projectOpened( string filePath );

	public slots:
	void newProject() {
		m_createDialog.show();
	}
	void openProject();
	void showErrorDialog( string message );

	public:
	explicit StartWindow( QWidget *parent = nullptr );
	~StartWindow();
};

#endif /* UI_START_WINDOW_HPP_ */
