#include "src/ui/user-data-link-widget.hpp"
#include "ui_user-data-link-widget.h"

#include <cstdio>
#include <cassert>
#include "src/core/blueprint.hpp"

UserDataLinkWidget::UserDataLinkWidget(
	QWidget *parent,
	LevelId levelId,
	uint linkOffset,
	const std::vector<PotentialLink> *availableLinks,
	UserDataLink *link
) :
	QWidget( parent ),
	m_ui( new Ui::UserDataLinkWidget ),
	m_levelId( levelId ),
	m_availableLinks( availableLinks ),
	m_link( link ),
	m_initializing( true )
{
	m_ui->setupUi( this );

	char locationText[9];
	assert( linkOffset < 0x150000 );
	std::sprintf( locationText, "0x%X", linkOffset );
	m_ui->linkLocation->setText( locationText );
	m_ui->targetSelect->addItem( "(Nothing)" );
	m_ui->targetSelect->setCurrentIndex( 0 );
	for( const PotentialLink &L : *availableLinks ) {
		m_ui->targetSelect->addItem( L.name.c_str() );
		if( L.type == link->linkType && L.targetId == link->targetId ) {
			m_ui->targetSelect->setCurrentIndex( m_ui->targetSelect->count() - 1 );
		}
	}
	refreshOffset();
	m_initializing = false;
}

UserDataLinkWidget::~UserDataLinkWidget() {
	delete m_ui;
}

void UserDataLinkWidget::refreshOffset() {
	if( m_link->linkType == LinkType::UserData ) {
		const UserData &targetData = Blueprint::current()->levels().at( m_levelId ).userData.at( m_link->targetId );
		m_ui->offsetLine->setVisible( true );
		m_ui->offsetSelect->setMaximum( (int)targetData.bytes - 1 );
	} else {
		m_ui->offsetLine->setVisible( false );
		m_ui->offsetSelect->setMaximum( 0x150000 );
	}
	m_ui->offsetSelect->setValue( (int)m_link->targetOffset );
}

void UserDataLinkWidget::linkTargetChanged() {
	if( m_initializing ) return;
	int index = m_ui->targetSelect->currentIndex();
	if( index == 0 ) {
		m_link->linkType = LinkType::None;
		m_link->targetId = 0;
		m_link->targetOffset = 0;
	} else {
		const PotentialLink &newLink = m_availableLinks->at( index - 1 );
		m_link->linkType = newLink.type;
		m_link->targetId = newLink.targetId;
		m_link->targetOffset = 0;
	}
	refreshOffset();
}

void UserDataLinkWidget::targetOffsetChanged() {
	if( m_initializing ) return;
	m_link->targetOffset = (uint)m_ui->offsetSelect->value();
}
