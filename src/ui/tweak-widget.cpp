#include "src/ui/tweak-widget.hpp"
#include "ui_tweak-widget.h"

#include "src/core/blueprint.hpp"
#include "src/ui/util.hpp"

TweakWidget::TweakWidget( QWidget *parent ) :
	QWidget( parent ),
	m_ui( new Ui::TweakWidget ),
	m_tweak( nullptr )
{
	m_ui->setupUi( this );
	setTweak( nullptr, false );
	FORWARD_SIGNAL( m_ui->editButton, this, clicked(), startedEditing() );
	FORWARD_SIGNAL( m_ui->saveButton, this, clicked(), finishedEditing() );
	FORWARD_SIGNAL( m_ui->cancelButton, this, clicked(), finishedEditing() );
}

TweakWidget::~TweakWidget() {
	delete m_ui;
}

static void makeReadonly( QVBoxLayout *layout, bool readonly ) {
	const int numWidgets = layout->count();
	for( int i = 0; i < numWidgets; i++ ) {
		TweakValueWidget *param = static_cast<TweakValueWidget*>( layout->itemAt( i )->widget() );
		param->setEnabled( !readonly );
	}
}

static TweakValueWidget *makeTweakValueWidget( QWidget *parent, const ISingularTweakDefinition *tweak, bool singular ) {
	switch( tweak->getType() ) {
		case TweakType::Basic: return new BooleanTweakValue( parent, static_cast<const BasicTweakDefinition*>( tweak ) );
		case TweakType::Integer: return new IntegerTweakValue( parent, static_cast<const IntegerTweakDefinition*>( tweak ), singular );
		case TweakType::Enum: return new EnumTweakValue( parent, static_cast<const EnumTweakDefinition*>( tweak ), singular );
		case TweakType::Switch: return new SwitchTweakValue( parent, static_cast<const SwitchTweakDefinition*>( tweak ), singular );
		case TweakType::Float: return new FloatTweakValue( parent, static_cast<const FloatTweakDefinition*>( tweak ), singular );
		case TweakType::Colour: return new ColourTweakValue( parent, static_cast<const ColourTweakDefinition*>( tweak ), singular );
		default: return nullptr;
	}
}

void TweakWidget::setTweak( const ITweakDefinition *tweak, bool isApplied ) {
	UiUtil::clearLayout( m_ui->parameters );
	m_tweak = tweak;
	if( tweak == nullptr ) {
		m_ui->nameLabel->setVisible( false );
		m_ui->descriptionLabel->setText( "Select a tweak." );

		m_ui->addButton->setVisible( false );
		m_ui->removeButton->setVisible( false );
		m_ui->editButton->setVisible( false );
		m_ui->cancelButton->setVisible( false );
		m_ui->saveButton->setVisible( false );
		m_ui->tweakPackLabel->setVisible( false );
		return;
	}

	m_ui->nameLabel->setVisible( true );
	m_ui->nameLabel->setText( tweak->getName().c_str() );
	m_ui->descriptionLabel->setText( tweak->getDescription().c_str() );

	m_ui->addButton->setVisible( !isApplied );
	m_ui->removeButton->setVisible( isApplied );
	m_ui->editButton->setVisible( isApplied && tweak->getType() != TweakType::Basic );
	m_ui->cancelButton->setVisible( false );
	m_ui->saveButton->setVisible( false );

	if( tweak->getType() == TweakType::Composite ) {
		for( const ISingularTweakDefinition *subtweak : static_cast<const CompositeTweakDefinition*>( tweak )->getComponents() ) {
			m_ui->parameters->addWidget( makeTweakValueWidget( this, subtweak, false ) );
		}
	} else if( tweak->getType() != TweakType::Basic ) {
		m_ui->parameters->addWidget( makeTweakValueWidget( this, static_cast<const ISingularTweakDefinition*>( tweak ), true ) );
	}

	if( tweak->getSource() == nullptr ) {
		m_ui->tweakPackLabel->setVisible( false );
	} else {
		m_ui->tweakPackLabel->setText( tweak->getSource()->name.c_str() );
		m_ui->tweakPackLabel->setVisible( true );
	}

	makeReadonly( m_ui->parameters, isApplied );
}

void TweakWidget::addTweak() {
	if( m_tweak->getType() == TweakType::Basic ) {
		Blueprint::currentEditable()->setTweakValue(
			static_cast<const BasicTweakDefinition*>( m_tweak )->getUniqueId(),
			Word::fromUInt( 1 )
		);
	} else {
		saveEdit();
	}
	emit tweakAdded();
}

void TweakWidget::removeTweak() {
	if( m_tweak->getType() == TweakType::Composite ) {
		for( const ISingularTweakDefinition *subtweak : static_cast<const CompositeTweakDefinition*>( m_tweak )->getComponents() ) {
			Blueprint::currentEditable()->removeTweak( subtweak->getUniqueId() );
		}
	} else {
		Blueprint::currentEditable()->removeTweak( static_cast<const ISingularTweakDefinition*>( m_tweak )->getUniqueId() );
	}
	emit tweakRemoved();
}

void TweakWidget::startEditing() {
	m_ui->removeButton->setVisible( false );
	m_ui->editButton->setVisible( false );
	m_ui->saveButton->setVisible( true );
	m_ui->cancelButton->setVisible( true );
	makeReadonly( m_ui->parameters, false );
}

void TweakWidget::saveEdit() {
	m_ui->removeButton->setVisible( true );
	m_ui->editButton->setVisible( true );
	m_ui->saveButton->setVisible( false );
	m_ui->cancelButton->setVisible( false );
	makeReadonly( m_ui->parameters, true );
	for( int i = 0; i < m_ui->parameters->count(); i++ ) {
		TweakValueWidget *param = static_cast<TweakValueWidget*>( m_ui->parameters->itemAt( i )->widget() );
		if( param->isDefault() ) {
			Blueprint::currentEditable()->removeTweak( param->tweakId() );
		} else {
			Blueprint::currentEditable()->setTweakValue( param->tweakId(), param->value() );
		}
	}
}

void TweakWidget::cancelEdit() {
	m_ui->removeButton->setVisible( true );
	m_ui->editButton->setVisible( true );
	m_ui->saveButton->setVisible( false );
	m_ui->cancelButton->setVisible( false );
	makeReadonly( m_ui->parameters, true );
	for( int i = 0; i < m_ui->parameters->count(); i++ ) {
		static_cast<TweakValueWidget*>( m_ui->parameters->itemAt( i )->widget() )->refreshValue();
	}
}
