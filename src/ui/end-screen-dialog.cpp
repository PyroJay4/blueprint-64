#include "src/ui/end-screen-dialog.hpp"
#include "ui_end-screen-dialog.h"

#include <QImage>
#include <QFileDialog>
#include <QMessageBox>
#include "src/core/blueprint.hpp"
#include "src/core/ending.hpp"
#include "src/ui/util.hpp"

EndScreenDialog::EndScreenDialog( QWidget *parent ) :
	QDialog( parent ),
	m_ui( new Ui::EndScreenDialog )
{
	m_ui->setupUi( this );
}

EndScreenDialog::~EndScreenDialog() {
	delete m_ui;
}


void EndScreenDialog::replaceImage() {
	QString imgPath = UiUtil::toNativePath( QFileDialog::getOpenFileName( this, "Select an Image (320x240)", QString(), "Images (*.png *.bmp *.gif *.jpg *.jpeg *.xpm *.xbm *.pbm *.pgm *.ppm)" ) );
	if( imgPath.isNull() ) return;
	//TODO: choose resize method
	if( EndScreen::saveImage( imgPath.toStdString(), EndScreen::ResizeMethod::Stretch ) ) {
		QMessageBox::information( this, "End Screen Imported", "End Screen was imported successfully.", QMessageBox::Ok );
	} else {
		QMessageBox::critical( this, "Importer Machine Broke", "Failed to import end screen. Is it a valid image format?", QMessageBox::Ok );
	}
	updateImage();
}

void EndScreenDialog::revertImage() {
	EndScreen::removeImage();
	QMessageBox::information( this, "End Screen Reverted", "End Screen has been reverted to its original form.", QMessageBox::Ok );
	close();
}

void EndScreenDialog::updateImage() {
	QImage image( Blueprint::current()->getEndScreenPath().string().c_str(), "PNG" );
	if( image.isNull() || image.width() != 320 || image.height() != 240 ) {
		m_ui->imagePreview->clearPixmap( 320, 240 );
	} else {
		m_ui->imagePreview->setPixmap( QPixmap::fromImage( image ), false );
	}
}

void EndScreenDialog::showEvent( QShowEvent *event ) {
	m_ui->interpolationCheckbox->setChecked( Blueprint::current()->endScreenHasInterpolation() );
	updateImage();
	QDialog::showEvent( event );
}

void EndScreenDialog::setInterpolation( bool useInterpolation ) {
	Blueprint::currentEditable()->setEndScreenInterpolation( useInterpolation );
}
