#ifndef SRC_UI_TWEAK_WIDGET_HPP_
#define SRC_UI_TWEAK_WIDGET_HPP_

#include <QWidget>
#include <vector>
#include "src/ui/tweak-values.hpp"

namespace Ui {
	class TweakWidget;
}

class TweakWidget : public QWidget {
	Q_OBJECT

	private:
	Ui::TweakWidget *m_ui;
	const ITweakDefinition *m_tweak;

	signals:
	void tweakAdded();
	void tweakRemoved();
	void startedEditing();
	void finishedEditing();

	public slots:
	void startEditing();
	void saveEdit();
	void cancelEdit();
	void addTweak();
	void removeTweak();

	public:
	explicit TweakWidget( QWidget *parent = nullptr );
	~TweakWidget();

	void setTweak( const ITweakDefinition *tweak, bool isApplied );
};




#endif /* SRC_UI_TWEAK_WIDGET_HPP_ */
