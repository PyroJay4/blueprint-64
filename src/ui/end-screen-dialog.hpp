#ifndef SRC_UI_END_SCREEN_DIALOG_HPP_
#define SRC_UI_END_SCREEN_DIALOG_HPP_

#include <QDialog>

namespace Ui {
	class EndScreenDialog;
}

class EndScreenDialog : public QDialog {
	Q_OBJECT

	private:
	Ui::EndScreenDialog *m_ui;

	public:
	explicit EndScreenDialog( QWidget *parent = nullptr );
	~EndScreenDialog();

	private:
	void updateImage();

	public slots:
	void replaceImage();
	void revertImage();
	void setInterpolation(bool);

	protected:
	virtual void showEvent( QShowEvent *event ) override;

};



#endif /* SRC_UI_END_SCREEN_DIALOG_HPP_ */
