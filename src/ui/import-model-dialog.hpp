#ifndef SRC_UI_IMPORT_MODEL_DIALOG_HPP_
#define SRC_UI_IMPORT_MODEL_DIALOG_HPP_

#include <QDialog>
#include <vector>
#include <string>

namespace Ui {
	class ImportModelDialog;
}

struct ModelFileImportInfo {
	std::string filePath;
	bool visible;
	bool solid;
};

struct ModelImportedEvent {
	std::vector<ModelFileImportInfo> models;
	double scale;
};

class ImportModelDialog : public QDialog {
	Q_OBJECT

	private:
	Ui::ImportModelDialog *m_ui;
	int m_rows;

	void removeRow( int row );
	void checkCanImport();

	public:
	explicit ImportModelDialog( QWidget *parent = nullptr );
	~ImportModelDialog();

	void showEvent( QShowEvent *event ) override;

	public slots:
	void import();
	void addFile();

	signals:
	void modelImported( ModelImportedEvent );

};

#endif /* SRC_UI_IMPORT_MODEL_DIALOG_HPP_ */
