#ifndef SRC_UI_BUILD_WARNINGS_DIALOG_HPP_
#define SRC_UI_BUILD_WARNINGS_DIALOG_HPP_

#include <QDialog>
#include <vector>
#include <string>

namespace Ui {
	class WarningsDialog;
}

class WarningsDialog : public QDialog {
	Q_OBJECT

	private:
	Ui::WarningsDialog *m_ui;

	public:
	explicit WarningsDialog( QWidget *parent = nullptr );
	~WarningsDialog();

	bool getResponse(
		const std::string &message,
		const std::vector<std::string> &warnings,
		bool hasRevertButton
	);

};



#endif /* SRC_UI_BUILD_WARNINGS_DIALOG_HPP_ */
