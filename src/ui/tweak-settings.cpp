#include "src/ui/tweak-settings.hpp"
#include "ui_tweak-settings.h"

#include <QListWidgetItem>
#include <QList>
#include "src/core/blueprint.hpp"

class TweakListItem : public QListWidgetItem {

	private:
	const ITweakDefinition *m_tweak;

	public:
	TweakListItem( const ITweakDefinition *tweak ) :
		QListWidgetItem( tweak->getName().c_str(), nullptr ),
		m_tweak( tweak ) {}

	~TweakListItem() {}

	const ITweakDefinition *tweak() { return m_tweak; }

};

TweakSettingsWidget::TweakSettingsWidget( QWidget *parent ) :
	QWidget( parent ),
	m_ui( new Ui::TweakSettingsWidget )
{
	m_ui->setupUi( this );
	connect( m_ui->tweakInfo, SIGNAL(tweakAdded()), this, SLOT(addTweak()) );
	connect( m_ui->tweakInfo, SIGNAL(tweakRemoved()), this, SLOT(removeTweak()) );
	connect( m_ui->tweakInfo, SIGNAL(startedEditing()), this, SLOT(lockLists()) );
	connect( m_ui->tweakInfo, SIGNAL(finishedEditing()), this, SLOT(unlockLists()) );
}

TweakSettingsWidget::~TweakSettingsWidget() {
	delete m_ui;
}

static bool isApplied( ITweakDefinition *tweak ) {
	if( CompositeTweakDefinition *compositeTweak = dynamic_cast<CompositeTweakDefinition*>( tweak ) ) {
		for( ISingularTweakDefinition *subtweak : compositeTweak->getComponents() ) {
			if( Blueprint::current()->usingTweak( subtweak->getUniqueId() ) ) {
				return true;
			}
		}
		return false;
	}
	const Uuid &tweakId = static_cast<ISingularTweakDefinition*>( tweak )->getUniqueId();
	return Blueprint::current()->usingTweak( tweakId );
}

static void clearList( QListWidget *list ) {
	for( int i = 0; i < list->count(); i++ ) {
		delete list->item( i );
	}
	list->clear();
}

void TweakSettingsWidget::refresh() {
	clearList( m_ui->availableTweaksList );
	clearList( m_ui->appliedTweaksList );
	for( ITweakDefinition *tweak : TweakStore::instance().getPublicTweaks() ) {
		if( isApplied( tweak ) ) {
			m_ui->appliedTweaksList->insertItem( 0, new TweakListItem( tweak ) );
		} else {
			m_ui->availableTweaksList->insertItem( 0, new TweakListItem( tweak ) );
		}
	}
	m_ui->appliedTweaksList->sortItems();
	m_ui->availableTweaksList->sortItems();

	m_ui->tweakInfo->setTweak( nullptr, false );
	unlockLists();
}

void TweakSettingsWidget::availableTweakSelected() {
	QList<QListWidgetItem*> selectedItem = m_ui->availableTweaksList->selectedItems();
	if( !selectedItem.empty() ) {
		m_ui->appliedTweaksList->clearSelection();
		m_ui->tweakInfo->setTweak( static_cast<TweakListItem*>( selectedItem.first() )->tweak(), false );
	}
}

void TweakSettingsWidget::appliedTweakSelected() {
	QList<QListWidgetItem*> selectedItem = m_ui->appliedTweaksList->selectedItems();
	if( !selectedItem.empty() ) {
		m_ui->availableTweaksList->clearSelection();
		m_ui->tweakInfo->setTweak( static_cast<TweakListItem*>( selectedItem.first() )->tweak(), true );
	}
}

void TweakSettingsWidget::addTweak() {
	QList<QListWidgetItem*> selectedItems = m_ui->availableTweaksList->selectedItems();
	if( selectedItems.empty() ) return;
	TweakListItem *tweak = static_cast<TweakListItem*>( selectedItems.first() );

	m_ui->availableTweaksList->removeItemWidget( tweak );
	QListWidgetItem *clonedTweak = new TweakListItem( tweak->tweak() );
	delete tweak;
	m_ui->appliedTweaksList->addItem( clonedTweak );
	m_ui->appliedTweaksList->sortItems();
	m_ui->appliedTweaksList->clearSelection();
	clonedTweak->setSelected( true );
	appliedTweakSelected();
}

void TweakSettingsWidget::removeTweak() {
	QList<QListWidgetItem*> selectedItems = m_ui->appliedTweaksList->selectedItems();
	if( selectedItems.empty() ) return;
	TweakListItem *tweak = static_cast<TweakListItem*>( selectedItems.first() );

	m_ui->appliedTweaksList->removeItemWidget( tweak );
	QListWidgetItem *clonedTweak = new TweakListItem( tweak->tweak() );
	delete tweak;
	m_ui->availableTweaksList->addItem( clonedTweak );
	m_ui->availableTweaksList->sortItems();
	m_ui->availableTweaksList->clearSelection();
	clonedTweak->setSelected( true );
	availableTweakSelected();
}

void TweakSettingsWidget::lockLists() {
	m_ui->availableTweaksList->setEnabled( false );
	m_ui->appliedTweaksList->setEnabled( false );
}

void TweakSettingsWidget::unlockLists() {
	m_ui->availableTweaksList->setEnabled( true );
	m_ui->appliedTweaksList->setEnabled( true );
}

void TweakSettingsWidget::manageTweakPacks() {
	m_tweakPacksDialog.open();
}
