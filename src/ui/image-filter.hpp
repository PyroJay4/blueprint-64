#ifndef SRC_UI_IMAGE_FILTER_HPP_
#define SRC_UI_IMAGE_FILTER_HPP_

#include <QImage>
#include "src/core/colour.hpp"
#include "src/core/enums.hpp"

namespace ImageFilter {

	void applyColourFormat( QImage &image, TextureFormat format );
	void colourize( QImage &image, const ColourRGB24 &colour );
	void makeValidTextureSize( QImage &image );
	void removeAlpha( QImage &image );

}



#endif /* SRC_UI_IMAGE_FILTER_HPP_ */
