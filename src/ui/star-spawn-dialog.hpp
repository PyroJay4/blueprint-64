#ifndef SRC_UI_STAR_SPAWN_DIALOG_HPP_
#define SRC_UI_STAR_SPAWN_DIALOG_HPP_

#include <QDialog>
#include "src/core/enums.hpp"

namespace Ui {
	class StarSpawnDialog;
}

class StarSpawnDialog : public QDialog {
	Q_OBJECT

	private:
	Ui::StarSpawnDialog *m_ui;
	LevelId m_levelId;

	public:
	explicit StarSpawnDialog( QWidget *parent = nullptr );
	~StarSpawnDialog();

	inline void open( LevelId levelId ) {
		m_levelId = levelId;
		starSelected();
		QDialog::open();
	}

	public slots:
	void locationChanged();
	void starSelected();

};



#endif /* SRC_UI_STAR_SPAWN_DIALOG_HPP_ */
