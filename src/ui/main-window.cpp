#include "src/ui/main-window.hpp"
#include "ui_main-window.h"

#include <QMessageBox>
#include <QFileDialog>
#include <fstream>
#include <cstdlib>
#include "src/polyfill/filesystem.hpp"
#include "src/ui/widgets/save-notification.hpp"
#include "src/core/baserom.hpp"
#include "src/core/blueprint.hpp"
#include "src/core/storage.hpp"
#include "src/core/util.hpp"
#include "src/core/checksum.hpp"
#include "src/core/backup.hpp"
#include "src/core/emulator.hpp"
#include "src/openmode.hpp"
#include "src/ui/util.hpp"

static const uint LOCATION_ROM_SETTINGS = 0;
static const uint LOCATION_TWEAKS = 1;
static const uint LOCATION_DIALOG_EDIT = 2;
static const uint LOCATION_LEVEL_EDIT = 3;
static const uint LOCATION_AREA_EDIT = 4;
static const uint LOCATION_AREA_MODEL_EDIT = 5;
static const uint LOCATION_MUSIC_EDIT = 6;
static const uint LOCATION_OBJECT_EDIT = 7;
static const uint LOCATION_OBJECT_MODEL_EDIT = 8;
static const uint LOCATION_WATER_EDIT = 9;
static const uint LOCATION_BACKGROUND_EDIT = 10;
static const uint LOCATION_ASM_MODULES_EDIT = 11;
static const uint LOCATION_MARIO_ACTIONS_EDIT = 12;
static const uint LOCATION_MODULE_DEV = 13;

static const uint BUILD_LEVELS = 0x10001;
static const uint BUILD_END_SCREEN = 0x10002;

static const uint BUILD_ALL = 0xFFFFFFFFu;
static const uint BUILD_CLEAN = 0xFFFFFFFEu;

using namespace std::string_literals;

MainWindow::MainWindow( QWidget *parent ) :
	QMainWindow( parent ),
	m_ui( new Ui::MainWindow ),
	m_currentLevel( LevelId::INVALID ),
	m_currentArea( 0 ),
	m_currentObject( 0 )
{
	m_ui->setupUi( this );
	syncHistory();

	m_ui->menubar->setNativeMenuBar( AppPreferences::current().nativeMenuBar );

	connect( m_ui->actionBuildAll, SIGNAL(triggered()), this, SLOT(buildAll()) );
	connect( m_ui->actionBuildRomSettings, &QAction::triggered, this, [=](){ buildPartial( LOCATION_ROM_SETTINGS ); } );
	connect( m_ui->actionBuildTweaks, &QAction::triggered, this, [=](){ buildPartial( LOCATION_TWEAKS ); } );
	connect( m_ui->actionBuildDialog, &QAction::triggered, this, [=](){ buildPartial( LOCATION_DIALOG_EDIT ); } );
	connect( m_ui->actionBuildLevels, &QAction::triggered, this, [=](){ buildPartial( BUILD_LEVELS ); } );
	connect( m_ui->actionBuildMusic, &QAction::triggered, this, [=](){ buildPartial( LOCATION_MUSIC_EDIT ); } );
	connect( m_ui->actionBuildEndScreen, &QAction::triggered, this, [=](){ buildPartial( BUILD_END_SCREEN ); } );
	connect( m_ui->actionUpdateChecksum, SIGNAL(triggered()), this, SLOT(updateChecksum()) );

	connect( &m_preferencesDialog, &QDialog::accepted, this, [=](){
		m_ui->menubar->setNativeMenuBar( AppPreferences::current().nativeMenuBar );
	});
}

MainWindow::~MainWindow() {
	delete m_ui;
}

void MainWindow::showErrorDialog( string message ) {
	QMessageBox::critical(
		this,
		"Oh no!",
		message.c_str(),
		QMessageBox::Ok
	);
}

bool MainWindow::trySave() {
	Blueprint *blueprint = Blueprint::currentEditable();
	if( blueprint == nullptr ) return false;

	try {
		const SurfacesEditor *surfacesEditor = dynamic_cast<const SurfacesEditor*>( m_ui->activeView->currentWidget() );
		if( surfacesEditor != nullptr ) {
			surfacesEditor->saveData();
		}
		blueprint->save();
		new SaveNotification( this, m_ui->saveButton->geometry().center() );
		return true;
	} catch( const std::exception &ex ) {
		showErrorDialog( "An error occurred attempting to save your blueprint. Please report this bug." );
		return false;
	}
}

void MainWindow::saveAs() {
	Blueprint *blueprint = Blueprint::currentEditable();
	if( blueprint == nullptr || blueprint->isCollab() ) return;
	QString path = UiUtil::toNativePath( QFileDialog::getSaveFileName( this, "Save As", QString(), "Blueprint Files (*.bbp)" ) );
	if( path.isNull() ) return;
	if( !path.endsWith( ".bbp" ) ) {
		path += ".bbp";
	}
	try {
		const SurfacesEditor *surfacesEditor = dynamic_cast<const SurfacesEditor*>( m_ui->activeView->currentWidget() );
		if( surfacesEditor != nullptr ) {
			surfacesEditor->saveData();
		}
		blueprint->saveAs( std::filesystem::path( path.toStdString() ) );
		RecentProjects::push( path.toStdString() );
	} catch( const std::exception &ex ) {
		showErrorDialog( "Failed to save blueprint to this location. Do you have permission to create files here?" );
	}
}

bool MainWindow::unsavedChangesPrompt() {
	if( Blueprint::current() == nullptr || !Blueprint::current()->hasUnsavedChanges() ) {
		return true;
	}

	QMessageBox::StandardButton answer = QMessageBox::question(
		this,
		"Save Changes?",
		"You have unsaved changes. Do you want to save now?",
		QMessageBox::Yes | QMessageBox::No | QMessageBox::Cancel,
		QMessageBox::Yes
	);

	switch( answer ) {
		case QMessageBox::Yes: return trySave();
		case QMessageBox::No: return true;
		default: return false;
	}
}

void MainWindow::loadBlueprint() {
	syncHistory();
	checkCanBuild();
	m_ui->breadcrumbTrail->clear();
	m_ui->breadcrumbTrail->push( "ROM Settings", LOCATION_ROM_SETTINGS );
	m_ui->activeView->setCurrentWidget( m_ui->romSettingsPage );
	m_ui->applySingleButton->setText( "Apply ROM Settings" );
	m_ui->romSettingsPage->blueprintChanged();
	m_ui->tweakSettingsPage->blueprintChanged();
	m_ui->dialogEditorPage->blueprintChanged();
	m_ui->waterEditorPage->blueprintChanged();
	m_ui->moduleEditorPage->blueprintChanged();
	m_ui->actionSaveAs->setEnabled( !Blueprint::current()->isCollab() );
	show();
}

void MainWindow::checkCanBuild() {
	const bool &canBuild = Blueprint::current()->hasTargetRom();
	m_ui->buildMenu->setEnabled( canBuild );
	m_ui->applyAllButton->setEnabled( canBuild );
	m_ui->playRomButton->setEnabled( canBuild );
	checkPartialBuildAllowed();
}

void MainWindow::syncHistory() {
	m_ui->openRecentMenu->clear();
	for( const string &filePath : RecentProjects::get() ) {
		QAction *action = new QAction( filePath.c_str(), m_ui->openRecentMenu );
		connect( action, &QAction::triggered, this, [=](){
			if( !unsavedChangesPrompt() ) return;
			try {
				Blueprint::open( std::filesystem::path( filePath ) );
				RecentProjects::push( filePath );
				loadBlueprint();
			} catch( ... ) {
				RecentProjects::remove( filePath );
				QMessageBox::critical(
					this,
					"File not found.",
					"Failed to open project. Has it been moved or deleted?",
					QMessageBox::Ok
				);
			}
			syncHistory();
		});
		m_ui->openRecentMenu->addAction( action );
	}
}

#define HANDLE_NAV( id, page, applyButtonText ) case id: \
	m_ui->activeView->setCurrentWidget( m_ui->page ); \
	m_ui->applySingleButton->setText( applyButtonText ); \
	break;

void MainWindow::navigated( uint locationId ) {
	switch( locationId ) {
		case LOCATION_LEVEL_EDIT: m_ui->levelEditorPage->setLevel( m_currentLevel ); break;
		case LOCATION_AREA_EDIT: m_ui->areaEditorPage->setArea( m_currentLevel, m_currentArea ); break;
		case LOCATION_AREA_MODEL_EDIT: m_ui->areaModelPage->setArea( m_currentLevel, m_currentArea ); break;
		case LOCATION_OBJECT_EDIT: m_ui->objectEditorPage->setObject( m_currentLevel, m_currentObject ); break;
		case LOCATION_OBJECT_MODEL_EDIT: m_ui->objectModelPage->setObject( m_currentLevel, m_currentObject ); break;
		case LOCATION_WATER_EDIT: m_ui->waterEditorPage->setArea( m_currentLevel,  m_currentArea ); break;
		default: break;
	}

	switch( locationId ) {
		HANDLE_NAV( LOCATION_ROM_SETTINGS, romSettingsPage, "Apply ROM Settings" )
		HANDLE_NAV( LOCATION_TWEAKS, tweakSettingsPage, "Apply Tweaks" )
		HANDLE_NAV( LOCATION_DIALOG_EDIT, dialogEditorPage, "Apply Dialog Changes" )
		HANDLE_NAV( LOCATION_LEVEL_EDIT, levelEditorPage, "Apply Level Changes" )
		HANDLE_NAV( LOCATION_AREA_EDIT, areaEditorPage, "Apply Level Changes" )
		HANDLE_NAV( LOCATION_AREA_MODEL_EDIT, areaModelPage, "Apply Level Changes" )
		HANDLE_NAV( LOCATION_MUSIC_EDIT, musicEditorPage, "Apply Music Changes" )
		HANDLE_NAV( LOCATION_OBJECT_EDIT, objectEditorPage, "Apply Level Changes" )
		HANDLE_NAV( LOCATION_OBJECT_MODEL_EDIT, objectModelPage, "Apply Level Changes" )
		HANDLE_NAV( LOCATION_WATER_EDIT, waterEditorPage, "Apply Level Changes" )
		HANDLE_NAV( LOCATION_BACKGROUND_EDIT, backgroundEditorPage, "Apply Background Changes" )
		HANDLE_NAV( LOCATION_ASM_MODULES_EDIT, moduleEditorPage, "Apply ASM Modules" )
		HANDLE_NAV( LOCATION_MARIO_ACTIONS_EDIT, marioActionEditorPage, "Apply ASM Modules" )
		HANDLE_NAV( LOCATION_MODULE_DEV, moduleDevelopPage, "Apply ASM Modules" )
	}
}

#undef HANDLE_NAV

void MainWindow::editTweaks() {
	m_ui->breadcrumbTrail->push( "Tweaks", LOCATION_TWEAKS );
	navigated( LOCATION_TWEAKS );
}

void MainWindow::editDialog() {
	m_ui->breadcrumbTrail->push( "Dialog", LOCATION_DIALOG_EDIT );
	navigated( LOCATION_DIALOG_EDIT );
}

void MainWindow::editLevel( LevelId levelId ) {
	m_currentLevel = levelId;
	m_ui->breadcrumbTrail->push( "Level Editor", LOCATION_LEVEL_EDIT );
	navigated( LOCATION_LEVEL_EDIT );
}

void MainWindow::editArea( ubyte areaIndex ) {
	m_currentArea = areaIndex;
	m_ui->breadcrumbTrail->push( "Area Editor", LOCATION_AREA_EDIT );
	navigated( LOCATION_AREA_EDIT );
}

void MainWindow::editAreaModel() {
	m_ui->breadcrumbTrail->push( "Surface Editor", LOCATION_AREA_MODEL_EDIT );
	navigated( LOCATION_AREA_MODEL_EDIT );
}

void MainWindow::editMusic() {
	m_ui->breadcrumbTrail->push( "Music Editor", LOCATION_MUSIC_EDIT );
	navigated( LOCATION_MUSIC_EDIT );
}

void MainWindow::editObject( objectId objectId ) {
	m_currentObject = objectId;
	m_ui->breadcrumbTrail->push( "Object Editor", LOCATION_OBJECT_EDIT );
	navigated( LOCATION_OBJECT_EDIT );
}

void MainWindow::editObjectModel() {
	m_ui->breadcrumbTrail->push( "Surface Editor", LOCATION_OBJECT_MODEL_EDIT );
	navigated( LOCATION_OBJECT_MODEL_EDIT );
}

void MainWindow::editObjectModel( objectId objectId ) {
	m_currentObject = objectId;
	editObjectModel();
}

void MainWindow::editWaterBoxes() {
	m_ui->breadcrumbTrail->push( "Water Box Editor", LOCATION_WATER_EDIT );
	navigated( LOCATION_WATER_EDIT );
}

void MainWindow::editBackgrounds() {
	m_ui->breadcrumbTrail->push( "Custom Background Editor", LOCATION_BACKGROUND_EDIT );
	navigated( LOCATION_BACKGROUND_EDIT );
}

void MainWindow::editAsmModules() {
	m_ui->breadcrumbTrail->push( "ASM Modules", LOCATION_ASM_MODULES_EDIT );
	navigated( LOCATION_ASM_MODULES_EDIT );
}

void MainWindow::editMarioActions() {
	m_ui->breadcrumbTrail->push( "Custom Mario Actions", LOCATION_MARIO_ACTIONS_EDIT );
	navigated( LOCATION_MARIO_ACTIONS_EDIT );
}

void MainWindow::developModule() {
	m_ui->breadcrumbTrail->push( "Develop and Test Modules", LOCATION_MODULE_DEV );
	navigated( LOCATION_MODULE_DEV );
}

#define IS_ACTIVE( type ) dynamic_cast<type*>( m_ui->activeView->currentWidget() ) != nullptr

void MainWindow::buildAll() {
	if( IS_ACTIVE( ModuleDevelopView ) ) {
		m_ui->moduleDevelopPage->rebuildAll();
	} else {
		buildPartial( BUILD_ALL );
	}
}

static const char *CLEAN_BUILD_WARNING = ""
	"Performing a clean build will erase all changes to the ROM that are not "
	"part of the blueprint, INCLUDING OBJECT PLACEMENTS. Make sure you save "
	"all object placements to the ROM before continuing!";

void MainWindow::buildClean() {
	if(
		Blueprint::current()->hasTargetRom() &&
		QMessageBox::warning( this, "Clean Build?", CLEAN_BUILD_WARNING, QMessageBox::Ok | QMessageBox::Abort, QMessageBox::Abort ) == QMessageBox::Ok
	) {
		buildPartial( BUILD_CLEAN );
	}
}

void MainWindow::buildCurrent() {
	if( IS_ACTIVE( RomSettingsWidget ) ) {
		buildPartial( LOCATION_ROM_SETTINGS );
	} else if( IS_ACTIVE( TweakSettingsWidget ) ) {
		buildPartial( LOCATION_TWEAKS );
	} else if( IS_ACTIVE( DialogEditor ) ) {
		buildPartial( LOCATION_DIALOG_EDIT );
	} else if( IS_ACTIVE( LevelEditor ) || IS_ACTIVE( AreaEditor ) || IS_ACTIVE( SurfacesEditor ) || IS_ACTIVE( ObjectEditor ) || IS_ACTIVE( WaterEditor ) ) {
		buildPartial( LOCATION_LEVEL_EDIT );
	} else if( IS_ACTIVE( MusicEditor ) ) {
		buildPartial( LOCATION_MUSIC_EDIT );
	} else if( IS_ACTIVE( BackgroundEditor ) ) {
		buildPartial( LOCATION_BACKGROUND_EDIT );
	} else if( IS_ACTIVE( AsmModuleEditor ) || IS_ACTIVE( CustomActionEditor ) ) {
		buildPartial( LOCATION_ASM_MODULES_EDIT );
	} else if( IS_ACTIVE( ModuleDevelopView ) ) {
		m_ui->moduleDevelopPage->rebuildModule();
	}
}

#undef IS_ACTIVE

void MainWindow::buildPartial( uint id ) {
	const Blueprint *blueprint = Blueprint::current();
	if( !blueprint->hasTargetRom() ) return;

	const SurfacesEditor *surfacesEditor = dynamic_cast<const SurfacesEditor*>( m_ui->activeView->currentWidget() );
	if( surfacesEditor != nullptr ) {
		surfacesEditor->saveData();
	}

	namespace fs = std::filesystem;
	const fs::path targetRomPath = blueprint->targetRomPath();

	if( !fs::exists( targetRomPath ) ) {
		fs::copy_file( BaseRom::filePath(), targetRomPath );
	}

	FileBackup backup( targetRomPath );
	std::fstream rom = std::fstream( targetRomPath, std::ios_base::in | std::ios_base::out | std::ios_base::binary );
	try {
		std::vector<string> warnings;
		switch( id ) {
			case BUILD_CLEAN: fs::tryCopyFileOverwrite( BaseRom::filePath(), targetRomPath ); [[fallthrough]];
			case BUILD_ALL: warnings = blueprint->applyAll( rom ); break;
			case LOCATION_ROM_SETTINGS: blueprint->applyRomSettings( rom ); break;
			case LOCATION_TWEAKS: warnings = blueprint->applyTweaks( rom ); break;
			case LOCATION_DIALOG_EDIT: blueprint->applyTextChanges( rom ); break;
			case LOCATION_LEVEL_EDIT: warnings = blueprint->applyLevelChanges( rom, m_currentLevel ); break;
			case LOCATION_MUSIC_EDIT: blueprint->applyMusicChanges( rom ); break;
			case LOCATION_BACKGROUND_EDIT: blueprint->applyBackgroundChanges( rom ); break;
			case LOCATION_MARIO_ACTIONS_EDIT: [[fallthrough]];
			case LOCATION_ASM_MODULES_EDIT: blueprint->applyAsmModules( rom ); break;
			case BUILD_LEVELS: warnings = blueprint->applyAllLevelChanges( rom, false ); break;
			case BUILD_END_SCREEN: blueprint->applyEndScreen( rom ); break;
			default: return;
		}
		if( warnings.empty() ) {
			backup.discard();
			setPartialBuildAllowed( true );
			QMessageBox::information( this, "Changes Applied", "ROM built successfully." );
		} else if( m_buildWarningsDialog.getResponse( "Build completed with warnings:", warnings, true ) ) {
			backup.discard();
			setPartialBuildAllowed( true );
		}
	} catch( const ArmipsException &ex ) {
		if( id == BUILD_ALL || id == BUILD_CLEAN ) {
			if( m_armipsErrorDialog.getResponse( ex.what() ) ) {
				backup.discard();
				if( blueprint->romSpecs().enableChecksum ) {
					blueprint->updateChecksum( rom );
				}
			}
		} else {
			const string msg = "Armips failed to build custom ASM:\n"s + ex.what();
			QMessageBox::critical( this, "Build Failed", msg.c_str() );
		}
	} catch( const std::exception &ex ) {
		const string msg = "Failed to build ROM:\n"s + ex.what();
		QMessageBox::critical( this, "Build Failed", msg.c_str() );
	}
	rom.close();
}

void MainWindow::playRom() {
	if( !Blueprint::current()->hasTargetRom() ) return;

	if( !Emulator::emulatorInstalled() ) {
		QMessageBox::critical( this, "Emulator not Found", "Your preferred emulator does not appear to be installed. You can change your preferred emulator in Settings > Preferences." );
		return;
	}

	if( !Emulator::launch( Blueprint::current()->targetRomPath().string() ) ) {
		QMessageBox::critical( this, "External Error", "Failed to launch emulator", QMessageBox::Ok );
	}
}

void MainWindow::updateChecksum() {
	const Blueprint *blueprint = Blueprint::current();
	if( !blueprint || !blueprint->hasTargetRom() ) return;
	std::fstream rom( blueprint->targetRomPath().string(), std::ios_base::in | std::ios_base::out | std::ios_base::binary );
	updateCRC( rom );
}

void MainWindow::checkPartialBuildAllowed() {
	if( !Blueprint::current()->hasTargetRom() ) {
		setPartialBuildAllowed( false );
		return;
	}

	std::ifstream rom( Blueprint::current()->targetRomPath(), mode::read );
	if( !rom.good() ) {
		setPartialBuildAllowed( false );
		return;
	}

	setPartialBuildAllowed( Blueprint::current()->canRunPartialBuild( rom ) );
}

void MainWindow::setPartialBuildAllowed( bool allowed ) {
	m_ui->applySingleButton->setEnabled( allowed );
	m_ui->actionBuildRomSettings->setEnabled( allowed );
	m_ui->actionBuildTweaks->setEnabled( allowed );
	m_ui->actionBuildDialog->setEnabled( allowed );
	m_ui->actionBuildLevels->setEnabled( allowed );
	m_ui->actionBuildMusic->setEnabled( allowed );
	m_ui->actionBuildEndScreen->setEnabled( allowed );
	m_ui->actionApplyBackgrounds->setEnabled( allowed );
}
