#include "src/ui/install-7zip-dialog.hpp"
#include "ui_install-7zip-dialog.h"

Windows7zipDialog::Windows7zipDialog( QWidget *parent ) :
	QDialog( parent ),
	m_ui( new Ui::Windows7zipDialog )
{
	m_ui->setupUi( this );
}

Windows7zipDialog::~Windows7zipDialog() {
	delete m_ui;
}

std::string Windows7zipDialog::getExePath() const {
	return m_ui->exePath->text().toStdString();
}
