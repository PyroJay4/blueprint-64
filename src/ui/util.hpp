#ifndef UI_UTIL_HPP_
#define UI_UTIL_HPP_

#include <iostream>
#include <string>
#include <QWidget>
#include <QListWidget>
#include <QGridLayout>

#define FORWARD_SIGNAL( source, target, signal1, signal2 ) \
	QObject::connect( source, SIGNAL(signal1), target, SIGNAL(signal2) )

class QResourceStream : public std::istream {

	public:
	QResourceStream( const char *resourcePath );
	~QResourceStream() {
		delete rdbuf();
	}

};

namespace UiUtil {

	void clearLayout( QLayout *layout );
	void deleteItemAt( QLayout *layout, int index );
	void deleteItemAt( QGridLayout *layout, int row, int col );
	void freeLayoutItem( QLayoutItem *item );
#ifdef _WIN32
	QString toNativePath( const QString &posixPath );
#else
	inline QString toNativePath( const QString &posixPath ) {
		return posixPath;
	}
#endif

}

#endif /* UI_UTIL_HPP_ */
