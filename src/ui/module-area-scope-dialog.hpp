#ifndef SRC_UI_MODULE_AREA_SCOPE_DIALOG_HPP_
#define SRC_UI_MODULE_AREA_SCOPE_DIALOG_HPP_

#include <QDialog>
#include "src/core/uuid.hpp"

namespace Ui {
	class ModuleAreaScopeDialog;
}

class ModuleAreaScopeDialog : public QDialog {
	Q_OBJECT

	private:
	Ui::ModuleAreaScopeDialog *m_ui;

	public:
	explicit ModuleAreaScopeDialog( QWidget *parent = nullptr );
	~ModuleAreaScopeDialog();

	void open( const Uuid &moduleId );

	public slots:
	void filterChanged();

};



#endif /* SRC_UI_MODULE_AREA_SCOPE_DIALOG_HPP_ */
