#ifndef SRC_UI_OBJECT_CREATE_DIALOG_HPP_
#define SRC_UI_OBJECT_CREATE_DIALOG_HPP_

#include <QDialog>
#include "src/core/enums.hpp"

namespace Ui {
	class ObjectCreateDialog;
}

class ObjectCreateDialog : public QDialog {
	Q_OBJECT

	private:
	Ui::ObjectCreateDialog *m_ui;

	public:
	explicit ObjectCreateDialog( QWidget *parent = nullptr );
	~ObjectCreateDialog();

	void open( LevelId levelId );
	ObjectType getSelection() const;

};


#endif /* SRC_UI_OBJECT_CREATE_DIALOG_HPP_ */
