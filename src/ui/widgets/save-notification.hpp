#ifndef SRC_UI_WIDGETS_SAVE_NOTIFICATION_HPP_
#define SRC_UI_WIDGETS_SAVE_NOTIFICATION_HPP_

#include <QLabel>
#include <QTimer>
#include <QGraphicsOpacityEffect>

class SaveNotification : public QLabel {
	Q_OBJECT

	private:
	QTimer m_timer;
	QGraphicsOpacityEffect m_gfx;
	int m_ticks;

	public:
	SaveNotification( QWidget *parent, const QPoint &startPos );
	virtual ~SaveNotification() {};

	private slots:
	void tick();

};



#endif /* SRC_UI_WIDGETS_SAVE_NOTIFICATION_HPP_ */
