#ifndef SRC_UI_WIDGETS_COLOUR_SWATCH_HPP_
#define SRC_UI_WIDGETS_COLOUR_SWATCH_HPP_

#include <QFrame>
#include "src/core/colour.hpp"

namespace Ui {
	class ColourSwatch;
}

class ColourSwatch : public QFrame {
	Q_OBJECT
	Q_PROPERTY(bool readOnly MEMBER m_readOnly)

	private:
	Ui::ColourSwatch *m_ui;
	ColourRGBA32 m_colour;
	bool m_readOnly;
	bool m_alphaSelect;

	private slots:
	void openPicker();

	signals:
	void colourChanged();
	void colourSelected();

	protected:
	virtual void mousePressEvent( QMouseEvent *event ) override;
	virtual void paintEvent( QPaintEvent *event ) override;

	public:
	explicit ColourSwatch( QWidget *parent = nullptr );
	virtual ~ColourSwatch();

	bool readOnly() const { return m_readOnly; }
	void setReadOnly( bool readOnly ) { m_readOnly = readOnly; update(); }

	bool alphaEnabled() const { return m_alphaSelect; }
	void setAlphaEnabled( bool alpha ) { m_alphaSelect = alpha; }

	const ColourRGBA32 &value() const { return m_colour; }
	void setValue( const ColourRGBA32 &colour );
	inline void setValue( const ColourRGB24 &colour ) { setValue( (ColourRGBA32)colour ); }
	inline void setValue( const ColourRGBA5551 &colour ) { setValue( (ColourRGBA32)colour ); }

};


#endif /* SRC_UI_WIDGETS_COLOUR_SWATCH_HPP_ */
