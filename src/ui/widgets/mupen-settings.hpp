#ifndef SRC_UI_WIDGETS_MUPEN_SETTINGS_HPP_
#define SRC_UI_WIDGETS_MUPEN_SETTINGS_HPP_

#include <QWidget>

namespace Ui {
	class MupenSettingsWidget;
}

class MupenSettingsWidget : public QWidget {
	Q_OBJECT

	private:
	Ui::MupenSettingsWidget *m_ui;

	public:
	explicit MupenSettingsWidget( QWidget *parent = nullptr );
	~MupenSettingsWidget();

	public slots:
	void save() const;
	void load();
	void pluginPathChanged();

};




#endif /* SRC_UI_WIDGETS_MUPEN_SETTINGS_HPP_ */
