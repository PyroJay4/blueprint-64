#ifndef SRC_UI_WIDGETS_MODULE_PROPERTY_SELECT_HPP_
#define SRC_UI_WIDGETS_MODULE_PROPERTY_SELECT_HPP_

#include <QFrame>
#include <QLabel>
#include <QLayout>
#include <functional>
#include "src/core/asm-modules.hpp"

class ModulePropertySelect : public QFrame {
	Q_OBJECT

	private:
	QLabel *m_label;
	QWidget *m_select;
	QLayout *m_sublayout;
	const Uuid m_moduleId;
	const string m_propertyName;
	bool m_noSignal;
	std::function<AsmModulePropertyValue(const QWidget*)> m_getter;
	std::function<void(QWidget*, const AsmModulePropertyValue &value)> m_setter;

	public:
	explicit ModulePropertySelect(
		QWidget *parent,
		const Uuid &moduleId,
		const AsmModuleProperty &property
	);
	virtual ~ModulePropertySelect();

	inline void setValue( const AsmModulePropertyValue &value ) {
		m_noSignal = true;
		m_setter( m_select, value );
		m_noSignal = false;
	}

	private slots:
	void valueChanged() const;

};

#endif /* SRC_UI_WIDGETS_MODULE_PROPERTY_SELECT_HPP_ */
