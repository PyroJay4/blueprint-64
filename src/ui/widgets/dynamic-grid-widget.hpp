#ifndef SRC_UI_WIDGETS_FLOW_LAYOUT_HPP_
#define SRC_UI_WIDGETS_FLOW_LAYOUT_HPP_

#include <QFrame>

class DynamicGridWidget : public QFrame {
	Q_OBJECT

	public:
	explicit DynamicGridWidget( QWidget *parent = nullptr );
	virtual ~DynamicGridWidget();

	int count() const;
	QWidget *widgetAt( int pos );

	void deleteAll();
	void addWidget( QWidget *widget );
	QWidget *takeWidget( QWidget *widget );

	inline void deleteWidget( QWidget *widget ) {
		takeWidget( widget )->deleteLater();
	}

};



#endif /* SRC_UI_WIDGETS_FLOW_LAYOUT_HPP_ */
