#include "src/ui/widgets/save-notification.hpp"

SaveNotification::SaveNotification( QWidget *parent, const QPoint &startPos ) :
	QLabel( "SAVED", parent ),
	m_ticks( 0 )
{
	QPalette colours = palette();
	colours.setColor( backgroundRole(), QColor( 0x00C800 ) );
	colours.setColor( foregroundRole(), QColor( 0xFFFFFF ) );
	setPalette( colours );

	setAutoFillBackground( true );
	setAlignment( Qt::AlignCenter );

	QRect geo = QRect( 0, 0, 50, 20 );
	geo.moveCenter( startPos );
	setGeometry( geo );
	show();

	m_gfx.setParent( this );
	m_gfx.setOpacity( 1.0 );
	setGraphicsEffect( &m_gfx );

	connect( &m_timer, SIGNAL(timeout()), this, SLOT(tick()) );
	m_timer.setInterval( 16 );
	m_timer.start();
}

void SaveNotification::tick() {
	if( m_ticks++ > 60 ) {
		m_timer.stop();
		hide();
		setParent( nullptr );
		deleteLater();
		return;
	}

	if( m_ticks > 40 ) {
		m_gfx.setOpacity( (60.0 - (double)m_ticks) / 20.0 );
	}

	QRect geo = geometry();
	geo.moveTop( geo.top() - 1 );
	setGeometry( geo );
	update();
}
