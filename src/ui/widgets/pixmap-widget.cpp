#include "src/ui/widgets/pixmap-widget.hpp"

#include <QPainter>

void PixmapWidget::forceResize( int width, int height ) {
	setBaseSize( width, height );
	setMinimumSize( width, height );
	setMaximumSize( width, height );
}

void PixmapWidget::setPixmap( const QPixmap &pixmap, bool resize ) {
	m_pixmap = pixmap;
	setEnabled( true );
	if( resize ) {
		forceResize( pixmap.width() + 2, pixmap.height() + 2 );
	}
	update();
}

void PixmapWidget::setPixmap( QPixmap &&pixmap, bool resize ) {
	m_pixmap = pixmap;
	setEnabled( true );
	if( resize ) {
		forceResize( pixmap.width() + 2, pixmap.height() + 2 );
	}
	update();
}

void PixmapWidget::clearPixmap() {
	m_pixmap = QPixmap();
	setEnabled( false );
	update();
}

void PixmapWidget::clearPixmap( int width, int height ) {
	m_pixmap = QPixmap();
	setEnabled( false );
	forceResize( width + 2, height + 2 );
	update();
}

void PixmapWidget::paintEvent( [[maybe_unused]] QPaintEvent *paintEvent ) {
	QPainter p( this );

	QPixmap alphaBackground( 16, 16 );
	alphaBackground.fill( QColor( 0x99, 0x99, 0x99 ) );
	QPainter ap( &alphaBackground );
	ap.fillRect( 0, 0, 8, 8, QColor( 0x66, 0x66, 0x66 ) );
	ap.fillRect( 8, 8, 8, 8, QColor( 0x66, 0x66, 0x66 ) );

	p.drawTiledPixmap( 1, 1, width() - 2, height() - 2, alphaBackground );
	if( !m_pixmap.isNull() ) {
		p.drawPixmap( 1, 1, m_pixmap.width(), m_pixmap.height(), m_pixmap );
	}
	QPen strokePen( Qt::black );
	strokePen.setStyle( Qt::PenStyle::SolidLine );
	strokePen.setWidth( 1 );
	strokePen.setJoinStyle( Qt::PenJoinStyle::MiterJoin );
	p.setPen( strokePen );
	p.drawRect( 0, 0, width() - 1, height() - 1 );
}
