#ifndef SRC_UI_WIDGETS_MULTI_LINE_EDIT_HPP_
#define SRC_UI_WIDGETS_MULTI_LINE_EDIT_HPP_

#include <QPlainTextEdit>

class MultiLineEdit : public QPlainTextEdit {
	Q_OBJECT

	public:
	explicit MultiLineEdit( QWidget *parent ) : QPlainTextEdit( parent ) {}
	virtual ~MultiLineEdit() {}

	signals:
	void editingFinished();

	protected:
	virtual void focusOutEvent( QFocusEvent *event ) override {
		emit editingFinished();
		QPlainTextEdit::focusOutEvent( event );
	}

};

#endif /* SRC_UI_WIDGETS_MULTI_LINE_EDIT_HPP_ */
