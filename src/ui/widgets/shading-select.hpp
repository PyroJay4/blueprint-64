#ifndef SRC_UI_WIDGETS_SHADING_SELECT_HPP_
#define SRC_UI_WIDGETS_SHADING_SELECT_HPP_

#include <QComboBox>
#include "src/core/enums.hpp"
#include "src/ui/widgets/manage-shading-dialog.hpp"

class ShadingSelectWidget : public QComboBox {
	Q_OBJECT

	private:
	LevelId m_level;
	bool m_initializing;
	ManageShadingDialog m_dialog;

	private slots:
	void itemSelected( int newIndex );
	void dialogClosed();

	public:
	ShadingSelectWidget( QWidget *parent = nullptr ) :
		QComboBox( parent ),
		m_level( LevelId::INVALID ),
		m_initializing( false )
	{
		connect( this, SIGNAL(currentIndexChanged(int)), this, SLOT(itemSelected(int)) );
		connect( &m_dialog, SIGNAL(finished(int)), this, SLOT(dialogClosed()) );
		setIconSize( QSize( 32, 16 ) );
	}
	~ShadingSelectWidget() {}

	void setLevel( LevelId levelId );

	void setSelectedShadeId( uint id );
	uint getSelectedShadeId() const;

	signals:
	void shadeSelected( uint );
	void dialogWillOpen();

};


#endif /* SRC_UI_WIDGETS_SHADING_SELECT_HPP_ */
