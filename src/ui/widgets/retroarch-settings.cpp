#include "src/ui/widgets/retroarch-settings.hpp"
#include "ui_retroarch-settings.h"

#include "src/core/storage.hpp"

using namespace Emulator;

RetroarchSettingsWidget::RetroarchSettingsWidget( QWidget *parent ) :
	QWidget( parent ),
	m_ui( new Ui::RetroarchSettingsWidget )
{
	m_ui->setupUi( this );
#ifndef _WIN32
	m_ui->exePathLabel->setVisible( false );
	m_ui->windowsExePath->setVisible( false );
#endif
}

RetroarchSettingsWidget::~RetroarchSettingsWidget() {
	delete m_ui;
}

void RetroarchSettingsWidget::load() {
	const RetroArch::Settings &config = AppPreferences::current().emuConfig.retroSettings;
	m_ui->corePath->setText( config.coreDirPath.c_str() );
	m_ui->emulatorCore->setCurrentIndex( (int)config.core );
	m_ui->cpuEmulation->setCurrentIndex( (int)config.cpuCore );
	m_ui->resolutionScale->setValue( (int)config.resolutionScale );
	m_ui->fullscreen->setChecked( config.fullscreen );
	m_ui->vsync->setChecked( config.vsync );
	m_ui->fullspeed->setChecked( config.fullspeed );
	m_ui->gfxPlugin->setCurrentIndex( (int)config.parallelConfig.gfxPlugin );
	m_ui->overclockVI->setChecked( config.parallelConfig.overclockVI );
#ifdef _WIN32
	m_ui->windowsExePath->setText( config.exePath.c_str() );
#endif
}

void RetroarchSettingsWidget::save() const {
	RetroArch::Settings &config = AppPreferences::current().emuConfig.retroSettings;
	config.coreDirPath = m_ui->corePath->text().toStdString();
	config.core = (RetroArch::EmulatorCore)m_ui->emulatorCore->currentIndex();
	config.cpuCore = (CpuEmulation)m_ui->cpuEmulation->currentIndex();
	config.resolutionScale = (ubyte)m_ui->resolutionScale->value();
	config.fullscreen = m_ui->fullscreen->isChecked();
	config.vsync = m_ui->vsync->isChecked();
	config.fullspeed = m_ui->fullspeed->isChecked();
	config.parallelConfig.gfxPlugin = (RetroArch::GraphicsPlugin)m_ui->gfxPlugin->currentIndex();
	config.parallelConfig.overclockVI = m_ui->overclockVI->isChecked();
#ifdef _WIN32
	config.exePath = m_ui->windowsExePath->text().toStdString();
#endif
}
