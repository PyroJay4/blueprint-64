#ifndef SRC_UI_WIDGETS_OTHER_EMULATOR_SETTINGS_HPP_
#define SRC_UI_WIDGETS_OTHER_EMULATOR_SETTINGS_HPP_

#include <QWidget>

namespace Ui {
	class OtherEmulatorSettingsWidget;
}

class OtherEmulatorSettingsWidget : public QWidget {
	Q_OBJECT

	private:
	Ui::OtherEmulatorSettingsWidget *m_ui;

	public:
	explicit OtherEmulatorSettingsWidget( QWidget *parent = nullptr );
	~OtherEmulatorSettingsWidget();

	public slots:
	void save() const;
	void load();

};


#endif /* SRC_UI_WIDGETS_OTHER_EMULATOR_SETTINGS_HPP_ */
