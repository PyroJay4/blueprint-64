#include "src/ui/drawing-layer-help-dialog.hpp"
#include "ui_drawing-layer-help-dialog.h"

DrawingLayerHelpDialog::DrawingLayerHelpDialog( QWidget *parent ) :
	QDialog( parent ),
	m_ui( new Ui::DrawingLayerHelpDialog )
{
	m_ui->setupUi( this );
}

DrawingLayerHelpDialog::~DrawingLayerHelpDialog() {
	delete m_ui;
}
