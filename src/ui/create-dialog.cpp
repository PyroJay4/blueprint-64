#include "src/ui/create-dialog.hpp"
#include "ui_create-dialog.h"

#include <QFileDialog>
#include "src/ui/util.hpp"

CreateDialog::CreateDialog( QWidget *parent ) :
	QDialog( parent ),
	m_ui( new Ui::CreateDialog )
{
	m_ui->setupUi( this );
}

CreateDialog::~CreateDialog() {
	delete m_ui;
}

void CreateDialog::resetToDefaults() {
	m_ui->projectPathField->clear();
	m_ui->createRomRadio->setChecked( true );
	m_ui->romPathField->clear();
	m_ui->romNameField->setText( "SUPER MARIO 64" );
	m_ui->disableChecksumCheckbox->setChecked( false );
	m_ui->disableDemosCheckbox->setChecked( true );
	m_ui->disableIntroCheckbox->setChecked( true );
	m_ui->removeBordersCheckbox->setChecked( false );
	m_ui->infiniteLivesCheckbox->setChecked( false );
	m_ui->verticalExtendCheckbox->setChecked( true );
	m_ui->bootToSelect->setCurrentIndex( 0 );
	m_ui->levelBoundarySelect->setCurrentIndex( 0 );
	m_ui->patchHandsFreeCheckbox->setChecked( false );
}

void CreateDialog::browseForProject() {
	QString filePath = QFileDialog::getSaveFileName( this, "Save Project As", QString(), "Blueprint Files (*.bbp)" );
	if( filePath.isNull() ) return;
	if( !filePath.endsWith( ".bbp" ) ) {
		filePath += ".bbp";
	}
	m_ui->projectPathField->setText( UiUtil::toNativePath( filePath ) );
}

void CreateDialog::browseForRom() {
	QString filePath = QFileDialog::getSaveFileName( this, "Save ROM As", QString(), "N64 ROMs (*.z64)" );
	if( filePath.isNull() ) return;
	if( !filePath.endsWith( ".z64" ) ) {
		filePath += ".z64";
	}
	m_ui->romPathField->setText( UiUtil::toNativePath( filePath ) );
}

void CreateDialog::validateAll() {
	m_ui->dialogButtons->button( QDialogButtonBox::Save )->setEnabled(
		!m_ui->projectPathField->text().isEmpty() &&
		(m_ui->noRomRadio->isChecked() || !m_ui->romPathField->text().isEmpty() ) &&
		!m_ui->romNameField->text().isEmpty()
	);
}

void CreateDialog::createProject() {
	emit projectCreated({
		m_ui->projectPathField->text().toStdString(),
		m_ui->noRomRadio->isChecked() ? "" : m_ui->romPathField->text().toStdString(),
		m_ui->romNameField->text().toStdString(),
		m_ui->disableChecksumCheckbox->isChecked(),
		m_ui->disableDemosCheckbox->isChecked(),
		m_ui->disableIntroCheckbox->isChecked(),
		m_ui->removeBordersCheckbox->isChecked(),
		m_ui->infiniteLivesCheckbox->isChecked(),
		m_ui->verticalExtendCheckbox->isChecked(),
		(ubyte)m_ui->bootToSelect->currentIndex(),
		(LevelBoundarySize)m_ui->levelBoundarySelect->currentIndex(),
		m_ui->patchHandsFreeCheckbox->isChecked()
	});
	close();
}
