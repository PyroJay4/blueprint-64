#ifndef UI_CREATE_DIALOG_HPP_
#define UI_CREATE_DIALOG_HPP_

#include <QDialog>
#include <string>

#include "src/core/blueprint.hpp"

using std::string;

namespace Ui {
	class CreateDialog;
}

class CreateDialog : public QDialog {
	Q_OBJECT

	private:
	Ui::CreateDialog *m_ui;

	void resetToDefaults();

	public slots:
	void browseForProject();
	void browseForRom();
	void createProject();
	void validateAll();

	signals:
	void projectCreated( CreateProjectOptions options );

	public:
	explicit CreateDialog( QWidget *parent = nullptr );
	~CreateDialog();

	void showEvent( QShowEvent *event ) override {
		QWidget::showEvent( event );
		resetToDefaults();
		validateAll();
	}

};

#endif /* UI_CREATE_DIALOG_HPP_ */
