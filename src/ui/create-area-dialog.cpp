#include "src/ui/create-area-dialog.hpp"
#include "ui_create-area-dialog.h"

#include <cstdio>
#include "src/core/blueprint.hpp"

CreateAreaDialog::CreateAreaDialog( QWidget *parent ) :
	QDialog( parent ),
	m_ui( new Ui::CreateAreaDialog )
{
	m_ui->setupUi( this );
}

CreateAreaDialog::~CreateAreaDialog() {
	delete m_ui;
}

void CreateAreaDialog::setLevel( LevelId levelId ) {
	m_ui->areaSelect->clear();
	char name[7];
	name[6] = '\0';
	for( ubyte i = 0; i < 8; i++ ) {
		if( !Blueprint::current()->hasArea( levelId, i ) ) {
			std::sprintf( name, "Area %d", i );
			m_ui->areaSelect->addItem( name, QVariant::fromValue<uint>( i ) );
		}
	}
	if( m_ui->areaSelect->count() > 1 && m_ui->areaSelect->itemData( 0 ).toUInt() == 0 ) {
		m_ui->areaSelect->setCurrentIndex( 1 );
	} else {
		m_ui->areaSelect->setCurrentIndex( 0 );
	}
}

ubyte CreateAreaDialog::areaIndex() const {
	return (ubyte)m_ui->areaSelect->currentData().toUInt();
}
