#include "src/ui/start-window.hpp"
#include "ui_start-window.h"

#include <QObject>
#include <QFileDialog>
#include <QMessageBox>
#include "src/ui/util.hpp"

StartWindow::StartWindow( QWidget *parent ) :
	QMainWindow( parent ),
	m_ui( new Ui::StartWindow )
{
	m_ui->setupUi( this );
	QObject::connect(
		&m_createDialog, SIGNAL(projectCreated(CreateProjectOptions)),
		this, SIGNAL(projectCreated(CreateProjectOptions))
	);
}

StartWindow::~StartWindow() {
	delete m_ui;
}

void StartWindow::openProject() {
	QString filePath = QFileDialog::getOpenFileName( this, "Open Project", QString(), "Blueprint (*.bbp *.bpc);;All Files (*)" );
	if( !filePath.isEmpty() ) {
		emit projectOpened( UiUtil::toNativePath( filePath ).toStdString() );
	}
}

void StartWindow::showErrorDialog( string message ) {
	if( !isVisible() ) return;
	QMessageBox::critical(
		this,
		QString( "Error Loading Project" ),
		QString( message.c_str() ),
		QMessageBox::Ok
	);
}
