#include "src/ui/star-spawn-dialog.hpp"
#include "ui_star-spawn-dialog.h"

#include "src/core/blueprint.hpp"

StarSpawnDialog::StarSpawnDialog( QWidget *parent ) :
	QDialog( parent ),
	m_ui( new Ui::StarSpawnDialog ),
	m_levelId( LevelId::INVALID )
{
	m_ui->setupUi( this );
	m_ui->starSelector->setCurrentRow( 0 );
}

StarSpawnDialog::~StarSpawnDialog() {
	delete m_ui;
}

void StarSpawnDialog::locationChanged() {
	Vec3f &location = Blueprint::currentEditable()->levels().at( m_levelId ).starSpawnLocations.at( m_ui->starSelector->currentRow() );
	location.x = m_ui->xPosn->value();
	location.y = m_ui->yPosn->value();
	location.z = m_ui->zPosn->value();
}

void StarSpawnDialog::starSelected() {
	if( m_levelId == LevelId::INVALID ) return;
	const Vec3f &location = Blueprint::currentEditable()->levels().at( m_levelId ).starSpawnLocations.at( m_ui->starSelector->currentRow() );
	m_ui->xPosn->setValue( location.x );
	m_ui->yPosn->setValue( location.y );
	m_ui->zPosn->setValue( location.z );
}
