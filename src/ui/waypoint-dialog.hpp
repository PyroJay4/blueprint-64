#ifndef SRC_UI_WAYPOINT_DIALOG_HPP_
#define SRC_UI_WAYPOINT_DIALOG_HPP_

#include <QDialog>
#include "src/core/enums.hpp"

namespace Ui {
	class WaypointDialog;
}

class WaypointDialog : public QDialog {
	Q_OBJECT

	private:
	Ui::WaypointDialog *m_ui;
	LevelId m_levelId;
	int m_numWaypoints;
	int m_waypointId;
	bool m_initializing;

	void loadWaypoints();
	void saveWaypoints() const;

	void removeRow( int rowIndex );
	void addRow( short x, short y, short z );

	public:
	explicit WaypointDialog( QWidget *parent = nullptr );
	~WaypointDialog();

	void open( LevelId levelId );

	public slots:
	void selectedWaypointUser();
	void addedWaypoint() {
		addRow( 0, 0, 0 );
	}

	void accept() override {
		saveWaypoints();
		QDialog::accept();
	}

};

#endif /* SRC_UI_WAYPOINT_DIALOG_HPP_ */
