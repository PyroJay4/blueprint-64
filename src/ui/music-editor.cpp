#include "src/ui/music-editor.hpp"
#include "ui_music-editor.h"

#include <cstdio>
#include <QFileDialog>
#include <QMessageBox>
#include "src/polyfill/filesystem.hpp"
#include "src/core/blueprint.hpp"
#include "src/core/music.hpp"
#include "src/ui/util.hpp"

static const char *const s_bankNames[38] = {
	"00 - SFX Misc 1",
	"01 - SFX Footsteps",
	"02 - SFX Water",
	"03 - SFX Sand",
	"04 - SFX Misc 2",
	"05 - SFX Misc 3",
	"06 - SFX Misc 4",
	"07 - SFX Misc 5",
	"08 - SFX Mario",
	"09 - SFX UI",
	"10 - SFX Voice",
	"11 - Snow Levels",
	"12 - Unused",
	"13 - TTC/RR/Slides",
	"14 - Inside Castle",
	"15 - SLL/LLL",
	"16 - Spooky (HMC)",
	"17 - Title Screen",
	"18 - Bowser Fight",
	"19 - Water Levels",
	"20 - Lullaby",
	"21 - HMC/WDW",
	"22 - Star Select",
	"23 - Wing/Vanish",
	"24 - Metal Cap",
	"25 - Bowser Course",
	"26 - Fanfares",
	"27 - Stage Boss",
	"28 - Infinite Stairs",
	"29 - Final Battle",
	"30 - Peach",
	"31 - Course Clear",
	"32 - Toad",
	"33 - Merry-Go-Round",
	"34 - Main Theme",
	"35 - Ending",
	"36 - File Select",
	"37 - Credits"
};

MusicEditor::MusicEditor( QWidget *parent ) :
	QWidget( parent ),
	m_ui( new Ui::MusicEditor ),
	m_initializing( true )
{
	m_ui->setupUi( this );
	for( int i = 0; i < 38; i++ ) {
		QCheckBox *checkbox = new QCheckBox( this );
		m_sbGroup.addButton( checkbox, i );
		checkbox->setText( s_bankNames[i] );
		m_ui->soundBankGrid->addWidget( checkbox, i % 10, i / 10 );
		connect( checkbox, SIGNAL(toggled(bool)), this, SLOT(soundBanksChanged()) );
	}
	m_sbGroup.setParent( this );
	m_initializing = false;
}

MusicEditor::~MusicEditor() {
	delete m_ui;
}

void MusicEditor::reload() {
	m_initializing = true;
	m_ui->musicList->clear();

	m_music = MusicData::getMusicInfo();
	for( uint i = 1; i < m_music.size(); i++ ) {
		const string displayName = std::to_string( i ) + " - " + m_music[i].name;
		m_ui->musicList->addItem( displayName.c_str() );
	}

	m_ui->musicList->setCurrentRow( 0 );
	m_ui->addMusicButton->setEnabled( m_music.size() < 255 );

	m_initializing = false;
	trackSelected();
}

void MusicEditor::trackSelected() {
	if( m_initializing || m_music.size() < 2 || m_ui->musicList->currentRow() < 0 ) return;

	m_initializing = true;
	const MusicInfo &music = m_music.at( m_ui->musicList->currentRow() + 1 );
	m_ui->musicInfoPane->setEnabled( !music.isPlaceholder && !music.isVanilla );
	m_ui->trackName->setText( music.name.c_str() );
	if( music.isPlaceholder ) {
		m_ui->trackSize->setText( "n/a" );
	} else if( music.numBytes < 1024 ) {
		char sizeStr[11];
		std::sprintf( sizeStr, "%d bytes", music.numBytes );
		m_ui->trackSize->setText( sizeStr );
	} else if( music.numBytes < 1024 * 1024 ) {
		char sizeStr[10];
		std::sprintf( sizeStr, "%.1f kB", (double)music.numBytes / 1024.0 );
		m_ui->trackSize->setText( sizeStr );
	} else {
		char sizeStr[8];
		std::sprintf( sizeStr, "%.2f MB", (double)music.numBytes / (1024.0 * 1024.0) );
		m_ui->trackSize->setText( sizeStr );
	}
	m_ui->multiBankCheckbox->setChecked( music.soundBanks.size() > 1 );
	m_sbGroup.setExclusive( music.soundBanks.size() <= 1 );
	for( QAbstractButton *checkbox : m_sbGroup.buttons() ) {
		checkbox->setChecked( false );
	}
	for( ubyte i : music.soundBanks ) {
		m_sbGroup.button( i )->setChecked( true );
	}

	m_ui->deleteOrRevertButton->setText( music.isVanilla ? "Revert" : "Delete" );
	m_ui->deleteOrRevertButton->setEnabled( !music.isPlaceholder );
	m_initializing = false;
}

void MusicEditor::syncName( ubyte seqId ) {
	const string displayName = std::to_string( (uint)seqId ) + " - " + m_music[seqId].name;
	m_ui->musicList->item( seqId - 1 )->setText( displayName.c_str() );
}

void MusicEditor::multiBankToggled( bool enabled ) {
	if( m_initializing ) return;
	m_sbGroup.setExclusive( !enabled );
	if( !enabled ) {
		std::vector<ubyte> banks = m_music.at( m_ui->musicList->currentRow() + 1 ).soundBanks;
		const ubyte bank = banks.empty() ? 22 : banks.at( 0 );
		for( ubyte i = 0; i < 38; i++ ) {
			m_initializing = true;
			m_sbGroup.button( i )->setChecked( i == bank );
			m_initializing = false;
		}
		banks.clear();
		banks.push_back( bank );
		Blueprint::currentEditable()->customMusic().at( m_ui->musicList->currentRow() + 1 ).soundBanks = banks;
	}
}

void MusicEditor::nameChanged() {
	if( m_initializing ) return;
	const ubyte seqId = m_ui->musicList->currentRow() + 1;
	const string name = m_ui->trackName->text().toStdString();
	m_music.at( seqId ).name = name;
	Blueprint::currentEditable()->customMusic().at( seqId ).name = name;
	syncName( seqId );
}

void MusicEditor::soundBanksChanged() {
	if( m_initializing ) return;

	std::vector<ubyte> usedBanks;
	for( int i = 37; i >= 0; i-- ) {
		if( m_sbGroup.button( i )->isChecked() ) {
			usedBanks.push_back( (ubyte)i );
		}
	}

	if( usedBanks.empty() ) {
		usedBanks.push_back( 22 );
	}

	const ubyte seqId = m_ui->musicList->currentRow() + 1;
	m_music.at( seqId ).soundBanks = usedBanks;
	Blueprint::currentEditable()->customMusic().at( seqId ).soundBanks = std::move( usedBanks );
}

bool MusicEditor::replaceMusicInternal( ubyte seqId ) {
	QString qFilePath = UiUtil::toNativePath( QFileDialog::getOpenFileName( this, "Open Sequence File", QString(), "N64 Sequence (*.m64);;All Files (*)" ) );
	if( qFilePath.isNull() ) return false;

	namespace fs = std::filesystem;
	const fs::path filePath( qFilePath.toStdString() );
	if( !fs::exists( filePath ) ) return false;

	const size_t fileSize = fs::file_size( filePath );
	if( fileSize >= 2 * 1024 * 1024 ) {
		QMessageBox::critical( this, "File too big!", "File must be smaller than 2MB", QMessageBox::Ok );
		return false;
	}

	fs::create_directories( Blueprint::current()->getMusicPath( 0 ).parent_path() );
	fs::tryCopyFileOverwrite( filePath, Blueprint::current()->getMusicPath( seqId ) );
	Blueprint::currentEditable()->customMusic()[seqId] = CustomMusicInfo{
		filePath.stem().string(),
		{ 37 }
	};

	m_music[seqId] = {
		filePath.stem().string(),
		{ 37 },
		(uint)fileSize,
		false,
		false
	};

	return true;
}

void MusicEditor::addMusic() {
	for( ubyte i = 1; i < m_music.size(); i++ ) {
		if( m_music[i].isPlaceholder ) {
			replaceMusicInternal( i );
			syncName( i );
			return;
		}
	}

	m_music.push_back( MusicInfo{ "", {}, 0, false, true });
	if( replaceMusicInternal( (ubyte)(m_music.size() - 1) ) ) {
		m_ui->musicList->addItem( m_music[m_music.size()-1].name.c_str() );
		trackSelected();
	} else {
		m_music.pop_back();
	}
}

void MusicEditor::replaceMusic() {
	const ubyte seqId = m_ui->musicList->currentRow() + 1;
	replaceMusicInternal( seqId );
	syncName( seqId );
	trackSelected();
}

void MusicEditor::revertMusic() {
	const ubyte seqId = (ubyte)m_ui->musicList->currentRow() + 1;
	string confirmMessage;
	if( m_music[seqId].isVanilla ) {
		confirmMessage = "Are you sure to want to revert '" + m_music[seqId].name + "' to vanilla?";
	} else {
		confirmMessage = "Are you sure to want to delete '" + m_music[seqId].name + "'?";
	}

	if( QMessageBox::question( this, "Confirm", confirmMessage.c_str() ) != QMessageBox::Yes ) {
		return;
	}

	Blueprint::currentEditable()->customMusic().erase( seqId );
	reload();
}
