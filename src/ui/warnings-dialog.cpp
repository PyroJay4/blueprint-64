#include "src/ui/warnings-dialog.hpp"
#include "ui_warnings-dialog.h"

using std::string;

WarningsDialog::WarningsDialog( QWidget *parent ) :
	QDialog( parent ),
	m_ui( new Ui::WarningsDialog )
{
	m_ui->setupUi( this );
}

WarningsDialog::~WarningsDialog() {
	delete m_ui;
}

bool WarningsDialog::getResponse(
	const std::string &message,
	const std::vector<std::string> &warnings,
	bool hasRevertButton
) {
	m_ui->message->setText( message.c_str() );
	m_ui->revertButton->setVisible( hasRevertButton );
	m_ui->warningsList->clear();
	for( const string &warning : warnings ) {
		m_ui->warningsList->addItem( warning.c_str() );
		m_ui->warningsList->item( m_ui->warningsList->count() - 1 )->setIcon( QIcon::fromTheme( "emblem-warning" ) );
	}

	return exec() == QDialog::Accepted;
}
