#include "src/ui/tweak-values/switch-tweak-value.hpp"
#include "ui_tweak-value-enum.h"

#include "src/core/blueprint.hpp"

using std::optional;

SwitchTweakValue::SwitchTweakValue( QWidget *parent, const SwitchTweakDefinition *tweak, bool singular ) :
	TweakValueWidget( parent, tweak, singular ),
	m_ui( new Ui::EnumTweakValue )
{
	m_ui->setupUi( this );
	m_ui->nameLabel->setText( singular ? "Value:" : tweak->getName().c_str() );
	for( const CasePatch &c : tweak->getCases() ) {
		m_ui->comboBox->addItem( c.name.c_str() );
	}
	refreshValue();
}

SwitchTweakValue::~SwitchTweakValue() {
	delete m_ui;
}

bool SwitchTweakValue::isDefault() const {
	return m_ui->comboBox->currentIndex() == (int)static_cast<const SwitchTweakDefinition*>( m_tweak )->getDefaultCaseIndex();
}

Word SwitchTweakValue::value() const {
	return Word::fromUInt( (uint)m_ui->comboBox->currentIndex() );
}

void SwitchTweakValue::refreshValue() {
	optional<Word> currentValue = Blueprint::current()->tryGetTweakValue( m_tweak->getUniqueId() );
	m_ui->comboBox->setCurrentIndex(
		currentValue.has_value() ?
			(int)currentValue.value().asUInt() :
			(int)static_cast<const SwitchTweakDefinition*>( m_tweak )->getDefaultCaseIndex()
	);
}
