#include "src/ui/tweak-values/float-tweak-value.hpp"
#include "ui_tweak-value-float.h"

#include "src/core/blueprint.hpp"

using std::optional;

FloatTweakValue::FloatTweakValue( QWidget *parent, const FloatTweakDefinition *tweak, bool singular ) :
	TweakValueWidget( parent, tweak, singular ),
	m_ui( new Ui::FloatTweakValue )
{
	m_ui->setupUi( this );
	m_ui->nameLabel->setText( singular ? "Value:" : tweak->getName().c_str() );
	m_ui->spinner->setMinimum( tweak->getMinValue() );
	m_ui->spinner->setMaximum( tweak->getMaxValue() );
	m_ui->spinner->setDecimals( tweak->getDecimalPrecision() );
	refreshValue();
}

FloatTweakValue::~FloatTweakValue() {
	delete m_ui;
}

bool FloatTweakValue::isDefault() const {
	return (float)m_ui->spinner->value() == static_cast<const FloatTweakDefinition*>( m_tweak )->getDefaultValue();
}

Word FloatTweakValue::value() const {
	return Word::fromFloat( (float)m_ui->spinner->value() );
}

void FloatTweakValue::refreshValue() {
	optional<Word> currentValue = Blueprint::current()->tryGetTweakValue( m_tweak->getUniqueId() );
	if( currentValue.has_value() ) {
		m_ui->spinner->setValue( currentValue.value().asFloat() );
	} else {
		m_ui->spinner->setValue( static_cast<const FloatTweakDefinition*>( m_tweak )->getDefaultValue() );
	}
}
