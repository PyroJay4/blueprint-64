#include "src/ui/tweak-values/enum-tweak-value.hpp"
#include "ui_tweak-value-enum.h"

#include "src/core/blueprint.hpp"

using std::optional;

EnumTweakValue::EnumTweakValue( QWidget *parent, const EnumTweakDefinition *tweak, bool singular ) :
	TweakValueWidget( parent, tweak, singular ),
	m_ui( new Ui::EnumTweakValue )
{
	m_ui->setupUi( this );
	m_ui->nameLabel->setText( singular ? "Value:" : tweak->getName().c_str() );
	for( const EnumEntry &e : tweak->getPossibleValues() ) {
		m_ui->comboBox->addItem( e.name.c_str(), e.value );
	}
	refreshValue();
}

EnumTweakValue::~EnumTweakValue() {
	delete m_ui;
}

bool EnumTweakValue::isDefault() const {
	return m_ui->comboBox->currentData().toUInt() == static_cast<const EnumTweakDefinition*>( m_tweak )->getDefaultValue().value;
}

Word EnumTweakValue::value() const {
	return Word::fromUInt( m_ui->comboBox->currentData().toUInt() );
}

static void setSelected( QComboBox *comboBox, uint value ) {
	for( int i = 0; i < comboBox->count(); i++ ) {
		if( comboBox->itemData( i ).toUInt() == value ) {
			comboBox->setCurrentIndex( i );
			break;
		}
	}
}

void EnumTweakValue::refreshValue() {
	optional<Word> currentValue = Blueprint::current()->tryGetTweakValue( m_tweak->getUniqueId() );
	if( currentValue.has_value() ) {
		setSelected( m_ui->comboBox, currentValue.value().asUInt() );
	} else {
		setSelected( m_ui->comboBox, static_cast<const EnumTweakDefinition*>( m_tweak )->getDefaultValue().value );
	}
}
