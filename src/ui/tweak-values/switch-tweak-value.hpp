#ifndef SRC_UI_TWEAK_VALUES_SWITCH_TWEAK_VALUE_HPP_
#define SRC_UI_TWEAK_VALUES_SWITCH_TWEAK_VALUE_HPP_

#include "src/ui/tweak-values/tweak-value-widget.hpp"

namespace Ui {
	class EnumTweakValue;
}

class SwitchTweakValue final : public TweakValueWidget {

	private:
	Ui::EnumTweakValue *m_ui;

	public:
	explicit SwitchTweakValue( QWidget *parent, const SwitchTweakDefinition *tweak, bool singular );
	~SwitchTweakValue();

	virtual bool isDefault() const;
	virtual Word value() const;
	virtual void refreshValue();

};



#endif /* SRC_UI_TWEAK_VALUES_SWITCH_TWEAK_VALUE_HPP_ */
