#ifndef SRC_UI_TWEAK_VALUES_INTEGER_TWEAK_VALUE_HPP_
#define SRC_UI_TWEAK_VALUES_INTEGER_TWEAK_VALUE_HPP_

#include "src/ui/tweak-values/tweak-value-widget.hpp"

namespace Ui {
	class IntegerTweakValue;
}

class IntegerTweakValue final : public TweakValueWidget {

	private:
	Ui::IntegerTweakValue *m_ui;

	public:
	explicit IntegerTweakValue( QWidget *parent, const IntegerTweakDefinition *tweak, bool singular );
	~IntegerTweakValue();

	virtual bool isDefault() const;
	virtual Word value() const;
	virtual void refreshValue();

};

#endif /* SRC_UI_TWEAK_VALUES_INTEGER_TWEAK_VALUE_HPP_ */
