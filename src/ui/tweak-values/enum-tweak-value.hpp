#ifndef SRC_UI_TWEAK_VALUES_ENUM_TWEAK_VALUE_HPP_
#define SRC_UI_TWEAK_VALUES_ENUM_TWEAK_VALUE_HPP_

#include "src/ui/tweak-values/tweak-value-widget.hpp"

namespace Ui {
	class EnumTweakValue;
}

class EnumTweakValue final : public TweakValueWidget {

	private:
	Ui::EnumTweakValue *m_ui;

	public:
	explicit EnumTweakValue( QWidget *parent, const EnumTweakDefinition *tweak, bool singular );
	~EnumTweakValue();

	virtual bool isDefault() const;
	virtual Word value() const;
	virtual void refreshValue();

};

#endif /* SRC_UI_TWEAK_VALUES_ENUM_TWEAK_VALUE_HPP_ */
