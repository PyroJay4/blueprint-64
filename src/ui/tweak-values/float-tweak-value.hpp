#ifndef SRC_UI_TWEAK_VALUES_FLOAT_TWEAK_VALUE_HPP_
#define SRC_UI_TWEAK_VALUES_FLOAT_TWEAK_VALUE_HPP_

#include "src/ui/tweak-values/tweak-value-widget.hpp"

namespace Ui {
	class FloatTweakValue;
}

class FloatTweakValue final : public TweakValueWidget {

	private:
	Ui::FloatTweakValue *m_ui;

	public:
	explicit FloatTweakValue( QWidget *parent, const FloatTweakDefinition *tweak, bool singular );
	~FloatTweakValue();

	virtual bool isDefault() const;
	virtual Word value() const;
	virtual void refreshValue();

};



#endif /* SRC_UI_TWEAK_VALUES_FLOAT_TWEAK_VALUE_HPP_ */
