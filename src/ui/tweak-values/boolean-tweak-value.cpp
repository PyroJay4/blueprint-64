#include "src/ui/tweak-values/boolean-tweak-value.hpp"
#include "ui_tweak-value-bool.h"

#include "src/core/blueprint.hpp"

BooleanTweakValue::BooleanTweakValue( QWidget *parent, const BasicTweakDefinition *tweak ) :
	TweakValueWidget( parent, tweak, false ),
	m_ui( new Ui::BooleanTweakValue )
{
	m_ui->setupUi( this );
	string tweakName = tweak->getName();
	for( size_t i = 0; i < tweakName.length(); i++ ) {
		if( tweakName[i] == '&' ) {
			tweakName.insert( i++, 1, '&' );
		}
	}
	m_ui->checkbox->setText( tweakName.c_str() );
	refreshValue();
}

BooleanTweakValue::~BooleanTweakValue() {
	delete m_ui;
}

bool BooleanTweakValue::isDefault() const {
	return !m_ui->checkbox->isChecked();
}

Word BooleanTweakValue::value() const {
	return Word::fromUInt( m_ui->checkbox->isChecked() ? 1 : 0 );
}

void BooleanTweakValue::refreshValue() {
	m_ui->checkbox->setChecked( Blueprint::current()->usingTweak( m_tweak->getUniqueId() ) );
}
