#include "src/ui/tweak-values/colour-tweak-value.hpp"
#include "ui_tweak-value-colour.h"

#include "src/core/blueprint.hpp"

using std::optional;

ColourTweakValue::ColourTweakValue( QWidget *parent, const ColourTweakDefinition *tweak, bool singular ) :
	TweakValueWidget( parent, tweak, singular ),
	m_ui( new Ui::ColourTweakValue )
{
	m_ui->setupUi( this );
	m_ui->nameLabel->setText( singular ? "Value:" : tweak->getName().c_str() );
	m_ui->colour->setValue( tweak->getDefaultColour() );
	m_ui->colour->setAlphaEnabled( tweak->supportsAlpha() );
	refreshValue();
}

ColourTweakValue::~ColourTweakValue() {
	delete m_ui;
}

bool ColourTweakValue::isDefault() const {
	return m_ui->colour->value() == static_cast<const ColourTweakDefinition*>( m_tweak )->getDefaultColour();
}

Word ColourTweakValue::value() const {
	return Word::fromUInt( m_ui->colour->value().toInt() );
}

void ColourTweakValue::refreshValue() {
	optional<Word> currentValue = Blueprint::current()->tryGetTweakValue( m_tweak->getUniqueId() );
	if( currentValue.has_value() ) {
		m_ui->colour->setValue( ColourRGBA32( currentValue.value().asUInt() ) );
	} else {
		m_ui->colour->setValue( static_cast<const ColourTweakDefinition*>( m_tweak )->getDefaultColour() );
	}
}
