#ifndef SRC_UI_TWEAK_VALUES_BOOLEAN_TWEAK_VALUE_HPP_
#define SRC_UI_TWEAK_VALUES_BOOLEAN_TWEAK_VALUE_HPP_

#include "src/ui/tweak-values/tweak-value-widget.hpp"

namespace Ui {
	class BooleanTweakValue;
}

class BooleanTweakValue final : public TweakValueWidget {

	private:
	Ui::BooleanTweakValue *m_ui;

	public:
	explicit BooleanTweakValue( QWidget *parent, const BasicTweakDefinition *tweak );
	~BooleanTweakValue();

	virtual bool isDefault() const;
	virtual Word value() const;
	virtual void refreshValue();

};

#endif /* SRC_UI_TWEAK_VALUES_BOOLEAN_TWEAK_VALUE_HPP_ */
