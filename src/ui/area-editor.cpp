#include "src/ui/area-editor.hpp"
#include "ui_area-editor.h"

#include <cassert>
#include <QMessageBox>
#include "src/ui/model-import-helper.hpp"
#include "src/polyfill/filesystem.hpp"
#include "src/core/blueprint.hpp"
#include "src/core/import/texture.hpp"
#include "src/core/import/model-importer.hpp"
#include "src/core/exceptions.hpp"
#include "src/core/music.hpp"

AreaEditor::AreaEditor( QWidget *parent ) :
	QWidget( parent ),
	m_ui( new Ui::AreaEditor ),
	m_initializing( true ),
	m_level( LevelId::INVALID ),
	m_area( 0 )
{
	m_ui->setupUi( this );
	for( CameraPreset camera : Enum::values<CameraPreset>() ) {
		m_ui->cameraSelect->addItem( Enum::toString<CameraPreset>( camera ) );
	}
	m_initializing = false;

	connect( &m_importDialog, SIGNAL(modelImported(ModelImportedEvent)), this, SLOT(modelImported(ModelImportedEvent)) );
	connect( m_ui->importButton, SIGNAL(clicked()), &m_importDialog, SLOT(open()) );
	connect( m_ui->editButton, SIGNAL(clicked()), this, SIGNAL(openModelEditor()) );
	connect( m_ui->editWaterButton, SIGNAL(clicked()), this, SIGNAL(openWaterEditor()) );
}

AreaEditor::~AreaEditor() {
	delete m_ui;
}

struct ClippingPlanes {
	ushort near;
	ushort far;
};

static const ClippingPlanes s_presetClippingPlanes[6] = {
	{ 64, 8196 },
	{ 100, 12800 },
	{ 100, 20000 },
	{ 128, 32767 },
	{ 175, 45000 },
	{ 256, 65535 }
};

template<typename E> static inline int reverseLookup( E val ) {
	int i = 0;
	for( E x : Enum::values<E>() ) {
		if( x == val ) return i;
		i++;
	}
	return 0;
}

static void syncMusicSelect( QComboBox *musicSelect, ubyte areaMusic ) {
	uint seqId = 0;
	uint selectedIndex = 0;

	musicSelect->clear();
	musicSelect->addItem( "None", QVariant::fromValue<uint>( 0 ) );
	for( const MusicInfo &music : MusicData::getMusicInfo() ) {
		if( !music.isPlaceholder && seqId > 0) {
			musicSelect->addItem( music.name.c_str(), QVariant::fromValue<uint>( seqId ) );
			if( seqId == areaMusic ) {
				selectedIndex = musicSelect->count() - 1;
			}
		}
		seqId++;
	}

	musicSelect->setCurrentIndex( selectedIndex );
}

static double depthToDistance( double depth, const AreaSpecs &areaSpecs ) {
	const double near = (double)areaSpecs.nearClippingPlane;
	const double far = (double)areaSpecs.farClippingPlane;

	const double k = far - (depth * (far - near));
	return (k == 0) ? 0 : (near * far / k);
}

void AreaEditor::setArea( LevelId levelId, ubyte areaIndex ) {
	m_initializing = true;
	m_level = levelId;
	m_area = areaIndex;

	assert( Blueprint::current()->hasArea( levelId, areaIndex ) );
	const AreaSpecs &area = Blueprint::current()->areas().find( AREA_ID( levelId, areaIndex ) )->second;

	syncMusicSelect( m_ui->musicSelect, area.music );
	m_ui->terrainSelect->setCurrentIndex( (int)area.terrainType );
	m_ui->envfxSelect->setCurrentIndex( reverseLookup( area.environmentEffect ) );
	m_ui->echoSelect->setEnabled( areaIndex == 1 || areaIndex == 2 );
	m_ui->echoSelect->setValue( (int)area.echoLevel );
	m_ui->fogSettings->setChecked( area.fog.enabled );
	m_ui->fogColourSelect->setValue( area.fog.colour );
	m_ui->fogStartSelect->setValue( area.fog.fadeStartDistance );
	m_ui->fogEndSelect->setValue( area.fog.fadeEndDistance );
	m_ui->numObjectsSelect->setValue( area.numObjects );
	m_ui->numWarpsSelect->setValue( area.numWarps );
	m_ui->numInstantWarpsSelect->setValue( area.numInstantWarps );
	m_ui->cameraSelect->setCurrentIndex( reverseLookup( area.cameraType ) );
	m_ui->fovSelect->setValue( area.fieldOfView );
	m_ui->fixedPauseCamCheckbox->setChecked( area.pauseCamera.fixed );
	m_ui->pauseCamSettings->setEnabled( area.pauseCamera.fixed );
	m_ui->pauseCamX->setValue( area.pauseCamera.position.x );
	m_ui->pauseCamY->setValue( area.pauseCamera.position.y );
	m_ui->pauseCamZ->setValue( area.pauseCamera.position.z );
	m_ui->pauseCamFocusX->setValue( area.pauseCamera.focus.x );
	m_ui->pauseCamFocusY->setValue( area.pauseCamera.focus.y );
	m_ui->pauseCamFocusZ->setValue( area.pauseCamera.focus.z );
	m_ui->clippingPlanesSelect->setCurrentIndex( 6 );
	m_ui->customClippingPlanes->setEnabled( true );
	for( int i = 0; i < 6; i++ ) {
		if(
			area.nearClippingPlane == s_presetClippingPlanes[i].near &&
			area.farClippingPlane == s_presetClippingPlanes[i].far
		) {
			m_ui->clippingPlanesSelect->setCurrentIndex( i );
			m_ui->customClippingPlanes->setEnabled( false );
			break;
		}
	}
	m_ui->nearClipSelect->setValue( area.nearClippingPlane );
	m_ui->farClipSelect->setValue( area.farClippingPlane );

	int customBackgroundIndex = 0;
	m_ui->customBackgroundSelect->clear();
	if( Blueprint::current()->customBackgrounds().empty() ) {
		m_ui->customBackgroundSelect->addItem( "No Custom Backgrounds" );
		m_ui->customBackgroundRadio->setEnabled( false );
		m_ui->customBackgroundSelect->setEnabled( false );
	} else for( const auto &i : Blueprint::current()->customBackgrounds() ) {
		m_ui->customBackgroundSelect->addItem( i.second.c_str(), QVariant::fromValue<uint>( (uint)i.first ) );
		m_ui->customBackgroundRadio->setEnabled( true );
		m_ui->customBackgroundSelect->setEnabled( true );
		if( i.first + 10 == (ubyte)area.backgroundImage ) {
			customBackgroundIndex = m_ui->customBackgroundSelect->count() - 1;
		}
	}
	m_ui->backgroundColour->setValue( area.backgroundColour );
	if( area.backgroundImage == BackgroundImage::None ) {
		m_ui->solidBackgroundRadio->setChecked( true );
		m_ui->backgroundImageSelect->setCurrentIndex( 0 );
		m_ui->customBackgroundSelect->setCurrentIndex( 0 );
		m_ui->backgroundColour->setEnabled( true );
		m_ui->backgroundImageSelect->setEnabled( false );
		m_ui->customBackgroundSelect->setEnabled( false );
	} else if( area.backgroundImage >= BackgroundImage::CustomBackgroundStart ) {
		m_ui->customBackgroundRadio->setChecked( true );
		m_ui->backgroundImageSelect->setCurrentIndex( 0 );
		m_ui->customBackgroundSelect->setCurrentIndex( customBackgroundIndex );
		m_ui->backgroundColour->setEnabled( false );
		m_ui->backgroundImageSelect->setEnabled( false );
		m_ui->customBackgroundSelect->setEnabled( true );
	} else {
		m_ui->imageBackgroundRadio->setChecked( true );
		m_ui->backgroundImageSelect->setCurrentIndex( (int)area.backgroundImage );
		m_ui->customBackgroundSelect->setCurrentIndex( 0 );
		m_ui->backgroundColour->setEnabled( false );
		m_ui->backgroundImageSelect->setEnabled( true );
		m_ui->customBackgroundSelect->setEnabled( false );
	}

	m_ui->deathPlaneCheckbox->setChecked( area.autoDeathFloor );
	m_ui->killHeightFrame->setEnabled( area.autoDeathFloor );
	m_ui->killHeightSelect->setValue( area.voidHeight );

	static const Uuid VERTICAL_EXTEND_TWEAK_ID = Uuid::parse( "df1bc311-561b-438e-b759-39351251b9c2" );
	m_ui->killHeightSelect->setMinimum( Blueprint::current()->usingTweak( VERTICAL_EXTEND_TWEAK_ID ) ? -27959 : -8951 );
	m_ui->editButton->setEnabled( Blueprint::current()->areaHasGeometry( levelId, areaIndex) );

	m_ui->fogStartUnits->setText( std::to_string( (int)depthToDistance( area.fog.fadeStartDistance, area ) ).c_str() );
	m_ui->fogEndUnits->setText( std::to_string( (int)depthToDistance( area.fog.fadeEndDistance, area ) ).c_str() );

	m_ui->manageObjectsButton->setEnabled( Blueprint::current()->hasTargetRom() );

	m_initializing = false;
}

void AreaEditor::clippingPlanePresetChanged() {
	if( m_initializing ) return;
	const uint preset = m_ui->clippingPlanesSelect->currentIndex();
	if( preset < 6 ) {
		m_ui->customClippingPlanes->setEnabled( false );
		m_ui->nearClipSelect->setValue( s_presetClippingPlanes[preset].near );
		m_ui->farClipSelect->setValue( s_presetClippingPlanes[preset].far );
		areaDataChanged();
	} else {
		m_ui->customClippingPlanes->setEnabled( true );
	}
}

void AreaEditor::areaDataChanged() {
	if( m_initializing ) return;

	assert( Blueprint::current()->hasArea( m_level, m_area ) );
	AreaSpecs &area = Blueprint::currentEditable()->areas()[AREA_ID( m_level, m_area )];

	area.terrainType = (TerrainType)m_ui->terrainSelect->currentIndex();
	area.environmentEffect = Enum::values<EnvironmentEffect>().at( m_ui->envfxSelect->currentIndex() );
	area.music = (ubyte)m_ui->musicSelect->currentData( Qt::UserRole ).toUInt();
	area.echoLevel = (ubyte)m_ui->echoSelect->value();
	area.fog.enabled = m_ui->fogSettings->isChecked();
	area.fog.colour = (ColourRGB24)m_ui->fogColourSelect->value();
	area.fog.fadeStartDistance = m_ui->fogStartSelect->value();
	area.fog.fadeEndDistance = m_ui->fogEndSelect->value();
	area.numObjects = (ubyte)m_ui->numObjectsSelect->value();
	area.numWarps = (ubyte)m_ui->numWarpsSelect->value();
	area.numInstantWarps = (ubyte)m_ui->numInstantWarpsSelect->value();
	area.cameraType = Enum::values<CameraPreset>().at( m_ui->cameraSelect->currentIndex() );
	area.fieldOfView = (short)m_ui->fovSelect->value();
	area.pauseCamera.fixed = m_ui->fixedPauseCamCheckbox->isChecked();
	area.pauseCamera.position.x = (short)m_ui->pauseCamX->value();
	area.pauseCamera.position.y = (short)m_ui->pauseCamY->value();
	area.pauseCamera.position.z = (short)m_ui->pauseCamZ->value();
	area.pauseCamera.focus.x = (short)m_ui->pauseCamFocusX->value();
	area.pauseCamera.focus.y = (short)m_ui->pauseCamFocusY->value();
	area.pauseCamera.focus.z = (short)m_ui->pauseCamFocusZ->value();
	area.nearClippingPlane = (ushort)m_ui->nearClipSelect->value();
	area.farClippingPlane = (ushort)m_ui->farClipSelect->value();
	area.backgroundColour = (ColourRGBA5551)m_ui->backgroundColour->value();
	area.autoDeathFloor = m_ui->deathPlaneCheckbox->isChecked();
	area.voidHeight = (short)m_ui->killHeightSelect->value();

	if( m_ui->solidBackgroundRadio->isChecked() ) {
		area.backgroundImage = BackgroundImage::None;
	} else if( m_ui->imageBackgroundRadio->isChecked() ) {
		area.backgroundImage = (BackgroundImage)m_ui->backgroundImageSelect->currentIndex();
	} else {
		area.backgroundImage = (BackgroundImage)((uint)BackgroundImage::CustomBackgroundStart + m_ui->customBackgroundSelect->currentData( Qt::UserRole ).toUInt());
	}

	m_ui->fogStartUnits->setText( std::to_string( (int)depthToDistance( area.fog.fadeStartDistance, area ) ).c_str() );
	m_ui->fogEndUnits->setText( std::to_string( (int)depthToDistance( area.fog.fadeEndDistance, area ) ).c_str() );

	m_initializing = false;
}

void AreaEditor::manageObjectPlacements() {
	m_objectPlacementDialog.open( m_level, m_area );
}

void AreaEditor::modelImported( ModelImportedEvent event ) {
	if( !AreaModelImportHelper( this, m_level, m_area ).handleImport( event ).isEmpty() ) {
		emit openModelEditor();
		m_ui->editButton->setEnabled( true );
	}
}

static const char *s_fogHelpString =
	"The distances given here are the normalized depth values, with 0 being the distance to the near clipping plane, and 1 being"
	" the distance to the far clipping plane. This is not a linear scale since objects nearer to the camera span more depth"
	" values than those farther away, as they require more precision. The numbers in brackets to the right of the spinners give"
	" the distances in world co-ordinates for your current clipping planes. You can use these to find appropriate values to use."
	" Also note that fog intensity scales linearly with eye coordinates, not world coordinates.";

void AreaEditor::fogHelpClicked() {
	QMessageBox::information( this, "About Fog", s_fogHelpString, QMessageBox::Ok );
}
