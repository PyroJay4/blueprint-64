#include "src/core/armips.hpp"

#include <fstream>
#include <regex>
#include <iterator>
#include <cstdio>
#include <QFile>
#include "src/polyfill/appdata.hpp"
#include "src/polyfill/filesystem.hpp"
#include "src/polyfill/exec.hpp"
#include "src/core/blueprint.hpp"
#include "src/core/util.hpp"
#include "src/core/storage.hpp"
#include "src/core/exceptions.hpp"
#include "src/core/checksum.hpp"
#include "src/ui/util.hpp"
#include "src/openmode.hpp"

namespace fs = std::filesystem;

static const std::regex s_romNameReplacer(
	"\\$\\{ROM_NAME\\}",
	std::regex_constants::ECMAScript | std::regex_constants::optimize
);

static const std::regex s_asmDirReplacer(
	"\\$\\{ASM_DIR\\}",
	std::regex_constants::ECMAScript | std::regex_constants::optimize
);

#ifdef _WIN32
	static const string ARMIPS_EXE = "armips.exe";
#else
	static const string ARMIPS_EXE = "armips";
#endif

bool Armips::exists() {
	const fs::path binPath = fs::path( AppData::dataDir() ) / ARMIPS_EXE;
	if( !fs::exists( binPath ) ) {
		return false;
	}

	// The old bundled version had a bug with .loadtable, so it needs to be updated
#ifndef _WIN32
	static const md5sum badVersion = { 0xB7, 0x94, 0xBC, 0x70, 0x3B, 0xA9, 0xC4, 0xB0, 0x0C, 0xAD, 0x11, 0x73, 0x00, 0x29, 0x1E, 0xB3 };
	std::ifstream file( binPath.string(), mode::read );
	if( getChecksumMD5( file ) == badVersion ) {
		file.close();
		fs::forceDelete( binPath );
		return false;
	}
#endif

	return true;
}

static const char *const s_skeletons[] = {
	"defines.asm",
	"on-game-start.asm",
	"on-mario-step-begin.asm",
	"on-mario-step-end.asm",
	"on-frame-end.asm",
	"on-hud-render.asm",
	"on-level-load.asm",
	"on-area-load.asm",
	"misc-injected.asm",
	"behaviour-script-linkers.asm",
	"global.asm"
};
static constexpr size_t s_numSkeletons = sizeof( s_skeletons ) / sizeof( const char* );

static const char s_referencesHeader[] = ""
	"/*\n"
	"This file was autogenerated by Bowser's Blueprint. Do not modify the file, as\n"
	"contents will be overwritten next time you build.\n"
	"\n"
	"This file contains labels referencing data generated by Bowser's Blueprints.\n"
	"*/";

void Armips::autogenerateWithoutReferences() {
	const string romName = Blueprint::current()->targetRomName();

	const fs::path asmDir = Blueprint::current()->getAsmDirectory();
	const fs::path asmFilePath = Blueprint::current()->getAsmFilePath();
	fs::create_directory( asmDir );

	QFile masterInput( ":/asm/master.asm" );
	masterInput.open( QIODevice::ReadOnly );

	const string masterAsm = std::regex_replace(
		std::regex_replace(
			QString( masterInput.readAll() ).toStdString(),
			s_romNameReplacer,
			romName
		),
		s_asmDirReplacer,
		asmDir.stem().string()
	);
	std::ofstream masterOutput( asmFilePath.c_str(), std::ios_base::out | std::ios_base::trunc );
	masterOutput.write( masterAsm.c_str(), masterAsm.length() );

	for( size_t i = 0; i < s_numSkeletons; i++ ) {
		const fs::path filePath = asmDir / s_skeletons[i];
		if( !fs::exists( filePath ) ) {
			QResourceStream input( (string(":/asm/") + s_skeletons[i]).c_str() );
			std::ofstream output( filePath.string(), std::ios_base::out | std::ios_base::trunc );
			while( input.peek() != std::char_traits<char>::eof() ) output.put( input.get() );
		}
	}

	if( !fs::exists( asmDir / "bb-refs.asm" ) ) {
		std::ofstream refsFile( (asmDir / "bb-refs.asm").string(), std::ios_base::out );
		refsFile << s_referencesHeader << std::endl << std::flush;
	}

	clearReadOnlyFlagOnWindows( asmDir );
}

void Armips::autogenerate( AsmReferences asmReferences ) {
	const fs::path asmDir = Blueprint::current()->getAsmDirectory();

	autogenerateWithoutReferences();
	std::ofstream refsFile( (asmDir / "bb-refs.asm").string(), std::ios_base::out | std::ios_base::trunc );
	refsFile << s_referencesHeader << std::endl << std::endl;
	for( const auto &i : asmReferences ) {
		if( i.second.empty() ) continue;
		refsFile << "; Level " << (uint)i.first << " (" + Blueprint::current()->levels().at( i.first ).name << ")" << std::endl;
		for( const auto &j : i.second ) {
			char hexAddress[11];
			std::sprintf( hexAddress, "0x%X", j.second );
			refsFile << "bb_level_" << (int)i.first << "_" << j.first << " equ " << hexAddress << std::endl;
		}
		refsFile << std::endl;
	}
	refsFile << std::flush;
}

void Armips::build() {
	fs::create_directories( fs::path( AppData::dataDir() ) );
	const string armipsCmd = quoteAndEscape( (fs::path( AppData::dataDir() ) / ARMIPS_EXE ).string() );

	string cmd, workingDirectory;
	if( Blueprint::current()->isCollab() ) {
		const fs::path collabDir = Blueprint::current()->getAsmDirectory().parent_path();
		workingDirectory = collabDir.string();
		cmd = armipsCmd + " " + quoteAndEscape( collabDir.stem().string() + ".asm" );
	} else {
		const fs::path romPath = Blueprint::current()->targetRomPath();
		workingDirectory = romPath.parent_path().string();
		cmd = armipsCmd + " " + quoteAndEscape( romPath.stem().string() + ".asm" );
	}

	string armipsOutput;
	if( !Command::execSync( cmd, workingDirectory, armipsOutput ) ) {
		throw ArmipsException( armipsOutput );
	}
}

static const std::regex s_hexAddr_regex(
	"^0x[0-9A-F]{1,8}$",
	std::regex_constants::ECMAScript | std::regex_constants::optimize
);

static const std::regex s_label_regex(
	"^bb_level_(\\d{1,2})_(\\S+)$",
	std::regex_constants::ECMAScript | std::regex_constants::optimize
);

AsmReferences Armips::getCurrentAsmReferences() {
	const fs::path asmRefFilePath = Blueprint::current()->getAsmDirectory() / "bb-refs.asm";

	AsmReferences references;
	if( !fs::exists( asmRefFilePath ) ) {
		return references;
	}

	std::ifstream fileStream( asmRefFilePath.string(), std::ios_base::in );
	while( fileStream.good() && !fileStream.eof() ) {
		string lineStr;
		std::getline( fileStream, lineStr );
		if( lineStr.empty() ) {
			continue;
		}

		string label, equ, addr;
		std::istringstream line( lineStr );
		line >> label >> equ >> addr;

		std::smatch matches;
		if(
			equ != "equ" ||
			!std::regex_match( addr, s_hexAddr_regex ) ||
			!std::regex_match( addr, matches, s_label_regex ) ||
			matches.size() < 3
		) continue;

		const LevelId level = (LevelId)std::stoi( matches[1] );
		const string ref = matches[2];
		const uint value = (uint)std::stol( addr.substr( 2 ), nullptr, 16 );

		references[level][ref] = value;
	}

	return references;
}
