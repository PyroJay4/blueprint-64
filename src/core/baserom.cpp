#include "src/core/baserom.hpp"

#include <fstream>
#include "src/polyfill/appdata.hpp"
#include "src/openmode.hpp"

namespace fs = std::filesystem;

const fs::path &BaseRom::filePath() {
	static const fs::path s_path = fs::path( AppData::cacheDir() ) / "baserom.z64";
	return s_path;
}

bool BaseRom::matches( std::istream &rom, uint bytes ) {
	std::ifstream baseRom( BaseRom::filePath().string(), mode::read );
	baseRom.seekg( rom.tellg() );
	for( uint i = 0; i < bytes; i++ ) {
		if( rom.get() != baseRom.get() ) {
			return false;
		}
	}
	return rom.good() && baseRom.good();
}

void BaseRom::revert( std::ostream &rom, uint bytes ) {
	std::ifstream baseRom( BaseRom::filePath().string(), mode::read );
	baseRom.seekg( rom.tellp() );
	for( uint i = 0; i < bytes; i++ ) {
		rom.put( baseRom.get() );
	}
}

void BaseRom::copyTo( std::ostream &rom, uint posn, uint bytes ) {
	std::ifstream baseRom( BaseRom::filePath().string(), mode::read );
	baseRom.seekg( posn );
	for( uint i = 0; i < bytes; i++ ) {
		rom.put( baseRom.get() );
	}
}

uint BaseRom::getWord( uint posn ) {
	std::ifstream baseRom( BaseRom::filePath().string(), mode::read );
	uint word;
	baseRom.seekg( posn );
	baseRom.read( (char*)&word, 4 );
	return ntohl( word );
}

const std::map<string,VanillaWaterTexture> &BaseRom::waterTextures() {
	static const std::map<string,VanillaWaterTexture> s_water({
		{ "18a04966c955a3616e289a6fe83eef64", { 0x02016AB8, TextureFormat::RGBA16 } },
		{ "6c3bdd69e099a0d8c6fb8421edddd09d", { 0x02015AB8, TextureFormat::RGBA16 } },
		{ "bece1f48cb87b874f118f4f695bde306", { 0x020162B8, TextureFormat::IA16 } },
		{ "d7a61aa7a30f31f421bb7f5d801ee254", { 0x020152B8, TextureFormat::RGBA16 } },
		{ "f615067efa00c1112ac503d006781c24", { 0x02014AB8, TextureFormat::RGBA16 } }
	});
	return s_water;
}
