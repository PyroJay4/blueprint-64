#ifndef SRC_CORE_BACKGROUND_HPP_
#define SRC_CORE_BACKGROUND_HPP_

#include <string>
#include <ostream>

namespace Background {

	void writeToRom( std::ostream &rom, const std::string &imagePath );

}



#endif /* SRC_CORE_BACKGROUND_HPP_ */
