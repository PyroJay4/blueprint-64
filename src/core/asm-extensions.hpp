#ifndef SRC_CORE_ASM_EXTENSIONS_HPP_
#define SRC_CORE_ASM_EXTENSIONS_HPP_

#include <ostream>

namespace AsmExtensions {
	extern void write( std::ostream &rom );
}


#endif /* SRC_CORE_ASM_EXTENSIONS_HPP_ */
