#ifndef CORE_TWEAK_HPP_
#define CORE_TWEAK_HPP_

#include <functional>
#include <vector>
#include <map>
#include <optional>
#include <cassert>

#include "src/types.hpp"
#include "src/core/colour.hpp"
#include "src/core/uuid.hpp"
#include "src/core/json.hpp"
#include "src/core/version.hpp"

struct TweakPack {
	string name;
	Uuid id;
	Version version;
};

enum class TweakType {
	Basic,
	Integer,
	Enum,
	Switch,
	Float,
	Colour,
	Composite
};

enum class ValueType : char {
	Byte,
	NegatedByte,
	Short,
	NegatedShort,
	Degrees,
	Int,
	NegatedInt,
	Float32u,
	Float32l,
	Float32,
	Float64,
	Colour16,
	Colour24,
	Colour32
};

struct StaticPatch {
	uint seekPosn;
	std::vector<ubyte> patchData;
};

struct DynamicPatch {
	uint seekPosn;
	ValueType type;
	std::optional<ubyte> transform;
	bool noRevert;
};

struct CasePatch {
	string name;
	std::vector<std::vector<ubyte>> patches;
};

typedef std::function<uint(uint)> IntegerTransform;
typedef std::function<double(double)> FloatTransform;

typedef std::vector<IntegerTransform> IntegerTransformChain;
typedef std::vector<FloatTransform> FloatTransformChain;

struct EnumEntry {
	string name;
	uint value;
};

struct StoredTweakData {
	Uuid tweakId;
	Word value;

	void writeToRom( std::ostream &rom ) const {
		tweakId.writeToRom( rom );
		value.writeToRom( rom );
	}

	static StoredTweakData readFromRom( std::istream &rom ) {
		return StoredTweakData{
			Uuid::readFromRom( rom ),
			Word::readFromRom( rom )
		};
	}
};

class ITweakDefinition {

	private:
	const string m_name;
	const string m_description;
	const TweakPack *m_pack;

	protected:
	ITweakDefinition( const Json &tweakJson, const TweakPack *pack );

	public:
	virtual ~ITweakDefinition() {}

	inline const string &getName() const { return m_name; }
	inline const string &getDescription() const { return m_description; }
	inline const TweakPack *getSource() const { return m_pack; }

	inline bool isBuiltIn() const { return m_pack == nullptr; }
	virtual TweakType getType() const = 0;

};

class ISingularTweakDefinition : public ITweakDefinition {

	private:
	const Uuid m_id;

	protected:
	ISingularTweakDefinition( const Json &tweakJson, const TweakPack *pack );
	virtual void applyOrRevert( std::ostream &rom, Word value, bool revert ) const = 0;

	public:
	virtual ~ISingularTweakDefinition() {}

	inline const Uuid getUniqueId() const { return m_id; }

	inline void apply( std::ostream &rom, Word value ) const {
		applyOrRevert( rom, value, false );
	}

	virtual void revert( std::ostream &rom ) const = 0;

};

class INonSwitchSingularTweakDefinition : public ISingularTweakDefinition {

	protected:
	INonSwitchSingularTweakDefinition( const Json &tweakJson, const TweakPack *pack );

	std::vector<StaticPatch> m_staticPatches;
	std::vector<DynamicPatch> m_dynamicPatches;

	public:
	virtual ~INonSwitchSingularTweakDefinition() {}

};

class BasicTweakDefinition final : public INonSwitchSingularTweakDefinition {

	protected:
	virtual void applyOrRevert( std::ostream &rom, Word value, bool revert ) const override;

	public:
	BasicTweakDefinition( const Json &tweakJson, const TweakPack *pack );
	TweakType getType() const { return TweakType::Basic; }

	inline void revert( std::ostream &rom ) const override {
		applyOrRevert( rom, Word::fromUInt( 0 ), true );
	}

};

class ITweakDefinitionWithTransforms : public INonSwitchSingularTweakDefinition {

	protected:
	std::vector<IntegerTransformChain> m_transforms;

	virtual void applyOrRevert( std::ostream &rom, Word value, bool revert ) const override;

	ITweakDefinitionWithTransforms( const Json &tweakJson, const TweakPack *pack );

};

class IntegerTweakDefinition final : public ITweakDefinitionWithTransforms {

	private:
	const uint m_minValue;
	const uint m_maxValue;
	const uint m_defaultValue;
	const bool m_hex;

	public:
	IntegerTweakDefinition( const Json &tweakJson, const TweakPack *pack );
	TweakType getType() const final { return TweakType::Integer; }

	inline uint getMinValue() const { return m_minValue; }
	inline uint getMaxValue() const { return m_maxValue; }
	inline uint getDefaultValue() const { return m_defaultValue; }
	inline bool shouldDisplayAsHex() const { return m_hex; }

	inline void revert( std::ostream &rom ) const override {
		applyOrRevert( rom, Word::fromUInt( m_defaultValue ), true );
	}
};

class EnumTweakDefinition : public ITweakDefinitionWithTransforms {

	protected:
	EnumTweakDefinition( const Json &tweakJson, const TweakPack *pack ) :
		ITweakDefinitionWithTransforms( tweakJson, pack ) {}

	public:
	TweakType getType() const { return TweakType::Enum; }

	virtual std::vector<EnumEntry> getPossibleValues() const = 0;
	virtual const EnumEntry getDefaultValue() const = 0;

	inline void revert( std::ostream &rom ) const override {
		applyOrRevert( rom, Word::fromUInt( getDefaultValue().value ), true );
	}

};

class PresetEnumTweakDefinition : public EnumTweakDefinition {

	protected:
	uint m_defaultValue;

	PresetEnumTweakDefinition( const Json &tweakJson, const TweakPack *pack );

};

class UserDefinedEnumTweakDefinition final : public EnumTweakDefinition {

	private:
	std::vector<EnumEntry> m_enumValues;
	EnumEntry m_defaultValue;

	public:
	UserDefinedEnumTweakDefinition( const Json &tweakJson, const TweakPack *pack );

	inline std::vector<EnumEntry> getPossibleValues() const override {
		return m_enumValues;
	}
	inline const EnumEntry getDefaultValue() const override {
		return m_defaultValue;
	}

};

class SwitchTweakDefinition final : public ISingularTweakDefinition {

	private:
	std::vector<uint> m_seeks;
	std::vector<CasePatch> m_cases;
	ushort m_defaultCase;

	protected:
	virtual void applyOrRevert( std::ostream &rom, Word value, bool revert ) const override;

	public:
	SwitchTweakDefinition( const Json &tweakJson, const TweakPack *pack );
	TweakType getType() const { return TweakType::Switch; }


	inline const std::vector<CasePatch> &getCases() const { return m_cases; }
	inline ushort getDefaultCaseIndex() const { return m_defaultCase; }

	inline void revert( std::ostream &rom ) const override {
		applyOrRevert( rom, Word::fromUInt( (uint)m_defaultCase ), true );
	}

};

class FloatTweakDefinition final : public INonSwitchSingularTweakDefinition {

	private:
	std::vector<FloatTransformChain> m_transforms;

	const float m_minValue;
	const float m_maxValue;
	const float m_defaultValue;
	const ubyte m_decimalPrecision;

	protected:
	virtual void applyOrRevert( std::ostream &rom, Word value, bool revert ) const override;

	public:
	FloatTweakDefinition( const Json &tweakJson, const TweakPack *pack );
	TweakType getType() const { return TweakType::Float; }

	inline float getMinValue() const { return m_minValue; }
	inline float getMaxValue() const { return m_maxValue; }
	inline float getDefaultValue() const { return m_defaultValue; }
	inline float getDecimalPrecision() const { return m_decimalPrecision; }

	inline void revert( std::ostream &rom ) const override {
		applyOrRevert( rom, Word::fromFloat( m_defaultValue ), true );
	}

};

class ColourTweakDefinition final : public INonSwitchSingularTweakDefinition {

	private:
	const bool m_supportsAlpha;
	const ColourRGBA32 m_defaultColour;

	protected:
	virtual void applyOrRevert( std::ostream &rom, Word value, bool revert ) const override;

	public:
	ColourTweakDefinition( const Json &tweakJson, const TweakPack *pack );
	TweakType getType() const { return TweakType::Colour; }

	inline bool supportsAlpha() const { return m_supportsAlpha; }
	inline ColourRGBA32 getDefaultColour() const { return m_defaultColour; }

	inline void revert( std::ostream &rom ) const override {
		applyOrRevert( rom, Word::fromUInt( m_defaultColour.toInt() ), true );
	}

};

class CompositeTweakDefinition final : public ITweakDefinition {

	private:
	std::vector<ISingularTweakDefinition*> m_components;

	public:
	CompositeTweakDefinition( const Json &tweakJson, const TweakPack *pack );
	TweakType getType() const { return TweakType::Composite; }

	inline std::vector<ISingularTweakDefinition*> &getComponents() {
		return m_components;
	}
	inline const std::vector<ISingularTweakDefinition*> &getComponents() const {
		return m_components;
	}

};

class TweakStore final {

	private:
	std::vector<ITweakDefinition*> m_publicTweaks;
	std::map<Uuid,ISingularTweakDefinition*> m_tweakMap;
	std::map<Uuid,TweakPack> m_tweakPacks;

	public:
	TweakStore() {}
	~TweakStore() {
		clear();
	}

	static TweakStore &instance();

	void clear();
	void removeTweakPack( Uuid packId );
	std::vector<Uuid> loadFileAndGetConflicts( std::istream &jsonFile, bool builtin );
	void loadInstalledTweakPacks();

	inline const std::vector<ITweakDefinition*> getPublicTweaks() const {
		return m_publicTweaks;
	}
	inline const std::map<Uuid,TweakPack> tweakPacks() const {
		return m_tweakPacks;
	}

	inline ISingularTweakDefinition const *tryGetTweak( const Uuid &id ) const {
		const auto tweak = m_tweakMap.find( id );
		return ( tweak == m_tweakMap.end() ) ? nullptr : tweak->second;
	}

	static std::vector<TweakPack> getInstalledTweakPacks();
	static std::vector<Uuid> installTweakPack( const string &filePath );
	static void uninstallTweakPack( const Uuid &packId );

};


#endif /* CORE_TWEAK_HPP_ */
