#ifndef CORE_BLUEPRINT_HPP_
#define CORE_BLUEPRINT_HPP_

#include <map>
#include <set>
#include <array>
#include <vector>
#include <iostream>
#include <optional>
#include <unordered_map>
#include "src/types.hpp"
#include "src/core/version.hpp"
#include "src/core/specs.hpp"
#include "src/core/uuid.hpp"
#include "src/core/checksum.hpp"
#include "src/core/text.hpp"
#include "src/core/tweak.hpp"
#include "src/core/armips.hpp"
#include "src/core/actors.hpp"
#include "src/core/music.hpp"
#include "src/core/asm-modules.hpp"
#include "src/core/import/model-importer.hpp"
#include "src/polyfill/filesystem.hpp"

inline constexpr ushort AREA_ID( LevelId levelId, ubyte areaIndex ) {
	return ((ushort)levelId << 3) + areaIndex;
}

struct CreateProjectOptions {
	string projectPath;
	string romPath;
	string internalRomName;
	bool disableChecksum;
	bool disableDemos;
	bool disableIntro;
	bool removeBorders;
	bool infiniteLives;
	bool extendVerticalBoundaries;
	ubyte romEntry;
	LevelBoundarySize horizontalBoundaries;
	bool patchHandsFreeHolding;
};

struct RomMetadata final {
	Version metadataVersion;
	Version romVersion;
	std::vector<Uuid> appliedTweaks;
	std::array<ubyte,40> customLevelTable;
	std::vector<std::pair<uint,uint>> moduleRegions;

	void writeToRom( std::ostream &rom ) const;
	static RomMetadata readFromRom( std::istream &rom );
};

class Blueprint final {

	private:
	std::filesystem::path m_filePath;
	string m_tempDir;
	Version m_version;
	RomSpecs m_romSpecs;
	std::map<Uuid,Word> m_tweaks;
	std::map<Uuid,AsmModule> m_modules;
	std::map<string,CustomMarioAction> m_customActions;
	std::vector<DialogEntry> m_dialogData;
	std::array<uint,16> m_dialogSfxTable;
	string m_genericLevelName;
	string m_genericStarName;
	std::map<LevelId,LevelSpecs> m_levels;
	std::map<ushort,AreaSpecs> m_areas;
	std::map<ubyte,CustomMusicInfo> m_customMusic;
	std::filesystem::path m_romPath;
	std::set<LevelId> m_texturesChanged;
	HashSet<WaterTextureInfo> m_waterTextures;
	std::map<ubyte,string> m_customBackgrounds;
	bool m_endScreenFiltering;
	bool m_dataChanged;
	bool m_collabMode;

	Blueprint(
		std::filesystem::path filePath,
		string tempDir,
		Version version,
		RomSpecs romSpecs,
		std::map<Uuid,Word> &&tweaks,
		std::map<Uuid,AsmModule> &&modules,
		std::map<string,CustomMarioAction> &&customActions,
		std::vector<DialogEntry> &&dialogData,
		std::array<uint,16> &&dialogSfxTable,
		string genericLevelName,
		string genericStarName,
		std::map<LevelId,LevelSpecs> &&levels,
		std::map<ushort,AreaSpecs> &&areas,
		std::map<ubyte,CustomMusicInfo> &&music,
		HashSet<WaterTextureInfo> &&waterTextures,
		std::map<ubyte,string> &&customBackgrounds,
		std::filesystem::path romPath,
		bool endScreenFiltering,
		bool collabMode
	) :
		m_filePath( filePath ),
		m_tempDir( tempDir ),
		m_version( version ),
		m_romSpecs( romSpecs ),
		m_tweaks( tweaks ),
		m_modules( modules ),
		m_customActions( customActions ),
		m_dialogData( dialogData ),
		m_dialogSfxTable( dialogSfxTable ),
		m_genericLevelName( genericLevelName ),
		m_genericStarName( genericStarName ),
		m_levels( levels ),
		m_areas( areas ),
		m_customMusic( music ),
		m_romPath( romPath ),
		m_waterTextures( waterTextures ),
		m_customBackgrounds( customBackgrounds ),
		m_endScreenFiltering( endScreenFiltering ),
		m_dataChanged( false ),
		m_collabMode( collabMode )
	{}

	~Blueprint();

	private:
	void syncTweakStore() const;
	void syncBlueprintTweaks() const;

	void removeUnusedTextures();

	void revertModuleRegions( std::ostream &rom, RomMetadata &metadata ) const;
	void writeAsmExtensions( std::ostream &rom ) const;
	void applyRomSettingsInternal( std::iostream &rom ) const;
	void applyTweaksInternal( std::iostream &rom, RomMetadata &metadata, std::vector<string> &warnings ) const;
	void applyTextInternal( std::iostream &rom ) const;
	void buildEndScreenInternal( std::ostream &rom ) const;
	void applyBackgroundChangesInternal( std::ostream &rom ) const;
	void buildLevelInternal(
		std::iostream &rom,
		LevelId levelId,
		const std::array<AreaActors,8> &actors,
		std::map<string,uint> &asmRefs,
		RomMetadata &romInfo,
		std::vector<string> &warnings
	) const;

	void generateModulesInternal( RomMetadata &metadata ) const;
	void revertDeletedLevels( std::iostream &rom, RomMetadata &romInfo ) const;
	void saveGeometry( const std::filesystem::path &dataDir, const HashMap<string,MaterialSpecs> &materials ) const;

	void switchToCollabMode( const std::filesystem::path &folderPath );
	void switchToNormalMode( const std::filesystem::path &blueprintPath );

	public:
	inline const Version &version() const { return m_version; }

	string targetRomName() const;
	std::filesystem::path targetRomPath() const;

	inline bool hasTargetRom() const { return !m_romPath.empty(); }
	inline void setTargetRom( const std::filesystem::path &path ) { m_romPath = path; }

	inline bool isCollab() const { return m_collabMode; }
	inline void switchMode( const std::filesystem::path &path ) { m_collabMode ? switchToNormalMode( path ) : switchToCollabMode( path ); }

	inline bool hasUnsavedChanges() const { return m_dataChanged; }

	inline const RomSpecs &romSpecs() const { return m_romSpecs; }
	inline RomSpecs &romSpecs() { return m_romSpecs; }

	inline const std::vector<DialogEntry> &dialogs() const { return m_dialogData; }
	inline std::vector<DialogEntry> &dialogs() { return m_dialogData; }

	inline const std::array<uint,16> &dialogSfxTable() const { return m_dialogSfxTable; }
	inline std::array<uint,16> &dialogSfxTable() { return m_dialogSfxTable; }

	inline const string &genericLevelName() const { return m_genericLevelName; }
	inline string &genericLevelName() { return m_genericLevelName; }

	inline const string &genericStarName() const { return m_genericStarName; }
	inline string &genericStarName() { return m_genericStarName; }

	inline const std::map<ubyte,CustomMusicInfo> &customMusic() const { return m_customMusic; }
	inline std::map<ubyte,CustomMusicInfo> &customMusic() { return m_customMusic; }

	inline const std::map<LevelId,LevelSpecs> &levels() const { return m_levels; }
	inline std::map<LevelId,LevelSpecs> &levels() { return m_levels; }
	inline bool hasLevel( LevelId levelId ) const { return m_levels.find( levelId ) != m_levels.end(); }

	inline bool endScreenHasInterpolation() const { return m_endScreenFiltering; }
	inline void setEndScreenInterpolation( bool enabled ) { m_endScreenFiltering = enabled; }

	inline const std::map<ushort,AreaSpecs> &areas() const { return m_areas; }
	inline std::map<ushort,AreaSpecs> &areas() { return m_areas; }
	inline bool hasArea( LevelId levelId, ubyte areaIndex ) const {
		return m_areas.find( AREA_ID( levelId, areaIndex ) ) != m_areas.end();
	}

	inline const std::map<ubyte,string> &customBackgrounds() const { return m_customBackgrounds; }
	inline std::map<ubyte,string> &customBackgrounds() { return m_customBackgrounds; }

	inline const HashSet<WaterTextureInfo> &waterTextures() const { return m_waterTextures; }
	inline HashSet<WaterTextureInfo> &waterTextures() { return m_waterTextures; }

	inline const std::map<Uuid,AsmModule> &asmModules() const { return m_modules; }
	inline std::map<Uuid,AsmModule> &asmModules() { return m_modules; }

	inline const std::map<string,CustomMarioAction> &customMarioActions() const { return m_customActions; }
	inline std::map<string,CustomMarioAction> &customMarioActions() { return m_customActions; }

	inline void setTweakValue( const Uuid &tweakId, Word value ) {
		m_tweaks[tweakId] = value;
	}

	inline bool removeTweak( const Uuid &tweakId ) {
		return m_tweaks.erase( tweakId ) > 0;
	}

	inline std::optional<Word> tryGetTweakValue( const Uuid &tweakId ) const {
		auto it = m_tweaks.find( tweakId );
		if( it == m_tweaks.end() ) {
			return std::optional<Word>();
		}
		return it->second;
	}

	inline bool usingTweak( const Uuid &tweakId ) const {
		return tryGetTweakValue( tweakId ).has_value();
	}

	void save();
	inline void saveAs( const std::filesystem::path &path ) {
		const std::filesystem::path oldPath = m_filePath;
		try {
			m_filePath = path;
			save();
		} catch( ... ) {
			m_filePath = oldPath;
			throw;
		}
	}

	inline bool canRunPartialBuild( std::istream &rom ) const {
		try {
			return RomMetadata::readFromRom( rom ).romVersion == CurrentVersion::RomVersion;
		} catch( ... ) {
			return false;
		}
	}

	void applyRomSettings( std::iostream &rom ) const {
		writeAsmExtensions( rom );
		applyRomSettingsInternal( rom );
		if( m_romSpecs.enableChecksum ) updateCRC( rom );
		Armips::autogenerateWithoutReferences();
	}

	std::vector<string> applyTweaks( std::iostream &rom ) const {
		std::vector<string> warnings;
		RomMetadata metadata = RomMetadata::readFromRom( rom );
		writeAsmExtensions( rom );
		applyTweaksInternal( rom, metadata, warnings );
		metadata.writeToRom( rom );
		if( m_romSpecs.enableChecksum ) updateCRC( rom );
		Armips::autogenerateWithoutReferences();
		return warnings;
	}

	void applyTextChanges( std::iostream &rom ) const {
		writeAsmExtensions( rom );
		applyTextInternal( rom );
		if( m_romSpecs.enableChecksum ) updateCRC( rom );
		Armips::autogenerateWithoutReferences();
	}

	std::vector<string> applyLevelChanges( std::iostream &rom, LevelId levelId, const std::array<AreaActors,8> &actors ) const {
		AsmReferences asmRefs = Armips::getCurrentAsmReferences();
		asmRefs.erase( levelId );

		std::vector<string> warnings;
		RomMetadata metadata = RomMetadata::readFromRom( rom );
		writeAsmExtensions( rom );
		applyTextInternal( rom );
		buildLevelInternal( rom, levelId, actors, asmRefs[levelId], metadata, warnings );
		metadata.writeToRom( rom );
		if( m_romSpecs.enableChecksum ) updateCRC( rom );
		Armips::autogenerate( asmRefs );
		return warnings;
	}

	inline std::vector<string> applyLevelChanges( std::iostream &rom, LevelId levelId ) const {
		std::array<AreaActors,8> actors = AreaActors::loadFromRom( rom, levelId );
		for( ubyte i = 0; i < 8; i++ ) {
			if( !actors[i].empty() || !hasArea( levelId, i ) ) continue;
			const std::filesystem::path actorsPath = getAreaActorsPath( levelId, i );
			if( !std::filesystem::exists( actorsPath ) ) continue;
			AreaActors areaActors = AreaActors::loadFromJson( actorsPath.string() );
			if( !areaActors.empty() ) {
				actors[i] = areaActors;
			}
		}
		return applyLevelChanges( rom, levelId, actors );
	}

	std::vector<string> applyAllLevelChanges( std::iostream &rom, bool useSavedObjectPlacements ) const {
		std::vector<string> warnings;
		RomMetadata metadata = RomMetadata::readFromRom( rom );
		AsmReferences asmRefs;
		writeAsmExtensions( rom );
		applyTextInternal( rom );
		revertDeletedLevels( rom, metadata );
		for( const auto &i : m_levels ) {
			std::array<AreaActors,8> actors = AreaActors::loadFromRom( rom, i.first );
			for( ubyte j = 0; j < 8; j++ ) {
				if( !useSavedObjectPlacements && !actors[j].empty() ) continue;
				if( !hasArea( i.first, j ) ) continue;
				const std::filesystem::path actorsPath = getAreaActorsPath( i.first, j );
				if( !std::filesystem::exists( actorsPath ) ) continue;
				AreaActors areaActors = AreaActors::loadFromJson( actorsPath.string() );
				if( !areaActors.empty() ) {
					actors[j] = areaActors;
				}
			}
			buildLevelInternal( rom, i.first, actors, asmRefs[i.first], metadata, warnings );
		}
		metadata.writeToRom( rom );
		if( m_romSpecs.enableChecksum ) updateCRC( rom );
		Armips::autogenerate( asmRefs );
		return warnings;
	}

	void applyCustomAsm( std::iostream &rom ) const {
		writeAsmExtensions( rom );
		Armips::autogenerateWithoutReferences();
		Armips::build();
		if( m_romSpecs.enableChecksum ) updateCRC( rom );
	}

	void applyEndScreen( std::iostream &rom ) const {
		writeAsmExtensions( rom );
		buildEndScreenInternal( rom );
		if( m_romSpecs.enableChecksum ) updateCRC( rom );
	}

	void applyMusicChanges( std::iostream &rom ) const {
		writeAsmExtensions( rom );
		MusicData::writeToRom( rom );
		if( m_romSpecs.enableChecksum ) updateCRC( rom );
	}

	void applyBackgroundChanges( std::iostream &rom ) const {
		writeAsmExtensions( rom );
		applyBackgroundChangesInternal( rom );
		if( m_romSpecs.enableChecksum ) updateCRC( rom );
	}

	void applyAsmModules( std::iostream &rom ) const {
		RomMetadata metadata = RomMetadata::readFromRom( rom );
		revertModuleRegions( rom, metadata );
		writeAsmExtensions( rom );
		Armips::autogenerateWithoutReferences();
		generateModulesInternal( metadata );
		metadata.writeToRom( rom );
		Armips::build();
		if( m_romSpecs.enableChecksum ) updateCRC( rom );
	}

	std::vector<string> applyAll( std::iostream &rom ) const {
		std::vector<string> warnings;
		RomMetadata metadata = RomMetadata::readFromRom( rom );
		revertModuleRegions( rom, metadata );
		writeAsmExtensions( rom );
		applyRomSettingsInternal( rom );
		applyTweaksInternal( rom, metadata, warnings );
		applyTextInternal( rom );
		buildEndScreenInternal( rom );
		applyBackgroundChangesInternal( rom );
		metadata.metadataVersion = CurrentVersion::MetadataFormat;
		metadata.romVersion = CurrentVersion::RomVersion;
		revertDeletedLevels( rom, metadata );
		AsmReferences asmRefs;
		for( const auto &i : m_levels ) {
			buildLevelInternal( rom, i.first, AreaActors::loadFromRom( rom, i.first ), asmRefs[i.first], metadata, warnings );
		}
		MusicData::writeToRom( rom );
		generateModulesInternal( metadata );
		Armips::autogenerate( asmRefs );
		metadata.writeToRom( rom );
		Armips::build();
		if( m_romSpecs.enableChecksum ) updateCRC( rom );
		return warnings;
	}

	void updateChecksum( std::iostream &rom ) const {
		updateCRC( rom );
	}

	std::vector<TweakPack> getEmbeddedTweakPacks() const;
	void forceSaveTweakPack( const Uuid &packId );

	std::filesystem::path getLevelPath( LevelId levelId ) const;
	std::filesystem::path getAreaPath( LevelId levelId, ubyte areaIndex ) const;
	std::filesystem::path getAreaActorsPath( LevelId levelId, ubyte areaIndex ) const;
	std::filesystem::path getObjectPath( LevelId levelId, objectId objectId ) const;
	std::filesystem::path getUserDataPath( LevelId levelId, uint userDataId ) const;
	std::filesystem::path getTweakPackPath( const Uuid &packId ) const;
	std::filesystem::path getMusicPath( ubyte sequenceId ) const;
	std::filesystem::path getEndScreenPath() const;
	std::filesystem::path getWaterTexturePath() const;
	std::filesystem::path getCustomBackgroundPath( ubyte slot ) const;
	std::filesystem::path getModulePath( const Uuid &moduleId ) const;
	HashMap<string,MaterialSpecs> getAreaGeometry( LevelId levelId, ubyte areaIndex ) const;
	HashMap<string,MaterialSpecs> getObjectGeometry( LevelId levelId, objectId objectId ) const;
	bool areaHasGeometry( LevelId levelId, ubyte areaIndex ) const;
	bool objectHasGeometry( LevelId levelId, objectId objectId ) const;
	bool levelHasGeometry( LevelId levelId ) const;

	std::filesystem::path getAsmFilePath() const;
	std::filesystem::path getAsmDirectory() const;

	inline void saveAreaGeometry( LevelId levelId, ubyte areaIndex, const HashMap<string,MaterialSpecs> &materials ) {
		m_texturesChanged.insert( levelId );
		saveGeometry( getAreaPath( levelId, areaIndex ), materials );
	}
	inline void saveObjectGeometry( LevelId levelId, objectId objectId, const HashMap<string,MaterialSpecs> &materials ) {
		m_texturesChanged.insert( levelId );
		saveGeometry( getObjectPath( levelId, objectId ), materials );
	}

	bool hasFreeModelIds( LevelId levelId ) const;
	std::set<ubyte> getFreeModelIds( LevelId levelId ) const;
	inline std::set<ubyte> getFreeModelIds( LevelId levelId, ubyte currentModelId ) const {
		std::set<ubyte> results = getFreeModelIds( levelId );
		results.insert( currentModelId );
		return results;
	}

	inline bool hasModule( const Uuid &moduleId ) const {
		return m_modules.count( moduleId ) > 0;
	}

	inline bool hasModule( const Uuid &moduleId, const Version &version ) const {
		const auto m = m_modules.find( moduleId );
		return m != m_modules.end() && m->second.definition.version == version;
	}

	static const Blueprint *current();
	static Blueprint *currentEditable();
	static void create( const CreateProjectOptions &options );
	static void open( const std::filesystem::path &path );
	static void free();

};

#endif /* CORE_BLUEPRINT_HPP_ */
