#include "src/core/specs.hpp"

const ShadingSpecs ShadingSpecs::Default = {
	"Default",
	ColourRGB24( 0x7F, 0x7F, 0x7F ),
	ColourRGB24( 0xFF, 0xFF, 0xFF ),
	false
};


const RomSpecs RomSpecs::Default = {
	/* internalName */ "SUPER MARIO 64",
	/* enableChecksum */ false,
	/* levelBoundaries */ LevelBoundarySize::Normal,
	/* startingLevel */ LevelId::CastleGrounds,
	/* pauseWarpLevel */ LevelId::CastleInterior,
	/* pauseWarpArea */ 1,
	/* pauseWarpId */ 31
};

const LevelSpecs LevelSpecs::Default = {
	/* name */ "",
	/* starNames */ { "", "", "", "", "", "" },
	/* marioSpawn */ { 1, 0, 0, 0, 0.0f },
	/* enableStarSelect */ false,
	/* objectBanks */ { -1, -1, -1 },
	/* maxSoundDistance */ 19500,
	/* defaultEchoLevel */ 0x28,
	/* starSpawnLocations */ std::array<Vec3f,25>(),
	/* waypoints */ std::array<std::vector<Vec3s>,22>(),
	/* shadingValues */ {{ 0, ShadingSpecs::Default }},
	/* simpleObjects */ HashMap<objectId,ObjectSpecs>(),
	/* partialObjects */ HashMap<objectId,PartialObjectSpecs>(),
	/* advancedObjects */ HashMap<objectId,AdvancedObjectSpecs>(),
	/* userData */ HashMap<uint,UserData>()
};

const AreaSpecs AreaSpecs::Default = {
	/* backgroundImage */ BackgroundImage::None,
	/* backgroundColour */ { ColourRGBA5551( 0, 0, 0 ) },
	/* pauseCamera */ {
		/* position */ { 0, 2048, -16384 },
		/* focus */ { 0, 0, 0 },
		/* fixed */ false
	},
	/* cameraType */ CameraPreset::SemiCardinal,
	/* fieldOfView */ 45,
	/* nearClippingPlane */ 128,
	/* farClippingPlane */ 0x7FFF,
	/* fog */ {
		/* fadeStartDistance */ 0.980,
		/* fadeEndDistance */ 1.0,
		/* colour */ ColourRGB24( 0, 0, 0 ),
		/* enabled */ false
	},
	/* environmentEffect */ EnvironmentEffect::None,
	/* terrainType */ TerrainType::Stone,
	/* echo level */ 0x28,
	/* music */ 0,
	/* numObjects */ 100,
	/* numWarps */ 8,
	/* numInstantWarps */ 4,
	/* autoDeathFloor */ false,
	/* voidHeight */ -8950,
	/* waterBoxes */ std::vector<WaterBoxSpecs>()
};

const MaterialSpecs MaterialSpecs::Default = {
	/* visible */ false,
	/* solid */ true,
	/* opacity */ 0xFF,
	/* removedTextureAlpha */ false,
	/* layer */ DrawingLayer::Opaque,
	/* format */ TextureFormat::RGBA16,
	/* filtering */ TextureFiltering::Bilinear,
	/* hwrap */ TextureWrapping::Repeat,
	/* vwrap */ TextureWrapping::Repeat,
	/* shadingRef */ 0,
	/* collision */ {
		/* type */ CollisionType::Normal,
		/* direction */ 0,
		/* force */ ForceIntensity::Medium
	},
	/* textureInfo */ std::nullopt,
	/* animation */ AnimationSpecs(),
	/* asmRefs */ {
		/* texData */ false,
		/* texPtr */ false,
		/* shadePtr */ false,
		/* opacity */ false
	}
};

const ObjectSpecs ObjectSpecs::Default = {
	/* modelId */ 0,
	/* name */ "New Object",
	/* cullingRadius */ {
		/* autoCompute */ true,
		/* alwaysUpright */ false,
		/* maxScale */ 1.0f,
		/* maxFOV */ 45,
		/* radius */ 300
	},
	/* shadow */ {
		/* shape */ ShadowType::Circle,
		/* size */ 100,
		/* opacity */ 0xB4,
		/* enabled */ false
	},
	/* fog */ {
		/* fadeStartDistance */ 0.98,
		/* fadeEndDistance */ 1.0,
		/* colour */ ColourRGB24( 0, 0, 0 ),
		/* enabled */ false
	},
	/* useObjectAlpha */ false,
	/* ditheredAlpha */ false,
	/* hasCollision */ false,
	/* collisionAsmRef */ true,
	/* collisionPtrAddr */ 0x21C7F0,
	/* max_xz */ 0,
	/* max_xyz */ 0
};

const WaterBoxSpecs WaterBoxSpecs::Default = {
	/* name */ "Untitled Water Box",
	/* isPoisonFog */ false,
	/* isVisible */ true,
	/* asmRefTexturePtr */ false,
	/* asmRefColour */ false,
	/* xMin */ 0,
	/* xMax */ 0,
	/* zMin */ 0,
	/* zMax */ 0,
	/* y */ 0,
	/* scale */ 25,
	/* opacity */ 0xB4,
	/* colour */ ColourRGB24( 0xFF, 0xFF, 0x00 ),
	/* texture */ { "f615067efa00c1112ac503d006781c24", "", TextureFormat::RGBA16, false },
	/* animationSpecs */ AnimationSpecs()
};

DrawingLayer TextureInfo::suggestLayer() const {
	return hasFullAlpha ? DrawingLayer::Translucent : ( hasEmptyPixels ? DrawingLayer::Alphatest : DrawingLayer::Opaque );
}

static inline TextureFormat suggestFormat4( const TextureInfo &t ) {
	return (t.hasEmptyPixels || t.hasFullAlpha) ? TextureFormat::IA4 : TextureFormat::I4;
}

static TextureFormat suggestFormat8( const TextureInfo &t ) {
	if( t.hasFullAlpha ) {
		return TextureFormat::IA8;
	} else if( t.hasEmptyPixels ) {
		return t.sourceImageDepth > 4 ? TextureFormat::IA8 : TextureFormat::IA4;
	} else {
		return t.sourceImageDepth > 4 ? TextureFormat::I8 : TextureFormat::I4;
	}
}

static TextureFormat suggestFormat32( const TextureInfo &t ) {
	if( t.isGreyscale ) {
		TextureFormat format8 = suggestFormat8( t );
		if( t.sourceImageDepth > 8 && format8 == TextureFormat::IA8 ) {
			return TextureFormat::IA16;
		}
		return format8;
	}
	return t.hasFullAlpha ? TextureFormat::RGBA32 : TextureFormat::RGBA16;
}

static inline TextureFormat suggestFormat16( const TextureInfo &t ) {
	TextureFormat preferredFormat = suggestFormat32( t );
	return (preferredFormat == TextureFormat::RGBA32) ? TextureFormat::RGBA16 : preferredFormat;
}

TextureFormat TextureInfo::suggestFormat() const {
	if( width == 0 && height == 0 ) {
		return TextureFormat::IA4;
	}

	const uint maxBitsPerPixel = (0x8000u / ((uint)width * (uint)height));
	if( maxBitsPerPixel < 8 && width >= 16 ) {
		return suggestFormat4( *this );
	} else if( maxBitsPerPixel < 16 && width >= 8 ) {
		return suggestFormat8( *this );
	} else if( maxBitsPerPixel < 32 && width >= 4 ) {
		return suggestFormat16( *this );
	} else {
		return suggestFormat32( *this );
	}
}

bool TextureInfo::supportsFormat( TextureFormat format ) const {
	const uint maxBitsPerPixel = (0x8000u / ((uint)width * (uint)height));
	switch( format ) {
		case TextureFormat::RGBA32:
			return maxBitsPerPixel >= 32 && width >= 2;
		case TextureFormat::RGBA16:
		case TextureFormat::IA16:
			return maxBitsPerPixel >= 16 && width >= 4;
		case TextureFormat::IA8:
		case TextureFormat::I8:
			return maxBitsPerPixel >= 8 && width >= 8;
		case TextureFormat::IA4:
		case TextureFormat::I4:
			return maxBitsPerPixel >= 4 && width >=16;
		default:
			return false;
	}

}

HashSet<TextureFormat> TextureInfo::getPossibleFormats() const {
	//TODO: support indexed colours and I guess YUV16 maybe
	const uint maxBitsPerPixel = (0x8000u / ((uint)width * (uint)height));
	HashSet<TextureFormat> possibleFormats;
	if( maxBitsPerPixel >= 4 ) {
		if( width >= 16 ) {
			possibleFormats.insert( TextureFormat::I4 );
			possibleFormats.insert( TextureFormat::IA4 );
		}
		if( maxBitsPerPixel >= 8 ) {
			if( width >= 8 ) {
				possibleFormats.insert( TextureFormat::I8 );
				possibleFormats.insert( TextureFormat::IA8 );
			}
			if( maxBitsPerPixel >= 16 ) {
				if( width >= 4 ) {
					possibleFormats.insert( TextureFormat::IA16 );
					possibleFormats.insert( TextureFormat::RGBA16 );
				}
				if( maxBitsPerPixel >= 32 && width >= 2 ) {
					possibleFormats.insert( TextureFormat::RGBA32 );
				}
			}
		}
	}
	return possibleFormats;
}

