#ifndef SRC_CORE_SOUND_HPP_
#define SRC_CORE_SOUND_HPP_

#include <utility>
#include <vector>
#include "src/types.hpp"

string getSoundName( uint soundId );
std::vector<std::pair<uint,const char*>> getAllNamedSounds();

#endif /* SRC_CORE_SOUND_HPP_ */
