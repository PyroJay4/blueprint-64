#include "src/core/objectid.hpp"

#include "src/polyfill/random.hpp"

objectId ObjectId::generateNew( ObjectType type ) {
	return ((uint)type << 29) | (Pseudorandom::getUInt() & 0x1FFFFFFF);
}
