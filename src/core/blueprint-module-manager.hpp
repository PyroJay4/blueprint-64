#ifndef SRC_CORE_BLUEPRINT_MODULE_MANAGER_HPP_
#define SRC_CORE_BLUEPRINT_MODULE_MANAGER_HPP_

#include "src/core/asm-modules.hpp"

namespace BlueprintModules {

	bool tryAddModule(
		const AsmModuleDefinition &module,
		string &errorReason
	);

}

#endif /* SRC_CORE_BLUEPRINT_MODULE_MANAGER_HPP_ */
