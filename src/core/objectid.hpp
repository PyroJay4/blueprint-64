#ifndef SRC_CORE_OBJECTID_HPP_
#define SRC_CORE_OBJECTID_HPP_

#include "src/types.hpp"
#include "src/core/enums.hpp"

typedef uint objectId;

namespace ObjectId {

	objectId generateNew( ObjectType type );
	inline ObjectType getType( objectId id ) {
		return (ObjectType)(id >> 29);
	}

}

#endif /* SRC_CORE_OBJECTID_HPP_ */
