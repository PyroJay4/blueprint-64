#ifndef SRC_CORE_HARDCODED_HPP_
#define SRC_CORE_HARDCODED_HPP_

#include "src/core/enums.hpp"
#include <ostream>

namespace HardcodedLevelCode {

	void remove( std::ostream &rom, LevelId levelId );
	void restore( std::ostream &rom, LevelId levelId );

}


#endif /* SRC_CORE_HARDCODED_HPP_ */
