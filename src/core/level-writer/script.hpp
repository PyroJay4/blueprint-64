#ifndef SRC_CORE_LEVEL_WRITER_SCRIPT_HPP_
#define SRC_CORE_LEVEL_WRITER_SCRIPT_HPP_

#include <vector>
#include <ostream>
#include "src/core/enums.hpp"
#include "src/core/specs.hpp"
#include "src/core/actors.hpp"
#include "src/core/level-writer/util.hpp"

struct AreaDataPointerLocations {
	RomAddress geoLayout;
	RomAddress collision;
};

struct LevelScriptLocations {
	std::map<ushort,AreaDataPointerLocations> areaLocations;
	std::map<objectId,RomAddress> objectLocations;
};

extern LevelScriptLocations writeLevelScript(
	std::ostream &rom,
	LevelId levelId,
	const LevelSpecs &specs,
	const std::array<AreaActors,8> &areaActors,
	std::vector<string> &warnings
);

#endif /* SRC_CORE_LEVEL_WRITER_SCRIPT_HPP_ */
