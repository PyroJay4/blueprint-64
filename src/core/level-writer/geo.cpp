#include "src/core/level-writer/geo.hpp"

#include <initializer_list>
#include <ostream>
#include "src/polyfill/filesystem.hpp"

namespace fs = std::filesystem;

#define INIT_RENDER_AREA writeRaw<uint>( rom, { 0x0800000A_be32, 0x00A00078_be32, 0x00A00078_be32 } );
#define START_NODE writeWordRaw( rom, 0x04000000_be32 );
#define END_NODE writeWordRaw( rom, 0x05000000_be32 );
#define TOGGLE_DEPTH_BUFFER( enable ) writeWordRaw( rom, enable ? 0x0C010000_be32 : 0x0C000000_be32 );
#define SCENE_ORTHO_PROJECTION writeWordRaw( rom, 0x09000064_be32 );
#define BACKGROUND_IMAGE( background ) writeRaw<ubyte>( rom, { 0x19, 0x00, 0x00, (ubyte)background, 0x80, 0x27, 0x63, 0xD4 });
#define BACKGROUND_COLOUR( colour ) writeHalfwordRaw( rom, 0x1900_be16 ); writeHalfword( rom, colour.toShort() ); writeWordRaw( rom, 0 );
#define SET_CAMERA_FRUSTRUM( fov, near, far ) \
	writeHalfwordRaw( rom, 0x0A01_be16 ); \
	writeSignedHalfword( rom, fov ); \
	writeHalfwords( rom, { near, far }); \
	writeWordRaw( rom, 0x8029AA3C_be32 );
#define SET_CAMERA_PRESET( preset, position, focus ) \
	writeHalfwordRaw( rom, 0x0F00_be16 ); \
	writeHalfword( rom, (ushort)(preset) ); \
	writeSignedHalfwords( rom, { \
		position.x, position.y, position.z, \
		focus.x, focus.y, focus.z \
	}); \
	writeWordRaw( rom, 0x80287D30_be32 );
#define LOAD_DISPLAY_LIST( segPtr, layer ) writeRaw<ubyte>( rom, { 0x15, (ubyte)layer, 0x00, 0x00 }); writeWord( segPtr );
#define CREATE_SCENE_NODE writeWordRaw( rom, 0x17000000_be32 );
#define LOAD_ENVFX( effect ) writeRaw<ubyte>( rom, { 0x18, 0x00, 0x00, (ubyte)effect, 0x80, 0x27, 0x61, 0xD0 });
#define LOAD_SCREEN_TRANSITION writeRaw<uint>( rom, { 0x18000000_be32, 0x802CD1E8_be32 });
#define INIT_WITH_CULLING_RADIUS( cullingRadius ) writeHalfwords( rom, { 0x2000, cullingRadius });
#define INIT_WITH_SHADOW( shadowSpecs ) \
	writeRaw<ubyte>( rom, { 0x16, 0x00, 0x00 }); \
	rom.put( (ubyte)shadowSpecs.shape ); \
	rom.put( 0x00 ); \
	rom.put( shadowSpecs.opacity ); \
	writeHalfword( rom, shadowSpecs.size );
#define END_GEO_LAYOUT writeWordRaw( rom, 0x01000000_be32 );
#define DL_RETURN writeWordRaw( rom, 0x03000000_be32 );

static inline HashMap<DrawingLayer,RomAddress> loadAreaDisplayLists(
	std::ostream &rom,
	const HashMap<string,MaterialSpecs> &materials
) {
	HashMap<DrawingLayer,RomAddress> dlMap;
	for( const auto &i : materials ) {
		if( dlMap.count( i.second.layer ) == 0 ) {
			writeRaw<ubyte>( rom, { 0x15, (ubyte)i.second.layer, 0x00, 0x00 });
			dlMap[i.second.layer] = rom.tellp();
			writeWordRaw( rom, 0 );
		}
	}
	return dlMap;
}

HashMap<DrawingLayer,RomAddress> writeAreaGeoLayout(
	std::ostream &rom,
	ushort areaId,
	const AreaSpecs &areaSpecs,
	const HashMap<string,MaterialSpecs> &materials,
	HashMap<ushort,RomAddress> &waterDls,
	[[maybe_unused]] std::vector<string> &warnings
) {
	INIT_RENDER_AREA
	START_NODE
		TOGGLE_DEPTH_BUFFER( false )
		START_NODE
			SCENE_ORTHO_PROJECTION
			START_NODE
				if( areaSpecs.backgroundImage != BackgroundImage::None ) {
					BACKGROUND_IMAGE( areaSpecs.backgroundImage )
				} else {
					BACKGROUND_COLOUR( areaSpecs.backgroundColour )
				}
			END_NODE
		END_NODE
		TOGGLE_DEPTH_BUFFER( true )
		START_NODE
			SET_CAMERA_FRUSTRUM( areaSpecs.fieldOfView, areaSpecs.nearClippingPlane, areaSpecs.farClippingPlane )
			START_NODE
				SET_CAMERA_PRESET( areaSpecs.cameraType, areaSpecs.pauseCamera.position, areaSpecs.pauseCamera.focus )
				START_NODE
					HashMap<DrawingLayer,RomAddress> dlMap = loadAreaDisplayLists( rom, materials );
					if( !areaSpecs.waterBoxes.empty() ) {
						writeWordRaw( rom, 0x15050000_be32 );
						waterDls[areaId] = rom.tellp();
						writeWordRaw( rom, 0x00000000 );
					}
					CREATE_SCENE_NODE
					if( areaSpecs.environmentEffect != EnvironmentEffect::None ) {
						LOAD_ENVFX( areaSpecs.environmentEffect )
					}
				END_NODE
			END_NODE
		END_NODE
		TOGGLE_DEPTH_BUFFER( false )
		START_NODE
			LOAD_SCREEN_TRANSITION
		END_NODE
	END_NODE
	END_GEO_LAYOUT
	return dlMap;
}

HashMap<DrawingLayer,RomAddress> writeObjectGeoLayout(
	std::ostream &rom,
	const ObjectSpecs &objectSpecs,
	const HashMap<string,MaterialSpecs> &materials,
	[[maybe_unused]] std::vector<string> &warnings
) {
	HashMap<DrawingLayer,RomAddress> dlMap;
	if( objectSpecs.shadow.enabled ) {
		INIT_WITH_SHADOW( objectSpecs.shadow )
	} else {
		INIT_WITH_CULLING_RADIUS( objectSpecs.cullingRadius.radius )
	}
	START_NODE
	for( const auto &i : materials ) {
		if( dlMap.count( i.second.layer ) == 0 ) {
			START_NODE
			if( objectSpecs.useObjectAlpha && ( objectSpecs.ditheredAlpha || i.second.layer >= DrawingLayer::Translucent ) ) {
				writeRaw<uint>( rom, { 0x18000000_be32, 0x8029D924_be32 });
			}
			writeRaw<ubyte>( rom, { 0x15, (ubyte)i.second.layer, 0x00, 0x00 });
			dlMap[i.second.layer] = rom.tellp();
			writeWordRaw( rom, 0 );
			END_NODE
		}
	}
	END_NODE
	END_GEO_LAYOUT
	return dlMap;
}

HashMap<DrawingLayer,RomAddress> writePartialObjectGeoLayout(
	std::ostream &rom,
	const PartialObjectSpecs &objectSpecs,
	const HashMap<string,MaterialSpecs> &materials,
	[[maybe_unused]] std::vector<string> &warnings
) {
	HashMap<DrawingLayer,RomAddress> dlMap;
	for( const auto &i : materials ) {
		if( dlMap.count( i.second.layer ) == 0 ) {
			START_NODE
			if( objectSpecs.useObjectAlpha && ( objectSpecs.ditheredAlpha || i.second.layer >= DrawingLayer::Translucent ) ) {
				writeRaw<uint>( rom, { 0x18000000_be32, 0x8029D924_be32 });
			}
			writeRaw<ubyte>( rom, { 0x15, (ubyte)i.second.layer, 0x00, 0x00 });
			dlMap[i.second.layer] = rom.tellp();
			writeWordRaw( rom, 0 );
			END_NODE
		}
	}
	DL_RETURN
	return dlMap;
}
