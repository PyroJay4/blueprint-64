#include "src/core/level-writer/water.hpp"

#include <cassert>
#include "src/core/level-writer/util.hpp"
#include "src/core/util.hpp"
#include "src/core/level-writer/texture.hpp"
#include "src/core/blueprint.hpp"
#include "src/core/baserom.hpp"
#include "src/core/import/model-importer.hpp"
#include "src/polyfill/byte-order.hpp"
#include "src/core/level-writer/f3d-macros.hpp"

void writeWaterCollision(
	std::ostream &rom,
	const std::vector<WaterBoxSpecs> &waterBoxes
) {
	if( waterBoxes.empty() ) return;
	writeHalfwordRaw( rom, 0x0044_be16 );
	writeHalfword( rom, (ushort)waterBoxes.size() );
	for( const WaterBoxSpecs &waterBox : waterBoxes ) {
		writeHalfwordRaw( rom, waterBox.isPoisonFog ? 0x32_be16 : 0x00 );
		writeSignedHalfwords( rom, {
			waterBox.xMin,
			waterBox.zMin,
			waterBox.xMax,
			waterBox.zMax,
			waterBox.y
		});
	}
}

#define POSITION_SEGMENTED toSegmentedPointer( 0x0E, mainSegmentStart, rom.tellp() )

struct WaterGrid {
	SegmentedPointer vertexPtr;
	WaterBoxSpecs specs;
	uint numCells;
};

static inline ushort be16s( short signedValue ) {
	const ushort value = reinterpret_cast<const ushort&>( signedValue );
	return htons( value );
}

struct ColourRef {
	ColourRGB24 colour;
	bool asmRef;
};

void writeWaterVisuals(
	std::ostream &rom,
	uint mainSegmentStart,
	const HashMap<ushort,RomAddress> &waterDls,
	RomAddress &animationHead,
	std::map<string,uint> &asmRefs
) {
	if( waterDls.empty() ) return;

	const ColourRGB24 WHITE = ColourRGB24( 0xFF, 0xFF, 0xFF );
	const SegmentedPointer DIFFUSE_LIGHT = POSITION_SEGMENTED;
	writeRaw<uint>( rom, { 0x000000FF_be32, 0x000000FF_be32, 0x00A6A600_be32, 0x00000000 });

	HashMap<WaterTextureInfo,TextureLocation> textures;
	for( const auto &i : waterDls ) {
		for( const WaterBoxSpecs &j : Blueprint::current()->areas().at( i.first ).waterBoxes ) {
			if( textures.count( j.texture ) > 0 ) continue;

			if( j.texture.asmRef ) {
				asmRefs[toAlphanumeric( j.texture.name ) + "_water_texture_data"] = POSITION_SEGMENTED;
			}

			if( Blueprint::current()->waterTextures().count( j.texture ) == 0 ) {
				const VanillaWaterTexture &vanillaWater = BaseRom::waterTextures().at( j.texture.textureHash );
				textures[j.texture] = TextureLocation{ vanillaWater.segmentedPointer, 32, 32 };

				continue;
			}

			textures[j.texture] = writeSingleTexture(
				rom,
				mainSegmentStart,
				(Blueprint::current()->getWaterTexturePath() / ( j.texture.textureHash + ".png" )).string(),
				j.texture.textureFormat
			);

		}
	}

	HashMap<const WaterBoxSpecs*,SegmentedPointer> colourPointers;
	HashMap<ColourRGB24,SegmentedPointer> reusableColours;
	reusableColours[WHITE] = POSITION_SEGMENTED;
	writeRaw<uint>( rom, { 0xFFFFFFFF, 0xFFFFFFFF, 0x28282800_be32, 0x00000000 });

	std::vector<WaterGrid> waterGrids;
	for( const auto &i : waterDls ) {
		const string areaPrefix = "area_"s + std::to_string( i.first );
		const FogSpecs &fog = Blueprint::current()->areas().at( i.first ).fog;

		const std::vector<WaterBoxSpecs> &waterBoxes = Blueprint::current()->areas().at( i.first ).waterBoxes;
		for( const WaterBoxSpecs &waterBox : waterBoxes ) {
			if( !waterBox.isVisible ) continue;
			assert( waterBox.xMin <= waterBox.xMax );
			assert( waterBox.zMin <= waterBox.zMax );
			assert( waterBox.scale > 0 );

			if( waterBox.texture.textureFormat >= TextureFormat::IA4 ) {
				if( waterBox.asmRefColour ) {
					asmRefs[areaPrefix + toAlphanumeric( waterBox.name ) + "_water_texture_colour"] = POSITION_SEGMENTED;
					colourPointers[&waterBox] = POSITION_SEGMENTED;
					waterBox.colour.writeToRom( rom, true );
					waterBox.colour.writeToRom( rom, true );
					writeRaw<uint>( rom, { 0x28282800_be32, 0x00000000 });
				} else if( reusableColours.count( waterBox.colour ) == 0 ) {
					reusableColours[waterBox.colour] = POSITION_SEGMENTED;
					waterBox.colour.writeToRom( rom, true );
					waterBox.colour.writeToRom( rom, true );
					writeRaw<uint>( rom, { 0x28282800_be32, 0x00000000 });
				}
			}

			const uint maxCellSize = 1920 * (uint)waterBox.scale;
			const uint xWidth = (int)waterBox.xMax - (int)waterBox.xMin;
			const uint zWidth = (int)waterBox.zMax - (int)waterBox.zMin;

			const uint xCells = (xWidth / maxCellSize) + ((xWidth % maxCellSize) != 0 ? 1 : 0);
			const uint zCells = (zWidth / maxCellSize) + ((zWidth % maxCellSize) != 0 ? 1 : 0);

			if( xCells == 0 || zCells == 0 ) continue;

			waterGrids.push_back({
				POSITION_SEGMENTED,
				waterBox,
				xCells * zCells
			});

			for( uint i = 0; i < zCells; i++ ) {
				const short z0 = waterBox.zMin + (short)(i * zWidth);
				const short z1 = (i == zCells - 1) ? waterBox.zMax : (z0 + zWidth);
				const short vmax = (i == zCells -1) ? (((((int)z1 - (int)z0) % maxCellSize) / (int)waterBox.scale) - 960) : 960;
				for( uint j = 0; j < xCells; j++ ) {
					const short x0 = waterBox.xMin + (short)(j * xWidth);
					const short x1 = (j == xCells - 1) ? waterBox.xMax : (x0 + xWidth);
					const short umax = (j == xCells -1) ? (((((int)x1 - (int)x0) % maxCellSize) / (int)waterBox.scale) - 960) : 960;

					const ushort X0 = be16s( x0 );
					const ushort X1 = be16s( x1 );
					const ushort Y = be16s( waterBox.y );
					const ushort Z0 = be16s( z0 );
					const ushort Z1 = be16s( z1 );
					constexpr ushort U0 = 0x8800_be16;
					const ushort U1 = be16s( umax << 5 );
					constexpr ushort V0 = 0x8800_be16;
					const ushort V1 = be16s( vmax << 5 );

					writeRaw<ushort>( rom, {
						X0, Y, Z0, 0, U0, V0, 0x00FF_be16, 0x00FF_be16,
						X1, Y, Z0, 0, U1, V0, 0x00FF_be16, 0x00FF_be16,
						X1, Y, Z1, 0, U1, V1, 0x00FF_be16, 0x00FF_be16,
						X0, Y, Z1, 0, U0, V1, 0x00FF_be16, 0x00FF_be16
					});
				}
			}

		}

		writeWordAt( rom, i.second, POSITION_SEGMENTED );

		RDP_PIPE_SYNC
		ENABLE_MODE_FLAGS( 0 )
		DISABLE_TD( 1 )
		RDP_TILE_SYNC
		RDP_LOAD_SYNC

		if( fog.enabled ) {
			SET_OTHERMODE_H( 20, 2, 1 )
			SET_OTHERMODE_L( 16, 16, 0xC810 )
			SET_FOG_COLOUR( fog.colour )
			SET_FOG_DISTANCE( rom, fog.fadeStartDistance, fog.fadeEndDistance );
			ENABLE_MODE_FLAGS( 0x00010000 )
		}

		CLEAR_GEOMETRY_FLAGS( 0x2000 )
		for( const WaterGrid &j : waterGrids ) {
			const WaterTextureInfo &texInfo = j.specs.texture;

			const bool useColour = (texInfo.textureFormat >= TextureFormat::IA4);
			const bool useOpacity = (texInfo.textureFormat != TextureFormat::IA8 && texInfo.textureFormat != TextureFormat::IA16);

			if( useOpacity ) {
				SET_ENV_COLOUR( 0xFFFFFF00 | (uint)j.specs.opacity );
				SET_COMBINE_ALPHA_ENV
			} else {
				SET_ENV_COLOUR( 0xFFFFFFFF )
				SET_COMBINE_ALPHA_TEX
			}

			SegmentedPointer colourPtr;
			if( useColour ) {
				colourPtr = j.specs.asmRefColour ? colourPointers.at( &j.specs ) : reusableColours.at( j.specs.colour );
			} else {
				colourPtr = reusableColours.at( WHITE );
			}
			LOAD_DARK_SHADE( colourPtr );
			LOAD_LIGHT_SHADE( DIFFUSE_LIGHT );

			const TextureLocation tex = textures.at( j.specs.texture );
			const ushort texels = (tex.width * tex.height) >> (((ubyte)texInfo.textureFormat & 3) ? 0 : 1);

			if( j.specs.asmRefTexturePtr ) {
				asmRefs[areaPrefix + toAlphanumeric( j.specs.name ) + "_water_texture_pointer"] = POSITION_SEGMENTED + 4;
			}
			SET_IMG( texInfo.textureFormat, tex.pointer )
			SET_TILE_7( texInfo.textureFormat )
			RDP_LOAD_SYNC
			LOAD_BLOCK( 7, texels, _dxt( tex.width, texInfo.textureFormat ) )
			RDP_PIPE_SYNC
			SET_TILE_0( rom, texInfo.textureFormat, tex.width, tex.height, TextureWrapping::Repeat, TextureWrapping::Repeat );
			writeTextureAnimation(
				rom,
				(ubyte)(1 << (i.first & 7)),
				j.specs.animation,
				tex.width,
				tex.height,
				TextureWrapping::Repeat,
				TextureWrapping::Repeat,
				animationHead,
				POSITION_SEGMENTED
			);
			SET_TILE_SIZE( 0, tex.width, tex.height );

			for( uint k = 0; k < j.numCells; ) {
				uint cellsToDraw = (k + 4 <= j.numCells) ? 4 : (j.numCells - k);
				LOAD_VTX( 4 * cellsToDraw, j.vertexPtr + (k * 64) );
				uint vo = 0;
				for( uint kNext = k + cellsToDraw; k < kNext; k++ ) {
					const uint tris[4] = {
						0xBF000000_be32,
						htonl( 0x00140A00 + vo ),
						0xBF000000_be32,
						htonl( 0x0014001E + vo )
					};
					rom.write( (const char*)tris, sizeof( tris ) );
					vo += 0x00282828;
				}
			}

		}
		SET_GEOMETRY_FLAGS( 0x2000 )

		if( fog.enabled ) {
			SET_OTHERMODE_H( 20, 2, 1 );
			SET_OTHERMODE_L( 16, 16, 0x0040 )
			DISABLE_MODE_FLAGS( 0x00010000 )
		}

		SET_ENV_COLOUR( 0xFFFFFFFF )
		DISABLE_TD( 0 )
		END_DL
	}

}

#undef POSITION_SEGMENTED
