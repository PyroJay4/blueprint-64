#ifndef SRC_CORE_LEVEL_WRITER_FAST3D_HPP_
#define SRC_CORE_LEVEL_WRITER_FAST3D_HPP_

#include <unordered_map>
#include <ostream>
#include "src/core/level-writer/util.hpp"
#include "src/core/specs.hpp"
#include "src/polyfill/filesystem.hpp"

extern void writeAreaFast3d(
	std::ostream &rom,
	const FogSpecs &fog,
	ubyte areaIndex,
	const std::filesystem::path &areaDir,
	uint mainSegmentStart,
	const HashMap<uint,SegmentedPointer> &shadeLocations,
	const HashMap<TextureInstance,TextureLocation> &textureLocations,
	const HashMap<DrawingLayer,RomAddress> &displayListLocations,
	const HashMap<string,MaterialSpecs> &materials,
	TextureFiltering defaultTextureFiltering,
	std::map<string,uint> &asmRefs,
	RomAddress &animationHead,
	std::vector<string> &warnings
);

extern void writeObjectFast3d(
	std::ostream &rom,
	const FogSpecs &fog,
	objectId objectId,
	const std::filesystem::path &objectDir,
	uint mainSegmentStart,
	const HashMap<uint,SegmentedPointer> &shadeLocations,
	const HashMap<TextureInstance,TextureLocation> &textureLocations,
	const HashMap<DrawingLayer,RomAddress> &displayListLocations,
	const HashMap<string,MaterialSpecs> &materials,
	TextureFiltering defaultTextureFiltering,
	std::map<string,uint> &asmRefs,
	const string &objectName,
	bool useEnvAlpha,
	bool ditheredAlpha,
	RomAddress &animationHead,
	std::vector<string> &warnings
);


#endif /* SRC_CORE_LEVEL_WRITER_FAST3D_HPP_ */
