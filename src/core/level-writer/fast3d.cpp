#include "src/core/level-writer/fast3d.hpp"

#include <map>
#include <unordered_map>
#include <utility>
#include <fstream>
#include "src/openmode.hpp"
#include "src/core/util.hpp"
#include "src/core/import/model-optimizer.hpp"
#include "src/core/level-writer/texture.hpp"
#include "src/core/level-writer/f3d-macros.hpp"

static inline void SET_TILE_0( std::ostream &rom, const MaterialSpecs &specs ) {
	SET_TILE_0( rom, specs.format, specs.textureInfo.value().width, specs.textureInfo.value().height, specs.hwrap, specs.vwrap );
}

struct ModelData {
	string material;
	SegmentedPointer verticesLocation;
	std::vector<PatchLoadInfo> patches;

	ModelData( string mat, SegmentedPointer vl, std::vector<PatchLoadInfo> &&p ) :
		material( mat ),
		verticesLocation( vl ),
		patches( p ) {}

	ModelData( const ModelData &other ) = delete;

	ModelData( ModelData &&other ) :
		material( std::move( other.material ) ),
		verticesLocation( other.verticesLocation ),
		patches( std::move( other.patches ) ) {}

};

#define POSITION_SEGMENTED (0x0E000000 | ((uint)rom.tellp() - mainSegmentStart))

static void writeFast3d(
	std::ostream &rom,
	const FogSpecs &fog,
	uint areaIndexOrObjectId,
	const string &context,
	const std::filesystem::path &areaOrObjectDir,
	uint mainSegmentStart,
	const HashMap<uint,SegmentedPointer> &shadeLocations,
	const HashMap<TextureInstance,TextureLocation> &textureLocations,
	const HashMap<DrawingLayer,RomAddress> &displayListLocations,
	const HashMap<string,MaterialSpecs> &materials,
	std::map<string,uint> &asmRefs,
	TextureFiltering defaultTextureFiltering,
	bool isObject,
	bool useEnvAlpha,
	bool ditheredAlpha,
	RomAddress &animationHead,
	[[maybe_unused]] std::vector<string> &warnings
) {
	std::multimap<float,ModelData> layers[7];
	for( const auto &i : materials ) {
		const string &name = i.first;
		const MaterialSpecs &mat = i.second;

		if( !mat.visible || !mat.textureInfo.has_value() ) continue;

		ModelLoadInfo model;
		std::ifstream modelFile( areaOrObjectDir / ( name + ".model" ), mode::read );
		model.load( modelFile );

		const SegmentedPointer verticesLocation = 0x0E000000 | ((uint)rom.tellp() - mainSegmentStart);
		rom.write( (const char*)model.vertexData.data(), 16 * model.vertexData.size() );

		ModelData modelData( name, verticesLocation, std::move( model.loadData ) );
		layers[(uint)mat.layer].insert( std::make_pair( -model.priority, std::move( modelData ) ) );
	}

	for( int i = 0; i < 7 ; i++ ) {
		if( layers[i].empty() ) continue;
		const DrawingLayer layer = (DrawingLayer)i;

		const SegmentedPointer fast3dLocation = 0x0E000000 | ((uint)rom.tellp() - mainSegmentStart);
		writeWordAt( rom, displayListLocations.at( layer ), fast3dLocation );

		RDP_PIPE_SYNC
		ENABLE_MODE_FLAGS( 0 ) //TODO: is this necessary? Seems like it should do nothing
		if( layer < DrawingLayer::Alphatest || ditheredAlpha ) {
			SET_COMBINE_OPAQUE
		} else {
			//TODO: Do Glide64, Glide64Mk2, and Parallel ACTUALLY not support texture-based alpha, or is this just a
			// weird colour combiner that those plugins don't like?
			SET_COMBINE_ALPHA_TEX
		}
		DISABLE_TD( 1 )
		RDP_TILE_SYNC
		RDP_LOAD_SYNC

		if( fog.enabled ) {
			const ushort fogBits = (layer == DrawingLayer::FastOpaque || layer >= DrawingLayer::Translucent) ? 0xC810 : 0xC811;
			SET_OTHERMODE_H( 20, 2, 1 )
			SET_OTHERMODE_L( 16, 16, fogBits )
			SET_FOG_COLOUR( fog.colour )
			SET_FOG_DISTANCE( rom, fog.fadeStartDistance, fog.fadeEndDistance );
			ENABLE_MODE_FLAGS( 0x00010000 )
		}

		if( ditheredAlpha ) {
			SET_OTHERMODE_L( 0, 2, 3 );
			// TODO? The two bits 4 bits left of the rightmost bits in OTHERMODE_H can change the dither pattern.
			// Maybe make that an option in the future?
		}

		for( const auto &j : layers[i] ) {
			const ModelData &model = j.second;
			const MaterialSpecs &specs = materials.at( model.material );
			const TextureLocation &texInfo = textureLocations.at({ specs.textureInfo.value().hash, specs.format, model.material, areaIndexOrObjectId, isObject ? std::optional<string>("") : std::nullopt, specs.asmRefs.texData });
			const ushort texels = (texInfo.width * texInfo.height) >> (((ubyte)specs.format & 3) ? 0 : 1);
			const string refPrefix = context + "_" + toAlphanumeric( model.material );

			SET_OTHERMODE_H( 12, 2, specs.filtering );

			if( layer >= DrawingLayer::Translucent && specs.opacity != 0xFF ) {
				if( specs.asmRefs.opacity ) {
					asmRefs[refPrefix + "_opacity_byte"] = POSITION_SEGMENTED + 7;
				}
				if( !useEnvAlpha ) {
					SET_ENV_COLOUR( 0xFFFFFF00 | specs.opacity )
				}
				SET_COMBINE_ALPHA_ENV
			}

			if( specs.asmRefs.shadePtr ) {
				asmRefs[refPrefix + "_shade_dark_pointer"] = POSITION_SEGMENTED + 4;
				asmRefs[refPrefix + "_shade_light_pointer"] = POSITION_SEGMENTED + 12;
			}
			LOAD_SHADE_VALUES( shadeLocations.at( specs.shadingRef ) )
			if( specs.asmRefs.texPtr ) {
				asmRefs[refPrefix + "_texture_pointer"] = POSITION_SEGMENTED + 4;
			}
			SET_IMG( specs.format, texInfo.pointer )
			SET_TILE_7( specs.format )
			RDP_LOAD_SYNC
			LOAD_BLOCK( 7, texels, _dxt( texInfo.width, specs.format ) )
			RDP_PIPE_SYNC
			SET_TILE_0( rom, specs );
			writeTextureAnimation(
				rom,
				isObject ? (ubyte)0xFF : (ubyte)(1 << areaIndexOrObjectId),
				specs.animation,
				specs.textureInfo.value().width,
				specs.textureInfo.value().height,
				specs.hwrap,
				specs.vwrap,
				animationHead,
				POSITION_SEGMENTED
			);
			SET_TILE_SIZE( 0, texInfo.width, texInfo.height );

			for( const PatchLoadInfo &patch : model.patches ) {
				LOAD_VTX( (uint)patch.numVertices, model.verticesLocation + ( 16 * patch.indexOffset ) );
				for( const std::array<ubyte,3> &face : patch.triangles ) {
					RENDER_TRI( face )
				}
			}

			if( layer >= DrawingLayer::Translucent && specs.opacity != 0xFF ) {
				if( !useEnvAlpha ) {
					SET_ENV_COLOUR( 0xFFFFFFFF )
				}
				SET_COMBINE_ALPHA_TEX
			}

			if( specs.filtering != defaultTextureFiltering ) {
				SET_OTHERMODE_H( 12, 2, defaultTextureFiltering );
			}
		}

		if( ditheredAlpha ) {
			SET_OTHERMODE_L( 0, 2, 0 );
		}

		if( fog.enabled ) {
			const ushort restoreBits = (layer == DrawingLayer::FastOpaque || layer >= DrawingLayer::Translucent) ? 0x0040 : 0x0044;
			SET_OTHERMODE_H( 20, 2, 1 );
			SET_OTHERMODE_L( 16, 16, restoreBits )
			DISABLE_MODE_FLAGS( 0x00010000 )
		}

		DISABLE_TD( 0 )
		END_DL
	}

}


void writeAreaFast3d(
	std::ostream &rom,
	const FogSpecs &fog,
	ubyte areaIndex,
	const std::filesystem::path &areaDir,
	uint mainSegmentStart,
	const HashMap<uint,SegmentedPointer> &shadeLocations,
	const HashMap<TextureInstance,TextureLocation> &textureLocations,
	const HashMap<DrawingLayer,RomAddress> &displayListLocations,
	const HashMap<string,MaterialSpecs> &materials,
	TextureFiltering defaultTextureFiltering,
	std::map<string,uint> &asmRefs,
	RomAddress &animationHead,
	std::vector<string> &warnings
) {
	writeFast3d(
		rom,
		fog,
		(uint)areaIndex,
		"area_" + std::to_string( (uint)areaIndex ),
		areaDir,
		mainSegmentStart,
		shadeLocations,
		textureLocations,
		displayListLocations,
		materials,
		asmRefs,
		defaultTextureFiltering,
		false,
		false,
		false,
		animationHead,
		warnings
	);
}

void writeObjectFast3d(
	std::ostream &rom,
	const FogSpecs &fog,
	objectId objectId,
	const std::filesystem::path &objectDir,
	uint mainSegmentStart,
	const HashMap<uint,SegmentedPointer> &shadeLocations,
	const HashMap<TextureInstance,TextureLocation> &textureLocations,
	const HashMap<DrawingLayer,RomAddress> &displayListLocations,
	const HashMap<string,MaterialSpecs> &materials,
	TextureFiltering defaultTextureFiltering,
	std::map<string,uint> &asmRefs,
	const string &objectName,
	bool useEnvAlpha,
	bool ditheredAlpha,
	RomAddress &animationHead,
	std::vector<string> &warnings
) {
	writeFast3d(
		rom,
		fog,
		objectId,
		"object_" + toAlphanumeric( objectName ),
		objectDir,
		mainSegmentStart,
		shadeLocations,
		textureLocations,
		displayListLocations,
		materials,
		asmRefs,
		defaultTextureFiltering,
		true,
		useEnvAlpha,
		ditheredAlpha,
		animationHead,
		warnings
	);
}

#undef POSITION_SEGMENTED
