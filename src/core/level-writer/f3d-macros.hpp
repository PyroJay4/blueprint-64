#ifndef SRC_CORE_LEVEL_WRITER_F3D_MACROS_HPP_
#define SRC_CORE_LEVEL_WRITER_F3D_MACROS_HPP_

#include "src/core/level-writer/util.hpp"

#define RDP_PIPE_SYNC writeRaw<uint>( rom, { 0xE7000000_be32, 0x00000000 });
#define RDP_TILE_SYNC writeRaw<uint>( rom, { 0xE8000000_be32, 0x00000000 });
#define RDP_LOAD_SYNC writeRaw<uint>( rom, { 0xE6000000_be32, 0x00000000 });
#define ENABLE_MODE_FLAGS( flags ) writeWords( rom, { 0xB7000000, flags });
#define DISABLE_MODE_FLAGS( flags ) writeWords( rom, { 0xB6000000, flags });
#define LOAD_DARK_SHADE( shadePtr ) writeWords( rom, { 0x03880010, shadePtr });
#define LOAD_LIGHT_SHADE( shadePtr ) writeWords( rom, { 0x03860010, shadePtr });
#define LOAD_SHADE_VALUES( shadePtr ) writeWords( rom, { 0x03860010, shadePtr + 8, 0x03880010, shadePtr });
#define SET_IMG( format, texPtr ) writeWords( rom, { 0xFD000000 | ((uint)(format) << 19), texPtr });
#define LOAD_BLOCK( td, texels, dxt ) writeWords( rom, { 0xF3000000, (td << 24) | (((texels)-1) << 12) | (uint)(dxt) });
#define SET_TILE_SIZE( td, width, height ) writeWords( rom, { 0xF2000000, (td << 24) | (((uint)(width)-1) << 14) | (((uint)(height)-1) << 2) });
#define END_DL writeRaw<uint>( rom, { 0xB8000000_be32, 0x00000000 });
#define LOAD_VTX( num, segPtr ) writeWords( rom, { 0x04000000 | (((num)-1) << 20) | ((num) << 4), segPtr });
#define RENDER_TRI( indices ) writeWordRaw( rom, 0xBF000000_be32 ); rom.put( 0x00 ); rom.put( indices[0] * 10 ); rom.put( indices[1] * 10 ); rom.put( indices[2] * 10 );
#define DISABLE_TD( td ) writeWords( rom, { 0xBB000000 | td, 0xFFFFFFFF });
#define SET_COMBINE_OPAQUE writeRaw<uint>( rom, { 0xFC127FFF_be32, 0xFFFFF838_be32 });
#define SET_COMBINE_ALPHA_TEX writeRaw<uint>( rom, { 0xFC121824_be32, 0xFF33FFFF_be32 });
#define SET_COMBINE_ALPHA_ENV writeRaw<uint>( rom, { 0xFC127E24_be32, 0xFFFFFBFD_be32 });
#define SET_FOG_COLOUR( colour ) writeWordRaw( rom, 0xF8000000_be32 ); colour.writeToRom( rom, true );
#define SET_OTHERMODE_H( shift, num, flags ) writeWords( rom, { 0xBA000000 | ((uint)(shift) << 8) | (uint)(num), (uint)(flags) << shift });
#define SET_OTHERMODE_L( shift, num, flags ) writeWords( rom, { 0xB9000000 | ((uint)(shift) << 8) | (uint)(num), (uint)(flags) << shift });
#define SET_TILE_7( format ) writeWords( rom, { 0xF5000000 | ((uint)(format) << 19), 0x07000000 });
#define SET_ENV_COLOUR( colour ) writeWords( rom, { 0xFB000000, colour });
#define SET_GEOMETRY_FLAGS( flags ) writeWords( rom, { 0xB7000000, flags });
#define CLEAR_GEOMETRY_FLAGS( flags ) writeWords( rom, { 0xB6000000, flags });

inline void SET_FOG_DISTANCE( std::ostream &rom, double min, double max ) {
	if( min == max ) {
		max += 0.002;
		min -= 0.002;
	}

	writeWordRaw( rom, 0xBC000008_be32 );
	const double fmd = (128.0 / (max - min));
	const double fod = (256.0 * (0.5 - min) / (max - min));

	const short fms = fmd > 32767.0 ? 0x7FFF : (fmd < -327678.0 ? 0x8000 : (short)fmd);
	const short fos = fod > 32767.0 ? 0x7FFF : (fod < -327678.0 ? 0x8000 : (short)fod);

	writeSignedHalfwords( rom, { fms, fos });
}

inline ushort _dxt( ushort width, TextureFormat format ) {
	ushort n = width >> (4 - ((ushort)format & 3));
	n = n > 0 ? n : 1;
	return (0x7FF + n) / n;
}

inline uint log2( uint x ) {
	assert( x > 0 );
	return 31 - __builtin_clz( x );
}

static inline void SET_TILE_0( std::ostream &rom, TextureFormat format, ushort width, ushort height, TextureWrapping hwrap, TextureWrapping vwrap ) {
	const uint upperWord =
		0xF5000000 |
		((uint)format << 19) |
		(((uint)width >> (4 - ((uint)format & 3))) << 9);
	const uint lowerWord =
		((uint)vwrap << 18) |
		(log2( (uint)height ) << 14) |
		((uint)hwrap << 8) |
		(log2( (uint)width ) << 4);
	writeWords( rom, { upperWord, lowerWord });
}

#endif /* SRC_CORE_LEVEL_WRITER_F3D_MACROS_HPP_ */
