#include "src/core/util.hpp"

#include <cstring>
#include <cctype>
#include <codecvt>
#include <cwctype>
#include <cstdio>
#include <locale>

using std::string;

string Util::toHexWord( uint word, bool includePrefix ) {
	if( includePrefix ) {
		char hex[11];
		std::sprintf( hex, "0x%08X", word );
		return string( hex );
	} else {
		char hex[9];
		std::sprintf( hex, "%08X", word );
		return string( hex );
	}
}

void Util::makeLowerCase( string &str ) {
	for( char &c : str ) {
		c = std::tolower( c );
	}
}

void Util::makeUpperCase( string &str ) {
	for( char &c : str ) {
		c = std::toupper( c );
	}
}

string quoteAndEscape( const string &path ) {
#ifdef _WIN32
	// Windows doesn't allow \ or " characters in file or directory names, and escaping doesn't really work, so just add quotes
	return "\""s + path + '"';
#else
	string escapedPath = "\"";
	escapedPath.reserve( path.size() + 2 );
	for( char c : path ) {
		if( c == '\\' || c == '"' ) {
			escapedPath += '\\';
		}
		escapedPath += c;
	}
	escapedPath += '"';
	return escapedPath;
#endif
}

static const LevelId COURSE_TO_LEVEL[26] = {
	LevelId::INVALID,
	LevelId::BobOmbBattlefield,
	LevelId::WhompsFortress,
	LevelId::JollyRodgerBay,
	LevelId::CoolCoolMountain,
	LevelId::BigBoosHaunt,
	LevelId::HazyMazeCave,
	LevelId::LethalLavaLand,
	LevelId::ShiftingSandLand,
	LevelId::DireDireDocks,
	LevelId::SnowmansLands,
	LevelId::WetDryWorld,
	LevelId::TallTallMountain,
	LevelId::TinyHugeIsland,
	LevelId::TickTockClock,
	LevelId::RainbowRide,
	LevelId::BowserInTheDarkWorld,
	LevelId::BowserInTheFireSea,
	LevelId::BowserInTheSky,
	LevelId::PeachsSecretSlide,
	LevelId::CavernOfTheMetalCap,
	LevelId::TowerOfTheWingCap,
	LevelId::VanishCapUnderTheMoat,
	LevelId::WingedMarioOverTheRainbow,
	LevelId::SecretAquarium,
	LevelId::EndScreen
};

static const CourseId LEVEL_TO_COURSE[37] = {
	CourseId::INVALID,
	CourseId::INVALID,
	CourseId::INVALID,
	CourseId::INVALID,
	CourseId::BigBoosHaunt,
	CourseId::CoolCoolMountain,
	CourseId::INVALID,
	CourseId::HazyMazeCave,
	CourseId::ShiftingSandLand,
	CourseId::BobOmbBattlefield,
	CourseId::SnowmansLands,
	CourseId::WetDryWorld,
	CourseId::JollyRodgerBay,
	CourseId::TinyHugeIsland,
	CourseId::TickTockClock,
	CourseId::RainbowRide,
	CourseId::INVALID,
	CourseId::BowserInTheDarkWorld,
	CourseId::VanishCapUnderTheMoat,
	CourseId::BowserInTheFireSea,
	CourseId::SecretAquarium,
	CourseId::BowserInTheSky,
	CourseId::LethalLavaLand,
	CourseId::DireDireDocks,
	CourseId::WhompsFortress,
	CourseId::EndScreen,
	CourseId::INVALID,
	CourseId::PeachsSecretSlide,
	CourseId::CavernOfTheMetalCap,
	CourseId::TowerOfTheWingCap,
	CourseId::BowserInTheDarkWorld,
	CourseId::WingedMarioOverTheRainbow,
	CourseId::INVALID,
	CourseId::INVALID,
	CourseId::INVALID,
	CourseId::INVALID,
	CourseId::TallTallMountain
};

CourseId levelIdToCourseId( LevelId levelId ) {
	return levelId < (LevelId)37 ? LEVEL_TO_COURSE[(ubyte)levelId] : CourseId::INVALID;
}

LevelId courseIdToLevelId( CourseId courseId ) {
	return courseId < (CourseId)26 ? COURSE_TO_LEVEL[(ubyte)courseId] : LevelId::INVALID;
}

bool isWhitespaceOrEmpty( const std::string &str ) {
	for( char c : str ) {
		if( !std::isspace( c ) ) return false;
	}
	return true;
}

bool stringEndsWith( const std::string &str, const std::string &suffix ) {
	return(
		str.size() <= suffix.size() &&
		std::memcmp( &str.c_str()[str.size() - suffix.size()], suffix.c_str(), suffix.size() ) == 0
	);
}

string trimStart( const string &str ) {
	if( str.empty() || !std::isspace( str[0] ) ) {
		return str;
	}

	for( size_t i = 1; i < str.length(); i++ ) {
		if( !std::isspace( str[i] ) ) {
			return str.substr( i );
		}
	}

	return "";
}

string trimEnd( const string &str ) {
	if( str.empty() || !std::isspace( str[str.length() - 1] ) ) {
		return str;
	}

	for( size_t i = str.length() - 1; i > 0; i++ ) {
		if( !std::isspace( str[i-1] ) ) {
			return str.substr( 0, i );
		}
	}

	return "";
}

class MemoryStreamBuffer final : public std::streambuf {

	public:
	MemoryStreamBuffer( char *data, size_t size ) : std::streambuf() {
		setg( data, data, &data[size] );
	}

	~MemoryStreamBuffer() {}

	protected:
	pos_type seekpos( pos_type pos, [[maybe_unused]] std::ios_base::openmode which ) override {
		setg( eback(), &eback()[pos], egptr() );
		return pos;
	}

	pos_type seekoff( off_type off, std::ios_base::seekdir dir, [[maybe_unused]] std::ios_base::openmode which ) override {
		char *start = eback();
		char *end = egptr();
		switch( dir ) {
			case std::ios_base::beg: setg( start, &start[off], end ); break;
			case std::ios_base::end: setg( start, &end[off], end ); break;
			case std::ios_base::cur: setg( start, &gptr()[off], end ); break;
			default: return -1;
		}
		return gptr() - start;
	}

};

MemoryStream::MemoryStream( char *data, size_t size ) :
	std::istream( new MemoryStreamBuffer( data, size ) ) {}

string toAlphanumeric( const std::string &str ) {
	std::wstring_convert<std::codecvt_utf8<wchar_t>, wchar_t> encoder;
	std::wstring safeName = encoder.from_bytes( str );
	for( size_t i = 0; i < safeName.length(); i++ ) {
		if( !std::iswalnum( safeName[i] ) ) {
			safeName[i] = '_';
		}
	}
	return encoder.to_bytes( safeName );
}
