#include "src/core/colour.hpp"

#include <string>
#include <cstdio>
#include <cstdlib>
#include <regex>

using std::regex;
namespace rx = std::regex_constants;

string ColourRGB24::toString() const {
	char hex[8];
	std::sprintf( hex, "#%02X%02X%02X", red, green, blue );
	return string( hex );
}

string ColourRGBA32::toString() const {
	char hex[10];
	std::sprintf( hex, "#%02X%02X%02X%02X", red, green, blue, alpha );
	return string( hex );
}

string ColourRGBA5551::toString() const {
	char hex[10];
	std::sprintf( hex, "#%02X%02X%02X%02X", getRed() << 3, getGreen() << 3, getBlue() << 3, getAlpha() ? 0xFF : 0x00 );
	return string( hex );
}

static const regex s_colourPattern(
	"^#[0-9a-fA-F]{6}([0-9a-fA-F]{2})?$",
	rx::ECMAScript | rx::optimize | rx::nosubs
);

static inline bool tryParseHelper( const string &str, uint &out ) {
	if( std::regex_match( str, s_colourPattern ) ) {
		out = (uint)std::strtoul( str.c_str() + 1, nullptr, 16 );
		if( str.size() < 9 ) {
			out = (out << 8) | 0xFF;
		}
		return true;
	}

	return false;
}

bool ColourRGB24::tryParse( const string &str ) {
	uint value;
	if( tryParseHelper( str, value ) ) {
		red = value >> 24;
		green = (value >> 16) & 0xFF;
		blue = (value >> 8) & 0xFF;
		return true;
	}
	return false;
}

bool ColourRGBA32::tryParse( const string &str ) {
	uint value;
	if( tryParseHelper( str, value ) ) {
		red = value >> 24;
		green = (value >> 16) & 0xFF;
		blue = (value >> 8) & 0xFF;
		alpha = value & 0xFF;
		return true;
	}
	return false;
}

bool ColourRGBA5551::tryParse( const string &str ) {
	uint value;
	if( tryParseHelper( str, value ) ) {
		setRed( value >> 24 );
		setGreen( (value >> 16) & 0xFF );
		setBlue( (value >> 8) & 0xFF );
		setAlpha( value & 0x80 );
		return true;
	}
	return false;
}

ColourRGB24 ColourRGB24::parse( const string &str ) {
	uint value;
	if( tryParseHelper( str, value ) ) {
		return ColourRGB24( value >> 24, (value >> 16) & 0xFF, (value >> 8) & 0xFF );
	}
	throw std::invalid_argument( "Failed to parse colour: "s + str );
}

ColourRGBA32 ColourRGBA32::parse( const string &str ) {
	uint value;
	if( tryParseHelper( str, value ) ) {
		return ColourRGBA32( value );
	}
	throw std::invalid_argument( "Failed to parse colour: "s + str );
}
