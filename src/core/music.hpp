#ifndef SRC_CORE_MUSIC_HPP_
#define SRC_CORE_MUSIC_HPP_

#include <map>
#include <vector>
#include <ostream>
#include "src/types.hpp"

struct MusicInfo {
	string name;
	std::vector<ubyte> soundBanks;
	uint numBytes;
	bool isVanilla;
	bool isPlaceholder;
};

struct CustomMusicInfo {
	string name;
	std::vector<ubyte> soundBanks;
};

namespace MusicData {

	void writeToRom( std::ostream &rom );
	std::vector<MusicInfo> getMusicInfo();
	const string &getVanillaMusicName( ubyte i );

}



#endif /* SRC_CORE_MUSIC_HPP_ */
