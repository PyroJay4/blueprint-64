#include "src/core/music.hpp"

#include <cassert>
#include <fstream>
#include "src/polyfill/filesystem.hpp"
#include "src/polyfill/byte-order.hpp"
#include "src/openmode.hpp"
#include "src/core/baserom.hpp"
#include "src/core/exceptions.hpp"
#include "src/core/blueprint.hpp"

namespace fs = std::filesystem;

struct VanillaMusicInfo {
	string name;
	const std::vector<ubyte> soundBanks;
	uint offset;
	uint numBytes;
};

static const VanillaMusicInfo s_vanillaMusic[35] = {
	{ "Sound Effects", { 10, 9, 8, 7, 6, 5, 4, 3, 2, 1, 0 }, 0x240, 0x3490 },
	{ "Star Catch Fanfare", { 34 }, 0x36D0, 0x270 },
	{ "Title Theme", { 17 }, 0x3940, 0x2040 },
	{ "SM64 Main Theme (BOB, WF, TTM, THI)", { 34 }, 0x5980, 0x1410 },
	{ "Inside the Castle Walls", { 14 }, 0x6D90, 0x9C0 },
	{ "Dire Dire Docks (JRB, DDD, SA)", { 19 }, 0x7750, 0x12B0 },
	{ "Lethal Lava Land (SSL, LLL)", { 15 }, 0x8A00, 0x9A0 },
	{ "Bowser's Theme (Bowser 1, Bowser 2)", { 18 }, 0x93A0, 0xD60 },
	{ "Snow Mountain (CCM, SML)", { 11 }, 0xA100, 0x1FD0 },
	{ "Slider (TTC, RR, PSS)", { 13 }, 0xC0D0, 0x1D10 },
	{ "Haunted House (BBH)", { 33, 16 }, 0xDDE0, 0x1630 },
	{ "Piranha Plant's Lullaby", { 20 }, 0xF410, 0x580 },
	{ "Cave Dungeon (HMC, WDW)", { 21 }, 0xF990, 0x1320 },
	{ "Game Start (Star Select)", { 22 }, 0x10CB0, 0x90 },
	{ "Powerful Mario (Wing Cap/Vanish Cap)", { 23 }, 0x10D40, 0xC40 },
	{ "Metallic Mario (Metal Cap)", { 24 }, 0x11980, 0x0AE0 },
	{ "Bowser's Message", { 18 }, 0x12460, 0x230 },
	{ "Bowser's Road (BitDW, BitFS, BitS)", { 25 }, 0x12690, 0x1290 },
	{ "Course Clear", { 31 }, 0x13920, 0x110 },
	{ "Merry-Go-Round", { 33 }, 0x13A30, 0x680 },
	{ "Race Fanfare", { 26 }, 0x140B0, 0xD0 },
	{ "Power Star (Star Spawn)", { 14 }, 0x14180, 0x290 },
	{ "Stage Boss", { 27 }, 0x14410, 0xD70 },
	{ "Bowser Clear (Key Get)", { 26 }, 0x15180, 0x2A0 },
	{ "Looping Steps (Infinite Staircase)", { 28 }, 0x15420, 0x700 },
	{ "Ultimate Bowser (Bowser 3)", { 29 }, 0x15B20, 0xDC0 },
	{ "Staff Role (Credits)", { 37 }, 0x168E0, 0x37F0 },
	{ "Correct Solution", { 20 }, 0x1A0D0, 0xE0 },
	{ "Toad's Message", { 32 }, 0x1A1B0, 0xD0 },
	{ "Peach's Message", { 30 }, 0x1A280, 0x1B0 },
	{ "Opening", { 27 }, 0x1A430, 0x6F0 },
	{ "Ultimate Bowser Clear (Grand Star Get)", { 26 }, 0x1AB20, 0x810 },
	{ "Ending Demo", { 35 }, 0x1B330, 0x760 },
	{ "File Select", { 36 }, 0x1BA90, 0x310 },
	{ "Lakitu Appears", { 27 }, 0x1BDA0, 0x140 }
};

static inline void wordAlign( std::ostream &rom ) {
	while( rom.tellp() % 4 != 0 ) rom.put( 0 );
}

void MusicData::writeToRom( std::ostream &rom ) {
	const std::vector<MusicInfo> music = MusicData::getMusicInfo();
	assert( music.size() >= 35 );
	assert( music[0].isVanilla );

	std::vector<uint> offsets;
	offsets.reserve( music.size() );

	/* Write sound bank usage */
	rom.seekp( 0x7F0000 + ( 2 * music.size() ) );
	for( const MusicInfo &track : music ) {
		offsets.push_back( (uint)rom.tellp() - 0x7F0000 );
		rom.put( (ubyte)track.soundBanks.size() );
		rom.write( (const char*)track.soundBanks.data(), track.soundBanks.size() );
	}

	if( rom.tellp() > 0x7F1000 ) {
		throw RomOverflowException( "How did you even MANAGE to overflow the sound bank sets data?" );
	}

	assert( offsets.size() == music.size() );
	rom.seekp( 0x7F0000 );
	for( uint offset : offsets ) {
		const ushort offsetBE = htons( (ushort)offset );
		rom.write( (const char*)&offsetBE, 2 );
	}

	/* Write m64 data */
	offsets.clear();
	rom.seekp( 0x3E00004 + (8 * music.size() ) );
	uint prevOffset = rom.tellp();
	for( uint i = 0; i < music.size(); i++ ) {
		const MusicInfo &track = music[i];

		wordAlign( rom );
		if( track.isPlaceholder ) {
			offsets.push_back( (uint)prevOffset - 0x3E00000 );
			continue;
		}

		offsets.push_back( (uint)rom.tellp() - 0x3E00000 );
		prevOffset = rom.tellp();

		if( track.isVanilla ) {
			BaseRom::copyTo( rom, 0x3E00000 + s_vanillaMusic[i].offset, s_vanillaMusic[i].numBytes );
		} else {
			const fs::path musicPath = Blueprint::current()->getMusicPath( i );
			if( !fs::exists( musicPath ) ) {
				throw BlueprintLoadError( "Blueprint is missing an m64 file." );
			}

			std::ifstream m64( musicPath.string(), mode::read );
			for( ubyte b = m64.get(); m64.good() && !m64.eof(); b = m64.get() ) {
				rom.put( b );
			}
		}
	}

	// Reserve 8kB of data at the end of the ROM for potential metadata
	if( (uint)rom.tellp() > 0x3FFE000 ) {
		throw RomOverflowException( "That's a lot of music. Exceeded 2040 kB limit." );
	}

	/* Write sequence pointers */
	const ushort numSeqBE = htons( (ushort)music.size() );
	rom.seekp( 0x3E00002 );
	rom.write( (const char*)&numSeqBE, 2 );

	assert( offsets.size() == music.size() );
	for( uint i = 0; i < music.size(); i++ ) {
		const uint data[2] = {
			htonl( offsets[i] ),
			htonl( music[i].numBytes )
		};
		rom.write( (const char*)data, 8 );
	}

}

std::vector<MusicInfo> MusicData::getMusicInfo() {
	const std::map<ubyte,CustomMusicInfo> &customMusic = Blueprint::current()->customMusic();
	assert( customMusic.count( 0 ) == 0 );

	std::vector<MusicInfo> music;

	ubyte foundTracks = 0;
	for( ubyte i = 0; i < 35; i++ ) {
		if( customMusic.count( i ) > 0 ) {
			foundTracks++;
			const fs::path musicPath = Blueprint::current()->getMusicPath( i );
			if( fs::exists( musicPath ) ) {
				const CustomMusicInfo &info = customMusic.at( i );
				music.push_back({
					info.name,
					info.soundBanks,
					(uint)fs::file_size( musicPath ),
					false,
					false
				});
				continue;
			}
		}

		const VanillaMusicInfo &info = s_vanillaMusic[i];
		music.push_back({
			info.name,
			info.soundBanks,
			info.numBytes,
			true,
			false
		});
	}

	for( ubyte i = 35; foundTracks < customMusic.size(); i++ ) {
		if( customMusic.count( i ) > 0 ) {
			foundTracks++;
			const fs::path musicPath = Blueprint::current()->getMusicPath( i );
			if( fs::exists( musicPath ) ) {
				const CustomMusicInfo &info = customMusic.at( i );
				music.push_back({
					info.name,
					info.soundBanks,
					(uint)fs::file_size( musicPath ),
					false,
					false
				});
				continue;
			}
		}

		const MusicInfo &info = music[music.size() - 1];
		music.push_back({
			"Placeholder",
			info.soundBanks,
			info.numBytes,
			false,
			true
		});
	}

	return music;
}

const string &MusicData::getVanillaMusicName( ubyte i ) {
	return s_vanillaMusic[i].name;
}
