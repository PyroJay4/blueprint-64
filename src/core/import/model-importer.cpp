#include "src/core/import/model-importer.hpp"

#include <type_traits>
#include <fstream>
#include <cstring>
#include "src/polyfill/byte-order.hpp"
#include "src/openmode.hpp"
#include "src/core/exceptions.hpp"
#include "src/core/import/model-optimizer.hpp"

#include "src/core/import/wavefront.hpp"

namespace fs = std::filesystem;

static_assert( std::is_trivially_copyable_v<ModelVertex> );
static_assert( std::is_trivially_copyable_v<ModelFace> );
static_assert( sizeof( ModelVertex ) == 16 );
static_assert( sizeof( ModelVertex ) % alignof( ModelVertex ) == 0 );
static_assert( sizeof( ModelFace ) == 48 );
static_assert( sizeof( ModelFace ) % alignof( ModelFace ) == 0 );

inline static short getShort( const ubyte *data, int index ) {
	const ushort value = ntohs( *((ushort*)&data[index]) );
	return reinterpret_cast<const short &>( value );
}

inline static void setShort( ubyte *data, short value, int index ) {
	((ushort*)&data[index])[0] = htons( reinterpret_cast<const ushort &>( value ) );
}

Vec3f ModelVertex::getPosition() const {
	return Vec3f(
		(float)getShort( data, 0 ),
		(float)getShort( data, 2 ),
		(float)getShort( data, 4 )
	);
}

Vec2f ModelVertex::getUV( uint textureWidth, uint textureHeight ) const {
	return Vec2f(
		(float)((double)getShort( data, 6 ) / (double)(textureWidth << 5)),
		(float)((double)getShort( data, 8 ) / (double)(textureHeight << 5))
	);
}

Vec3f ModelVertex::getNormal() const {
	return Vec3f(
		(float)data[12] / (float)0xFF,
		(float)data[13] / (float)0xFF,
		(float)data[14] / (float)0xFF
	);
}

uint ModelVertex::getColour() const {
	return ntohl( *((uint*)&data[12]) );
}

void ModelVertex::setPosition( const Vec3f &posn ) {
	if(
		posn.x >= 32767.5 || posn.x <= -32769.5 ||
		posn.y >= 32767.5 || posn.y <= -32769.5 ||
		posn.z >= 32767.5 || posn.z <= -32769.5
	) {
		throw FileParserException( "Vertex lies out of range of 16-bit integer. Geometry must lie within the range [-32768,32767]." );
	}
	setShort( data, (short)(posn.x + 0.5), 0 );
	setShort( data, (short)(posn.y + 0.5), 2 );
	setShort( data, (short)(posn.z + 0.5), 4 );
	data[6] = data[7] = 0;
}

void ModelVertex::setUV( const Vec2f &uv, uint textureWidth, uint textureHeight ) {
	setShort( data, (short)((double)uv[0] * (double)(textureWidth << 5)), 8 );
	setShort( data, (short)((1.0 - (double)uv[1]) * (double)(textureHeight << 5)), 10 );
}

void ModelVertex::setNormal( const Vec3f &normal ) {
	const Vec3f n = normal / normal.magnitude();
	data[12] = (ubyte)(n.x * (float)0xFF);
	data[13] = (ubyte)(n.y * (float)0xFF);
	data[14] = (ubyte)(n.z * (float)0xFF);
	data[15] = 0xFF;
}

void ModelVertex::setColour( uint colour ) {
	((uint*)&data[12])[0] = htonl( colour );
}

bool ModelVertex::operator==( const ModelVertex &other ) const {
	return std::memcmp( this, &other, sizeof( ModelVertex ) ) == 0;
}

static const HashMap<string,const IModelImporter*> &getImporters() {
	static HashMap<string,const IModelImporter*> s_importers = {
		{ WavefrontImporter::extension(), new WavefrontImporter() }
	};
	return s_importers;
}

const IModelImporter *IModelImporter::tryGetImporter( const string &fileExtension ) {
	const auto i = getImporters().find( fileExtension );
	return (i == getImporters().end()) ? nullptr : i->second;
}

BoundingBox IModelImporter::optimizeAndSave(
	HashMap<std::string,MaterialFaces> &materials,
	const fs::path &outputDir
) const {
	BoundingBox bounds = { 0, 0, 0, 0, 0, 0 };
	for( const auto &i : materials ) {
		if( i.second.faces.empty() ) {
			fs::forceDelete( outputDir / (i.first + ".verts") );
			fs::forceDelete( outputDir / (i.first + ".model") );
			continue;
		}

		std::ofstream vertexFile( ( outputDir / (i.first + ".verts") ).string(), mode::write );
		for( const ModelFace &face : i.second.faces ) {
			for( const ModelVertex &vertex : face ) {
				vertexFile.write( (const char*)&vertex, 6 );
			}
		}

		OptimizedModel model = optimizeModel( i.second );
		std::ofstream modelFile( ( outputDir / (i.first + ".model") ).string(), mode::write );
		model.modelLoader.save( modelFile );
		bounds.merge( model.boundingBox );
	}
	return bounds;
}

static string buildSupportedFormatsString() {
	string formats = "";
	for( const auto &i : getImporters() ) {
		formats += i.second->formatName() + " (*" + i.second->fileExtension() + ");;";
	}
	return formats + "All Files (*)";
}

const string &IModelImporter::getSupportedFormats() {
	static string s_formats = buildSupportedFormatsString();
	return s_formats;
}
