#ifndef CORE_WAVEFRONT_HPP_
#define CORE_WAVEFRONT_HPP_

#include "src/core/import/model-importer.hpp"

class WavefrontImporter final : public IModelImporter {

	public:
	WavefrontImporter() {}
	~WavefrontImporter() {}

	static const string &extension() { static string s_ext = ".obj"; return s_ext; }

	const string &formatName() const override { static string s_name = "Wavefront"; return s_name; }
	const string &fileExtension() const override { return extension(); }

	ImportedModel import(
		const std::filesystem::path &filePath,
		double scale,
		const std::filesystem::path &textureOutputDir,
		const std::filesystem::path &modelOutputDir,
		const string &prefix = ""
	) const override;

};

#endif /* CORE_WAVEFRONT_HPP_ */
