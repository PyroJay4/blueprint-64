#ifndef SRC_CORE_IMPORT_MODEL_IMPORTER_HPP_
#define SRC_CORE_IMPORT_MODEL_IMPORTER_HPP_

#include "src/types.hpp"
#include "src/core/specs.hpp"
#include "src/core/vector.hpp"
#include "src/polyfill/filesystem.hpp"

#include <optional>
#include <vector>
#include <array>
#include <unordered_map>

struct ModelVertex {
	ubyte data[16];

	Vec3f getPosition() const;
	Vec2f getUV( uint textureWidth, uint textureHeight ) const;
	Vec3f getNormal() const;
	uint getColour() const;

	void setPosition( const Vec3f &posn );
	void setUV( const Vec2f &uv, uint textureWidth, uint textureHeight );
	void setNormal( const Vec3f &normal );
	void setColour( uint colour );

	inline static ModelVertex create( Vec3f position, Vec2f uv, Vec3f normal, uint textureWidth, uint textureHeight ) {
		ModelVertex v;
		v.setPosition( position );
		v.setUV( uv, textureWidth, textureHeight );
		v.setNormal( normal );
		return v;
	}

	inline static ModelVertex create( Vec3f position, Vec2f uv, uint colour, uint textureWidth, uint textureHeight ) {
		ModelVertex v;
		v.setPosition( position );
		v.setUV( uv, textureWidth, textureHeight );
		v.setColour( colour );
		return v;
	}

	bool operator==( const ModelVertex &other ) const;

};

static_assert( sizeof( ModelVertex ) == 16 );

namespace std {
	template <> struct hash<ModelVertex> {
		size_t operator()(const ModelVertex & x) const {
			if constexpr( sizeof( size_t ) == 8 ) {
				const size_t &A = *((size_t*)x.data);
				const size_t &B = *((size_t*)&x.data[8]);
				return A ^ B;
			} else if constexpr( sizeof( long ) == 8 ) {
				const hash<long> h;
				return h( *((long*)x.data) ) ^ h( *((long*)&x.data[8]) );
			} else {
				const hash<uint> h;
				return(
					( h( *((uint*)x.data) ) ^ h( *((uint*)&x.data[4]) ) ) ^
					( h( *((uint*)&x.data[8]) ) ^ h( *((uint*)&x.data[12]) ) )
				);
			}
		}
	};
}

typedef std::array<ModelVertex,3> ModelFace;

struct MaterialFaces {
	string materialName;
	std::optional<TextureInfo> textureInfo;
	bool useVertexColouring;
	std::vector<ModelFace> faces;

	MaterialFaces() : useVertexColouring(false) {}

	MaterialFaces( const MaterialFaces &other ) = delete;

	MaterialFaces( MaterialFaces &&other ) noexcept :
		materialName( std::move( other.materialName ) ),
		textureInfo( std::move( other.textureInfo ) ),
		useVertexColouring( other.useVertexColouring ),
		faces( std::move( other.faces ) )
	{}

};

struct ImportedModel {
	HashMap<std::string,MaterialFaces> materials;
	BoundingBox boundingBox;
	std::vector<std::string> warnings;
};


class IModelImporter {

	protected:
	IModelImporter() {}

	public:
	virtual const string &formatName() const = 0;
	virtual const string &fileExtension() const = 0;

	virtual ImportedModel import(
		const std::filesystem::path &filePath,
		double scale,
		const std::filesystem::path &textureOutputDir,
		const std::filesystem::path &modelOutputDir,
		const string &prefix = ""
	) const = 0;

	virtual ~IModelImporter() {}

	static const IModelImporter *tryGetImporter( const string &fileExtension );
	static const string &getSupportedFormats();

	protected:
	BoundingBox optimizeAndSave(
		HashMap<std::string,MaterialFaces> &materials,
		const std::filesystem::path &outputDir
	) const;

};


#endif /* SRC_CORE_IMPORT_MODEL_IMPORTER_HPP_ */
