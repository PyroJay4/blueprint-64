#include "src/core/import/wavefront.hpp"

#include <regex>
#include <map>
#include <fstream>
#include <utility>
#include "src/polyfill/filesystem.hpp"
#include "src/openmode.hpp"
#include "src/core/exceptions.hpp"
#include "src/core/util.hpp"
#include "src/core/import/texture.hpp"

using namespace std;
namespace fs = std::filesystem;

static const regex IndexTupleRegex(
	"^(\\d+)/(\\d*)(?:/(\\d+))$",
	regex_constants::ECMAScript | regex_constants::optimize
);

static void importMaterials(
	HashMap<string,MaterialFaces> &materialMap,
	const fs::path &matFile,
	const fs::path &textureOutputDir,
	const string &prefix,
	std::vector<string> &warnings
) {
	string currentMaterial;

	ifstream file( matFile.string(), mode::read );
	while( file.good() && !file.eof() ) {
		string lineStr, cmd;
		getline( file, lineStr );
		if( lineStr.length() < 1 ) {
			continue;
		}

		istringstream line( lineStr );
		line >> cmd;

		if( cmd == "newmtl" ) {
			currentMaterial.clear();
			getline( line, currentMaterial );
			currentMaterial = prefix + trim( currentMaterial );
			materialMap[currentMaterial].materialName = currentMaterial;
		} else if( cmd == "map_Kd" && !currentMaterial.empty() ) {
			string tex;
			getline( line, tex );
			fs::path texturePath( trim( tex ) );

			if( texturePath.is_relative() ) {
				texturePath = fs::path( matFile ).parent_path() / texturePath;
			}

			if( !fs::exists( texturePath ) ) {
				warnings.push_back( "Texture file not found: " + texturePath.string() );
				currentMaterial.clear();
				continue;
			}

			TextureFile texFile;
			TextureFile::TextureLoadStatus status = TextureFile::tryOpen( texturePath.string().c_str(), texFile );
			if( status != TextureFile::Error ) {
				MaterialFaces &material = materialMap[currentMaterial];
				material.textureInfo = texFile.info();
				texFile.saveAsPng( ( textureOutputDir / ( texFile.info().hash + ".png" ) ).string().c_str() );
				if( status == TextureFile::Resized ) {
					warnings.push_back( "Texture file \""s + texturePath.string() + "\" was resized." );
				}
			} else {
				warnings.push_back( "Error loading texture file: "s + texturePath.string() );
			}

			currentMaterial.clear();
		}
	}

	if( !file.good() && !file.eof() ) {
		throw FileParserException( "Error reading material file." );
	}
}

ImportedModel WavefrontImporter::import(
	const fs::path &filePath,
	double scale,
	const fs::path &textureOutputDir,
	const fs::path &modelOutputDir,
	const string &prefix
) const {
	ifstream file( filePath, mode::read );
	HashMap<string,MaterialFaces> materials;
	size_t lineNumber = 0;

	string currentMaterial = "undefined";
	vector<Vec3f> vertices;
	vector<Vec2f> uvcs;
	vector<Vec3f> normals;

	fs::create_directories( textureOutputDir );
	fs::create_directories( modelOutputDir );

	bool materialUVsGood = true;
	std::vector<string> warnings;
	while( file.good() && !file.eof() ) {
		string lineStr;
		std::getline( file, lineStr );
		lineNumber++;
		if( isWhitespaceOrEmpty( lineStr ) ) continue;

		istringstream line( lineStr );
		string cmd;
		line >> cmd;

		if( cmd == "mtllib") {
			string matFile;
			std::getline( line, matFile );

			fs::path matFilePath( trim( matFile ) );
			if( matFilePath.is_relative() ) {
				matFilePath = fs::path( filePath ).parent_path() / matFilePath;
			}
			importMaterials( materials, matFilePath, textureOutputDir, prefix, warnings );
		} else if( cmd == "usemtl" ) {
			currentMaterial.clear();
			std::getline( line, currentMaterial );
			currentMaterial = prefix + trim( currentMaterial );
			materialUVsGood = true;
		} else if( cmd == "v" ) {
			float x, y, z;
			line >> x >> y >> z;
			if( file.bad() ) {
				throw new FileParserException( "Failed to parse vertex.", lineNumber );
			}
			vertices.push_back({ x, y, z });
		} else if( cmd == "vt" ) {
			float u, v;
			line >> u >> v;
			if( file.bad() ) {
				throw new FileParserException( "Failed to parse vertex UV coordinates.", lineNumber );
			}
			uvcs.push_back({ u, v });
		} else if( cmd == "vn" ) {
			float x, y, z;
			line >> x >> y >> z;
			if( file.bad() ) {
				throw new FileParserException( "Failed to parse vertex normal.", lineNumber );
			}
			normals.push_back({ x, y, z });
		} else if( cmd == "f" ) {
			ModelFace face;

			Vec3f positions[3];
			bool autocalulateNormals = false;
			for( int i = 0; i < 3; i++ ) {
				string vrefs;
				line >> vrefs;

				smatch matches;
				if( !regex_search( vrefs, matches, IndexTupleRegex ) || matches.size() < 3 ) {
					throw FileParserException( "Invalid face vertex: " + vrefs, lineNumber );
				}

				size_t vi = stoull( matches[1].str() ) - 1;
				if( vi >= vertices.size() ) {
					throw FileParserException( "Vertex index out of range: " + vi, lineNumber );
				}
				positions[i] = vertices[vi];
				face[i].setPosition( vertices[vi] * scale );

				if( !matches[2].str().empty() ) {
					size_t vti = stoull( matches[2].str() ) - 1;
					if( vti >= uvcs.size() ) {
						throw FileParserException( "UV index out of range: " + vti, lineNumber );
					}

					if( materials.find( currentMaterial ) == materials.end() ) {
						face[i].setUV( uvcs[vti], 32, 32 );
					} else {
						const MaterialFaces &mat = materials.find( currentMaterial )->second;
						if( mat.textureInfo.has_value() ) {
							const Vec2f &uv = uvcs[vti];
							const ushort width = mat.textureInfo.value().width;
							const ushort height = mat.textureInfo.value().height;
							if( materialUVsGood && (
								uv.x < -1024.0 / width || uv.x >= 1024.0 / width ||
								uv.y < -1024.0 / height || uv.y >= 1024.0 / height
							)) {
								warnings.push_back( "UV coordinates on material \""s + mat.materialName + "\" are out of range. UV coordinates should be in the range [-1024,1024)." );
								materialUVsGood = false;
							}
							face[i].setUV( uv, width, height );
						} else {
							face[i].setUV( uvcs[vti], 32, 32 );
						}
					}
				} else {
					face[i].setUV( { 0, 0 }, 32, 32 );
				}

				if( !matches[3].str().empty() ) {
					size_t vni = stoull( matches[3].str() ) - 1;
					if( vni >= normals.size() ) {
						throw FileParserException( "Vertex normal index out of range: " + vni, lineNumber );
					}
					face[i].setNormal( normals[vni] );
				} else {
					autocalulateNormals = true;
				}

			}

			if( autocalulateNormals ) {
				Vec3f normal = ( positions[1] - positions[0] ).cross( positions[2] - positions[1] );
				normal /= normal.magnitude();
				face[0].setNormal( normal );
				face[1].setNormal( normal );
				face[2].setNormal( normal );
			}

			materials[currentMaterial].faces.push_back( face );
		}

	}

	if( !file.eof() ) {
		throw FileParserException( lineNumber );
	}

	BoundingBox boundingBox = optimizeAndSave( materials, modelOutputDir );
	return ImportedModel{
		std::move( materials ),
		boundingBox,
		std::move( warnings )
	};
}
