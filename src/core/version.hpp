#ifndef CORE_VERSION_HPP_
#define CORE_VERSION_HPP_

#include <optional>
#include <iostream>
#include "src/types.hpp"

struct Version final {

	ushort major, minor, patch;

	string toString() const;
	static std::optional<Version> tryParse( const string &versionString );
	static Version parse( const string &versionString );

	inline void writeToRom( std::ostream &rom ) const {
		ushort data[] = { htons( major ), htons( minor ), htons( patch ) };
		static_assert( sizeof( data ) == 6 );
		rom.write( (char*)data, 6 );
	}

	static inline Version readFromRom( std::istream &rom ) {
		ushort major, minor, patch;
		rom.read( (char*)&major, 2 );
		rom.read( (char*)&minor, 2 );
		rom.read( (char*)&patch, 2 );
		return Version{ ntohs( major), ntohs( minor ), ntohs( patch ) };
	}

	static int64 compare( const Version &a, const Version &b ) {
		int64 A = ((int64)a.major << 32) + ((int64)a.minor << 16) + (int64)a.patch;
		int64 B = ((int64)b.major << 32) + ((int64)b.minor << 16) + (int64)b.patch;
		return A - B;
	}

};

inline bool operator==( const Version &lhs, const Version &rhs ) {
	return Version::compare( lhs, rhs ) == 0ll;
}
inline bool operator!=( const Version &lhs, const Version &rhs ) {
	return Version::compare( lhs, rhs ) != 0ll;
}
inline bool operator<( const Version &lhs, const Version &rhs ) {
	return Version::compare( lhs, rhs ) < 0ll;
}
inline bool operator<=( const Version &lhs, const Version &rhs ) {
	return Version::compare( lhs, rhs ) <= 0ll;
}
inline bool operator>( const Version &lhs, const Version &rhs ) {
	return Version::compare( lhs, rhs ) > 0ll;
}
inline bool operator>=( const Version &lhs, const Version &rhs ) {
	return Version::compare( lhs, rhs ) >= 0ll;
}

namespace CurrentVersion {
	extern const Version Application;

	extern const Version BlueprintSchema;
	extern const Version PreferencesSchema;
	extern const Version TweakSchema;
	extern const Version MetadataFormat;
	extern const Version RomVersion;
}

#endif /* CORE_VERSION_HPP_ */
