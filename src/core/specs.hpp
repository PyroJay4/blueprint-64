#ifndef CORE_SPECS_HPP_
#define CORE_SPECS_HPP_

#include <functional>
#include <cstring>
#include <unordered_set>
#include <unordered_map>
#include <optional>
#include <variant>
#include <map>
#include "src/types.hpp"
#include "src/core/vector.hpp"
#include "src/core/enums.hpp"
#include "src/core/colour.hpp"
#include "src/core/objectid.hpp"
#include "src/core/uuid.hpp"

struct ShadingSpecs {
	//TODO: support alpha?
	string name;
	ColourRGB24 darkShade;
	ColourRGB24 lightShade;
	bool asmRef;

	static const ShadingSpecs Default;
};

struct RomSpecs {

	string internalName;
	bool enableChecksum;
	LevelBoundarySize levelBoundaries;
	LevelId startingLevel;

	LevelId pauseWarpLevel;
	ubyte pauseWarpArea;
	ubyte pauseWarpId;

	static const RomSpecs Default;

};

struct FogSpecs {
	double fadeStartDistance;
	double fadeEndDistance;
	ColourRGB24 colour;
	bool enabled;
};

struct NullAnimationSpecs {};

struct LinearAnimationSpecs {
	int speedX;
	int speedY;
};

struct SineWaveAnimationSpecs {
	ushort angle;
	ushort period;
	ushort amplitude;
};

typedef std::variant<NullAnimationSpecs, LinearAnimationSpecs,SineWaveAnimationSpecs> AnimationSpecs;

struct WaterTextureInfo {
	string textureHash;
	string name;
	TextureFormat textureFormat;
	bool asmRef;

	bool operator==( const WaterTextureInfo &other ) const {
		return(
			textureHash == other.textureHash &&
			textureFormat == other.textureFormat &&
			asmRef == other.asmRef &&
			(!asmRef || (name == other.name))
		);
	}

	inline bool operator!=( const WaterTextureInfo &other ) const {
		return !(*this == other);
	}
};

namespace std {
	template <> struct hash<WaterTextureInfo> {
		size_t operator()(const WaterTextureInfo & x) const {
			static const hash<string> strHash;
			const size_t h = strHash( x.textureHash ) ^ (size_t)x.textureFormat;
			return x.asmRef ? (h ^ strHash( x.name )) : h;
		}
	};
}

struct VanillaWaterTexture {
	uint segmentedPointer;
	TextureFormat textureFormat;
};

struct WaterBoxSpecs {
	string name;
	bool isPoisonFog;
	bool isVisible;
	bool asmRefTexturePtr;
	bool asmRefColour;
	short xMin, xMax;
	short zMin, zMax;
	short y;
	short scale;
	ubyte opacity;
	ColourRGB24 colour;
	WaterTextureInfo texture;
	AnimationSpecs animation;

	static const WaterBoxSpecs Default;
};

struct AreaSpecs {

	BackgroundImage backgroundImage;
	ColourRGBA5551 backgroundColour;

	struct {
		Vec3s position;
		Vec3s focus;
		bool fixed;
	} pauseCamera;
	CameraPreset cameraType;

	short fieldOfView;
	ushort nearClippingPlane;
	ushort farClippingPlane;
	FogSpecs fog;

	EnvironmentEffect environmentEffect;
	TerrainType terrainType;
	sbyte echoLevel;
	ubyte music;

	ubyte numObjects;
	ubyte numWarps;
	ubyte numInstantWarps;
	//ubyte numPaintingWarps;

	bool autoDeathFloor;
	short voidHeight;

	std::vector<WaterBoxSpecs> waterBoxes;

	static const AreaSpecs Default;

};

struct ObjectSpecs {

	ubyte modelId;
	string name;

	struct {
		bool autoCompute;
		bool alwaysUpright;
		float maxScale;
		short maxFOV;
		ushort radius;
	} cullingRadius;

	struct {
		ShadowType shape;
		ushort size;
		ubyte opacity;
		bool enabled;
	} shadow;

	FogSpecs fog;

	bool useObjectAlpha;
	bool ditheredAlpha;

	bool hasCollision;
	bool collisionAsmRef;
	uint collisionPtrAddr;

	// These properties are cached from the model data and used to auto-compute the culling radius
	ushort max_xz;
	ushort max_xyz;

	static const ObjectSpecs Default;

};

struct PartialObjectSpecs {

	string name;
	bool useObjectAlpha;
	bool ditheredAlpha;

};

struct AdvancedObjectSpecs {

	ubyte modelId;
	string name;
	uint customDataId;

};

enum class LinkType : ubyte {
	None = 0,
	UserData = 1,
	PartialObject = 2
};

struct UserDataLink {
	LinkType linkType;
	uint targetId;
	uint targetOffset;
};

struct UserData {
	string name;
	std::map<uint,UserDataLink> links;
	bool asmRef;
	size_t bytes;
};

struct LevelSpecs {

	string name;
	string starNames[6];

	struct {
		ubyte area;
		short x;
		short y;
		short z;
		float yaw;
	} marioSpawn;

	bool enableStarSelect;
	sbyte objectBanks[3];
	ushort maxSoundDistance;
	sbyte defaultEchoLevel;

	std::array<Vec3f,25> starSpawnLocations;
	std::array<std::vector<Vec3s>,22> waypoints;
	std::map<uint,ShadingSpecs> shadingValues;

	HashMap<objectId,ObjectSpecs> simpleObjects;
	HashMap<objectId,PartialObjectSpecs> partialObjects;
	HashMap<objectId,AdvancedObjectSpecs> advancedObjects;

	HashMap<uint,UserData> userData;

	static const LevelSpecs Default;

};

struct TextureInfo {
	string hash;

	ushort width;
	ushort height;

	bool hasEmptyPixels;
	bool hasFullAlpha;
	bool hasVaryingAlpha;
	bool isGreyscale;
	ubyte sourceImageDepth;

	ubyte averageAlpha;
	ushort uniqueColours;

	DrawingLayer suggestLayer() const;
	TextureFormat suggestFormat() const;
	HashSet<TextureFormat> getPossibleFormats() const;
	bool supportsFormat( TextureFormat format ) const;
};

struct MaterialSpecs {

	bool visible;
	bool solid;

	ubyte opacity;
	bool removedTextureAlpha;

	DrawingLayer layer;
	TextureFormat format;
	TextureFiltering filtering;

	TextureWrapping hwrap;
	TextureWrapping vwrap;

	uint shadingRef;

	struct {
		CollisionType type;
		ubyte direction;
		ForceIntensity force;
	} collision;

	std::optional<TextureInfo> textureInfo;
	AnimationSpecs animation;

	struct {
		bool texData;
		bool texPtr;
		bool shadePtr;
		bool opacity;
	} asmRefs;

	static const MaterialSpecs Default;

};

struct BoundingBox {
	short min_x, max_x;
	short min_y, max_y;
	short min_z, max_z;

	inline bool isEmpty() const {
		return
			min_x == 0 && max_x == 0 &&
			min_y == 0 && max_y == 0 &&
			min_z == 0 && max_z == 0;
	}

	void merge( const BoundingBox &other ) {
		min_x = other.min_x < min_x ? other.min_x : min_x;
		max_x = other.max_x > max_x ? other.max_x : max_x;
		min_y = other.min_y < min_y ? other.min_y : min_y;
		max_y = other.max_y > max_y ? other.max_y : max_y;
		min_z = other.min_z < min_z ? other.min_z : min_z;
		max_z = other.max_z > max_z ? other.max_z : max_z;
	}
};

#endif /* CORE_SPECS_HPP_ */
