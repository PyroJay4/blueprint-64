#ifndef CORE_PREFERENCES_HPP_
#define CORE_PREFERENCES_HPP_

#include <list>
#include "src/core/version.hpp"
#include "src/core/emulator.hpp"

struct AppPreferences {
	Version prefVersion;
	bool openOnStart;
	bool nativeMenuBar;
	double defaultScale;
	Emulator::EmulatorSettings emuConfig;
	string theme;

#if !defined(__linux__) && !defined(__unix__)
	string sevenZipPath;
#endif

	static AppPreferences &current();

	static AppPreferences load();
	void save() const;
};

namespace RecentProjects {
	using namespace std;

	extern const list<string> &get() noexcept;
	extern void push( const string &filePath );
	extern void remove( const string &filePath );
}


#endif /* CORE_PREFERENCES_HPP_ */
