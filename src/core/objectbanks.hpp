#ifndef SRC_CORE_OBJECTBANKS_HPP_
#define SRC_CORE_OBJECTBANKS_HPP_

#include <vector>
#include "src/types.hpp"

extern const char *objectBank1Names[11];
extern const char *objectBank2Names[6];
extern const char *objectBank3Names[30];

extern const std::vector<const char*> objectBank1Contents[11];
extern const std::vector<const char*> objectBank2Contents[6];
extern const std::vector<const char*> objectBank3Contents[30];

#endif /* SRC_CORE_OBJECTBANKS_HPP_ */
