#ifndef SRC_CORE_EMULATOR_HPP_
#define SRC_CORE_EMULATOR_HPP_

#include <vector>
#include "src/types.hpp"
#include "src/core/json.hpp"

namespace Emulator {

	enum class Emulator : ubyte {
		RetroArch = 0,
		Mupen64Plus = 1,
		Other = 2
	};

	enum class CpuEmulation : ubyte {
		DynamicRecompiler = 0,
		CachedInterpreter = 1,
		PureInterpreter = 2
	};

	namespace RetroArch {

		enum class EmulatorCore : ubyte {
			ParaLLEl = 0,
			Mupen64plus = 1
		};

		enum class GraphicsPlugin : ubyte {
			Auto = 0,
			Parallel = 1,
			Angrylion = 2,
			Glide64 = 3,
			Rice = 4,
			GlideN64 = 5
		};

		struct Settings {
#ifdef _WIN32
			string exePath;
#endif
			string coreDirPath;

			EmulatorCore core;
			CpuEmulation cpuCore;

			bool fullscreen;
			ubyte resolutionScale;
			bool vsync;
			bool fullspeed;

			struct ParallelCoreOptions {
				GraphicsPlugin gfxPlugin;
				bool overclockVI;
			} parallelConfig;

			static Settings parseJSON( const Json &json );
			void writeJson( JsonWriter &jw ) const;

			void createConfigFile() const;
		};

	}

	namespace Mupen64Plus {

		struct Settings {
#ifdef _WIN32
			string exePath;
#endif
			string pluginDirPath;

			CpuEmulation cpuCore;
			string gfxPlugin;
			string rspPlugin;

			bool fullscreen;
			ubyte resolutionScale;
			bool onScreenDisplay;

			static Settings parseJSON( const Json &json );
			void writeJson( JsonWriter &jw ) const;
		};

		extern std::vector<string> getGfxPlugins( const string &pluginDir );
		extern std::vector<string> getRspPlugins( const string &pluginDir );

	}

	struct EmulatorSettings {
		Emulator preferredEmulator;
		RetroArch::Settings retroSettings;
		Mupen64Plus::Settings mupenSettings;
		string otherEmulatorCmd;

		static EmulatorSettings parseJSON( const Json &json );
		void writeJson( JsonWriter &jw ) const;
	};

	extern bool launch( const string &romPath );
	extern bool emulatorInstalled();

}



#endif /* SRC_CORE_EMULATOR_HPP_ */
