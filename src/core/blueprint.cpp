#include "src/core/blueprint.hpp"

#include <fstream>
#include <string>
#include <cstdio>
#include <iostream>
#include <ios>
#include <cassert>
#include <cstring>
#include <unordered_set>
#include "src/openmode.hpp"
#include "src/polyfill/filesystem.hpp"
#include "src/polyfill/appdata.hpp"
#include "src/polyfill/random.hpp"
#include "src/polyfill/tar.hpp"
#include "src/core/exceptions.hpp"
#include "src/core/json.hpp"
#include "src/core/tweak.hpp"
#include "src/core/storage.hpp"
#include "src/core/util.hpp"
#include "src/core/locations.hpp"
#include "src/core/hardcoded.hpp"
#include "src/core/extended-bounds.hpp"
#include "src/core/level-writer/master.hpp"
#include "src/core/asm-extensions.hpp"
#include "src/core/ending.hpp"
#include "src/core/baserom.hpp"
#include "src/core/background.hpp"

using namespace std;
namespace fs = std::filesystem;

static const uint BBP_SIGNATURE = 0x4242503A;

static const uint DEFAULT_SFX_TABLE[16] = {
	0x50210081,
	0x502D0081,
	0x505F8091,
	0x503DA081,
	0x50410081,
	0x50480081,
	0x50584081,
	0x50388081,
	0x90694081,
	0x506F0081,
	0x30703081,
	0x701EFF81,
	0, 0, 0
};
static_assert( sizeof( DEFAULT_SFX_TABLE) == sizeof( uint ) * 16 );

namespace JsonSerializers {

	static void serializeVector3s( JsonWriter &jw, const Vec3s &v ) {
		jw.writeObjectStart();
			jw.writeProperty( "x", v.x );
			jw.writeProperty( "y", v.y );
			jw.writeProperty( "z", v.z );
		jw.writeObjectEnd();
	}

	static Vec3s deserializeVector3s( const Json &json ) {
		return Vec3s(
			json["x"].get<short>(),
			json["y"].get<short>(),
			json["z"].get<short>()
		);
	}

	static void serializeRomSpecs( JsonWriter &jw, const RomSpecs &romSpecs ) {
		jw.writeObjectStart();
			jw.writeProperty( "name", romSpecs.internalName );
			jw.writeProperty( "checksum", romSpecs.enableChecksum );
			jw.writeProperty( "boundary", romSpecs.levelBoundaries );
			jw.writeProperty( "start_level", romSpecs.startingLevel );
			jw.writePropertyName( "pause_warp" );
			jw.writeObjectStart();
				jw.writeProperty( "level", romSpecs.pauseWarpLevel );
				jw.writeProperty( "area", romSpecs.pauseWarpArea );
				jw.writeProperty( "warp_id", romSpecs.pauseWarpId );
			jw.writeObjectEnd();
		jw.writeObjectEnd();
	}

	static RomSpecs deserializeRomSpecs( const Json &json ) {
		const Json &pauseWarp = json["pause_warp"];
		return RomSpecs{
			json["name"].get<string>(),
			json["checksum"].get<bool>(),
			json["boundary"].getOrDefault<LevelBoundarySize>( RomSpecs::Default.levelBoundaries ),
			json["start_level"].getOrDefault<LevelId>( RomSpecs::Default.startingLevel ),
			pauseWarp["level"].getOrDefault<LevelId>( RomSpecs::Default.pauseWarpLevel ),
			pauseWarp["area"].getOrDefault<ubyte>( RomSpecs::Default.pauseWarpArea ),
			pauseWarp["warp_id"].getOrDefault<ubyte>( RomSpecs::Default.pauseWarpId )
		};
	}

	static void serializeDialogEntry( JsonWriter &jw, const DialogEntry &custom, const DialogEntry &vanilla ) {
		if( custom == vanilla ) {
			jw.writeNull();
			return;
		}

		jw.writeObjectStart();
			jw.writeProperty( "name", custom.name );
			jw.writeProperty( "halign", custom.halign );
			jw.writeProperty( "valign", custom.valign );
			jw.writeProperty( "hoffset", custom.hoffset );
			jw.writeProperty( "voffset", custom.voffset );
			jw.writeProperty( "page_size", custom.pageSize );
			jw.writeProperty( "sound_effect", custom.soundEffect );
			jw.writeProperty( "text", custom.text == vanilla.text ? nullptr : custom.text );
		jw.writeObjectEnd();
	}

	static DialogEntry deserializeDialogEntry( const Json &json, const DialogEntry &vanilla ) {
		if( json.isNull() ) {
			return vanilla;
		}

		DialogEntry d;
		d.name = json["name"].get<string>();
		d.halign = json["halign"].get<HAlign>();
		d.valign = json["valign"].get<VAlign>();
		d.hoffset = json["hoffset"].get<ushort>();
		d.voffset = json["voffset"].get<ushort>();
		d.pageSize = json["page_size"].get<ubyte>();
		d.soundEffect = json["sound_effect"].get<sbyte>();
		d.text = json["text"].isNull() ? vanilla.text : json["text"].get<string>();
		return d;
	}

	static void serializeLevelSpecs( JsonWriter &jw, LevelId levelId, const LevelSpecs &level ) {
		jw.writeObjectStart();
			jw.writeProperty( "id", levelId );
			jw.writeProperty( "name", level.name );
			jw.writePropertyName( "star_names" );
			jw.writeArrayStart();
			for( int j = 0; j < 6; j++ ) {
				jw.writeString( level.starNames[j] );
			}
			jw.writeArrayEnd();
			jw.writeProperty( "star_select", level.enableStarSelect );
			jw.writePropertyName( "spawn" );
			jw.writeObjectStart();
				jw.writeProperty( "area", level.marioSpawn.area );
				jw.writeProperty( "x", level.marioSpawn.x );
				jw.writeProperty( "y", level.marioSpawn.y );
				jw.writeProperty( "z", level.marioSpawn.z );
				jw.writeProperty( "angle", level.marioSpawn.yaw );
			jw.writeObjectEnd();
			jw.writePropertyName( "object_banks" );
			jw.writeArrayStart();
			for( int j = 0; j < 3; j++ ) {
				jw.writeNumber( (long)level.objectBanks[j] );
			}
			jw.writeArrayEnd();
			jw.writeProperty( "sound_carry_distance", level.maxSoundDistance );
			jw.writeProperty( "echo", level.defaultEchoLevel );
			jw.writePropertyName( "shading" );
			jw.writeArrayStart();
			for( const auto &j : level.shadingValues ) {
				jw.writeObjectStart();
				jw.writeProperty( "id", j.first );
				jw.writeProperty( "name", j.second.name );
				jw.writeProperty( "light", j.second.lightShade.toString() );
				jw.writeProperty( "dark", j.second.darkShade.toString() );
				jw.writeProperty( "asm_ref", j.second.asmRef );
				jw.writeObjectEnd();
			}
			jw.writeArrayEnd();
			jw.writePropertyName( "star_spawns" );
			jw.writeArrayStart();
			for( int i = 0; i < 25; i++ ) {
				const Vec3f &location = level.starSpawnLocations[i];
				if( !location.zero() ) {
					jw.writeObjectStart();
					jw.writeProperty( "id", i );
					jw.writeProperty( "x", location.x );
					jw.writeProperty( "y", location.y );
					jw.writeProperty( "z", location.z );
					jw.writeObjectEnd();
				}
			}
			jw.writeArrayEnd();
			jw.writePropertyName( "waypoints" );
			jw.writeArrayStart();
			for( int i = 0; i < 22; i++ ) {
				const vector<Vec3s> &waypoints = level.waypoints[i];
				if( waypoints.empty() || (waypoints.size() == 1 && waypoints[0].zero()) ) continue;
				jw.writeObjectStart();
					jw.writeProperty( "id", i );
					jw.writePropertyName( "waypoints" );
					jw.writeArrayStart();
					for( const Vec3s &waypoint : waypoints ) {
						serializeVector3s( jw, waypoint );
					}
					jw.writeArrayEnd();
				jw.writeObjectEnd();
			}
			jw.writeArrayEnd();
			jw.writePropertyName( "custom_objects" );
			jw.writeArrayStart();
			for( const auto &i : level.simpleObjects ) {
				const ObjectSpecs &object = i.second;
				jw.writeObjectStart();
					jw.writeProperty( "id", i.first );
					jw.writeProperty( "model_id", object.modelId );
					jw.writeProperty( "name", object.name );
					jw.writePropertyName( "culling" );
					jw.writeObjectStart();
						jw.writeProperty( "auto_compute", object.cullingRadius.autoCompute );
						jw.writeProperty( "upright", object.cullingRadius.alwaysUpright );
						jw.writeProperty( "scale", object.cullingRadius.maxScale );
						jw.writeProperty( "fov", object.cullingRadius.maxFOV );
						jw.writeProperty( "radius", object.cullingRadius.radius );
					jw.writeObjectEnd();
					jw.writePropertyName( "shadow" );
					jw.writeObjectStart();
						jw.writeProperty( "shape", object.shadow.shape );
						jw.writeProperty( "size", object.shadow.size );
						jw.writeProperty( "opacity", object.shadow.opacity );
						jw.writeProperty( "enabled", object.shadow.enabled );
					jw.writeObjectEnd();
					jw.writePropertyName( "fog" );
					jw.writeObjectStart();
						jw.writeProperty( "fade_start", object.fog.fadeStartDistance );
						jw.writeProperty( "fade_end", object.fog.fadeEndDistance );
						jw.writeProperty( "colour", object.fog.colour.toString() );
						jw.writeProperty( "enabled", object.fog.enabled );
					jw.writeObjectEnd();
					jw.writeProperty( "use_object_alpha", object.useObjectAlpha );
					jw.writeProperty( "dithered_alpha", object.ditheredAlpha );
					jw.writeProperty( "has_collision", object.hasCollision );
					jw.writeProperty( "collision_asm_ref", object.collisionAsmRef );
					jw.writeProperty( "collision_ptr_loc", object.collisionPtrAddr );
					jw.writeProperty( "max_xz", object.max_xz );
					jw.writeProperty( "max_xyz", object.max_xyz );
				jw.writeObjectEnd();
			}
			for( const auto &i : level.partialObjects ) {
				jw.writeObjectStart();
					jw.writeProperty( "id", i.first );
					jw.writeProperty( "name", i.second.name );
					jw.writeProperty( "use_object_alpha", i.second.useObjectAlpha );
					jw.writeProperty( "dithered_alpha", i.second.ditheredAlpha );
				jw.writeObjectEnd();
			}
			for( const auto &i : level.advancedObjects ) {
				const AdvancedObjectSpecs &object = i.second;
				jw.writeObjectStart();
					jw.writeProperty( "id", i.first );
					jw.writeProperty( "model_id", object.modelId );
					jw.writeProperty( "name", object.name );
					jw.writeProperty( "data_id", object.customDataId );
				jw.writeObjectEnd();
			}
			jw.writeArrayEnd();
			jw.writePropertyName( "user_data" );
			jw.writeArrayStart();
			for( const auto &i : level.userData ) {
				jw.writeObjectStart();
					jw.writeProperty( "id", i.first );
					jw.writeProperty( "name", i.second.name );
					jw.writeProperty( "size", (uint)i.second.bytes );
					jw.writeProperty( "asm_ref", i.second.asmRef );
					jw.writePropertyName( "links" );
					jw.writeArrayStart();
					for( const auto &j : i.second.links ) {
						jw.writeObjectStart();
							jw.writeProperty( "offset", j.first );
							jw.writeProperty( "type", j.second.linkType );
							if( j.second.linkType != LinkType::None ) {
								jw.writeProperty( "target", j.second.targetId );
								if( j.second.linkType == LinkType::UserData ) {
									jw.writeProperty( "target_offset", j.second.targetOffset );
								}
							}
						jw.writeObjectEnd();
					}
					jw.writeArrayEnd();
				jw.writeObjectEnd();
			}
			jw.writeArrayEnd();
		jw.writeObjectEnd();
	}

	static void deserializeLevelSpecs( const Json &json, LevelSpecs &level ) {
		level.name = json["name"].get<string>();
		level.enableStarSelect = json["star_select"].getOrDefault<bool>( false );
		const Json &spawnJson = json["spawn"];
		level.marioSpawn.area = spawnJson["area"].get<ubyte>();
		level.marioSpawn.x = spawnJson["x"].get<short>();
		level.marioSpawn.y = spawnJson["y"].get<short>();
		level.marioSpawn.z = spawnJson["z"].get<short>();
		level.marioSpawn.yaw = spawnJson["angle"].get<float>();
		for( int i = 0; i < 3; i++ ) {
			level.objectBanks[i] = json["object_banks"].array()[i].get<sbyte>();
		}
		for( int i = 0; i < 6; i++ ) {
			level.starNames[i] = json["star_names"].array()[i].getOrDefault<string>( "" );
		}
		level.maxSoundDistance = json["sound_carry_distance"].getOrDefault<ushort>( 19500 );
		level.defaultEchoLevel = json["echo"].getOrDefault<sbyte>( 0x28 );
		const JArray &shading = json["shading"].array();
		for( const Json &shade : shading ) {
			level.shadingValues[shade["id"].get<uint>()] = {
				shade["name"].get<string>(),
				ColourRGB24::parse( shade["dark"].get<string>() ),
				ColourRGB24::parse( shade["light"].get<string>() ),
				shade["asm_ref"].getOrDefault<bool>( false )
			};
		}
		level.starSpawnLocations.fill( Vec3f() );
		const JArray &starSpawns = json["star_spawns"].array();
		for( const Json &starSpawn : starSpawns ) {
			const int i = starSpawn["id"].get<int>();
			if( i >= 25 ) continue;
			level.starSpawnLocations[i] = Vec3f(
				starSpawn["x"].get<float>(),
				starSpawn["y"].get<float>(),
				starSpawn["z"].get<float>()
			);
		}
		const JArray &waypointsList = json["waypoints"].array();
		for( const Json &waypointsEntry : waypointsList ) {
			const int i = waypointsEntry["id"].get<int>();
			if( i >= 22 ) continue;
			vector<Vec3s> &waypoints = level.waypoints[i];
			const JArray &waypointsJson = waypointsEntry["waypoints"].array();
			for( const Json &waypoint : waypointsJson ) {
				waypoints.push_back( deserializeVector3s( waypoint ) );
			}
		}

		for( const Json &object : json["custom_objects"].array() ) {
			const objectId objectId = object["id"].get<uint>();
			switch( ObjectId::getType( objectId ) ) {
				case ObjectType::Simple: {
					level.simpleObjects[objectId] = ObjectSpecs{
						object["model_id"].get<ubyte>(),
						object["name"].get<string>(),
						{
							object["culling"]["auto_compute"].get<bool>(),
							object["culling"]["upright"].get<bool>(),
							object["culling"]["scale"].get<float>(),
							object["culling"]["fov"].get<short>(),
							object["culling"]["radius"].get<ushort>()
						},
						{
							object["shadow"]["shape"].get<ShadowType>(),
							object["shadow"]["size"].get<ushort>(),
							object["shadow"]["opacity"].get<ubyte>(),
							object["shadow"]["enabled"].get<bool>()
						},
						{
							object["fog"]["fade_start"].get<double>(),
							object["fog"]["fade_end"].get<double>(),
							ColourRGB24::parse( object["fog"]["colour"].get<string>() ),
							object["fog"]["enabled"].get<bool>()
						},
						object["use_object_alpha"].get<bool>(),
						object["dithered_alpha"].get<bool>(),
						object["has_collision"].get<bool>(),
						object["collision_asm_ref"].get<bool>(),
						object["collision_ptr_loc"].get<uint>(),
						object["max_xz"].get<ushort>(),
						object["max_xyz"].get<ushort>(),
					};
					break;
				}
				case ObjectType::Partial: {
					level.partialObjects[objectId] = PartialObjectSpecs{
						object["name"].get<string>(),
						object["use_object_alpha"].get<bool>(),
						object["dithered_alpha"].get<bool>()
					};
					break;
				}
				case ObjectType::Advanced: {
					level.advancedObjects[objectId] = AdvancedObjectSpecs{
						object["model_id"].get<ubyte>(),
						object["name"].get<string>(),
						object["data_id"].get<uint>()
					};
					break;
				}
				default: break;
			}
		}

		for( const Json &data : json["user_data"].array() ) {
			std::map<uint,UserDataLink> links;
			for( const Json &link : data["links"].array() ) {
				links[ link["offset"].get<uint>() ] = UserDataLink{
					link["type"].get<LinkType>(),
					link["target"].getOrDefault<uint>( 0 ),
					link["target_offset"].getOrDefault<uint>( 0 )
				};
			}

			level.userData[ data["id"].get<uint>() ] = UserData{
				data["name"].get<string>(),
				links,
				data["asm_ref"].get<bool>(),
				data["size"].get<uint>()
			};
		}
	}

	static void serializeAnimationSpecs( JsonWriter &jw, const AnimationSpecs &animation ) {
		jw.writeObjectStart();
		jw.writeProperty( "type", (int)animation.index() );
		switch( (int)animation.index() ) {
			case 1: {
				const LinearAnimationSpecs &props = std::get<LinearAnimationSpecs>( animation );
				jw.writeProperty( "speed_x", props.speedX );
				jw.writeProperty( "speed_y", props.speedY );
				break;
			}
			case 2: {
				const SineWaveAnimationSpecs &props = std::get<SineWaveAnimationSpecs>( animation );
				jw.writeProperty( "amplitude", props.amplitude );
				jw.writeProperty( "angle", props.angle );
				jw.writeProperty( "period", props.period );
				break;
			}
			default: break;
		}
		jw.writeObjectEnd();
	}

	static AnimationSpecs deserializeAnimationSpecs( const Json &json ) {
		switch( json["type"].get<int>() ) {
			case 0: return NullAnimationSpecs();
			case 1: return LinearAnimationSpecs{
				json["speed_x"].get<int>(),
				json["speed_y"].get<int>()
			};
			case 2: return SineWaveAnimationSpecs{
				json["angle"].get<ushort>(),
				json["period"].get<ushort>(),
				json["amplitude"].get<ushort>()
			};
			default: throw FileParserException( "Unknown animation type" );
		}
	}

	static void serializeWaterTextureInfo( JsonWriter &jw, const WaterTextureInfo &info ) {
		jw.writeObjectStart();
			jw.writeProperty( "hash", info.textureHash );
			jw.writeProperty( "format", info.textureFormat );
			if( info.asmRef ) {
				jw.writeProperty( "asm_ref", info.name );
			}
		jw.writeObjectEnd();
	}

	static WaterTextureInfo deserializeWaterTextureInfo( const Json &json ) {
		return WaterTextureInfo{
			json["hash"].get<string>(),
			json["asm_ref"].getOrDefault<string>( "" ),
			json["format"].get<TextureFormat>(),
			json["asm_ref"].exists()
		};
	}

	static void serializeWaterBoxSpecs( JsonWriter &jw, const WaterBoxSpecs &waterBox ) {
		jw.writeObjectStart();
			jw.writeProperty( "name", waterBox.name );
			jw.writePropertyName( "texture" );
			serializeWaterTextureInfo( jw, waterBox.texture );
			jw.writeProperty( "poison_fog", waterBox.isPoisonFog );
			jw.writeProperty( "visible", waterBox.isVisible );
			jw.writeProperty( "asm_ref_texture", waterBox.asmRefTexturePtr );
			jw.writeProperty( "asm_ref_colour", waterBox.asmRefColour );
			jw.writeProperty( "x0", waterBox.xMin );
			jw.writeProperty( "x1", waterBox.xMax );
			jw.writeProperty( "z0", waterBox.zMin );
			jw.writeProperty( "z1", waterBox.zMax );
			jw.writeProperty( "y", waterBox.y );
			jw.writeProperty( "scale", waterBox.scale );
			jw.writeProperty( "opacity", waterBox.opacity );
			jw.writeProperty( "colour", waterBox.colour.toString() );
			jw.writePropertyName( "animation" );
			serializeAnimationSpecs( jw, waterBox.animation );
		jw.writeObjectEnd();
	}

	static WaterBoxSpecs deserializeWaterBoxSpecs( const Json &json ) {
		return WaterBoxSpecs {
			json["name"].get<string>(),
			json["poison_fog"].get<bool>(),
			json["visible"].get<bool>(),
			json["asm_ref_texture"].get<bool>(),
			json["asm_ref_colour"].get<bool>(),
			json["x0"].get<short>(),
			json["x1"].get<short>(),
			json["z0"].get<short>(),
			json["z1"].get<short>(),
			json["y"].get<short>(),
			json["scale"].get<short>(),
			json["opacity"].get<ubyte>(),
			ColourRGB24::parse( json["colour"].get<string>() ),
			deserializeWaterTextureInfo( json["texture"] ),
			deserializeAnimationSpecs( json["animation"] )
		};
	}

	static void serializeAreaSpecs( JsonWriter &jw, ushort areaId, const AreaSpecs &area ) {
		jw.writeObjectStart();
			jw.writeProperty( "id", areaId );
			jw.writeProperty( "background_image", area.backgroundImage );
			jw.writeProperty( "background_colour", area.backgroundColour.toString() );
			jw.writePropertyName( "pause_camera" );
			jw.writeObjectStart();
				jw.writePropertyName( "position" );
				serializeVector3s( jw, area.pauseCamera.position );
				jw.writePropertyName( "focus" );
				serializeVector3s( jw, area.pauseCamera.focus );
				jw.writeProperty( "fixed", area.pauseCamera.fixed );
			jw.writeObjectEnd();
			jw.writeProperty( "camera_type", area.cameraType );
			jw.writeProperty( "fov", area.fieldOfView );
			jw.writeProperty( "clip_near", area.nearClippingPlane );
			jw.writeProperty( "clip_far", area.farClippingPlane );
			jw.writePropertyName( "fog" );
			jw.writeObjectStart();
				jw.writeProperty( "fade_start", area.fog.fadeStartDistance );
				jw.writeProperty( "fade_end", area.fog.fadeEndDistance );
				jw.writeProperty( "colour", area.fog.colour.toString() );
				jw.writeProperty( "enabled", area.fog.enabled );
			jw.writeObjectEnd();
			jw.writeProperty( "envfx", area.environmentEffect );
			jw.writeProperty( "terrain", area.terrainType );
			jw.writeProperty( "echo", area.echoLevel );
			jw.writeProperty( "music", area.music );
			jw.writeProperty( "num_objects", area.numObjects );
			jw.writeProperty( "num_warps", area.numWarps );
			jw.writeProperty( "num_instant_warps", area.numInstantWarps );
			jw.writeProperty( "death_floor", area.autoDeathFloor );
			jw.writeProperty( "void_height", area.voidHeight );
			jw.writePropertyName( "water_boxes" );
			jw.writeArrayStart();
			for( const WaterBoxSpecs &waterBox : area.waterBoxes ) {
				serializeWaterBoxSpecs( jw, waterBox );
			}
			jw.writeArrayEnd();
		jw.writeObjectEnd();
	}

	static void deserializeAreaSpecs( const Json &json, AreaSpecs &area ) {
		area.backgroundImage = json["background_image"].get<BackgroundImage>();
		area.backgroundColour = ColourRGBA5551::parse( json["background_colour"].get<string>() );
		area.pauseCamera.position = deserializeVector3s( json["pause_camera"]["position"] );
		area.pauseCamera.focus = deserializeVector3s( json["pause_camera"]["focus"] );
		area.pauseCamera.fixed = json["pause_camera"]["fixed"].get<bool>();
		area.cameraType = json["camera_type"].get<CameraPreset>();
		area.fieldOfView = json["fov"].get<short>();
		area.nearClippingPlane = json["clip_near"].get<ushort>();
		area.farClippingPlane = json["clip_far"].get<ushort>();
		area.fog.fadeStartDistance = json["fog"]["fade_start"].get<double>();
		area.fog.fadeEndDistance = json["fog"]["fade_end"].get<double>();
		area.fog.colour = ColourRGB24::parse( json["fog"]["colour"].get<string>() );
		area.fog.enabled = json["fog"]["enabled"].get<bool>();
		area.environmentEffect = json["envfx"].get<EnvironmentEffect>();
		area.terrainType = json["terrain"].get<TerrainType>();
		area.echoLevel = json["echo"].get<sbyte>();
		area.music = json["music"].get<ubyte>();
		area.numObjects = json["num_objects"].get<ubyte>();
		area.numWarps = json["num_warps"].get<ubyte>();
		area.numInstantWarps = json["num_instant_warps"].get<ubyte>();
		area.autoDeathFloor = json["death_floor"].getOrDefault<bool>( false );
		area.voidHeight = json["void_height"].getOrDefault<short>( AreaSpecs::Default.voidHeight );
		area.waterBoxes.clear();
		if( json["water_boxes"].isArray() ) {
			for( const Json &waterBoxJson : json["water_boxes"].array() ) {
				area.waterBoxes.push_back( deserializeWaterBoxSpecs( waterBoxJson ) );
			}
		}
	}

	static void serializeMaterialSpecs( JsonWriter &jw, const MaterialSpecs &mat ) {
		jw.writeObjectStart();
			jw.writeProperty( "visible", mat.visible );
			jw.writeProperty( "solid", mat.solid );
			jw.writeProperty( "opacity", mat.opacity );
			jw.writeProperty( "removed_alpha", mat.removedTextureAlpha );
			jw.writeProperty( "layer", mat.layer );
			jw.writeProperty( "format", mat.format );
			jw.writeProperty( "filtering", mat.filtering );
			jw.writeProperty( "hwrap", mat.hwrap );
			jw.writeProperty( "vwrap", mat.vwrap );
			jw.writeProperty( "shade", mat.shadingRef );
			jw.writePropertyName( "collision" );
			jw.writeObjectStart();
				jw.writeProperty( "type", mat.collision.type );
				jw.writeProperty( "direction", mat.collision.direction );
				jw.writeProperty( "force", mat.collision.force );
			jw.writeObjectEnd();
			jw.writePropertyName( "texture_info" );
			if( mat.textureInfo.has_value() ) {
				const TextureInfo &tex = mat.textureInfo.value();
				jw.writeObjectStart();
				jw.writeProperty( "hash", tex.hash );
				jw.writeProperty( "width", tex.width );
				jw.writeProperty( "height", tex.height );
				jw.writeProperty( "has_empty_pixels", tex.hasEmptyPixels );
				jw.writeProperty( "has_full_alpha", tex.hasFullAlpha );
				jw.writeProperty( "has_varying_alpha", tex.hasVaryingAlpha );
				jw.writeProperty( "is_greyscale", tex.isGreyscale );
				jw.writeProperty( "colour_depth", tex.sourceImageDepth );
				jw.writeProperty( "average_alpha", tex.averageAlpha );
				jw.writeProperty( "unique_colours", tex.uniqueColours );
				jw.writeObjectEnd();
			} else {
				jw.writeNull();
			}
			jw.writePropertyName( "asm_refs" );
			jw.writeObjectStart();
				jw.writeProperty( "texture_data", mat.asmRefs.texData );
				jw.writeProperty( "texture_ptr", mat.asmRefs.texPtr );
				jw.writeProperty( "shade_ptr", mat.asmRefs.shadePtr );
				jw.writeProperty( "opacity_data", mat.asmRefs.opacity );
			jw.writeObjectEnd();
			jw.writePropertyName( "animation" );
			serializeAnimationSpecs( jw, mat.animation );
		jw.writeObjectEnd();
	}

	static MaterialSpecs deserializeMaterialSpecs( const Json &json ) {
		MaterialSpecs mat;
		mat.visible = json["visible"].get<bool>();
		mat.solid = json["solid"].get<bool>();
		mat.opacity = json["opacity"].getOrDefault<ubyte>( 0xFF );
		mat.removedTextureAlpha = json["removed_alpha"].getOrDefault<bool>( false );
		mat.layer = json["layer"].get<DrawingLayer>();
		mat.format = json["format"].get<TextureFormat>();
		mat.filtering = json["filtering"].getOrDefault<TextureFiltering>( TextureFiltering::Bilinear );
		mat.hwrap = json["hwrap"].get<TextureWrapping>();
		mat.vwrap = json["vwrap"].get<TextureWrapping>();
		mat.shadingRef = json["shade"].get<uint>();
		mat.collision.type = json["collision"]["type"].get<CollisionType>();
		mat.collision.direction = json["collision"]["direction"].getOrDefault<ubyte>( 0 );
		mat.collision.force = json["collision"]["force"].getOrDefault<ForceIntensity>( ForceIntensity::Medium );
		const Json &texJson = json["texture_info"];
		if( texJson.hasValue() ) {
			mat.textureInfo = {
				texJson["hash"].get<string>(),
				texJson["width"].get<ushort>(),
				texJson["height"].get<ushort>(),
				texJson["has_empty_pixels"].get<bool>(),
				texJson["has_full_alpha"].get<bool>(),
				texJson["has_varying_alpha"].get<bool>(),
				texJson["is_greyscale"].get<bool>(),
				texJson["colour_depth"].get<ubyte>(),
				texJson["average_alpha"].get<ubyte>(),
				texJson["unique_colours"].get<ushort>()
			};
		} else {
			mat.textureInfo = std::nullopt;
		}
		const Json &asmJson = json["asm_refs"];
		mat.asmRefs.texData = asmJson["texture_data"].get<bool>();
		mat.asmRefs.texPtr = asmJson["texture_ptr"].get<bool>();
		mat.asmRefs.shadePtr = asmJson["shade_ptr"].get<bool>();
		mat.asmRefs.opacity = asmJson["opacity_data"].get<bool>();
		const Json &animJson = json["animation"];
		if( animJson.exists() ) {
			mat.animation = deserializeAnimationSpecs( animJson );
		}
		return mat;
	}

}
using namespace JsonSerializers;

static inline string areaMaskToHex( const array<ubyte,40> &mask ) {
	char hex[81];
	const uint *words = (const uint*)mask.data();
	for( int i = 0; i < 10; i++ ) {
		std::sprintf( &hex[i << 3], "%08X", ntohl( words[i] ) );
	}
	return string( hex );
}

static inline std::array<ubyte,40> parseAreaMask( const string &hex ) {
	std::array<ubyte,40> mask;
	uint *words = (uint*)mask.data();
	for( int i = 0; i < 10; i++ ) {
		words[i] = htonl( (uint)std::stoull( hex.substr( i << 3, 8 ), nullptr, 16 ) );
	}
	return mask;
}

static Blueprint *s_blueprint = nullptr;

Blueprint *Blueprint::currentEditable() {
	s_blueprint->m_dataChanged = true;
	return s_blueprint;
}

const Blueprint *Blueprint::current() {
	return s_blueprint;
}

void Blueprint::create( const CreateProjectOptions &options ) {
	array<uint,16> sfxTable;
	memcpy( sfxTable.data(), DEFAULT_SFX_TABLE, sizeof(DEFAULT_SFX_TABLE) );

	Blueprint *blueprint = new Blueprint(
		fs::path( options.projectPath ).make_preferred(),
		createTemporaryDirectory(),
		CurrentVersion::BlueprintSchema,
		RomSpecs{
			options.internalRomName,
			!options.disableChecksum,
			options.horizontalBoundaries,
			RomSpecs::Default.startingLevel,
			RomSpecs::Default.pauseWarpLevel,
			RomSpecs::Default.pauseWarpArea,
			RomSpecs::Default.pauseWarpId
		},
		map<Uuid,Word>(),
		map<Uuid,AsmModule>(),
		map<string,CustomMarioAction>(),
		vector<DialogEntry>( VanillaText::dialog(), VanillaText::dialog() + 170 ),
		std::move( sfxTable ),
		VanillaText::levelNames()[25],
		VanillaText::starNames()[90],
		map<LevelId,LevelSpecs>(),
		map<ushort,AreaSpecs>(),
		map<ubyte,CustomMusicInfo>(),
		HashSet<WaterTextureInfo>(),
		std::map<ubyte,string>(),
		fs::path( options.romPath ).make_preferred(),
		false,
		false
	);
	blueprint->syncTweakStore();

	static const Uuid DISABLE_DEMOS_TWEAK_ID = Uuid::parse( "fcaf2969-53a7-4680-a85b-29b112728a95" );
	static const Uuid DISABLE_INTRO_TWEAK_ID = Uuid::parse( "2172db30-ff5d-4d76-937c-51c4f4f3fdde" );
	static const Uuid REMOVE_BORDERS_TWEAK_ID = Uuid::parse( "96da2680-41cd-4094-86bc-c00c708aad08" );
	static const Uuid INFINITE_LIVES_TWEAK_ID = Uuid::parse( "b3890b05-940d-4670-9eb5-717da98cb6b4" );
	static const Uuid EXTEND_VERTICAL_BOUNDS_TWEAK_ID = Uuid::parse( "df1bc311-561b-438e-b759-39351251b9c2" );
	static const Uuid BOOT_TO_TWEAK_ID = Uuid::parse( "75533338-06d9-4012-aafb-e77db81f508e" );
	static const Uuid PATCH_HANDS_FREE_HOLDING_TWEAK_ID = Uuid::parse( "1e76a068-a806-42ba-b26e-8300c1ecd911" );

	const Word ENABLED = Word::fromUInt( 1 );
	if( options.disableDemos ) blueprint->setTweakValue( DISABLE_DEMOS_TWEAK_ID, ENABLED );
	if( options.disableIntro ) blueprint->setTweakValue( DISABLE_INTRO_TWEAK_ID, ENABLED );
	if( options.removeBorders ) blueprint->setTweakValue( REMOVE_BORDERS_TWEAK_ID, ENABLED );
	if( options.infiniteLives ) blueprint->setTweakValue( INFINITE_LIVES_TWEAK_ID, ENABLED );
	if( options.extendVerticalBoundaries ) blueprint->setTweakValue( EXTEND_VERTICAL_BOUNDS_TWEAK_ID, ENABLED );
	if( options.romEntry != 0 ) blueprint->setTweakValue( BOOT_TO_TWEAK_ID, Word::fromUInt( options.romEntry ) );
	if( options.patchHandsFreeHolding ) blueprint->setTweakValue( PATCH_HANDS_FREE_HOLDING_TWEAK_ID, ENABLED );

	/* Default Enabled Tweaks */
	static const Uuid FIX_TREE_PARTICLES_TWEAK_ID = Uuid::parse( "dcda688a-5a7c-4c06-8f6d-4b20f0177dba" );
	static const Uuid BOO_COIN_TWEAK_ID = Uuid::parse( "b1110ef0-2a8e-4b4e-95d4-5406934f4664" );
	static const Uuid RACE_MUSIC_FIX_TWEAK_ID = Uuid::parse( "3d51b7ea-4ef1-4baf-94d1-422189c0dfe0" );
	static const Uuid CULLING_RADIUS_FIX_TWEAK_ID = Uuid::parse( "1b190c62-a131-4eea-bbd9-d63d5fdccf02" );
	static const Uuid SEGMENT_TABLE_BOUNDS_CHECK_TWEAK_ID = Uuid::parse( "8d809d82-8dda-44cb-9646-c52e97c458c6" );
	blueprint->setTweakValue( FIX_TREE_PARTICLES_TWEAK_ID, ENABLED );
	blueprint->setTweakValue( BOO_COIN_TWEAK_ID, Word::fromUInt( 1 ) );
	blueprint->setTweakValue( RACE_MUSIC_FIX_TWEAK_ID, ENABLED );
	blueprint->setTweakValue( CULLING_RADIUS_FIX_TWEAK_ID, ENABLED );
	blueprint->setTweakValue( SEGMENT_TABLE_BOUNDS_CHECK_TWEAK_ID, ENABLED );

	try {
		blueprint->save();
		RecentProjects::push( options.projectPath );
	} catch( ... ) {
		delete blueprint;
		throw;
	}

	if( s_blueprint != nullptr ) {
		delete s_blueprint;
	}
	s_blueprint = blueprint;
}

void Blueprint::open( const fs::path &path ) {
	const fs::path tempDir( createTemporaryDirectory() );

	bool collabMode = (path.extension() == ".bpc");
	if( collabMode ) {
		fs::copy( path.parent_path() / "blueprint", tempDir, fs::copy_options::recursive );
	} else if( !tarball::extract( path.string(), tempDir.string() ) ) {
		throw BlueprintLoadError( "Failed to decompress and extract blueprint file." );
	}

	ifstream jsonStream = ifstream( ( tempDir / "blueprint.json" ).string(), mode::read );
	if( jsonStream.bad() || jsonStream.eof() ) {
		throw BlueprintLoadError( "Failed to open blueprint file." );
	}

	const Json json = Json::parse( jsonStream );
	optional<Version> maybeVersion = Version::tryParse( json["version"].get<string>() );
	if( !maybeVersion.has_value() || maybeVersion.value().major > CurrentVersion::BlueprintSchema.major ) {
		throw BlueprintLoadError( "Blueprint file was created with a newer version of Bowser's Blueprints that is not compatible with this version." );
	}

	map<Uuid,Word> tweaks;
	for( const Json &tweak : json["tweaks"].array() ) {
		const Uuid id = Uuid::parse( tweak["id"].get<string>() );
		const uint value = Util::parseHexWord( tweak["val"].get<string>() );
		tweaks[id] = Word::fromUInt( value );
	}

	vector<DialogEntry> dialogEntries;
	dialogEntries.reserve( 170 );
	if( json["dialog"].isArray() && json["dialog"].array().size() == 170 ) {
		const JArray &dialogJson = json["dialog"].array();
		for( int i = 0; i < 170; i++ ) {
			dialogEntries.push_back(
				deserializeDialogEntry( dialogJson[i], VanillaText::dialog()[i] )
			);
		}
	} else {
		dialogEntries.assign( VanillaText::dialog(), VanillaText::dialog() + 170 );
	}

	array<uint,16> dialogSfxTable;
	if( json["dialog_sfx_table"].isArray() && json["dialog_sfx_table"].array().size() == 16 ) {
		const JArray &jsonTable = json["dialog_sfx_table"].array();
		for( int i = 0; i < 16; i++ ) {
			dialogSfxTable[i] = jsonTable[i].get<uint>();
		}
	} else {
		memcpy( dialogSfxTable.data(), DEFAULT_SFX_TABLE, sizeof(DEFAULT_SFX_TABLE) );
	}

	map<LevelId,LevelSpecs> levelSpecs;
	if( json["levels"].isArray() ) {
		for( const Json &levelJson : json["levels"].array() ) {
			LevelId levelId = levelJson["id"].get<LevelId>();
			deserializeLevelSpecs( levelJson, levelSpecs[levelId] );
		}
	}

	map<ushort,AreaSpecs> areaSpecs;
	if( json["areas"].isArray() ) {
		for( const Json &areaJson : json["areas"].array() ) {
			ushort areaId = areaJson["id"].get<ushort>();
			deserializeAreaSpecs( areaJson, areaSpecs[areaId] );
		}
	}

	map<ubyte,CustomMusicInfo> customMusic;
	if( json["music"].isArray() ) {
		for( const Json &musicJson : json["music"].array() ) {
			CustomMusicInfo music;
			music.name = musicJson["name"].get<string>();
			for( const Json &bank : musicJson["banks"].array() ) {
				music.soundBanks.push_back( bank.get<ubyte>() );
			}
			customMusic[ musicJson["id"].get<ubyte>() ] = std::move( music );
		}
	}

	HashSet<WaterTextureInfo> waterTextures;
	if( json["water_textures"].isArray() ) {
		for( const Json &texture : json["water_textures"].array() ) {
			waterTextures.insert( deserializeWaterTextureInfo( texture ) );
		}
	}

	map<ubyte,string> customBackgrounds;
	if( json["backgrounds"].isArray() ) {
		for( const Json &bg : json["backgrounds"].array() ) {
			customBackgrounds[bg["slot"].get<ubyte>()] = bg["name"].get<string>();
		}
	}

	map<Uuid,AsmModule> asmModules;
	if( json["modules"].isArray() ) {
		for( const Json &module : json["modules"].array() ) {
			const AsmModuleDefinition definition = AsmModuleDefinition::loadFromFile(
				(tempDir / "modules" / module["id"].get<string>() / "module.json").string()
			);

			HashMap<string,ModulePropertyType> typeMap;
			for( const auto &i : definition.properties ) {
				typeMap[i.name] = i.type;
			}

			HashMap<string,std::variant<int,float,bool,string,ColourRGBA32>> params;
			for( const Json &param : module["parameters"].array() ) {
				const string paramName = param["param"].get<string>();
				const auto ptype = typeMap.find( paramName );
				if( ptype == typeMap.end() ) continue;
				const Json &valueJson = param["value"];
				switch( ptype->second ) {
					case ModulePropertyType::Integer: {
						const std::optional<int> value = valueJson.tryGet<int>();
						if( value.has_value() ) {
							params[paramName] = value.value();
						}
						break;
					}
					case ModulePropertyType::Float:
					case ModulePropertyType::Angle: {
						const std::optional<float> value = valueJson.tryGet<float>();
						if( value.has_value() ) {
							params[paramName] = value.value();
						}
						break;
					}
					case ModulePropertyType::String: {
						const std::optional<string> value = valueJson.tryGet<string>();
						if( value.has_value() ) {
							params[paramName] = value.value();
						}
						break;
					}
					case ModulePropertyType::Boolean: {
						const std::optional<bool> value = valueJson.tryGet<bool>();
						if( value.has_value() ) {
							params[paramName] = value.value();
						}
						break;
					}
					case ModulePropertyType::Colour: {
						try {
							params[paramName] = ColourRGBA32::parse( valueJson.get<string>() );
						} catch( ... ) {}
						break;
					}
				}
			}

			asmModules[Uuid::parse( module["id"].get<string>() )] = {
				std::move( definition ),
				parseAreaMask( module["area_mask"].get<string>() ),
				std::move( params )
			};
		}
	}

	map<string,CustomMarioAction> customActions;
	if( json["custom_actions"].isArray() ) {
		for( const Json &action : json["custom_actions"].array() ) {
			const string name = action["name"].get<string>();
			customActions[name] = {
				name,
				action["category"].get<MarioActionCategory>(),
				action["flags"].get<MarioActionFlag>(),
				true
			};
		}
	}

	RecentProjects::push( path.string() );
	const Version &version = maybeVersion.value();
	s_blueprint = new Blueprint(
		collabMode ? path.parent_path() : path,
		tempDir.string(),
		version,
		deserializeRomSpecs( json["rom"] ),
		std::move( tweaks ),
		std::move( asmModules ),
		std::move( customActions ),
		std::move( dialogEntries ),
		std::move( dialogSfxTable ),
		json["generic_level_name"].getOrDefault<string>( VanillaText::levelNames()[25] ),
		json["generic_star_name"].getOrDefault<string>( VanillaText::starNames()[90] ),
		std::move( levelSpecs ),
		std::move( areaSpecs ),
		std::move( customMusic ),
		std::move( waterTextures ),
		std::move( customBackgrounds ),
		fs::path( json["target_rom"].getOrDefault<string>( "" ) ).make_preferred(),
		json["end_screen_filtering"].getOrDefault<bool>( false ),
		collabMode
	);
	s_blueprint->syncTweakStore();
}

void Blueprint::save() {
	removeUnusedTextures();
	const fs::path tempDir( m_tempDir );

	ofstream jsonStream = ofstream( (tempDir / "blueprint.json").string(), mode::write );
	if( jsonStream.bad() ) {
		throw FileParserException( "Error opening blueprint data file." );
	}

	JsonWriter jw = JsonWriter( &jsonStream, true );
	jw.writeObjectStart();

	jw.writeProperty( "version", m_version.toString() );
	jw.writePropertyName( "target_rom" );
	if( !m_romPath.empty() ) {
		jw.writeString( m_romPath.generic_string() );
	} else {
		jw.writeNull();
	}

	jw.writePropertyName( "rom" );
	serializeRomSpecs( jw, m_romSpecs );

	jw.writePropertyName( "tweaks" );
	jw.writeArrayStart();
	for( const auto &tweak : m_tweaks ) {
		jw.writeObjectStart();
		jw.writeProperty( "id", tweak.first.toString() );
		jw.writeProperty( "val", Util::toHexWord( tweak.second.asUInt() ) );
		jw.writeObjectEnd();
	}
	jw.writeArrayEnd();

	jw.writePropertyName( "dialog" );
	jw.writeArrayStart();
	for( int i = 0; i < 170; i++ ) {
		serializeDialogEntry( jw, m_dialogData[i], VanillaText::dialog()[i] );
	}
	jw.writeArrayEnd();

	jw.writePropertyName( "dialog_sfx_table" );
	jw.writeArrayStart();
	for( int i = 0; i < 16; i++ ) {
		jw.writeNumber( (ulong)m_dialogSfxTable[i] );
	}
	jw.writeArrayEnd();

	jw.writeProperty( "generic_level_name", m_genericLevelName );
	jw.writeProperty( "generic_star_name", m_genericStarName );

	jw.writePropertyName( "levels" );
	jw.writeArrayStart();
	for( const auto &i : m_levels ) {
		serializeLevelSpecs( jw, i.first, i.second );
	}
	jw.writeArrayEnd();

	jw.writePropertyName( "areas" );
	jw.writeArrayStart();
	for( const auto &i : m_areas ) {
		serializeAreaSpecs( jw, i.first, i.second );
	}
	jw.writeArrayEnd();

	jw.writePropertyName( "music" );
	jw.writeArrayStart();
	for( const auto &i : m_customMusic ) {
		jw.writeObjectStart();
		jw.writeProperty( "id", i.first );
		jw.writeProperty( "name", i.second.name );
		jw.writePropertyName( "banks" );
		jw.writeArrayStart();
		for( ubyte bank : i.second.soundBanks ) {
			jw.writeNumber( (ulong)bank );
		}
		jw.writeArrayEnd();
		jw.writeObjectEnd();
	}
	jw.writeArrayEnd();

	jw.writePropertyName( "water_textures" );
	jw.writeArrayStart();
	for( const WaterTextureInfo &i : m_waterTextures ) {
		serializeWaterTextureInfo( jw, i );
	}
	jw.writeArrayEnd();

	jw.writePropertyName( "backgrounds" );
	jw.writeArrayStart();
	for( const auto &i : m_customBackgrounds ) {
		jw.writeObjectStart();
		jw.writeProperty( "slot", i.first );
		jw.writeProperty( "name", i.second );
		jw.writeObjectEnd();
	}
	jw.writeArrayEnd();

	jw.writeProperty( "end_screen_filtering", m_endScreenFiltering );

	jw.writePropertyName( "modules" );
	jw.writeArrayStart();
	for( const auto &i : m_modules ) {
		jw.writeObjectStart();
		jw.writeProperty( "id", i.first.toString() );
		jw.writeProperty( "area_mask", areaMaskToHex( i.second.areaMask ) );
		jw.writePropertyName( "parameters" );
		jw.writeArrayStart();
		for( const auto &j : i.second.parameters ) {
			jw.writeObjectStart();
			jw.writeProperty( "param", j.first );
			if( std::holds_alternative<int>( j.second ) ) {
				jw.writeProperty( "value", std::get<int>( j.second ) );
			} else if( std::holds_alternative<float>( j.second ) ) {
				jw.writeProperty( "value", std::get<float>( j.second ) );
			} else if( std::holds_alternative<bool>( j.second ) ) {
				jw.writeProperty( "value", std::get<bool>( j.second ) );
			} else if( std::holds_alternative<string>( j.second ) ) {
				jw.writeProperty( "value", std::get<string>( j.second ) );
			} else if( std::holds_alternative<ColourRGBA32>( j.second ) ) {
				jw.writeProperty( "value", std::get<ColourRGBA32>( j.second ).toString() );
			} else assert( false );
			jw.writeObjectEnd();
		}
		jw.writeArrayEnd();
		jw.writeObjectEnd();
	}
	jw.writeArrayEnd();

	jw.writePropertyName( "custom_actions" );
	jw.writeArrayStart();
	for( const auto &i : m_customActions ) {
		const CustomMarioAction &action = i.second;
		jw.writeObjectStart();
		jw.writeProperty( "name", action.variableName );
		jw.writeProperty( "category", action.category );
		jw.writeProperty( "flags", action.flags );
		jw.writeObjectEnd();
	}
	jw.writeArrayEnd();

	jw.writeObjectEnd();
	jsonStream.put( '\n' );
	jsonStream.flush();
	jsonStream.close();

	syncBlueprintTweaks();

	if( m_collabMode ) {
		const fs::path blueprintDir = m_filePath / "blueprint";
		fs::forceDeleteRecursive( blueprintDir );
		fs::copy( tempDir, blueprintDir, fs::copy_options::recursive );
		clearReadOnlyFlagOnWindows( blueprintDir );
	} else if( !tarball::create( m_filePath.string(), tempDir.string() ) ) {
		throw std::runtime_error( "Failed to save Blueprint file. Did you somehow manage to find a version of Linux that doesn't come with tar installed?" );
	}

	m_dataChanged = false;
}

inline static void assertGood( ios &rom, const string &message = "" ) {
	static const string defaultMessage = "An unknown I/O error occured.";
	if( !rom.good() ) {
		throw std::ios_base::failure( message.empty() ? defaultMessage : message );
	}
}

void Blueprint::writeAsmExtensions( std::ostream &rom ) const {
	AsmExtensions::write( rom );
}

void Blueprint::applyRomSettingsInternal( iostream &rom ) const {
	assertGood( rom );

	size_t nameLength = m_romSpecs.internalName.size();
	rom.seekp( 0x20 );
	rom.write( m_romSpecs.internalName.c_str(), nameLength < 20 ? nameLength : 20 );
	while( nameLength++ < 20 ) {
		rom.put( ' ' );
	}

	rom.seekp( 0x666B );
	rom.write( (char*)&m_romSpecs.pauseWarpLevel, 1 );
	rom.seekp( 0x666F );
	rom.write( (char*)&m_romSpecs.pauseWarpArea, 1 );
	rom.seekp( 0x6673 );
	rom.write( (char*)&m_romSpecs.pauseWarpId, 1 );

	const ubyte levelPatch1[4] = { 0x34, 0x05, 0x00, (ubyte)m_romSpecs.startingLevel }; // ORI A1, R0, startingLevel
	const ubyte levelPatch2[4] = { 0xAF, 0xA5, 0x00, 0x2C }; // SW A1, 0x2C (SP)
	rom.seekp( 0x6D68 );
	rom.write( (char*)levelPatch1, 4 );
	rom.seekp( 0x6D78 );
	rom.write( (char*)levelPatch2, 4 );

	CourseId startingCourse = levelIdToCourseId( m_romSpecs.startingLevel );
	if( startingCourse == CourseId::INVALID ) {
		startingCourse = CourseId::Overworld;
	}
	rom.seekp( 0x6DA9 );
	rom.put( (char)0x08 );
	const ubyte coursePatch1[4] = { 0x34, 0x09, 0x00, (ubyte)startingCourse }; // ORI T1, R0, startingCourse
	const ubyte coursePatch2[4] = { 0xA5, 0x09, 0xBA, 0xC6 }; // SH T1, 0xBAC6 (T0)
	rom.seekp( 0x6DAC );
	rom.write( (const char*)&coursePatch1, 4 );
	rom.seekp( 0x6DCC );
	rom.write( (const char*)&coursePatch2, 4 );

	const uint w1 = m_romSpecs.enableChecksum ? htonl( 0x14E80006 ) : 0;
	const uint w2 = m_romSpecs.enableChecksum ? htonl( 0x16080003 ) : 0;
	rom.seekp( 0x66C );
	rom.write( (char*)&w1, 4 );
	rom.ignore( 8 );
	rom.write( (char*)&w2, 4 );

	setLevelBoundaries( rom, m_romSpecs.levelBoundaries );

	assertGood( rom, "Failed to write ROM settings." );
}

void Blueprint::applyTweaksInternal( iostream &rom, RomMetadata &metadata, vector<string> &warnings ) const {
	assertGood( rom );

	vector<Uuid> unknownTweaks;
	const TweakStore &tweakStore = TweakStore::instance();
	for( const Uuid &tweakId : metadata.appliedTweaks ) {
		if( m_tweaks.find( tweakId ) == m_tweaks.end() ) {
			const ISingularTweakDefinition *tweak = tweakStore.tryGetTweak( tweakId );
			if( tweak == nullptr ) {
				unknownTweaks.push_back( tweakId );
				warnings.push_back( "The ROM has a tweak with ID "s + tweakId.toString() + " applied, but I don't know what that is. Unable to revert it."s );
			} else {
				tweak->revert( rom );
			}
		}
	}

	for( const auto &tweakValue : m_tweaks ) {
		const ISingularTweakDefinition *tweak = tweakStore.tryGetTweak( tweakValue.first );
		if( tweak == nullptr ) {
			warnings.push_back( "Unknown tweak with ID "s + tweakValue.first.toString() + ". Unable to apply this tweak."s );
		} else {
			tweak->apply( rom, tweakValue.second );
		}
	}

	assertGood( rom, "Failed to apply tweaks" );

	metadata.metadataVersion = CurrentVersion::MetadataFormat;
	metadata.appliedTweaks = unknownTweaks;
	metadata.appliedTweaks.reserve( unknownTweaks.size() + m_tweaks.size() );
	for( const auto &tweak : m_tweaks ) {
		metadata.appliedTweaks.push_back( tweak.first );
	}
}

void Blueprint::applyTextInternal( iostream &rom ) const {
	string levelNames[26];
	string starNames[97];

	const Uuid LOWER_CASE_STAR_HINTS_TWEAK_ID = Uuid::parse( "b3d03e80-95ff-409f-a56d-440d9bbcdb5a" );
	const bool supportsLowerCase = m_tweaks.count( LOWER_CASE_STAR_HINTS_TWEAK_ID );

	for( ubyte i = 0; i < 26; i++ ) {
		const LevelId levelId = courseIdToLevelId( (CourseId)(i + 1) );
		const auto levelIt = m_levels.find( levelId );
		if( levelIt != m_levels.end() ) {
			const LevelSpecs &L = levelIt->second;
			levelNames[i] = L.name;
			if( i < 15 ) {
				for( ubyte j = 0; j < 6; j++ ) {
					string starHint = L.starNames[j];
					if( !supportsLowerCase && L.enableStarSelect ) {
						for( char &c : starHint ) {
							c = std::toupper( (ubyte)c );
						}
					}
					starNames[(i*6)+j] = starHint;
				}
			}
		} else {
			levelNames[i] = VanillaText::levelNames()[i];
			if( i < 15 ) {
				for( ubyte j = 0; j < 6; j++ ) {
					starNames[(i*6)+j] = VanillaText::starNames()[(i*6)+j];
				}
			}
		}
	}

	levelNames[25] = m_genericLevelName;
	starNames[90] = m_genericStarName;

	Text::applyChangesToRom(
		rom,
		m_dialogData.data(),
		levelNames,
		starNames,
		m_dialogSfxTable.data()
	);
}

void Blueprint::applyBackgroundChangesInternal( ostream &rom ) const {
	for( const auto &i : m_customBackgrounds ) {
		const ubyte slot = i.first;
		rom.seekp( 0x0399C000 + ( (uint)slot * 0x20140 ) );
		Background::writeToRom( rom, getCustomBackgroundPath( slot ).string() );
	}
}

static void revertLevel( ostream &rom, LevelId levelId, RomMetadata &romInfo ) {
	if( romInfo.customLevelTable[(uint)levelId] != 0 ) {
		HardcodedLevelCode::restore( rom, levelId );
		rom.seekp( RomLocations::getLevelLocation( levelId ).scriptLoaderLocation );
		BaseRom::revert( rom, 16 );
		romInfo.customLevelTable[(uint)levelId] = 0;
	}
}

void Blueprint::buildLevelInternal(
	iostream &rom,
	LevelId levelId,
	const array<AreaActors,8> &actors,
	map<string,uint> &asmRefs,
	RomMetadata &romInfo,
	std::vector<string> &warnings
) const {
	if( levelHasGeometry( levelId ) ) {
		LevelWriter::applyLevelAndAreaFlags( rom );
		LevelWriter::writeToRom( levelId, rom, actors, asmRefs, warnings );
		romInfo.customLevelTable[(uint)levelId] = 0x1;
	} else {
		warnings.push_back( "Level '"s + m_levels.at( levelId ).name + "' has no geometry. Skipping level." );
		revertLevel( rom, levelId, romInfo );
	}
}

void Blueprint::buildEndScreenInternal( ostream &rom ) const {
	if( EndScreen::hasCustomImage() ) {
		EndScreen::import( rom );
	} else {
		EndScreen::revert( rom );
	}
}

void Blueprint::revertDeletedLevels( iostream &rom, RomMetadata &romInfo ) const {
	uint signature;
	rom.seekg( 0x3FFFFF8 );
	rom.read( (char*)&signature, 4 );
	if( ntohl( signature ) != BBP_SIGNATURE ) {
		romInfo.customLevelTable.fill( 0 );
		return;
	}

	for( LevelId levelId : Enum::values<LevelId>() ) {
		if( !levelHasGeometry( levelId ) ) {
			revertLevel( rom, levelId, romInfo );
		}
	}
}

void RomMetadata::writeToRom( ostream &rom ) const {
	const uint signature = htonl( BBP_SIGNATURE );
	uint metadataSize = 16 + (appliedTweaks.size() << 4) + (moduleRegions.size() << 3);
	const uint metadataStart = 0x3FFFFF8 - metadataSize;
	rom.seekp( 0x3FFFFF8 );
	rom.write( (char*)&signature, 4 );
	uint startBE = htonl( metadataStart );
	rom.write( (char*)&startBE, 4 );
	rom.seekp( metadataStart );
	metadataVersion.writeToRom( rom );
	romVersion.writeToRom( rom );

	ushort numTweaksBE = htons( appliedTweaks.size() );
	rom.write( (char*)&numTweaksBE, 2 );
	for( const Uuid &tweakId : appliedTweaks ) {
		tweakId.writeToRom( rom );
	}

	const ushort numModuleRegionsBE = htons( moduleRegions.size() );
	rom.write( (const char*)&numModuleRegionsBE, 2 );
	for( const std::pair<uint,uint> &region : moduleRegions ) {
		const uint regionBE[2] = { htonl( region.first ), htonl( region.second ) };
		rom.write( (const char*)regionBE, 8 );
	}

	assertGood( rom, "Failed to write metadata to ROM" );
	assert( (uint)rom.tellp() == 0x3FFFFF8u );

	rom.seekp( RomLocations::importedLevelsTable );
	rom.write( (const char*)customLevelTable.data(), 40 );
};

RomMetadata RomMetadata::readFromRom( istream &rom ) {
	RomMetadata metadata;
	uint metadataStart;
	uint signature;
	rom.seekg( 0x3FFFFF8 );
	rom.read( (char*)&signature, 4 );
	if( ntohl( signature ) != BBP_SIGNATURE ) {
		metadata.metadataVersion = CurrentVersion::MetadataFormat;
		metadata.romVersion = Version{ 0, 0, 0 };
		metadata.customLevelTable.fill( 0 );
		return metadata;
	}
	rom.read( (char*)&metadataStart, 4 );
	metadataStart = ntohl( metadataStart );
	rom.seekg( metadataStart );
	metadata.metadataVersion = Version::readFromRom( rom );
	metadata.romVersion = Version::readFromRom( rom );

	metadata.appliedTweaks.clear();
	ushort numAppliedTweaks;
	rom.read( (char*)&numAppliedTweaks, 2 );
	numAppliedTweaks = ntohs( numAppliedTweaks );
	metadata.appliedTweaks.reserve( numAppliedTweaks );
	for( uint i = 0; i < numAppliedTweaks; i++ ) {
		metadata.appliedTweaks.push_back( Uuid::readFromRom( rom ) );
	}

	metadata.moduleRegions.clear();
	if( metadata.metadataVersion >= Version{ 0, 1, 0 } ) {
		ushort numModuleRegions;
		rom.read( (char*)&numModuleRegions, 2 );
		numModuleRegions = ntohs( numModuleRegions );
		metadata.moduleRegions.reserve( numModuleRegions );
		for( uint i = 0; i < numModuleRegions; i++ ) {
			uint startBE, endBE;
			rom.read( (char*)&startBE, 4 );
			rom.read( (char*)&endBE, 4 );
			metadata.moduleRegions.push_back({ ntohl( startBE ), ntohl( endBE ) });
		}
	}

	assertGood( rom, "Failed to read metadata from ROM" );
	assert( (uint)rom.tellg() == 0x3FFFFF8u );

	if( metadata.romVersion < Version{ 0, 3, 0 } ) {
		rom.seekg( 0x399D3D8 );
	} else {
		rom.seekg( RomLocations::importedLevelsTable );
	}
	rom.read( (char*)metadata.customLevelTable.data(), 40 );

	metadata.metadataVersion = CurrentVersion::MetadataFormat;
	return metadata;
};

Blueprint::~Blueprint() {
	fs::tryDeleteRecursive( fs::path( m_tempDir ) );
}

void Blueprint::free() {
	if( s_blueprint != nullptr ) {
		delete s_blueprint;
		s_blueprint = nullptr;
	}
}

fs::path Blueprint::getLevelPath( LevelId levelId ) const {
	return fs::path( m_tempDir ) / "level" / to_string( (int)levelId );
}

fs::path Blueprint::getAreaPath( LevelId levelId, ubyte areaIndex ) const {
	return getLevelPath( levelId ) / "area" / to_string( (int)areaIndex );
}

fs::path Blueprint::getAreaActorsPath( LevelId levelId, ubyte areaIndex ) const {
	return fs::path( m_tempDir ) / ( "actors_" + to_string( (int)levelId ) + "_" + to_string( (int)areaIndex ) + ".json" );
}

fs::path Blueprint::getObjectPath( LevelId levelId, objectId objectId ) const {
	return getLevelPath( levelId ) / "object" / Util::toHexWord( objectId );
}

fs::path Blueprint::getUserDataPath( LevelId levelId, uint userDataId ) const {
	return getLevelPath( levelId ) / "user_data" / ( Util::toHexWord( userDataId ) + ".bin" );
}

fs::path Blueprint::getMusicPath( ubyte sequenceId ) const {
	return fs::path( m_tempDir ) / "music" / ( to_string( (uint)sequenceId ) + ".m64" );
}

fs::path Blueprint::getEndScreenPath() const {
	return fs::path( m_tempDir ) / "end_screen.png";
}

fs::path Blueprint::getTweakPackPath( const Uuid &packId ) const {
	return fs::path( m_tempDir ) / "tweak_packs" / (packId.toString() + ".json");
}

fs::path Blueprint::getWaterTexturePath() const {
	return fs::path( m_tempDir ) / "water";
}

fs::path Blueprint::getCustomBackgroundPath( ubyte slot ) const {
	return fs::path( m_tempDir ) / "backgrounds" / (to_string( (uint)slot ) + ".png");
}

fs::path Blueprint::getModulePath( const Uuid &moduleId ) const {
	return fs::path( m_tempDir ) / "modules" / moduleId.toString();
}

HashMap<string,MaterialSpecs> Blueprint::getAreaGeometry( LevelId levelId, ubyte areaIndex ) const {
	const fs::path areaDir = getAreaPath( levelId, areaIndex );
	HashMap<string,MaterialSpecs> materials;
	if( !fs::is_directory( areaDir ) ) {
		return materials;
	}

	for( const fs::directory_entry &f : fs::directory_iterator( areaDir ) ) {
		if( f.status().type() != fs::file_type::regular ) continue;
		if( f.path().extension().string() != ".json" ) continue;
		if( !fs::exists( f.path().parent_path() / ( f.path().stem().string() + ".model" ) ) ) continue;

		ifstream specFile( f.path().string(), mode::read );
		MaterialSpecs specs = deserializeMaterialSpecs( Json::parse( specFile ) );
		materials.insert_or_assign( f.path().stem().string(), std::move( specs ) );
	}

	return materials;
}

HashMap<string,MaterialSpecs> Blueprint::getObjectGeometry( LevelId levelId, objectId objectId ) const {
	const fs::path objectDir = getObjectPath( levelId, objectId );
	HashMap<string,MaterialSpecs> materials;
	if( !fs::is_directory( objectDir ) ) {
		return materials;
	}

	for( const fs::directory_entry &f : fs::directory_iterator( objectDir ) ) {
		if( f.status().type() != fs::file_type::regular ) continue;
		if( f.path().extension().string() != ".json" ) continue;
		if( !fs::exists( f.path().parent_path() / ( f.path().stem().string() + ".model" ) ) ) continue;

		ifstream specFile( f.path().string(), mode::read );
		MaterialSpecs specs = deserializeMaterialSpecs( Json::parse( specFile ) );
		materials.insert_or_assign( f.path().stem().string(), std::move( specs ) );
	}

	return materials;
}

void Blueprint::saveGeometry( const fs::path &dataDir, const HashMap<string,MaterialSpecs> &materials ) const {
	fs::create_directories( dataDir );

	for( const fs::directory_entry &f : fs::directory_iterator( dataDir ) ) {
		if( f.status().type() != fs::file_type::regular ) continue;

		string extension = f.path().extension().string();
		if( materials.find( f.path().stem().string() ) != materials.end() ) continue;
		if( extension == ".verts" || extension == ".model" || extension == ".json" ) {
			fs::forceDelete( f.path() );
		}
	}

	for( const auto &i : materials ) {
		ofstream specFile( dataDir / ( i.first + ".json" ), mode::write );
		JsonWriter jsonWriter( &specFile, true );
		serializeMaterialSpecs( jsonWriter, i.second );
	}

}

bool Blueprint::areaHasGeometry( LevelId levelId, ubyte areaIndex ) const {
	const fs::path areaDir = getAreaPath( levelId, areaIndex );
	if( !fs::exists( areaDir ) ) return false;

	for( const fs::directory_entry &f : fs::directory_iterator( areaDir ) ) {
		if( f.status().type() != fs::file_type::regular ) continue;
		if( f.path().extension().string() != ".json" ) continue;

		if( fs::exists( f.path().parent_path() / (f.path().stem().string() + ".model" ) ) ) {
			return true;
		}
	}

	return false;
}

bool Blueprint::objectHasGeometry( LevelId levelId, objectId objectId ) const {
	const fs::path objectDir = getObjectPath( levelId, objectId );
	if( !fs::exists( objectDir ) ) return false;

	for( const fs::directory_entry &f : fs::directory_iterator( objectDir ) ) {
		if( f.status().type() != fs::file_type::regular ) continue;
		if( f.path().extension().string() != ".json" ) continue;

		if( fs::exists( f.path().parent_path() / (f.path().stem().string() + ".model" ) ) ) {
			return true;
		}
	}

	return false;
}

static const set<ubyte> &vanillaUnusedModelIds() {
	static const set<ubyte> s_unusedModelIds({
		// Model IDs that are never assigned a geo layout in the base game
		0x1A,
		0x1E,
		0x21,
		0x28,
		0x2A, 0x2B,
		0x31, 0x32, 0x33, 0x34,
		0x51, 0x52,
		0x5B, 0x5C, 0x5D, 0x5E, 0x5F, 0x60, 0x61, 0x62, 0x63,
		0x6C, 0x6D, 0x6E, 0x6F, 0x70, 0x71, 0x72, 0x73,
		0x7D, 0x7E,
		0x8D,
		0x92, 0x93,
		0x97, 0x98, 0x99, 0x9A, 0x9B,
		0x9D,
		0xA9,
		0xAB, 0xAC,
		0xB8,
		0xBD,
		0xC4, 0xC5, 0xC6,
		0xD3,
		0xE2, 0xE3, 0xE4, 0xE5, 0xE6, 0xE7, 0xE8, 0xE9, 0xEA, 0xEB, 0xEC, 0xED, 0xEE, 0xEF,
		0xF0, 0xF1, 0xF2, 0xF3, 0xF4, 0xF5, 0xF6, 0xF7, 0xF8, 0xF9, 0xFA, 0xFB, 0xFC, 0xFD, 0xFE, 0xFF
	});
	return s_unusedModelIds;
}

set<ubyte> Blueprint::getFreeModelIds( LevelId levelId ) const {
	const LevelSpecs &levelSpecs = m_levels.at( levelId );

	set<ubyte> freeIds = vanillaUnusedModelIds();
	for( const auto &i : levelSpecs.simpleObjects ) {
		freeIds.erase( i.second.modelId );
	}
	for( const auto &i : levelSpecs.advancedObjects ) {
		freeIds.erase( i.second.modelId );
	}
	return freeIds;
}

bool Blueprint::hasFreeModelIds( LevelId levelId ) const {
	const LevelSpecs &levelSpecs = m_levels.at( levelId );
	return (levelSpecs.simpleObjects.size() + levelSpecs.advancedObjects.size()) < vanillaUnusedModelIds().size();
}

std::vector<TweakPack> Blueprint::getEmbeddedTweakPacks() const {
	const fs::path tweakDir = fs::path( m_tempDir ) / "tweak_packs";

	std::vector<TweakPack> packs;
	if( !fs::exists( tweakDir ) ) {
		return packs;
	}

	for( const fs::directory_entry &f : fs::directory_iterator( tweakDir ) ) {
		if( f.status().type() != fs::file_type::regular ) continue;
		if( f.path().extension().string() != ".json" ) continue;

		try {
			const Uuid id = Uuid::parse( f.path().stem().string() );
			if( TweakStore::instance().tweakPacks().count( id ) > 0 ) {
				packs.push_back( TweakStore::instance().tweakPacks().at( id ) );
			} else {
				// Tweak pack failed to load when the blueprint was loaded
				packs.push_back({ id.toString(), id, Version{ 0, 0, 0 } });
			}
		} catch( ... ) {}
	}

	return packs;
}

void Blueprint::forceSaveTweakPack( const Uuid &packId ) {
	const fs::path sourcePath = fs::path( AppData::dataDir() ) / "tweak_packs" / (packId.toString() + ".json");
	if( !fs::exists( sourcePath ) ) {
		return;
	}

	const fs::path savePath = getTweakPackPath( packId );
	fs::create_directories( savePath.parent_path() );
	fs::tryCopyFileOverwrite( sourcePath, savePath );
}

void Blueprint::syncTweakStore() const {
	TweakStore &store = TweakStore::instance();
	while( !store.tweakPacks().empty() ) {
		store.removeTweakPack( store.tweakPacks().begin()->first );
	}

	const fs::path tweakDir = fs::path( m_tempDir ) / "tweak_packs";
	if( fs::exists( tweakDir ) ) {
		for( const fs::directory_entry &f : fs::directory_iterator( tweakDir ) ) {
			if( f.status().type() != fs::file_type::regular ) continue;
			if( f.path().extension().string() != ".json" ) continue;

			try {
				Uuid::parse( f.path().stem().string() );
				std::ifstream tweakFile( f.path().string(), mode::read );
				store.loadFileAndGetConflicts( tweakFile, false );
			} catch( ... ) {}
		}
	}

	store.loadInstalledTweakPacks();
}

void Blueprint::syncBlueprintTweaks() const {
	HashSet<Uuid> usedTweakPackIds;
	for( const auto &i : m_tweaks ) {
		const ISingularTweakDefinition *tweak = TweakStore::instance().tryGetTweak( i.first );
		if( tweak != nullptr && tweak->getSource() != nullptr ) {
			usedTweakPackIds.insert( tweak->getSource()->id );
		}
	}

	const fs::path tweakDir = fs::path( m_tempDir ) / "tweak_packs";
	if( usedTweakPackIds.empty() ) {
		fs::forceDeleteRecursive( tweakDir );
		return;
	}

	vector<fs::path> packsToDelete;
	fs::create_directories( tweakDir );
	for( const fs::directory_entry &f : fs::directory_iterator( tweakDir ) ) {
		if( f.status().type() != fs::file_type::regular ) continue;
		if( f.path().extension().string() != ".json" ) continue;

		try {
			const Uuid id = Uuid::parse( f.path().stem().string() );
			if( usedTweakPackIds.count( id ) > 0 ) {
				usedTweakPackIds.erase( id );
			} else {
				packsToDelete.push_back( f.path() );
			}
		} catch( ... ) {}
	}

	for( const fs::path &pack : packsToDelete ) {
		fs::forceDelete( pack );
	}

	for( const Uuid &packId : usedTweakPackIds ) {
		const fs::path sourcePath = fs::path( AppData::dataDir() ) / "tweak_packs" / (packId.toString() + ".json");
		if( fs::exists( sourcePath ) ) {
			fs::copy_file( sourcePath, getTweakPackPath( packId ) );
		}
	}
}

void Blueprint::removeUnusedTextures() {
	for( LevelId levelId : m_texturesChanged ) {
		if( m_levels.count( levelId ) <= 0 ) continue;

		HashSet<string> usedTextures;
		for( ubyte i = 0; i < 8; i++ ) {
			for( const auto &j : getAreaGeometry( levelId, i ) ) {
				if( j.second.textureInfo.has_value() ) {
					usedTextures.insert( j.second.textureInfo.value().hash );
				}
			}
		}

		const LevelSpecs &level = m_levels.at( levelId );
		std::vector<objectId> objectIds;
		for( const auto &i : level.simpleObjects ) {
			objectIds.push_back( i.first );
		}
		for( const auto &i : level.partialObjects ) {
			objectIds.push_back( i.first );
		}

		for( objectId objectId : objectIds ) {
			for( const auto &i : getObjectGeometry( levelId, objectId ) ) {
				if( i.second.textureInfo.has_value() ) {
					usedTextures.insert( i.second.textureInfo.value().hash );
				}
			}
		}

		std::vector<fs::path> texturesToDelete;
		for( const fs::directory_entry &f : fs::directory_iterator( getLevelPath( levelId ) ) ) {
			if( f.status().type() != fs::file_type::regular ) continue;
			if( f.path().extension().string() != ".png" ) continue;

			if( usedTextures.count( f.path().stem().string() ) <= 0 ) {
				texturesToDelete.push_back( f.path() );
			}
		}

		for( const fs::path &texture : texturesToDelete ) {
			fs::forceDelete( texture );
		}
	}
	m_texturesChanged.clear();
}

bool Blueprint::levelHasGeometry( LevelId levelId ) const {
	if( m_levels.count( levelId ) == 0 ) {
		return false;
	}

	for( auto i = m_areas.lower_bound( AREA_ID( levelId, 0 ) ); i != m_areas.end() && i->first <= AREA_ID( levelId, 7 ); i++ ) {
		if( areaHasGeometry( levelId, i->first & 7 ) ) {
			return true;
		}
	}

	return false;
}

fs::path Blueprint::getAsmFilePath() const {
	if( m_collabMode ) {
		return m_filePath / (m_filePath.stem().string() + ".asm");
	} else {
		return fs::path( m_romPath ).replace_extension( ".asm" );
	}
}

fs::path Blueprint::getAsmDirectory() const {
	if( m_collabMode ) {
		return m_filePath / "asm";
	} else {
		return m_romPath.parent_path() / ( m_romPath.stem().string() + "-asm" );
	}
}

void Blueprint::switchToCollabMode( const fs::path &folderPath ) {
	fs::create_directories( folderPath );
	const fs::path oldPath = m_filePath;
	m_filePath = folderPath;
	const string bpcPath = (folderPath / (folderPath.stem().string() + ".bpc")).string();
	std::fstream( bpcPath, mode::write ).close();

	const fs::path oldAsmFolder = getAsmDirectory();
	const fs::path oldAsmFile = getAsmFilePath();

	m_collabMode = true;
	m_romPath = (m_romPath.empty() ? folderPath : m_romPath).stem();
	m_romPath += ".z64";
	if( fs::exists( oldAsmFolder ) ) {
		fs::rename( oldAsmFolder, getAsmDirectory() );
	}
	if( fs::exists( oldAsmFile ) ) {
		fs::rename( oldAsmFile, getAsmFilePath() );
	}

	save();
	RecentProjects::remove( oldPath.string() );
	RecentProjects::push( bpcPath );
	fs::forceDelete( oldPath.string() );
}

void Blueprint::switchToNormalMode( const fs::path &blueprintPath ) {
	m_filePath = blueprintPath;

	const fs::path oldAsmFolder = getAsmDirectory();
	const fs::path oldAsmFile = getAsmFilePath();

	m_collabMode = false;
	m_romPath = m_romPath.empty() ? fs::path( blueprintPath ).replace_extension( ".z64" ) : (blueprintPath.parent_path() / m_romPath);
	if( fs::exists( oldAsmFolder ) ) {
		fs::forceDeleteRecursive( getAsmDirectory() );
		fs::copy( oldAsmFolder, getAsmDirectory(), fs::copy_options::recursive );
	}
	if( fs::exists( oldAsmFile ) ) {
		fs::tryCopyFileOverwrite( oldAsmFile, getAsmFilePath() );
	}

	save();
	RecentProjects::push( blueprintPath.string() );
}

string Blueprint::targetRomName() const {
	return m_collabMode ? fs::path( m_romPath ).string() : fs::path( m_romPath ).filename().string();
}

std::filesystem::path Blueprint::targetRomPath() const {
	return m_collabMode ? m_filePath / m_romPath : m_romPath;
}

void Blueprint::generateModulesInternal( RomMetadata &metadata ) const {
	AsmModuleBuilder::generate(
		m_modules,
		m_customActions,
		std::filesystem::path( m_tempDir ) / "modules",
		getAsmDirectory() / "modules"
	);

	metadata.moduleRegions.clear();
	for( const auto &i : m_modules ) {
		for( const AbsoluteAsmFile &globalAsm : i.second.definition.globalFiles ) {
			metadata.moduleRegions.push_back({
				globalAsm.romStart,
				globalAsm.romEnd
			});
		}
		for( const AsmModuleHook &hook : i.second.definition.hooks ) {
			if( hook.type != ModuleHook::Custom ) continue;
			metadata.moduleRegions.push_back({
				hook.shimAddress,
				hook.shimAddress + 4
			});
		}
	}
}

void Blueprint::revertModuleRegions( std::ostream &rom, RomMetadata &metadata ) const {
	for( const std::pair<uint,uint> &region : metadata.moduleRegions ) {
		if( region.first >= region.second ) continue;
		rom.seekp( region.first );
		BaseRom::revert( rom, region.second - region.first );
	}
}
