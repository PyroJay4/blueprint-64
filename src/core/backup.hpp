#include "src/types.hpp"
#include "src/polyfill/filesystem.hpp"

class FileBackup {

	private:
	std::filesystem::path m_sourcePath;
	std::filesystem::path m_backupPath;
	bool m_discard;

	public:
	FileBackup( const std::filesystem::path &filePath );
	FileBackup( string filePath ) : FileBackup( std::filesystem::path( filePath ) ) {}
	~FileBackup();

	inline void discard() {
		m_discard = true;
	}

};
