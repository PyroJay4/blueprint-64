#include "src/core/emulator.hpp"

#include <fstream>
#include <map>
#include <regex>
#include "src/polyfill/filesystem.hpp"
#include "src/polyfill/appdata.hpp"
#include "src/polyfill/exec.hpp"
#include "src/openmode.hpp"
#include "src/core/util.hpp"
#include "src/core/storage.hpp"

#ifdef _WIN32
#include "src/polyfill/window-slit.hpp"
static const string s_libraryExtension = ".dll";
#else
static const string s_libraryExtension = ".so";
#endif

namespace fs = std::filesystem;

#if _WIN32
static inline fs::path getRetroarchDefaultDir() {
	const fs::path defaultPath = fs::path( Windows::getKnownFolderPath( Windows::KnownFolder::RoamingAppData ) ) / "RetroArch";
	fs::create_directories( defaultPath );
	return defaultPath;
}
#endif

void Emulator::RetroArch::Settings::writeJson( JsonWriter &jw ) const {
	jw.writeObjectStart();
#ifdef _WIN32
		jw.writeProperty( "exe_path", exePath );
#endif
		jw.writeProperty( "core_directory", coreDirPath );
		jw.writeProperty( "core", core );
		jw.writeProperty( "cpu", cpuCore );
		jw.writeProperty( "fullscreen", fullscreen );
		jw.writeProperty( "scale", resolutionScale );
		jw.writeProperty( "vsync", vsync );
		jw.writeProperty( "full_speed", fullspeed );
		jw.writePropertyName( "parallel_config" );
		jw.writeObjectStart();
			jw.writeProperty( "gfx", parallelConfig.gfxPlugin );
			jw.writeProperty( "overclock", parallelConfig.overclockVI );
		jw.writeObjectEnd();
	jw.writeObjectEnd();
}

Emulator::RetroArch::Settings Emulator::RetroArch::Settings::parseJSON( const Json &json ) {
#ifdef _WIN32
	static const fs::path defaultPath = getRetroarchDefaultDir();
#endif
	return Settings{
#ifdef _WIN32
		json["exe_path"].getOrDefault<string>( (defaultPath / "retroarch.exe").string() ),
		json["core_directory"].getOrDefault<string>( (defaultPath / "cores").string() ),
#else
		json["core_directory"].getOrDefault<string>( "/usr/lib/libretro" ),
#endif
		json["core"].getOrDefault<EmulatorCore>( EmulatorCore::ParaLLEl ),
		json["cpu"].getOrDefault<CpuEmulation>( CpuEmulation::DynamicRecompiler ),
		json["fullscreen"].getOrDefault<bool>( false ),
		json["scale"].getOrDefault<ubyte>( 4 ),
		json["vsync"].getOrDefault<bool>( true ),
		json["full_speed"].getOrDefault<bool>( true ),
		ParallelCoreOptions{
			json["parallel_config"]["gfx"].getOrDefault<GraphicsPlugin>( GraphicsPlugin::Parallel ),
			json["parallel_config"]["overclock"].getOrDefault<bool>( true )
		}
	};
}

void Emulator::Mupen64Plus::Settings::writeJson( JsonWriter &jw ) const {
	jw.writeObjectStart();
#ifdef _WIN32
		jw.writeProperty( "exe_path", exePath );
#endif
		jw.writeProperty( "plugin_directory", pluginDirPath );
		jw.writeProperty( "cpu", cpuCore );
		jw.writeProperty( "gfx", gfxPlugin );
		jw.writeProperty( "rsp", rspPlugin );
		jw.writeProperty( "fullscreen", fullscreen );
		jw.writeProperty( "scale", resolutionScale );
		jw.writeProperty( "osd", onScreenDisplay );
	jw.writeObjectEnd();
}

Emulator::Mupen64Plus::Settings Emulator::Mupen64Plus::Settings::parseJSON( const Json &json ) {
#ifdef _WIN32
	static const fs::path defaultPath = getHomeDirectory() / "Downloads" / "mupen64plus";
#endif
	return Settings{
#ifdef _WIN32
		json["exe_path"].getOrDefault<string>( (defaultPath / "mupen64plus-ui-console.exe").string() ),
		json["plugin_directory"].getOrDefault<string>( defaultPath.string() ),
#else
		json["plugin_directory"].getOrDefault<string>( "/usr/lib/x86_64-linux-gnu/mupen64plus" ),
#endif
		json["cpu"].getOrDefault<CpuEmulation>( CpuEmulation::DynamicRecompiler ),
		json["gfx"].getOrDefault<string>( "glide64mk2" ),
		json["rsp"].getOrDefault<string>( "hle" ),
		json["fullscreen"].getOrDefault<bool>( false ),
		json["scale"].getOrDefault<ubyte>( 4 ),
		json["osd"].getOrDefault<bool>( true )
	};
}

void Emulator::EmulatorSettings::writeJson( JsonWriter &jw ) const {
	jw.writeObjectStart();
		jw.writeProperty( "perferred_emulator", preferredEmulator );
		jw.writePropertyName( "retroarch_config" );
		retroSettings.writeJson( jw );
		jw.writePropertyName( "mupen_config" );
		mupenSettings.writeJson( jw );
		jw.writeProperty( "other_emu_cmd", otherEmulatorCmd );
	jw.writeObjectEnd();
}

Emulator::EmulatorSettings Emulator::EmulatorSettings::parseJSON( const Json &json ) {
	return EmulatorSettings{
		json["perferred_emulator"].getOrDefault<Emulator>( Emulator::RetroArch ),
		RetroArch::Settings::parseJSON( json["retroarch_config"] ),
		Mupen64Plus::Settings::parseJSON( json["mupen_config"] ),
		json["other_emu_cmd"].getOrDefault<string>( "cen64 -multithread ~/Documents/pifdata.bin \"%s\"" )
	};
}

static inline bool startsWith( const string &str, const string &prefix ) {
	return str.compare( 0, prefix.length(), prefix ) == 0;
}

static std::vector<string> getMupenPlugins( const string &prefix, const string &pluginDir ) {
	const fs::path pluginsPath = fs::path( pluginDir );
	std::vector<string> plugins;

	if( !fs::exists( pluginsPath ) ) {
		return plugins;
	}

	for( const auto &p : fs::directory_iterator( pluginsPath ) ) {
		if( p.status().type() != fs::file_type::regular ) continue;
		if( p.path().extension().string() != s_libraryExtension ) continue;
		if( !startsWith( p.path().stem().string(), prefix ) ) continue;

		plugins.push_back( p.path().stem().string().substr( prefix.length() ) );
	}

	return plugins;
}

std::vector<string> Emulator::Mupen64Plus::getGfxPlugins( const string &pluginDir ) {
	return getMupenPlugins( "mupen64plus-video-", pluginDir );
}

std::vector<string> Emulator::Mupen64Plus::getRspPlugins( const string &pluginDir ) {
	return getMupenPlugins( "mupen64plus-rsp-", pluginDir );
}

static const string &getRetroCfgPath() {
	static const string s_path = (fs::path( AppData::cacheDir() ) / "retroarch.cfg").string().c_str();
	return s_path;
}

static const string &getRetroCoreCfgPath() {
	static const string s_path = (fs::path( AppData::cacheDir() ) / "retroarch-core-options.cfg").string().c_str();
	return s_path;
}

static const std::regex s_cfgRegex(
	"^([^=]+)\\s*=\\s*\"([^\"]*)\"",
	std::regex_constants::ECMAScript | std::regex_constants::optimize
);

static void readRetroConfig( const string &config, std::map<string,string> &options ) {
	fs::path configPath = fs::path( AppData::cacheDir() ) / config;
	if( !fs::exists( configPath ) ) {
#if _WIN32
		static const fs::path fallbackPath = getRetroarchDefaultDir();
		configPath = fallbackPath / config;
#else
		configPath = fs::path( AppData::configDir() ).parent_path() / "retroarch" / config;
		if( !fs::exists( configPath ) ) {
			configPath = fs::path( "~/."s + config );
			if( !fs::exists( configPath ) ) return;
		}
#endif
	}

	std::ifstream cfgFile( configPath.string(), std::ios_base::in );
	while( cfgFile.good() && !cfgFile.eof() ) {
		string cfgLine;
		std::getline( cfgFile, cfgLine );

		std::smatch matches;
		if( !cfgLine.empty() && std::regex_search( cfgLine, matches, s_cfgRegex ) ) {
			options[trimEnd( matches[1] )] = matches[2];
		}
	}
}

static inline std::map<string,string> getBaseRetroConfig() {
	/* The --appendconfig command line option does not override properties in the
	 * main config file because RetroArch is stupid and also dumb. So instead, create
	 * a whole new config file by copying the base config and overriding the options.
	 */
	std::map<string,string> options;
	readRetroConfig( "retroarch.cfg", options );
	readRetroConfig( "retroarch-core-options.cfg", options );
	return options;
}

void Emulator::RetroArch::Settings::createConfigFile() const {
	const int width = 320 * (int)resolutionScale;
	const int height = 240 * (int)resolutionScale;
	const string resolution = std::to_string( width ) + "x" + std::to_string( height );

	std::map<string,string> options = getBaseRetroConfig();
	options["video_fullscreen"] = fullscreen ? "True" : "False";
	options["video_scale"] = "1.000000";
	options["video_vsync"] = vsync ? "True" : "False";
	options["video_scale_integer"] = fullscreen ? "False" : "True";
	options["video_force_aspect"] = fullscreen ? "False": "True";
	options["video_windowed_position_width"] = std::to_string( width );
	options["video_windowed_position_height"] = std::to_string( height );
	options["custom_viewport_width"] = "320";
	options["custom_viewport_height"] = "240";

	switch( cpuCore ) {
		case CpuEmulation::DynamicRecompiler:
			options["parallel-n64-cpucore"] = options["mupen64plus-cpucore"] = "dynamic_recompiler";
			break;
		case CpuEmulation::CachedInterpreter:
			options["parallel-n64-cpucore"] = options["mupen64plus-cpucore"] = "cached_interpreter";
			break;
		case CpuEmulation::PureInterpreter:
			options["parallel-n64-cpucore"] = options["mupen64plus-cpucore"] = "pure_interpreter";
			break;
	}

	options["parallel-n64-gfxplugin-accuracy"] = "veryhigh";
	switch( parallelConfig.gfxPlugin ) {
		case GraphicsPlugin::Parallel:
			options["parallel-n64-gfxplugin"] = "parallel";
			break;
		case GraphicsPlugin::Angrylion:
			options["parallel-n64-gfxplugin"] = "angrylion";
			break;
		case GraphicsPlugin::Glide64:
			options["parallel-n64-gfxplugin"] = "glide64";
			break;
		case GraphicsPlugin::Rice:
			options["parallel-n64-gfxplugin"] = "rice";
			break;
		case GraphicsPlugin::GlideN64:
			options["parallel-n64-gfxplugin"] = "gln64";
			break;
		case GraphicsPlugin::Auto:
			options["parallel-n64-gfxplugin"] = "auto";
			break;
	}

	options["parallel-n64-rspplugin"] = "auto";
	options["parallel-n64-screensize"] = resolution;
	options["parallel-n64-virefresh"] = parallelConfig.overclockVI ? "2200" : "auto";
	options["parallel-n64-framerate"] = fullspeed ? "fullspeed" : "original";

	options["mupen64plus-43screensize"] = resolution;
	options["mupen64plus-aspect"] = "4:3";
	options["mupen64plus-EnableFBEmulation"] = "True";
	options["mupen64plus-CountPerOp"] = fullspeed ? "1": "2";

	options["config_save_on_exit"] = "True";

	std::ofstream baseConfig( getRetroCfgPath(), mode::write );
	std::ofstream coreConfig( getRetroCoreCfgPath(), mode::write );
	for( const auto &i : options ) {
		const string &key = i.first;
		const string &value = i.second;

		if( startsWith( key, "parallel-n64-" ) || startsWith( key, "mupen64plus-" ) ) {
			coreConfig << key << " = \"" << value << "\"" << std::endl;
		} else {
			baseConfig << key << " = \"" << value << "\"" << std::endl;
		}
	}
	baseConfig << std::endl;
	coreConfig << std::endl;
}

static inline bool launchRetroArch( const string &romPath, const Emulator::RetroArch::Settings config ) {
	static const string s_parallelCore = "parallel_n64_libretro" + s_libraryExtension;
	static const string s_mupenCore = "mupen64plus_libretro" + s_libraryExtension;

	const fs::path corePath = fs::path( config.coreDirPath ) / (config.core == Emulator::RetroArch::EmulatorCore::Mupen64plus ? s_mupenCore : s_parallelCore);
	config.createConfigFile();

#ifdef _WIN32
	string retroArchPath = AppPreferences::current().emuConfig.retroSettings.exePath;
	if( retroArchPath.empty() ) {
		retroArchPath = "retroarch";
	} else {
		retroArchPath = quoteAndEscape( retroArchPath );
	}
#else
	const string retroArchPath = "retroarch";
#endif

	return Command::execDetached(
		retroArchPath +
		" -L "s + quoteAndEscape( corePath.string() ) +
		" --config " + quoteAndEscape( getRetroCfgPath() ) +
		" " + quoteAndEscape( romPath )
	);
}

static inline bool launchMupen64Plus( const string &romPath, const Emulator::Mupen64Plus::Settings config ) {
#ifdef _WIN32
	string mupenPath = AppPreferences::current().emuConfig.mupenSettings.exePath;
	if( mupenPath.empty() ) {
		mupenPath = "mupen64plus";
	} else {
		mupenPath = quoteAndEscape( mupenPath );
	}
#else
	const string mupenPath = "mupen64plus";
#endif

	const string resolution = std::to_string( 320 * (int)config.resolutionScale ) + "x" + std::to_string( 240 * (int)config.resolutionScale );
	return Command::execDetached(
		mupenPath +
		(config.onScreenDisplay ? " --osd" : " --noosd") +
		(config.fullscreen ? " --fullscreen" : " --windowed" ) +
		" --resolution " + resolution +
		" --plugindir " + quoteAndEscape( config.pluginDirPath ) +
		" --gfx mupen64plus-video-" + config.gfxPlugin +
		" --rsp mupen64plus-rsp-" + config.rspPlugin +
		" --emumode " + std::to_string( 2 - (int)config.cpuCore ) +
		" " + quoteAndEscape( romPath )
	);
}

static inline bool launchOtherEmulator( const string &romPath, const string &cmd ) {
	if( cmd.empty() ) return false;

	string processedCmd;
	bool replace = false;
	for( char c : cmd ) {
		if( replace ) {
			switch( c ) {
				case '%': break;
				case 's': processedCmd += romPath; break;
				default: processedCmd += "%" + c; break;
			}
			replace = false;
		} else if( c == '%' ) {
			replace = true;
		} else {
			processedCmd += c;
		}
	}

	if( replace ) {
		processedCmd += '%';
	}

	return Command::execDetached( processedCmd );
}

bool Emulator::launch( const string &romPath ) {
	const EmulatorSettings &config = AppPreferences::current().emuConfig;
	switch( config.preferredEmulator ) {
		case Emulator::RetroArch:
			return launchRetroArch( romPath, config.retroSettings );
		case Emulator::Mupen64Plus:
			return launchMupen64Plus( romPath, config.mupenSettings );
		default:
			return launchOtherEmulator( romPath, config.otherEmulatorCmd );
	}
}

bool Emulator::emulatorInstalled() {
	const EmulatorSettings &config = AppPreferences::current().emuConfig;
	switch( config.preferredEmulator ) {
		case Emulator::RetroArch:
#if _WIN32
			return fs::exists( fs::path( config.retroSettings.exePath ) );
#else
			return Command::execSync( "which retroarch" );
#endif
		case Emulator::Mupen64Plus:
#if _WIN32
			return fs::exists( fs::path( config.mupenSettings.exePath ) );
#else
			return Command::execSync( "which mupen64plus" );
#endif
		default:
			return !config.otherEmulatorCmd.empty();
	}
}
