#include <iostream>
#include <cassert>

#include <QApplication>
#include <QMessageBox>
#include <QFileDialog>
#include <QNetworkAccessManager>
#include <QNetworkReply>

#if !defined(__linux__) && !defined(__unix__)
#include "src/polyfill/tar.hpp"
#include "src/polyfill/window-slit.hpp"
#include "src/ui/install-7zip-dialog.hpp"
#include <QFile>
#endif

#include "src/polyfill/filesystem.hpp"
#include "src/polyfill/appdata.hpp"
#include "src/ui/start-window.hpp"
#include "src/ui/main-window.hpp"
#include "src/ui/blueprint-controller.hpp"
#include "src/ui/util.hpp"
#include "src/ui/base-rom.hpp"
#include "src/ui/tooltip-hack.hpp"
#include "src/core/tweak.hpp"
#include "src/core/storage.hpp"
#include "src/core/armips.hpp"
#include "src/core/version.hpp"
#include "src/core/asm-modules.hpp"

static void updateCheckAsync() {
	QNetworkAccessManager *web = new QNetworkAccessManager();
	QNetworkReply *response = web->get(
		QNetworkRequest( QUrl( "https://blueprint64.ca/version/latest" ) )
	);

	QObject::connect( response, QOverload<QNetworkReply::NetworkError>::of( &QNetworkReply::error ), [=](QNetworkReply::NetworkError error) {
		(void)error;
		response->deleteLater();
		web->deleteLater();
	});

	QObject::connect( response, &QNetworkReply::finished, [=](){
		QByteArray rawData = response->readAll();
		if( !rawData.isNull() ) {
			std::optional<Version> latest = Version::tryParse( rawData.toStdString() );
			if( latest.has_value() && latest.value() > CurrentVersion::Application ) {
				QMessageBox::information( nullptr, "Update Available", "A new version of Bowser's Blueprints is available. Download the latest version at <a href=\"https://blueprint64.ca\">https://blueprint64.ca</a>", QMessageBox::Ok );
			}
		}

		response->deleteLater();
		web->deleteLater();
	});
}

static void loadTweakDataFile( const char *tweakFile ) {
	QResourceStream resourceStream( tweakFile );
	std::vector<Uuid> conflicts = TweakStore::instance().loadFileAndGetConflicts( resourceStream, true );
	for( const Uuid &conflict : conflicts ) {
		std::cout << conflict.toString() << std::endl << std::flush;
	}
	assert( conflicts.size() == 0 );
}

static inline void loadTweakDefinitions() {
	loadTweakDataFile( ":/tweaks/default.json" );
	loadTweakDataFile( ":/tweaks/basic.json" );
	loadTweakDataFile( ":/tweaks/numeric.json" );
	loadTweakDataFile( ":/tweaks/composite.json" );
	loadTweakDataFile( ":/tweaks/enemies.json" );
	loadTweakDataFile( ":/tweaks/switch.json" );
	loadTweakDataFile( ":/tweaks/extend-vertical.json" );
}

static inline void connectSignalsAndSlots(
	BlueprintController *fileController,
	StartWindow *startupWindow,
	MainWindow *mainWindow
) {
	QObject::connect(
		startupWindow, SIGNAL(projectCreated(CreateProjectOptions)),
		fileController, SLOT(createProject(CreateProjectOptions))
	);

	QObject::connect(
		startupWindow, SIGNAL(projectOpened(string)),
		fileController, SLOT(openProject(string))
	);

	QObject::connect(
		fileController, SIGNAL(blueprintLoaded()),
		mainWindow, SLOT(loadBlueprint())
	);

	QObject::connect(
		fileController, SIGNAL(blueprintLoaded()),
		startupWindow, SLOT(close())
	);

	QObject::connect(
		fileController, SIGNAL(loadFailed(string)),
		startupWindow, SLOT(showErrorDialog(string))
	);

	QObject::connect(
		mainWindow, SIGNAL(openNew()),
		startupWindow, SLOT(openProject())
	);

	QObject::connect(
		mainWindow, SIGNAL(createNew()),
		startupWindow, SLOT(newProject())
	);

	QObject::connect(
		fileController, SIGNAL(loadFailed(string)),
		mainWindow, SLOT(showErrorDialog(string))
	);
}

#if !defined(__linux__) && !defined(__unix__)
static inline bool test7zip() {
	namespace fs = std::filesystem;
	const fs::path tempDir = fs::path( createTemporaryDirectory() );
	QFile::copy( ":/modules/Pseudoinstructions_v2.0.0.tar.gz", (tempDir / "test.tar.gz").string().c_str() );
	fs::create_directory( tempDir / "extracted" );
	clearReadOnlyFlagOnWindows( tempDir );
	const bool success =
		tarball::extract( (tempDir / "test.tar.gz").string(), (tempDir / "extracted").string() ) &&
		fs::exists( tempDir / "extracted" / "module.json" );
	fs::forceDeleteRecursive( tempDir );
	return success;
}
#endif

static int run( QApplication &app ) {
	loadTweakDefinitions();

	BlueprintController fileController;
	StartWindow startupWindow;
	MainWindow mainWindow;

	if( !Armips::exists() ) {
		namespace fs = std::filesystem;

		#ifdef _WIN32
		const string ARMIPS_EXE = "armips.exe";
		#else
		const string ARMIPS_EXE = "armips";
		#endif

		const fs::path bundledArmipsPath = currentProgramPath().parent_path() / "armips" / ARMIPS_EXE;
		if( fs::exists( bundledArmipsPath ) ) {
			fs::create_directories( fs::path( AppData::dataDir() ) );
			fs::copy( bundledArmipsPath, fs::path( AppData::dataDir() ) / ARMIPS_EXE );
		} else {
			QMessageBox::information( nullptr, "Missing Armips", "The Armips executable bundled with Bowser's Blueprints could not be found. You will need to provide the path to Armips now." );
			QString armipsPath = UiUtil::toNativePath( QFileDialog::getOpenFileName( nullptr, "Where is Armips?" ) );
			if( armipsPath.isNull() ) {
				return 1;
			}
			fs::create_directories( fs::path( AppData::dataDir() ) );
			fs::copy( fs::path( armipsPath.toStdString() ), fs::path( AppData::dataDir() ) / ARMIPS_EXE );
		}
	}

	connectSignalsAndSlots( &fileController, &startupWindow, &mainWindow );
	AppPreferences::current().save();
	if( AppPreferences::current().openOnStart && !RecentProjects::get().empty() ) {
		try {
			Blueprint::open( std::filesystem::path( RecentProjects::get().front() ) );
			mainWindow.loadBlueprint();
		} catch( ... ) {
			std::cerr << "Failed to load most recent project." << std::endl << std::flush;
			startupWindow.show();
		}
	} else {
		startupWindow.show();
	}

	AsmModuleStore::installBuiltinModules();

	updateCheckAsync();
	return app.exec();
}

#ifdef _WIN32
static inline void clearTemporaryFiles() {
	namespace fs = std::filesystem;
	for( const auto &t : fs::directory_iterator( fs::path( Windows::getKnownFolderPath( Windows::KnownFolder::Temp ) ) ) ) {
		if( t.path().stem().string().compare( 0, 13, "bbp_blueprint" ) != 0 ) {
			continue;
		}

		std::error_code err;
		fs::tryDeleteRecursive( t.path() );
	}
}
#endif

int main( int argc, char *argv[] ) {
	namespace fs = std::filesystem;
	fs::create_directories( fs::path( AppData::cacheDir() ) );
	fs::create_directories( fs::path( AppData::configDir() ) );
	fs::create_directories( fs::path( AppData::dataDir() ) );

#ifdef _WIN32
	// Windows doesn't actually clear temporary files because of course it doesn't
	if( !Windows::isAlreadyRunning() ) {
		clearTemporaryFiles();
	}
#endif

	QApplication app( argc, argv );

	const string &theme = AppPreferences::current().theme;
	if( !theme.empty() ) {
		QApplication::setStyle( theme.c_str() );
	}

	if( !QIcon::themeName().toLower().contains( "breeze" ) ) {
		QIcon::setThemeName( "breeze-fallback" );
	}

	TooltipHack tooltipHack( &app );
	app.installEventFilter( &tooltipHack );
	BaseRom::init();

#if !defined(__linux__) && !defined(__unix__)
	while( !tarball::toolInstalled() || !test7zip() ) {
		Windows7zipDialog dialog;
		if( dialog.exec() != QDialog::Accepted ) {
			return 0;
		}

		AppPreferences::current().sevenZipPath = dialog.getExePath();
	}
#endif

	int exitCode = run( app );
	Blueprint::free();
	return exitCode;
}
