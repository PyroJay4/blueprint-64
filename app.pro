QT += core gui widgets network
TEMPLATE = app

TARGET = "blueprint64"

CONFIG += qt object_parallel_to_source
CONFIG(debug, debug|release){
	OBJECTS_DIR = "build/debug/obj"
	MOC_DIR = "build/debug/moc"
	UI_DIR = "build/debug/ui"
	DEFINES += DEBUG
}
CONFIG(release, debug|release){
	OBJECTS_DIR = "build/release/obj"
	MOC_DIR = "build/release/moc"
	UI_DIR = "build/release/ui"
}

QMAKE_CXXFLAGS += -std=c++17 -Werror -fno-strict-aliasing
QMAKE_LFLAGS += -no-pie -fno-pie
DEFINES += QT_DEPRECATED_WARNINGS

SOURCES += src/main.cpp src/core/*.cpp src/core/import/*.cpp src/core/level-writer/*.cpp src/polyfill/*.cpp src/ui/*.cpp src/ui/tweak-values/*.cpp src/ui/widgets/*.cpp
HEADERS += src/types.hpp src/core/*.hpp src/core/import/*.hpp src/core/level-writer/*.hpp src/polyfill/*.hpp src/ui/*.hpp src/ui/tweak-values/*.hpp src/ui/widgets/*.hpp
FORMS += src/ui/designer/*.ui
RESOURCES = data/resources.qrc
LIBS += -lstdc++fs
