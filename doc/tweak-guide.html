<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8">
		<title>Bowser's Blueprints Tweak Specification</title>
		<style>
			body {
				font-family: sans-serif;
				max-width: 1200px;
				margin: auto;
				padding-bottom: 4rem;
			}
			
			h2 {
				text-decoration: underline;
			}
			
			pre {
				color: black;
				font-family: monospace;
			}
			
			pre > a {
				color: #26802f;
				text-decoration: none;
			}
			
			pre > a:hover {
				text-decoration: underline dotted;
			}
			
			pre > span {
				color: #707070;
			}
			
			div.props {
				color: black;
			}
			
			div.props > b {
				font-family: monospace;
				font-size: 1.25em;
			}
			
			div.props > div {
				margin-left: 4ch;
			}
		</style>
	</head>
	<body>
		<h1>Tweak Developer's Guide</h1>
		<p>This document describes how to write tweak packs for Bowser's Blueprints. You can click on green
		text in type definitions to be taken to the definition of the referenced type. A schema document is
		also provided for you to validate your tweak definitions against (tweak-format-schema.json).</p>
		
		<h2>The Tweak Pack Schema</h2>
		<p>A tweak pack is a JSON file with the following format:</p>
		
		<pre>
{
    "version": "1.1.0",
    "pack_info": {
        "name": <span>(string)</span>,
        "uuid": <a href="#uuid">(uuid)</a>,
        "version": <span>(string)</span>
    },
    "tweaks": [
        <a href="#TweakDefinition">&lt;TweakDefinition&gt;</a>,
        ...
    ]
}
		</pre>
		
		<div class="props">
			<b>version</b> - string, required
			<div>The version of the tweak schema used to define the tweaks. Currently, the only valid value is "1.1.0".</div>
			<b>pack_info.name</b> - string, required
			<div>The name of the tweak pack</div>
			<b>pack_info.uuid</b> - uuid, required
			<div>A unique id (see below) for the tweak pack. This id should <b>not</b> change between versions.</div>
			<b>pack_info.version</b> - string, optional
			<div>A version string for the current revision of the tweak pack. The version format must have a major, minor,
			and patch version. The version should follow the semantic versioning convention where incrementing the major
			version indicates a breaking change (eg. removing a tweak or changing a tweak's behaviour), incrementing the
			minor version indicates the addition of new features (eg. adding a new tweak), and incrementing the patch
			version indicates a bugfix. If not present, defaults to <code>"1.0.0"</code></div>
			<b>tweaks</b> - array[TweakDefinition], required
			<div>An array of tweak definitions. Must contain at least one entry.</div>
		</div>
		
		<h2>Some Type Definitions</h2>
		<p>This section describes the JSON spec for some objects and special string types used by various tweak definitions.</p>
		
		<h3 id="uuid">uuid</h3>
		<p>A <i>uuid</i> is a <b>string</b> representing a version 4 <a href="https://en.wikipedia.org/wiki/Universally_unique_identifier">Universally Unique_Identifier</a>.
		You can generate a uuid on Linux easily using the command <code>uuidgen -r</code>. On Windows,
		the equivalent command is <code>powershell -Command "[Guid]::NewGuid().toString()"</code>.</p>
		
		<h3 id="patchString">patchString</h3>
		<p>A <i>patchString</i> is a <b>string</b> representing a series of bytes as space-separated hexidecimal pairs. For example, <code>"0D 15 EA 5E"</code>.</p>
		
		<h3 id="hexValue">hexValue</h3>
		<p>A <i>hexValue</i> is a <b>string</b> representing a positive integer in hexidecimal format. The '0x' prefix should <i>not</i> be included.</p>
		
		<h3 id="FloatTransform">FloatTransform</h3>
		<p>A <i>FloatTransform</i> represents a series of operations to tranform one floating point value into another.
		It can be used to write multiple different related values to the ROM using only one value as input.</p>
		
		<pre>
{
    "ops": [
        {
            "op": <span>(enum)</span>,
            "val": <span>(number)</span>
        },
        ...
    ]
}
		</pre>
		
		<div class="props">
			<b>ops</b> - array, required
			<div>An array of operations to apply to the input value in series</div>
			<b>op</b> - string (enum), required
			<div>
				The operation to apply. Can be any of the following values:
				<ul>
					<li><b>add</b> - Adds the given value to the current value</li>
					<li><b>subtract</b> - Subtracts the given value from the current value</li>
					<li><b>subtract_other</b> - Subtracts the current value from the given value, and sets the result as the new current value</li>
					<li><b>multiply</b> - Multiplies the current value by the given value</li>
					<li><b>divide</b> - Divides the current value by the given value.</li>
					<li><b>modulo</b> - Sets the current result to the current value modulo the given value</li>
					<li><b>min</b> - Sets the current value to the minimum of the current value and the given value</li>
					<li><b>max</b> - Sets the current value to the maximum of the current value and the given value</li>
				</ul>
			</div>
			<b>value</b> - number, required
			<div>The second operand of the operation</div>
		</div>
		
		<h3 id="IntegerTransform">IntegerTransform</h3>
		<p>An <i>IntegerTransform</i> is identical to a <i>FloatTransform</i> as described above except that all op values
		should be integers instead of floating point values, and the following additional operations are allowed:</p>
		<ul>
			<li><b>left_shift</b> - Performs a bit shift left of <i>value</i> bytes</li>
			<li><b>right_shift</b> - Performs a bit shift right of <i>value</i> bytes</li>
			<li><b>and</b> - Performs a bitwise AND of the current value and the given value</li>
			<li><b>or</b> - Performs a bitwise OR of the current value and the given value</li>
			<li><b>left_shift_other</b> - Sets the current value to the given value shifted left by a number of bytes equal to the current value</li>
		</ul>
		
		<h3 id="StaticPatch">StaticPatch</h3>
		<p>A <i>StaticPatch</i> object describes a patch that is not dependant on any property value.
		This is mostly used for basic tweaks, but can also be used in other tweaks as well.</p>
		
		<pre>
{
    "seek": <a href="#hexValue">(hexValue)</a>,
    "apply": <a href="#patchString">(patchString)</a>
}
		</pre>
		
		<div class="props">
			<b>seek</b> - hexValue, required
			<div>The position in the ROM to start writing the patch to</div>
			<b>apply</b> - patchString, required
			<div>The patch to apply</div>
		</div>
		
		<h3 id="NumericPatch">NumericPatch</h3>
		<p>A <i>NumericPatch</i> object describes a patch that writes values depending on a numeric parameter.</p>
		
		<pre>
{
    "seek": <a href="#hexValue">(hexValue)</a>,
    "type": <span>(enum)</span>,
    "transform": <span>[Optional] (number)</span>,
    "no_revert": <span>[Optional] (boolean)</span>
}
		</pre>
		
		<div class="props">
			<b>seek</b> - hexValue, required
			<div>The position in the ROM to start writing the value to</div>
			<b>type</b> - string (enum), required
			<div>
				How the value should be encoded. Accepts the following values:
				<ul>
					<li><b>byte</b> <i>(1 byte)</i> - writes the value as an unsigned byte</li>
					<li><b>byte.neg</b> <i>(1 byte)</i> - negates the value, then writes it as a signed byte</li>
					<li><b>short</b> <i>(2 bytes)</i> - writes the value as an unsigned short</li>
					<li><b>short.neg</b> <i>(2 bytes)</i> - negates the value, then writes it as a signed short</li>
					<li><b>int</b> <i>(4 bytes)</i> - writes the value as an unsigned int</li>
					<li><b>int.neg</b> <i>(4 bytes)</i> - negates the value, then writes it as a signed int</li>
					<li><b>float32.upper</b> <i>(2 bytes)</i> - writes the value as the upper half of a float. Usually used to set the immediate value of a LUI instruction.</li>
					<li><b>float32.lower</b> <i>(2 bytes)</i> - writes the value as the lower half of a float. Usually used to set the immediate value of an ORI instruction following a LUI.</li>
					<li><b>float16</b> <i>(2 bytes)</i> - an alias for float32.upper</li>
					<li><b>float32</b> <i>(4 bytes)</i> - writes the value as a single precision float</li>
					<li><b>float64</b> <i>(8 bytes)</i> - writes the value as a double precision float</li>
				</ul>
			</div>
			<b>transform</b> - number, optional
			<div>(Optional) If present, indicates the index of the transform to apply to the input value before
			writing the result to the ROM. The first transform is index 0, the second is index 1, etc.</div>
			<b>no_revert</b> - boolean, optional
			<div>(Optional) If <code>true</code>, the default value will not be written to the ROM when the tweak
			is reverted. Use this when a patch value overlaps a static patch.</div>
		</div>

		<h3 id="TweakDefinition">TweakDefinition</h3>
		<p>A <i>TweakDefinition</i> is one of any of the objects described in the <a href="#tweak-definitions">Tweak Definitions</a> section.</p>
		
		<h3 id="SingularTweakDefinition">SingularTweakDefinition</h3>
		<p>A <i>SingularTweakDefinition</i> is the same as a <i>TweakDefinition</i> except that it cannot be a composite tweak.</p>
		
		<h2 id="tweak-definitions">Tweak Definitions<h2>
		
		<h3 id="CompositeTweakDefinition">Composite Tweak</h3>
		<p>A composite tweak is a container for other tweaks. It will appear as a single entry in the tweak list
		in Bowser's Blueprints, and its subtweaks will define its various parameters. Composite tweaks may not be
		nested (A composite tweak may not contain another composite tweak).</p>
		
		<pre>
{
    "name": <span>(string)</span>,
    "description": <span>(string)</span>,
    "type": "composite",
    "components": [
        <a href="#SingularTweakDefinition">&lt;SingularTweakDefinition&gt;</a>,
        ...
    ]
}
		</pre>
		
		<div class="props">
			<b>name</b> - string, required
			<div>The name of the tweak as it will be displayed in Bowser's Blueprints.</div>
			<b>description</b> - string, required
			<div>A description of the tweak that will be displayed in the right pane.</div>
			<b>components</b> - array[SingularTweakDefinition], required
			<div>An array of non-composite tweak definitions to be grouped into this tweak.
			Must contain at least 2 entries.</div>
		</div>
		
		<h3 id="BasicTweakDefinition">Basic Tweak</h3>
		<p>A basic tweak is a simple tweak that does not take any parameters.
		You do not need to provide a revert patch- Bowser's Blueprints handles
		reversion of tweaks automatically.</p>
		
		<pre>
{
    "name": <span>(string)</span>,
    "uuid": <a href="#uuid">(uuid)</a>,
    "description": <span>(string)</span>,
    "type": "basic",
    "patch_static": [
        <a href="#StaticPatch">&lt;StaticPatch&gt;</a>,
        ...
    ]
}
		</pre>
		
		<div class="props">
			<b>name</b> - string, required
			<div>The name of the tweak as it will be displayed in Bowser's Blueprints.</div>
			<b>uuid</b> - uuid, required
			<div>A unique identifier for this tweak</div>
			<b>description</b> - string, required
			<div>A description of the tweak. For top-level tweaks, the description will be
			displayed in the right pane. For tweaks that are part of a composite tweak, the
			description will be displayed in the tooltip.</div>
			<b>patch_static</b> - array[StaticPatch], required
			<div>An array of patches to apply. Must have at least one entry.</div>
		</div>
		
		<h3 id="IntegerTweakDefinition">Integer Tweak</h3>
		<p>An integer tweak takes an integer value as input, and writes a patch
		using that value. Use this tweak if the input should be an integer. You
		can still write the value to the ROM as either an integer or a float.</p>
		
		<pre>
{
    "name": <span>(string)</span>,
    "uuid": <a href="#uuid">(uuid)</a>,
    "description": <span>(string)</span>,
    "type": "integer",
    "min": <span>(number)</span>,
    "max": <span>(number)</span>,
    "default": <span>(number)</span>,
    "hex": <span>[Optional] (boolean)</span>
    "patch_static": <span>[Optional]</span> [
        <a href="#StaticPatch">&lt;StaticPatch&gt;</a>,
        ...
    ],
    "patch_value": [
        <a href="#NumericPatch">&lt;NumericPatch&gt;</a>,
        ...
    ],
    "transforms": <span>[Optional]</span> [
        <a href="#IntegerTransform">&lt;IntegerTransform&gt;</a>,
        ...
    ]
}
		</pre>
		
		<div class="props">
			<b>name</b> - string, required
			<div>The name of the tweak as it will be displayed in Bowser's Blueprints.</div>
			<b>uuid</b> - uuid, required
			<div>A unique identifier for this tweak</div>
			<b>description</b> - string, required
			<div>A description of the tweak. For top-level tweaks, the description will be
			displayed in the right pane. For tweaks that are part of a composite tweak, the
			description will be displayed in the tooltip.</div>
			<b>min</b> - number, required
			<div>The minimum value allowed for the tweak input</div>
			<b>max</b> - number, required
			<div>The maximum value allowed for the tweak input</div>
			<b>hex</b> - boolean, optional
			<div>(Optional) If <code>true</code>, the value is displayed in hexidecimal in
			the UI. Defaults to <code>false</code> if not specified.</div>
			<b>default</b> - number, required
			<div>The default value for the tweak. Applying the tweak with this value should
			be functionally equivalent to not having applied the tweak at all.</div>
			<b>patch_static</b> - array[StaticPatch], optional
			<div>(Optional) An array of static patches to apply.</div>
			<b>patch_value</b> - array[NumericPatch], required
			<div>An array of value patches to apply. Must have at least one entry.</div>
			<b>transforms</b> - array[IntegerTransform], optional
			<div>(Optional) An array of transforms that the value patches may reference.</div>
		</div>
		
		<h3 id="FloatTweakDefinition">Float Tweak</h3>
		<p>A float tweak is just like an integer tweak except that it takes a
		floating point number as input.</p>
		
		<pre>
{
    "name": <span>(string)</span>,
    "uuid": <a href="#uuid">(uuid)</a>,
    "description": <span>(string)</span>,
    "type": "float",
    "precision": <span>(number)</span>,
    "min": <span>(number)</span>,
    "max": <span>(number)</span>,
    "default": <span>(number)</span>,
    "patch_static": <span>[Optional]</span> [
        <a href="#StaticPatch">&lt;StaticPatch&gt;</a>,
        ...
    ],
    "patch_value": [
        <a href="#NumericPatch">&lt;NumericPatch&gt;</a>,
        ...
    ],
    "transforms": <span>[Optional]</span> [
        <a href="#FloatTransform">&lt;FloatTransform&gt;</a>,
        ...
    ]
}
		</pre>
		
		<div class="props">
			<b>name</b> - string, required
			<div>The name of the tweak as it will be displayed in Bowser's Blueprints.</div>
			<b>uuid</b> - uuid, required
			<div>A unique identifier for this tweak</div>
			<b>description</b> - string, required
			<div>A description of the tweak. For top-level tweaks, the description will be
			displayed in the right pane. For tweaks that are part of a composite tweak, the
			description will be displayed in the tooltip.</div>
			<b>precision</b> - number, required
			<div>The number of decimal places the input spinner has. Must be at least 1.</div>
			<b>min</b> - number, required
			<div>The minimum value allowed for the tweak input</div>
			<b>max</b> - number, required
			<div>The maximum value allowed for the tweak input</div>
			<b>default</b> - number, required
			<div>The default value for the tweak. Applying the tweak with this value should
			be functionally equivalent to not having applied the tweak at all.</div>
			<b>patch_static</b> - array[StaticPatch], optional
			<div>(Optional) An array of static patches to apply.</div>
			<b>patch_value</b> - array[NumericPatch], required
			<div>An array of value patches to apply. Must have at least one entry.</div>
			<b>transforms</b> - array[FloatTransform], optional
			<div>(Optional) An array of transforms that the value patches may reference.</div>
		</div>
		
		<h3 id="ColourTweakDefinition">Colour Tweak</h3>
		<p>A colour tweak, as you would expect, takes a colour as input.</p>
		
		<pre>
{
    "name": <span>(string)</span>,
    "uuid": <a href="#uuid">(uuid)</a>,
    "description": <span>(string)</span>,
    "type": "colour",
    "alpha": <span>[Optional] (boolean)</span>,
    "default": <span>(string)</span>,
    "patch_static": <span>[Optional]</span> [
        <a href="#StaticPatch">&lt;StaticPatch&gt;</a>,
        ...
    ],
    "patch_value": [
        {
            "seek": <a href="#hexString">(hexString)</a>,
            "type": <span>(enum)</span>,
            "no_revert": <span>[Optional] (boolean)</span>
        },
        ...
    ]
}
		</pre>
		
		<div class="props">
			<b>name</b> - string, required
			<div>The name of the tweak as it will be displayed in Bowser's Blueprints.</div>
			<b>uuid</b> - uuid, required
			<div>A unique identifier for this tweak</div>
			<b>description</b> - string, required
			<div>A description of the tweak. For top-level tweaks, the description will be
			displayed in the right pane. For tweaks that are part of a composite tweak, the
			description will be displayed in the tooltip.</div>
			<b>alpha</b> - boolean, optional
			<div>(Optional) If <code>true</code>, the colour picker will allow the alpha value
			to be specified. If <code>false</code> or omitted, the alpha value is assumed to be
			fully opaque.</div>
			<b>default</b> - string, required
			<div>The default colour for the tweak, formatted as a hash (#) followed by either
			6 or 8 hexidecimal digits. (eg. <code>"#26802F"</code>) Applying the tweak with this
			value should be functionally equivalent to not having applied the tweak at all.</div>
			<b>patch_static</b> - array[StaticPatch], optional
			<div>(Optional) An array of static patches to apply.</div>
			<b>patch_value</b> - array[NumericPatch], required
			<div>An array of value patches to apply. Must have at least one entry.</div>
			<b>patch_value[].seek</b> - hexValue, required
			<div>The position in the ROM to start writing the colour to</div>
			<b>patch_value[].type</b> - string (enum), required
			<div>
				How the colour should be encoded. Accepts the following values:
				<ul>
					<li><b>colour16</b> <i>(2 bytes)</i> - writes the colour in RGBA5551 format</li>
					<li><b>colour24</b> <i>(3 bytes)</i> - writes the colour in RGB888 format</li>
					<li><b>colour32</b> <i>(4 bytse)</i> - writes the colour in RGBA8888 format</li>
				</ul>
			</div>
			<b>patch_value[].no_revert</b> - boolean, optional
			<div>(Optional) If <code>true</code>, the default colour will not be written to the ROM when the tweak
			is reverted. Use this when a patch value overlaps a static patch.</div>
		</div>
		
		<h3 id="EnumTweakDefinition">Enum Tweak</h3>
		<p>An enum tweak is an integer tweak that only allows certain specific values to be chosen.
		The UI will use a combo box instead of an integer spinner.</p>
		
		<pre>
{
    "name": <span>(string)</span>,
    "uuid": <a href="#uuid">(uuid)</a>,
    "description": <span>(string)</span>,
    "type": "enum",
    "default": <span>(number)</span>,
    "preset": <span>[Optional] (enum)</span>,
    "values": <span>[Optional]</span> [
        {
            "name": <span>(string)</span>,
            "value": <span>(number)</span>
        },
        ...
    ],
    "patch_static": <span>[Optional]</span> [
        <a href="#StaticPatch">&lt;StaticPatch&gt;</a>,
        ...
    ],
    "patch_value": [
        <a href="#NumericPatch">&lt;NumericPatch&gt;</a>,
        ...
    ],
    "transforms": <span>[Optional]</span> [
        <a href="#IntegerTransform">&lt;IntegerTransform&gt;</a>,
        ...
    ]
}
		</pre>
		
		<div class="props">
			<b>name</b> - string, required
			<div>The name of the tweak as it will be displayed in Bowser's Blueprints.</div>
			<b>uuid</b> - uuid, required
			<div>A unique identifier for this tweak</div>
			<b>description</b> - string, required
			<div>A description of the tweak. For top-level tweaks, the description will be
			displayed in the right pane. For tweaks that are part of a composite tweak, the
			description will be displayed in the tooltip.</div>
			<b>default</b> - number, required
			<div>The default value for the tweak. Applying the tweak with this value should
			be functionally equivalent to not having applied the tweak at all. If this value
			is not present in the <i>values</i> array, an entry named "Default" will be
			automatically added to the combo box.</div>
			<b>values</b> - array, required
			<div>A list of possible integer values to chose from</div>
			<b>values[].name</b> - string, required
			<div>The name to show for this entry in the combo box</div>
			<b>values[].value</b> - number, required
			<div>The input value this entry represents</div>
			<b>patch_static</b> - array[StaticPatch], optional
			<div>(Optional) An array of static patches to apply.</div>
			<b>patch_value</b> - array[NumericPatch], required
			<div>An array of value patches to apply. Must have at least one entry.</div>
			<b>transforms</b> - array[IntegerTransform], optional
			<div>(Optional) An array of transforms that the value patches may reference.</div>
		</div>
		
		<h3 id="SwitchTweakDefinition">Switch Tweak</h3>
		<p>A <i>switch tweak</i> is similar to an enum tweak, but each choice
		represents arbitrary data to write to the ROM instead of needing to be
		based off of a numeric value.</p>
		
		<pre>
{
    "name": <span>(string)</span>,
    "uuid": <a href="#uuid">(uuid)</a>,
    "description": <span>(string)</span>,
    "type": "switch",
    "default": <span>(number)</span>,
    "seeks": [
        <a href="#hexString">(hexString)</a>,
        ...
    ],
    "patch_static": <span>[Optional]</span> [
        <a href="#StaticPatch">&lt;StaticPatch&gt;</a>,
        ...
    ],
    "patch_case": [
        [
            {
                "name": <span>(string)</span>,
                "patches": [
                    <a href="#patchString">(patchString)</a>,
                    ...
                ]
            },
            ...
        ],
        ...
    ]
}
		</pre>
		
		<div class="props">
			<b>name</b> - string, required
			<div>The name of the tweak as it will be displayed in Bowser's Blueprints.</div>
			<b>uuid</b> - uuid, required
			<div>A unique identifier for this tweak</div>
			<b>description</b> - string, required
			<div>A description of the tweak. For top-level tweaks, the description will be
			displayed in the right pane. For tweaks that are part of a composite tweak, the
			description will be displayed in the tooltip.</div>
			<b>default</b> - number, required
			<div>The zero-based index into the <i>patch_case</i> array that gives the default value.
			The referenced patch should describe the original contents of the ROM, reverting the tweak.</div>
			<b>seeks</b> - array[hexString], required
			<div>A list of locations to seek to in the ROM to write each part of the patch. Must have
			at least one entry.</div>
			<b>patch_static</b> - array[StaticPatch], optional
			<div>(Optional) An array of static patches to apply.</div>
			<b>patch_case</b> - array, required
			<div>An array of patches to choose from. Must have at least three entries.</div>
			<b>patch_case[].name</b> - string, required
			<div>The name of the patch to show in the combo box</div>
			<b>patch_case[].patches</b> - array[patchString], required
			<div>An array of patch data to write to each seek position in the ROM. The length of the
			array must equal the length of the <i>seeks</i> array.</div>
		</div>

	</body>
</html>
