.create "end-screen-dl.bin",0
.n64

.macro drawQuad, w, h, dxt, i, tileFlags
	.word 0xFD100000, (0x07000000 + (i * 0x1000)) ; G_SETTIMG
	.word 0xF5100000, (0x07000000 | tileFlags) ; G_SETTILE
	.word 0xE6000000, 0x00000000 ; G_RDPLOADSYNC
	.word 0xF3000000, (0x07000000 | (((w * h) - 1) << 12) | dxt) ; G_LOADBLOCK
	.word 0xE7000000, 0x00000000 ; G_RDPPIPESYNC
	.word (0xF5100000 | ((w >> 2) << 9)), tileFlags ; G_SETTILE
	.word 0xF2000000, (((w - 1) << 14) | ((h - 1) << 2)) ; G_SETTILESIZE
	.word 0x04300040, (0x07025800 + (i * 0x40)) ; G_VTX
	.word 0xBF000000, 0x00000A14 ; G_TRI1
	.word 0xBF000000, 0x0000141E ; G_TRI1
.endmacro

.macro drawMain, i
	drawQuad 64, 32, 0x080, i, 0x94260
.endmacro

.word 0xE7000000, 0x00000000 ; G_RDPPIPESYNC
.word 0xFCFFFFFF, 0xFFFCF87C ; G_SETCOMBINE
.word 0xB900031D, 0x00552048 ; G_SetOtherMode_L
.word 0xBB000001, 0xFFFFFFFF ; G_TEXTURE

drawMain 0
drawMain 1
drawMain 2
drawMain 3
drawMain 4

drawMain 5
drawMain 6
drawMain 7
drawMain 8
drawMain 9

drawMain 10
drawMain 11
drawMain 12
drawMain 13
drawMain 14

drawMain 15
drawMain 16
drawMain 17
drawMain 18
drawMain 19

drawMain 20
drawMain 21
drawMain 22
drawMain 23
drawMain 24

drawMain 25
drawMain 26
drawMain 27
drawMain 28
drawMain 29

drawMain 30
drawMain 31
drawMain 32
drawMain 33
drawMain 34

drawQuad 128, 16, 0x040, 35, 0x90270
drawQuad 128, 16, 0x040, 36, 0x90270
drawQuad 64, 16, 0x080, 37, 0x90260

.word 0xE7000000, 0x00000000 ; G_RDPPIPESYNC
.word 0xBB000000, 0xFFFFFFFF ; G_TEXTURE
.word 0xB7000000, 0x00020000 ; G_SETGEOMETRYMODE
.word 0xFCFFFFFF, 0xFFFE793C ; G_SETCOMBINE
.word 0xB900031D, 0x00552078 ; G_SetOtherMode_L
.word 0xB8000000, 0x00000000 ; G_ENDDL

.close
