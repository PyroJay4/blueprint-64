.create "extended-boundaries.bin",0x80381F08
.n64

@main_pool_alloc equ 0x80278120
@commit_all_writes equ 0x80322F40
@invalidate_instruction_cache equ 0x80324610

/*
This patch allocates space in memory for a larger partition table so that the
game can still use the same cell size even with a larger level area. This avoids
slowdowns due to having to collision check against ~4 or ~16 times the polygons.
After allocating the space, the patch then overwrites the game code to read from
the new table location instead of the old one, and invalidates the instruction
cache. This patch does not actually make the game use this larger space-- that
part is handled by the code in Bowser's Blueprints (see extend.cpp). This merely
reallocates the table to have more space. This patch is written over a debug
function related to this partition table.
*/

.area 908

; If someone tries to call this replaced debug function, just return immediately
JR RA
NOP

@space_allocated:
.word 0 ; this word gets overwriten by the code in Bowser's Blueprints, storing
		; the number of bytes to allocate here. (49152 (~48 kB) for extended,
		; and 196608 (~192kB) for super-extended.)
		
/* Shim calls the code starting here at ROM 0xFEC94, RAM 0x80381F14 */
ADDIU SP, SP, 0xFFE0
SW RA, 0x1C (SP)

; Make the original function call we overwrote to jump to this code
JAL @main_pool_alloc
NOP
SW V0, 0x18 (SP)

; Allocate space for the larger partition table on the main pool
LW A0, @space_allocated
JAL @main_pool_alloc
OR A1, R0, R0

LW T5, @space_allocated
SRL V1, T5, 0x1
ADDU V1, V0, V1

; Zero out the memory
ADDU T6, V0, T5
SLL T5, V0, 0x0
@@loop:
	SW R0, 0x0 (T5)
	ADDIU T5, T5, 0x4
	SLT AT, T5, T6
	BNE AT, R0, @@loop
	NOP

; I don't follow the MIPS convention for my helper function since I don't need
; to. I rely on it preserving the values of registers I'm using:
; V0 = address of static surface partition table
; V1 = address of dynamic surface partition table
; T0 = 0x8038
; T1 = LUI for staticParitionTable address
; T2 = ADDIU for staticParitionTable address
; T3 = LUI for dynamicParitionTable address
; T4 = ADDIU for dynamicParitionTable address
; A0 = offset
; T5 = scratch register

LUI T0, 0x8038

.macro @WRITE_STATIC, upper, lower
	SH T1, upper (T0)
	SH T2, lower (T0)
.endmacro

.macro @WRITE_DYNAMIC, upper, lower
	SH T3, upper (T0)
	SH T4, lower (T0)
.endmacro

JAL @set_registers_for_offset
ORI A0, R0, 0x0

@WRITE_DYNAMIC 0x2742, 0x274E
@WRITE_STATIC  0x2786, 0x2792
@WRITE_DYNAMIC 0x35E2, 0x35EA
@WRITE_STATIC  0x25DA, 0x25E2
@WRITE_DYNAMIC 0x188A, 0x18AE
@WRITE_DYNAMIC 0x1A06, 0x1A2A
@WRITE_STATIC  0x1A5A, 0x1A7E

JAL @set_registers_for_offset
ORI A0, R0, 0x8

@WRITE_DYNAMIC 0x136A, 0x138E
@WRITE_STATIC  0x13BE, 0x13E2

JAL @set_registers_for_offset
ORI A0, R0, 0x10

@WRITE_DYNAMIC 0x0F6A, 0x0F8E
@WRITE_STATIC  0x0FBA, 0x0FDE

; Now the code has been overwritten to load from the new address of the
; partition table. We need to now invalidate the CPU instruction cache
; to make sure that stale code isn't executed.
JAL @commit_all_writes
NOP
LI A0, 0x80380F6A
JAL @invalidate_instruction_cache
ORI A1, R0, 0x2684

LW V0, 0x18 (SP)
LW RA, 0x1C (SP)
JR RA
ADDIU SP, SP, 0x20

; helper function to set T1-T4 to the values that will be writen over the
; LUI and ADDIU instruction immediate values to update references to the
; partition table
@set_registers_for_offset:
ADDU T5, V0, A0
SRL T1, T5, 0x10
ANDI T2, T5, 0xFFFF
ORI AT, R0, 0x8000
SLTU AT, T2, AT
BNE AT, R0, @@endif_1
ADDU T5, V1, A0
	ADDIU T1, T1, 0x1
@@endif_1:
SRL T3, T5, 0x10
ANDI T4, T5, 0xFFFF
ORI AT, R0, 0x8000
SLTU AT, T4, AT
BNE AT, R0, @@endif_2
NOP
	ADDIU T3, T3, 0x1
@@endif_2:
JR RA
NOP

.endarea
.close
