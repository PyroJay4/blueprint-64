.create "fix-segment-loader.bin",0x80401500
.n64

LW T0, 0x8038BE28
LBU AT, 0x2 (T0)
BNE AT, R0, @@endif_normal
NOP
	J 0x8037ECA4
	NOP
@@endif_normal:
ADDIU SP, SP, 0xFFE8
SW RA, 0x14 (SP)

LI T1, @segment_map
LBU AT, 0x03 (T0)
ADDIU AT, AT, 0xFFFB
SLL AT, AT, 0x2
ADDU T1, T1, AT
LW A0, 0x0 (T1)
SW A0, 0x10 (SP)
LUI AT, 0x8000
OR A0, A0, AT
LW A1, 0x4 (T0)
JAL 0x80278504 ; dma_read
LW A2, 0x8 (T0)
LW T0, 0x10 (SP)
LW T1, 0x8038BE28
LBU AT, 0x3 (T1)
LI T1, 0x8033B400
SLL AT, AT, 0x2
ADDU T1, T1, AT
SW T0, 0x0 (T1)

; This jumps to the middle of a function, then end of which will load the
; return address and set the stack pointer to what it should be.
J 0x8037ECC8
NOP

@segment_map:
.word 0x0072BE00 ; 0x5 loads to 0x8072BE00 .. 0x80750000
.word 0x005FD0F0 ; 0x6 loads to 0x805FD0F0 .. 0x80660000
.word 0x00583FD0 ; 0x7 loads to 0x80583FD0 .. 0x805FD0F0
.fill 24
.word 0x00420000 ; 0x0E loads to 0x80420000 .. 0x80583FD0 (0x163FD0 available space)

.close
