.create "lower-cell-index-extended.bin",0
.n64

; Replacement for the lower_cell_index function when using extended boundaries
.area 0x94

SLL A0, A0, 0x10
SRA A0, A0, 0x10
ADDIU A0, A0, 0x4000
BLEZ A0, @@return_zero
SRA T0, A0, 0xA
SLTI AT, T0, 0x20
BEQ AT, R0, @@return
ORI V0, R0, 0x1F
BLEZ T0, @@return_zero
OR V0, T0, R0
ANDI T0, A0, 0x3FF
SLTI AT, T0, 0x32
BEQ AT, R0, @@return
NOP
ADDIU V0, V0, 0xFFFF
@@return:
JR RA
NOP
@@return_zero:
JR RA
OR V0, R0, R0

.endarea
.close
