/* dot_product_3d
Computes the dot product of two 3 dimensional vectors.
args:
	A0 - [pointer] pointer to vector A
	A1 - [pointer] pointer to vector B
returns:
	F0 - dot product
*/
dot_product_3d:
L.S F5, 0x0 (A0)
L.S F6, 0x0 (A1)
MUL.S F4, F5, F6
L.S F5, 0x4 (A0)
L.S F6, 0x4 (A1)
MUL.S F5, F5, F6
ADD.S F4, F4, F5
L.S F5, 0x8 (A0)
L.S F6, 0x8 (A1)
MUL.S F5, F5, F6
JR RA
ADD.S F0, F4, F5

/* cross_product
Computes the cross product of two vectors. Result is both stored in the return
registers F0 - F2 and writen to memory at A2
args:
	A0 - [pointer] pointer to vector A
	A1 - [pointer] pointer to vector B
	A2 - [pointer] pointer to output vector (or NULL to not store the result)
*/
cross_product:
L.S F4, 0x4 (A0)
L.S F5, 0x8 (A1)
MUL.S F4, F4, F5
L.S F5, 0x8 (A0)
L.S F6, 0x4 (A1)
MUL.S F5, F5, F6
SUB.S F0, F4, F5

L.S F4, 0x8 (A0)
L.S F5, 0x0 (A1)
MUL.S F4, F4, F5
L.S F5, 0x0 (A0)
L.S F6, 0x8 (A1)
MUL.S F5, F5, F6
SUB.S F1, F4, F5

L.S F4, 0x0 (A0)
L.S F5, 0x4 (A1)
MUL.S F4, F4, F5
L.S F5, 0x4 (A0)
L.S F6, 0x0 (A1)
MUL.S F5, F5, F6
BEQ A2, R0, @@return
SUB.S F2, F4, F5

S.S F0, 0x0 (A2)
S.S F1, 0x4 (A2)
S.S F2, 0x8 (A2)

@@return:
JR RA
NOP

/* copy_vector
Copy a 3-dimensional vector from one location to another
Components of the copied vector are also stores in the F0 - F2 registers
args:
	A0 - [pointer] pointer to source vector
	A1 - [pointer] pointer to memory to copy the vector to
returns:
	F0,F1,F2: [float] vector components
*/
copy_vector:
L.S F0, 0x0 (A0)
L.S F1, 0x4 (A0)
L.S F2, 0x8 (A0)
S.S F0, 0x0 (A1)
S.S F1, 0x4 (A1)
JR RA
S.S F2, 0x8 (A1)

/* get_vector_magnitude
Computes the magnitude of a vector
args:
	A0 - [pointer] pointer to vector
returns:
	F0 - [float] magnitude
*/
get_vector_magnitude:
L.S F4, 0x0 (A0)
L.S F5, 0x4 (A0)
L.S F6, 0x8 (A0)
MUL.S F4, F4, F4
MUL.S F5, F5, F5
MUL.S F6, F6, F6
ADD.S F4, F4, F5
ADD.S F4, F4, F6
JR RA
SQRT.S F0, F4

/* normalize_vector
Normalizes a vector and stores the result in A1. It is safe to use the same
pointer for A0 and A1 in which case the vector is normalized in place.
args:
	A0 - [pointer] pointer to source vector
	A1 - [pointer] pointer to memory to store the result vector in
returns:
	F0,F1,F2: [float] vector components
	F3: [float] magnitude of the source vector
*/
normalize_vector:
L.S F0, 0x0 (A0)
L.S F1, 0x4 (A0)
L.S F2, 0x8 (A0)
MUL.S F4, F0, F0
MUL.S F5, F1, F1
MUL.S F6, F2, F2
ADD.S F3, F4, F5
ADD.S F3, F3, F6
MTC1 R0, F4
SQRT.S F3, F3
C.EQ.S F3, F4
NOP
BC1T @@return
SLL V0, R0, 0x0
DIV.S F0, F0, F3
DIV.S F1, F1, F3
DIV.S F2, F2, F3
ORI V0, R0, 0x1
@@return:
S.S F0, 0x0 (A1)
S.S F1, 0x4 (A1)
JR RA
S.S F2, 0x8 (A1)

/* add_vectors_3d
Adds two 3-dimensional vectors and stores the result in memory
Does not alter the argument register values.
args:
	A0 - [pointer] pointer to vector A
	A1 - [pointer] pointer to vector B
	A2 - [pointer] pointer to where to store A+B
returns:
	F0,F1,F2: [float] result vector components
*/
add_vectors_3d:
L.S F4, 0x0 (A0)
L.S F5, 0x0 (A1)
ADD.S F0, F4, F5
S.S F0, 0x0 (A2)
L.S F4, 0x4 (A0)
L.S F5, 0x4 (A1)
ADD.S F1, F4, F5
S.S F1, 0x4 (A2)
L.S F4, 0x8 (A0)
L.S F5, 0x8 (A1)
ADD.S F2, F4, F5
JR RA
S.S F2, 0x8 (A2)

/* subtract_vectors_3d
Subtracts two 3-dimensional vectors and stores the result in memory
Does not alter the argument register values.
args:
	A0 - [pointer] pointer to vector A
	A1 - [pointer] pointer to vector B
	A2 - [pointer] pointer to where to store A-B
returns:
	F0,F1,F2: [float] result vector components
*/
subtract_vectors_3d:
L.S F4, 0x0 (A0)
L.S F5, 0x0 (A1)
SUB.S F0, F4, F5
S.S F0, 0x0 (A2)
L.S F4, 0x4 (A0)
L.S F5, 0x4 (A1)
SUB.S F1, F4, F5
S.S F1, 0x4 (A2)
L.S F4, 0x8 (A0)
L.S F5, 0x8 (A1)
SUB.S F2, F4, F5
JR RA
S.S F2, 0x8 (A2)

/* subtract_vectors_3d
Multiplies a 3-dimensional vector by a scalar and stores the result in memory
Does not alter the argument register values.
args:
	A0 - [pointer] pointer to vector
	A1 - [pointer] pointer to where to store the result
	F12 - [float] scalar value to multiply the result by
returns:
	F0,F1,F2: [float] result vector components
*/
scale_vector_3d:
L.S F4, 0x0 (A0)
MUL.S F0, F4, F12
S.S F0, 0x0 (A1)
L.S F4, 0x4 (A0)
MUL.S F1, F4, F12
S.S F1, 0x4 (A1)
L.S F4, 0x8 (A0)
MUL.S F2, F4, F12
JR RA
S.S F2, 0x8 (A1)

/* get_distance_between_points
Gets the distance between two 3D points
Does not alter the argument register values.
args:
	A0 - [pointer] pointer to point A
	A1 - [pointer] pointer to point B
returns:
	F0 - distance
*/
get_distance_between_points:
ADDIU SP, SP, 0xFFE0
SW RA, 0x1C (SP)
SW A0, 0x20 (SP)
JAL subtract_vectors_3d
ADDIU A2, SP, 0x10
JAL get_vector_magnitude
SLL A0, A2, 0x0
LW A0, 0x20 (SP)
LW RA, 0x1C (SP)
JR RA
ADDIU SP, SP, 0x20

/* turn_vector_3d
Computes a unit vector that is a vector turned from the direction of vector A0
towards the direction of vector A1, with a maximum angle change of A2. The
result is stored in A3. All vectors are 3 dimensional.

Does not alter the argument register values.
args:
	A0 - [pointer (float[3])] the source vector
	A1 - [pointer (float[3])] the target vector
	A2 - [short] the maximum angle change
	A3 - [pointer (float[3])] allocated space for the result vector
retuns:
	V0 - [short] angle change
*/
turn_vector_3d:
ADDIU SP, SP, 0xFFC0
SW RA, 0x3C (SP)
SW A0, 0x40 (SP)
SW A1, 0x44 (SP)
SW A2, 0x48 (SP)
SW A3, 0x4C (SP)

; Store S = normalized source vector at SP+0x10
JAL normalize_vector
ADDIU A1, SP, 0x10

; if the source vector is a zero vector, return a zero vector
BNE V0, R0, @@endif_source_vector_is_zero
	LW T0, 0x4C (SP)
	SW R0, 0x0 (T0)
	SW R0, 0x4 (T0)
	SW R0, 0x8 (T0)
	B @@return
	ORI V0, R0, 0x0
@@endif_source_vector_is_zero:

; Store T = normalized source vector at SP+0x1C
LW A0, 0x44 (SP)
JAL normalize_vector
ADDIU A1, SP, 0x1C

; if the target vector is a zero vector, return S
BNE V0, R0, @@endif_target_vector_is_zero
	@@return_normalized_source:
	LI RA, @@return
	ADDIU A0, SP, 0x10
	LW A1, 0x4C (SP)
	J copy_vector
	ORI V0, R0, 0x0
@@endif_target_vector_is_zero:

ADDIU A0, SP, 0x10
ADDIU A1, SP, 0x1C
JAL cross_product
ADDIU A2, SP, 0x28

ADDIU A0, SP, 0x28
JAL normalize_vector
SLL A1, A0, 0x0

; Store the value of ‖S⨯T‖, which is the sine of the difference of angles, in SP+0x34
MTC1 R0, F4
S.S F3, 0x34 (SP)

; if S and T are parallel, return S
LUI AT, 0x3F80
C.EQ.S F3, F4
MTC1 AT, F5
BC1T @@return_normalized_source

; Compute the angle between the two vectors, and store it in SP+0x38
ADDIU A0, SP, 0x10
JAL dot_product_3d
ADDIU A1, SP, 0x1C
MOV.S F12, F0
JAL $atan2s
L.S F14, 0x34 (SP)
SW V0, 0x38 (SP)

; In case of rounding errors, check again that the angle is not 0 or 180
ANDI V0, V0, 0xFFFF
BEQ V0, R0, @@return_normalized_source
ORI AT, R0, 0x8000
SLTU AT, V0, AT
BEQ AT, R0, @@return_normalized_source
SW V0, 0x38 (SP)

; If angle difference is within max angle change, return T
LW T0, 0x48 (SP)
ADDIU T0, T0, 0x1
SLT AT, V0, T0
BEQ AT, R0, @@endif_within_max_angle_range
	LI RA, @@return
	ADDIU A0, SP, 0x1C
	J copy_vector
	LW A1, 0x4C (SP)
@@endif_within_max_angle_range:

ADDIU A0, SP, 0x28
ADDIU A1, SP, 0x10
JAL cross_product
SLL A2, A0, 0x0

JAL cos_u16
LW A0, 0x48 (SP)

ADDIU A0, SP, 0x10
SLL A1, A0, 0x0
JAL scale_vector_3d
MOV.S F12, F0

JAL sin_u16
LW A0, 0x48 (SP)

ADDIU A0, SP, 0x1C
SLL A1, A0, 0x0
JAL scale_vector_3d
MOV.S F12, F0

ADDIU A0, SP, 0x10
ADDIU A1, SP, 0x1C
JAL add_vectors_3d
LW A2, 0x4C (SP)

; Normalize again to eliminate drift from rounding errors
SLL A0, A2, 0x0
JAL normalize_vector
SLL A1, A2, 0x0

LW V0, 0x38 (SP)

@@return:
LW RA, 0x3C (SP)
LW A0, 0x40 (SP)
LW A1, 0x44 (SP)
LW A2, 0x48 (SP)
LW A3, 0x4C (SP)
JR RA
ADDIU SP, SP, 0x40


/* vector_to_yaw_and_pitch
Computes the yaw and pitch of a vector.
Preserves the value of the A0 register
args:
	A0 - [pointer] pointer to vector
returns:
	V0 - yaw
	V1 - pitch
*/
vector_to_yaw_and_pitch:
ADDIU SP, SP, 0xFFE8
SW RA, 0x14 (SP)
SW A0, 0x18 (SP)

L.S F12, 0x8 (A0)
JAL $atan2s
L.S F14, 0x0 (A0)
SW V0, 0x10 (SP)

L.S F4, 0x0 (A0)
L.S F5, 0x8 (A0)
MUL.S F4, F4, F4
MUL.S F5, F5, F5
ADD.S F4, F4, F5
SQRT.S F12, F4
L.S F14, 0x4 (A0)
JAL $atan2s
NEG.S F14, F14

SLL V1, V0, 0x0
LW V0, 0x10 (SP)

LW A0, 0x18 (SP)
LW RA, 0x14 (SP)
JR RA
ADDIU SP, SP, 0x18
