; Module-scoped labels for code readability

/* Functions */
.definelabel $atan2s, 0x8037A9A8
.definelabel $segmented_to_virtual, 0x80277F50
.definelabel $print, 0x802D62D8
.definelabel $get_random_float, 0x80383CB4
.definelabel $get_random_short, 0x80383BB0
.definelabel $get_dist_2d, 0x8029E27C

/* Global Variables */
$g_display_list_head equ 0x8033B06C
$g_camera_state equ 0x8033C698
$g_camera_fov equ 0x8033C5A4
$g_mario equ 0x8033B170
$g_current_obj_ptr equ 0x80361160
$g_mario_obj_ptr equ 0x80361158
$g_cutscene_finished equ 0x8033CBC8

/* Object Properties */
$o_x equ 0xA0
$o_y equ 0xA4
$o_z equ 0xA8
$o_hitbox_height equ 0x1FC
$o_hitbox_radius equ 0x1F8
$o_hitbox_down_offset equ 0x208
$o_num_collided_objects equ 0x76
$o_collided_objects equ 0x78

/* Mario Properties */
$m_y equ 0x40

/* Surface Triangle Properties */
$t_normal_x equ 0x1C
$t_normal_y equ 0x20
$t_normal_z equ 0x24

/* Camera State Properties */
$cam_x equ 0x8C
$cam_y equ 0x90
$cam_z equ 0x94
$cam_pitch equ 0x4C
$cam_yaw equ 0x4E
