/* Global Variables */
$g_mario equ 0x8033B170
$g_mario_obj_ptr equ 0x80361158
$g_zero_vector equ 0x803331F0

/* Mario State Properties */
$m_health equ 0xAE
$m_action equ 0xC
$m_x equ 0x3C
$m_y equ 0x40
$m_z equ 0x44
$m_water_level equ 0x76
$m_yaw equ 0x2E

/* Object Properties */
$o_coin_value equ 0x180
$o_interaction_status equ 0x134

/* Vanilla Actions */
$ACT_DROWNING equ 0x300032C4

/* Action Categories */
$ACT_FLAG_SWIMMING equ 0x2000

/* Sounds */
$SOUND_ALMOST_DROWNING equ 0x1C180001

/* Functions */
.definelabel $set_mario_action, 0x80252CF4
.definelabel $set_sound, 0x8031EB00
