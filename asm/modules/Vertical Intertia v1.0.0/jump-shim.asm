$jump_shim:
LI T0, $g_mario
LW T1, $m_floor (T0)
BEQ T1, R0, @@return
NOP
LW T1, $t_object (T1)
BEQ T1, R0, @@return
L.S F4, $m_speed_y (T0)
L.S F5, $o_speed_y (T1)
ADD.S F4, F4, F5
S.S F4, $m_speed_y (T0)
@@return:
#continue jump_shim
NOP
