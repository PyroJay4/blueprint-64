$is_2d_area:
LI T0, $area_mask
LHU T1, $g_level_num
ADDU T0, T0, T1
LBU T0, 0x0 (T0)
LW T1, ($g_mario + $m_area)
LBU T1, 0x0 (T1)
SETU AT, 0x1
SLLV AT, AT, T1
JR RA
AND V0, T0, AT
