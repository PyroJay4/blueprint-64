/* Global Variables */
$g_mario equ 0x8033B170
$g_level_num equ 0x8032DDF8

/* Mario State Properties */
$m_pos equ 0x3C
	$m_x equ 0x3C
	$m_y equ 0x40
	$m_z equ 0x44
$m_speed_x equ 0x48
$m_speed_z equ 0x50
$m_speed_h equ 0x54
$m_slide_speed_z equ 0x5C
$m_angle_yaw equ 0x2E
$m_angle_vel_yaw equ 0x34
$m_slide_angle_yaw equ 0x38
$m_area equ 0x90
$m_action equ 0xC
$m_body equ 0x98
$m_controller equ 0x9C
$m_floor equ 0x68
$m_object equ 0x88

/* Object Properties */
$o_gfx_pos equ 0x20

/* Controller Properties */
$c_x equ 0x4
$c_y equ 0x8
$c_mag equ 0xC
$c_buttons_held equ 0x10

/* Controller C Button Flags */
$C_BUTTON_C_RIGHT equ	0x1
$C_BUTTON_C_LEFT equ	0x2
$C_BUTTON_C_DOWN equ	0x4
$C_BUTTON_C_UP equ		0x8

/* Camera Properties */
$cam_custcene equ 0x30
$cam_mode equ 0x0
$cam_yaw equ 0x2
$cam_next_yaw equ 0x3A
$cam_focus equ 0x4
	$cam_focus_x equ 0x4
	$cam_focus_y equ 0x8
	$cam_focus_z equ 0xC
$cam_pos equ 0x10
	$cam_x equ 0x10
	$cam_y equ 0x14
	$cam_z equ 0x18

/* Mario Body State Properties */
$b_lean_angle_x equ 0x0C
$b_lean_angle_z equ 0x10

/* Vanilla Actions */
$ACT_WALKING equ 0x04000440
$ACT_TURNING_AROUND equ 0x00000443

/* Functions */
.definelabel $abs_angle_diff, 0x802A11A8
.definelabel $holding_back, 0x80265514
.definelabel $set_mario_action, 0x80252CF4
.definelabel $find_floor, 0x80381900
.definelabel $copy_vector, 0x80378800
.definelabel $sync_mario_state, 0x8029C780
.definelabel $update_mario_camera_info, 0x802542B4
.definelabel $approach_float, 0x8029E494
.definelabel $set_camera_height, 0x8027FFF8
