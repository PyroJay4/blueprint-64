@previous_position:
.word 0, 0

$save_position:
LI T0, $g_mario
LI T1, @previous_position
LW AT, $m_x (T0)
SW AT, 0x0 (T1)
LW AT, $m_y (T0)
#continue save_position
SW AT, 0x4 (T1)

$constrain_2d:
ADDIU SP, SP, 0xFFE8
SW RA, 0x14 (SP)

JAL $is_2d_area
NOP

BEQ V0, R0, @@return
NOP

LI A0, $g_mario
LW T0, $m_body (A0)
SH R0, $b_lean_angle_x (T0)
SH R0, $b_lean_angle_z (T0)

LI AT, $ACT_WALKING
LW T0, $m_action (A0)
BNE T0, AT, @@endif_turn_from_walk
NOP
JAL $holding_back
NOP
BEQ V0, R0, @@endif_turn_from_walk
LI A0, $g_mario
LI A1, $ACT_TURNING_AROUND
JAL $set_mario_action
MOVE A2, R0

@@endif_turn_from_walk:

LI T0, $g_mario

SW R0, $m_z (T0)
SW R0, $m_speed_z (T0)
SW R0, $m_slide_speed_z (T0)
SW R0, $m_angle_vel_yaw (T0)

LBU AT, $m_speed_h (T0)
ANDI AT, AT, 0x80

L.S F4, $m_speed_x (T0)
BEQ AT, R0, @@endif_moving_backwards
ABS.S F4, F4
	NEG.S F4, F4
@@endif_moving_backwards:
S.S F4, $m_speed_h (T0)

LH A0, ($g_mario + $m_angle_yaw)
JAL @snap_angle
NOP
SH V0, ($g_mario + $m_angle_yaw)

LH A0, ($g_mario + $m_slide_angle_yaw)
JAL @snap_angle
NOP
SH V0, ($g_mario + $m_slide_angle_yaw)

LI T0, $g_mario
L.S F12, $m_x (T0)
L.S F14, $m_y (T0)
MOVE A2, R0
JAL $find_floor
ADDIU A3, SP, 0x10
LW V0, 0x10 (SP)

LI T0, $g_mario
BNE V0, R0, @@endif_oob
	LI T1, @previous_position
	LW AT, 0x0 (T1)
	SW AT, $m_x (T0)
	LW AT, 0x4 (T1)
	B @@update_gfx
	SW AT, $m_y (T0)
@@endif_oob:

SW V0, $m_floor (T0)

@@update_gfx:
LW A0, $m_object (T0)
ADDIU A0, A0, $o_gfx_pos
JAL $copy_vector
ADDIU A1, T0, $m_x

LI.U A0, $g_mario
JAL $update_mario_camera_info
LI.L A0, $g_mario

@@return:
LW RA, 0x14 (SP)
JR RA
ADDIU SP, SP, 0x18

/* Helper Functions */

@snap_angle:
	ADDIU SP, SP, 0xFFE8
	SW RA, 0x14 (SP)

	JAL $abs_angle_diff
	SETU A1, 0x4000

	SLTI AT, V0, 0x4001
	BEQ AT, R0, @@return
	SETU V0, 0xC000
		SETU V0, 0x4000

	@@return:
	LW RA, 0x14 (SP)
	JR RA
	ADDIU SP, SP, 0x18
